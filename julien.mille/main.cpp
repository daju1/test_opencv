#include "activecontour.h"
#include "binaryregion.h"
#include <iostream>
#include <stdio.h>

int active_contour_main()
{
	CImage2D imgInput, imgOutput, imgContour, imgMask;
	CDeformableModelBase *pDeformableModel;
	CCouple<float> pfCircleCenter;
	float fCircleRadius;
	int iIteration, iNbIterationsMax, iNbPassesPerIteration;
	char strFilename[50];
	bool bSaveAllIterations;
	bool bIsEvolving;
	int iZoom;
	enum {MODELTYPE_PARAMETRIC, MODELTYPE_IMPLICIT} iModelType;

	CBinaryRegionBase::InitNeighborhood();
	CImage2D::StartImage2D();

	// Choose either to use parametric implementation or the implicit one
	iModelType = MODELTYPE_PARAMETRIC;

	// Load the image file
	if (imgInput.Load("brain.jpg")==false)
	//if (imgInput.Load("color_objects1.jpg")==false)
	{
		cerr<<"ERROR: unable to open input image."<<endl;
		return -1;
	}

	// Check if image is stored in RGB format and contains only grayscale values
	// If this is the case, convert it to 8-bit grayscale format
	if (imgInput.IsGrayScaleRGB()==true)
	{
		cout<<"Image has only grayscale values but is actually stored in RGB format. Converting it to 8-bit format..."<<endl;
		imgInput = imgInput.GrayScale();
	}

	// Set zooming factor for output images
	iZoom = 1;

	// Allocate the deformable model to a grayscale or color instance depending on image format
	if (imgInput.GetBitsPerPixel()==8)
	{
		cout<<"Grayscale model"<<endl;

		// Allocate the deformable model to CActiveContourGrayscale or CBinaryRegionGrayscale
		// depending on model type
		if (iModelType==MODELTYPE_PARAMETRIC)
			pDeformableModel = new CActiveContourGrayscale();
		else
			pDeformableModel = new CBinaryRegionGrayscale();

		// Convert input image to 24-bit RGB format for output
		imgInput.Convert8bitsto24bits(imgOutput);

		// Rescale output image if needed
		if (iZoom>1)
			imgOutput = imgOutput.ResizeRGB(imgOutput.GetWidth()*iZoom, imgOutput.GetHeight()*iZoom);
	}
	else if (imgInput.GetBitsPerPixel()==24 || imgInput.GetBitsPerPixel()==32)
	{
		cout<<"Color model"<<endl;

		// Allocate the deformable model to CActiveContourColor or CBinaryRegionColor
		// depending on model type
		if (iModelType==MODELTYPE_PARAMETRIC)
		{
			CActiveContourColor *pActiveContourColor;

			pDeformableModel = new CActiveContourColor();
			pActiveContourColor = (CActiveContourColor *)pDeformableModel;

			// Set parameters specific to the color binary region model
			pActiveContourColor->iColorSpace = CActiveContourColor::COLORSPACE_YUV; // or COLORSPACE_RGB, COLORSPACE_LAB
			pActiveContourColor->bIgnoreBrightnessComponent = true;
		}
		else {
			CBinaryRegionColor *pBinaryRegionColor;

			pDeformableModel = new CBinaryRegionColor();
			pBinaryRegionColor = (CBinaryRegionColor *)pDeformableModel;

			// Set parameters specific to the color active contour model
			pBinaryRegionColor->iColorSpace = CBinaryRegionColor::COLORSPACE_YUV; // or COLORSPACE_RGB, COLORSPACE_LAB
			pBinaryRegionColor->bIgnoreBrightnessComponent = true;
		}

		// Copy the input image to the output image (with rescaling if needed)
		if (iZoom>1)
			imgOutput = imgInput.ResizeRGB(imgInput.GetWidth()*iZoom, imgInput.GetHeight()*iZoom);
		else
			imgOutput = imgInput;
	}
	else {
		cerr<<"ERROR: number of bits/pixel is not supported."<<endl;
		return -1;
	}

	// Set energy configuration
	pDeformableModel->iInsideMode = CDeformableModelBase::INSIDEMODE_REGION; // Other possible values are INSIDEMODE_BAND
	pDeformableModel->iOutsideMode = CDeformableModelBase::OUTSIDEMODE_LINE; // Other possible values are OUTSIDEMODE_REGION, OUTSIDEMODE_BAND

	// To work properly, the second narrow band region energy (the local one)
	// related to equations (6) and (19) in [Mille09] often requires the bias to be enabled
	if (pDeformableModel->iOutsideMode==CDeformableModelBase::OUTSIDEMODE_LINE)
	{
		pDeformableModel->bRegionEnergyBias = true;
		pDeformableModel->fWeightBalloon = -0.3f;
	}

	// Other parameters may be modified here, before attaching the model to the image
	// ...

	// Attach model to image data
	pDeformableModel->AttachImage(&imgInput);

	// Initial location for 'brain'
	pfCircleCenter.Set(260.0f, 232.0f);
	fCircleRadius = 30.0f;

	// Initial location for 'color_objects1'
	//pfCircleCenter.Set(490.0f, 210.0f);
	//fCircleRadius = 25.0f;

	// Bad initial location for 'color_objects1'
	//pfCircleCenter.Set(650.0f, 210.0f);
	//fCircleRadius = 25.0f;

	iNbIterationsMax = 1000;
	iNbPassesPerIteration = 10;
	bSaveAllIterations = true;

	// Initialize the region as a circle with given center and radius
	pDeformableModel->InitCircle(pfCircleCenter, fCircleRadius);

	// Main loop
	bIsEvolving = true;
	for (iIteration=0; iIteration<iNbIterationsMax && bIsEvolving==true; iIteration++)
	{
		cout<<"Iteration "<<iIteration<<endl;

		if (bSaveAllIterations==true)
		{
			// Draw the model in output image at current iteration
			imgContour = imgOutput;
			pDeformableModel->DrawInImageRGB(imgContour, iZoom);

			// Write the output image to BMP file
			sprintf(strFilename, "output_%04d.bmp", iIteration);
			if (imgContour.Save(strFilename, CImage2D::FORMAT_BMP)==false)
			{
				cerr<<"ERROR: unable to write to image file "<<strFilename<<endl;
				return -1;
			}
		}

		// Evolve the model with a few passes of gradient descent
		bIsEvolving = pDeformableModel->EnergyGradientDescent(iNbPassesPerIteration);
	}

	if (bIsEvolving==false)
	    cout<<endl<<"Stopping criterion is met: deformable model has reached stability"<<endl;
    else if (iIteration>=iNbIterationsMax)
        cout<<endl<<"Maximum number of iterations is exceeded, but deformable model has not reached stability"<<endl;

	// Draw final model in output image
	imgContour = imgOutput;
	pDeformableModel->DrawInImageRGB(imgContour, iZoom);

	// Write final output image to BMP file
	if (imgContour.Save("output_final.bmp", CImage2D::FORMAT_BMP)==false)
	{
		cerr<<"ERROR: unable to write to image file output_final.bmp"<<endl;
		return -1;
	}

    // Make binary mask of final segmentation
    pDeformableModel->MakeBinaryMask(imgMask);

    // Write the mask to BMP file
    if (imgMask.Save("mask_final.bmp", CImage2D::FORMAT_BMP)==false)
    {
        cerr<<"ERROR: unable to write to image file "<<strFilename<<endl;
        return -1;
    }

	// Destroy model
	delete pDeformableModel;

	return 0;
}

