/*
	activecontour.cpp

	Copyright 2010 Julien Mille (julien.mille@liris.cnrs.fr)
	http://liris.cnrs.fr/~jmille/code.html

	Source file of library implementing the active contour model with narrow band
	region energy described in

	[Mille09] J. Mille, "Narrow band region-based active contours and surfaces for
	          2D and 3D segmentation", Computer Vision and Image Understanding,
			  volume 113, issue 9, pages 946-965, 2009

	A preprint version of this paper may be downloaded at
	http://liris.cnrs.fr/~jmille/doc/cviu09temp.pdf

	If you use this code for research purposes, please cite the aforementioned paper
	in any resulting publication.

	The current source file corresponds to the explicit (parametric) implementation
	of the active contour model.
	See sections 2 and 5 in [Mille09] for the continuous variational model and its
	explicit implementation.
*/

#include "activecontour.h"
#include <iostream>

#include "../testHOG/vdouble.h"

/**********************************************************************
*                      CActiveContourBase                             *
**********************************************************************/

CActiveContourBase::CActiveContourBase():CDeformableModelBase()
{
	// Enable the precomputation of image integrals
	bUseImageIntegrals = true;

	// Sampling parameters
	fSampling = 2.0f;
	bResample = false;

	// Display parameters
	bDisplayEdges = true;
	bDisplayVertices = false;
	bDisplayParallelCurves = true;

	rgbEdgeColor.Set(255, 0, 0);
	rgbEdgeTransformedColor.Set(0, 255, 0);
	rgbVertexColor.Set(0, 0, 255);
	rgbEdgeParallelColor.Set(127,127,0);
	iVertexWidth = 2;

	// Narrow band areas
	fInnerNarrowBandArea = 0.0f;
	fOuterNarrowBandArea = 0.0f;

	// Threshold on maximal total motion, to compute the stopping criterion
	// See comments in activecontour.h
	fStoppingCriterionMotionThreshold = 0.02f;
}

void CActiveContourBase::Resample()
{
	list<CVertex2D>::iterator itVertex, itVertexNext;
	CVertex2D vertexNew;
	unsigned int i;
	float dmin2, dmax2;

	// Compute min and max bounds for allowed edge lengths
	dmin2 = fSampling*fSampling;
	dmax2 = 4.0f*dmin2;

	if (listVertices.size()<2)
		return;

	// Merge vertices
	i=0;
	itVertex = listVertices.begin();
	itVertexNext = itVertex;
	itVertexNext++;

	while (i<listVertices.size()-1)
	{
		while (i<listVertices.size()-1 && (itVertex->GetPos()-itVertexNext->GetPos()).L2Norm2()<dmin2)
		{
			itVertex->SetPos((itVertex->GetPos() + itVertexNext->GetPos())*0.5f);
			itVertex->SetNormal((itVertex->GetNormal() + itVertexNext->GetNormal())*0.5f);

			listVertices.erase(itVertexNext);

			itVertexNext = itVertex;
			if (i<listVertices.size()-1)
				itVertexNext++;
		}
		if (i<listVertices.size()-1)
		{
			itVertex++;
			itVertexNext++;
			i++;
		}
	}

	// Add vertices
	i = 0;
	itVertex = listVertices.begin();
	itVertexNext = itVertex;
	itVertexNext++;

	while (i<listVertices.size()-1)
	{
		while ((itVertex->GetPos()-itVertexNext->GetPos()).L2Norm2()>dmax2)
		{
			vertexNew.SetPos((itVertex->GetPos() + itVertexNext->GetPos())*0.5f);
			vertexNew.SetNormal((itVertex->GetNormal() + itVertexNext->GetNormal())*0.5f);
            vertexNew.ResetTotalMotion();

			listVertices.insert(itVertexNext, vertexNew);

			itVertexNext = itVertex;
			itVertexNext++;
		}
		itVertex++;
		itVertexNext++;
		i++;
	}
}

void CActiveContourBase::Translate(const CCouple<float> &vfTranslate)
{
	list<CVertex2D>::iterator itVertex;

	for (itVertex=listVertices.begin(); itVertex!=listVertices.end(); itVertex++)
		itVertex->SetPos(itVertex->GetPos()+vfTranslate);
}

void CActiveContourBase::Rotate(float angle)
{
	list<CVertex2D>::iterator itVertex;
	CCouple<float> pfCentroid, pfCentered;
	float cs, ss;

	cs = cos(angle);
	ss = sin(angle);

	pfCentroid = GetCentroid();

	for (itVertex=listVertices.begin(); itVertex!=listVertices.end(); itVertex++)
	{
		pfCentered = itVertex->GetPos() - pfCentroid;
		itVertex->SetPos(pfCentroid.x + cs*pfCentered.x - ss*pfCentered.y, pfCentroid.y + ss*pfCentered.x + cs*pfCentered.y);
	}
}

bool CActiveContourBase::EnergyGradientDescent(int iNbPasses)
{
	list<CVertex2D>::iterator itVertex, itVertexPrev, itVertexNext;
	int i, iPass, iNbVertices;
	CCouple<float> vfForceSmoothness, vfForceRegion, vfForceGradient, vfForceBalloon;
	CArray1D<CCouple<float> > forces;
	CCouple<float> ptMin, ptMax; // Image bounds
    CCouple<float> pfNew;

	ptMin.Set(1.0f, 1.0f);
	ptMax.Set((float)pInputImage->GetWidth()-2.0f, (float)pInputImage->GetHeight()-2.0f);

	// If edge information is used, check if vector field is initialized
	if (fWeightGradient!=0.0f && arrayGradientField.GetSize()!=pInputImage->GetSize())
	{
		cerr<<"ERROR in CActiveContourBase::EnergyGradientDescent(...): vector field derived from image gradient is not initialized. Call AttachImage() again."<<endl;
		return false;
	}

    // If stopping criterion is enabled, the total motion since the last iteration should be stored for each vertex
    if (bStoppingCriterion==true)
    {
        for (itVertex=listVertices.begin(); itVertex!=listVertices.end(); itVertex++)
            itVertex->ResetTotalMotion();
    }

	for (iPass=0; iPass<iNbPasses; iPass++)
	{
		iNbVertices = listVertices.size();
		if (iNbVertices<3)
			return false;

		forces.Init(iNbVertices);
		forces.Fill(0.0f);

		// Smoothness forces
		if (fWeightSmoothness!=0.0f && bUseGaussianFilter==false)
		{
			// Smoothness force of first vertex
			itVertex = listVertices.begin();
			itVertexPrev = --listVertices.end();
			itVertexNext = itVertex;
			itVertexNext++;

			vfForceSmoothness = (itVertexPrev->GetPos() + itVertexNext->GetPos())*0.5f - itVertex->GetPos();
			forces[0] += vfForceSmoothness*fWeightSmoothness;

			// Smoothness forces of other vertices
			for (itVertex=++listVertices.begin(), i=1; itVertex!=--listVertices.end(); itVertex++, i++)
			{
				itVertexPrev = itVertex;
				itVertexPrev--;
				itVertexNext = itVertex;
				itVertexNext++;

				vfForceSmoothness = (itVertexPrev->GetPos() + itVertexNext->GetPos())*0.5f - itVertex->GetPos();
				forces[i] += vfForceSmoothness*fWeightSmoothness;
			}

			// Smoothness force of last vertex
			itVertex = --listVertices.end();
			itVertexPrev = itVertex;
			itVertexPrev--;
			itVertexNext = listVertices.begin();

			vfForceSmoothness = (itVertexPrev->GetPos() + itVertexNext->GetPos())*0.5f - itVertex->GetPos();
			forces[iNbVertices-1] += vfForceSmoothness*fWeightSmoothness;
		}

		// Region forces
		if (fWeightRegion!=0.0f)
		{
			// Region force of first vertex
			itVertex = listVertices.begin();
			itVertexPrev = --listVertices.end();
			itVertexNext = itVertex;
			itVertexNext++;

			vfForceRegion = RegionForce(&(*itVertexPrev), &(*itVertex), &(*itVertexNext));
			forces[0] += vfForceRegion*fWeightRegion;

			// Region forces of other vertices
			for (itVertex=++listVertices.begin(), i=1; itVertex!=--listVertices.end(); itVertex++, i++)
			{
				itVertexPrev = itVertex;
				itVertexPrev--;
				itVertexNext = itVertex;
				itVertexNext++;

				vfForceRegion = RegionForce(&(*itVertexPrev), &(*itVertex), &(*itVertexNext));
				forces[i] += vfForceRegion*fWeightRegion;
			}

			// Region force of last vertex
			itVertex = --listVertices.end();
			itVertexPrev = itVertex;
			itVertexPrev--;
			itVertexNext = listVertices.begin();

			vfForceRegion = RegionForce(&(*itVertexPrev), &(*itVertex), &(*itVertexNext));
			forces[iNbVertices-1] += vfForceRegion*fWeightRegion;
		}

		// Gradient forces
		if (fWeightGradient!=0.0f)
		{
			for (itVertex=listVertices.begin(), i=0; itVertex!=listVertices.end(); itVertex++, i++)
			{
				vfForceGradient = arrayGradientField.GetElementInterpolate(itVertex->GetPos());
				forces[i] += vfForceGradient*fWeightGradient;
			}
		}

		// Balloon forces
		// If region energy is used and bias is enabled, do not apply extra balloon force
		if (fWeightBalloon!=0.0f && (fWeightRegion==0.0f || bRegionEnergyBias==false))
		{
			for (itVertex=listVertices.begin(), i=0; itVertex!=listVertices.end(); itVertex++, i++)
			{
				vfForceBalloon = itVertex->GetNormal();
				forces[i] += vfForceBalloon*fWeightBalloon;
			}
		}

		// Initial forces
		if (fWeightInitial != 0.0f)
		{
			list<CVertex2D>::iterator itInitialVertex = listInitialVertices.begin();

			// initial x, y
			float sum_n = 0.0;
			float sum_x = 0.0;
			float sum_y = 0.0;
			float sum_xx = 0.0;
			float sum_xy = 0.0;
			float sum_yy = 0.0;

			// current X,Y
			float sum_X = 0.0;
			float sum_Xx = 0.0;
			float sum_Xy = 0.0;

			float sum_Y = 0.0;
			float sum_Yx = 0.0;
			float sum_Yy = 0.0;


			int n = listVertices.size();



			for (itVertex = listVertices.begin(), itInitialVertex = listInitialVertices.begin(), i = 0;
				itVertex != listVertices.end() && itInitialVertex != listInitialVertices.end();
				itVertex++, itInitialVertex++, i++)
			{
				CCouple<float> pfPos = (*itVertex).GetPos();
				CCouple<float> pfInitialPos = (*itInitialVertex).GetPos();

				float x = pfInitialPos.x;
				float y = pfInitialPos.y;

				float X = pfPos.x;
				float Y = pfPos.y;

				//cout << i << " " << x << " " << y << "      " << X << " " << Y << endl;

				sum_n += 1.0;
				sum_x += x;
				sum_y += y;
				sum_xx += x*x;
				sum_xy += x*y;
				sum_yy += y*y;

				sum_X += X;
				sum_Xx += X*x;
				sum_Xy += X*y;

				sum_Y += Y;
				sum_Yx += Y*x;
				sum_Yy += Y*y;
			}


			float _a[3][3] = { 
				{ sum_n, sum_x, sum_y },
				{ sum_x, sum_xx, sum_xy},
				{ sum_y, sum_xy, sum_yy}
			};

			float bX[3][1] = {
				{ sum_X },
				{ sum_Xx },
				{ sum_Xy }
			};

			float bY[3][1] = {
				{ sum_Y },
				{ sum_Yx },
				{ sum_Yy }
			};


			vdouble MA, vBX, vBY;
			MA.resize(3, 3);
			MA(0, 0) = sum_n; MA(0, 1) = sum_x; MA(0, 2) = sum_y;
			MA(1, 0) = sum_x; MA(1, 1) = sum_xx; MA(1, 2) = sum_xy;
			MA(2, 0) = sum_y; MA(2, 1) = sum_xy; MA(2, 2) = sum_yy;

			vBX.resize(3);
			vBY.resize(3);
			vBX(0) = sum_X; vBX(1) = sum_Xx; vBX(2) = sum_Xy;
			vBY(0) = sum_Y; vBY(1) = sum_Yx; vBY(2) = sum_Yy;

			vdouble va;// = MA.Tichonov(vBX);
			vdouble vb;// = MA.Tichonov(vBY);
			/*
			cout << "va=" << endl;
			for (int i = 0; i < 3; ++i)
				cout << " " << va(i);
			cout << endl;

			cout << "vb=" << endl;
			for (int i = 0; i < 3; ++i)
				cout << " " << vb(i);
			cout << endl;

			*/
			double det;
			MA.sls_det(vBX, va, det);
			MA.sls_det(vBY, vb, det);

			/*cout << "va=" << endl;
			for (int i = 0; i < 3; ++i)
				cout << " " << va(i);
			cout << endl;

			cout << "vb=" << endl;
			for (int i = 0; i < 3; ++i)
				cout << " " << vb(i);
			cout << endl;*/


			/*
			cv::Mat A = cv::Mat(3, 3, CV_32FC1, _a);
			cv::Mat BX = cv::Mat(3, 1, CV_32FC1, bX);
			cv::Mat BY = cv::Mat(3, 1, CV_32FC1, bY);

			cout << "A=" << endl << " " << A << endl;
			cout << "BX=" << endl << " " << BX << endl;
			cout << "BY=" << endl << " " << BY << endl;

			cv::Mat invA = A.inv();
			cout << "invA = " << endl << " " << invA << endl;


			cv::Mat a = invA * BX;
			cv::Mat b = invA * BY;

			cout << "a=" << endl << " " << a << endl;
			cout << "b=" << endl << " " << b << endl;

			cout << "A * a=" << endl << " " << A * a << endl;
			cout << "A * b=" << endl << " " << A * b << endl;*/

			list<CVertex2D>::iterator itInitialVertexTransformed = listInitialVerticesTransformed.begin();


			for (itVertex = listVertices.begin(), itInitialVertex = listInitialVertices.begin(), i = 0;
				itVertex != listVertices.end() && itInitialVertex != listInitialVertices.end();
				itVertex++, itInitialVertex++, i++, itInitialVertexTransformed++)
			{
				CCouple<float> pfPos = (*itVertex).GetPos();
				CCouple<float> pfInitialPos = (*itInitialVertex).GetPos();

				float x = pfInitialPos.x;
				float y = pfInitialPos.y;
/*
				float _X = a.at<float>(0) + a.at<float>(1) * x + a.at<float>(2) * y;
				float _Y = b.at<float>(0) + b.at<float>(1) * x + b.at<float>(2) * y;
				*/				
				float _X = va(0) + va(1) * x + va(2) * y;
				float _Y = vb(0) + vb(1) * x + vb(2) * y;

				(*itInitialVertexTransformed).SetPos(_X, _Y);

				float X = pfPos.x;
				float Y = pfPos.y;

				//cout << i << " " << x << " " << y << "      " << X << " " << Y << "      " << _X << " " << _Y << endl;


				forces[i].x += (_X - X) * fWeightInitial;
				forces[i].y += (_Y - Y) * fWeightInitial;
			}

		}

		// Update vertices positions
		for (itVertex=listVertices.begin(), i=0; itVertex!=listVertices.end(); itVertex++, i++)
		{
			pfNew = itVertex->GetPos()+forces[i];
			pfNew.LimitWithinRange(ptMin, ptMax);
			itVertex->SetPos(pfNew);
		}

		// If gaussian smoothing is enabled...
		if (fWeightSmoothness!=0.0f && fStdDeviationGaussianFilter!=0.0f && bUseGaussianFilter==true)
			GaussianSmooth();

		// If resampling is enabled...
		if (bResample==true)
			Resample();

		// Update normal vectors
		ComputeNormals();
	}
	UpdateAfterEvolution();

    if (bStoppingCriterion==true)
    {
        // Get the maximal motion magnitude over vertices and compare it to threshold
        // Not quite satisfied with this criterion, maybe better in future versions...
        float fMotion, fMotionMax = 0.0f;
        for (itVertex=listVertices.begin(); itVertex!=listVertices.end(); itVertex++)
        {
            fMotion = itVertex->GetTotalMotion().L2Norm2();
            if (fMotion>fMotionMax)
                fMotionMax = fMotion;
        }

        if (sqrt(fMotionMax)<fStoppingCriterionMotionThreshold*iNbPasses)
            return false;
    }
	return true;
}

// Curve smoothing with Gaussian kernel
// Each vertex is moved with the Gaussian smoothing force of equation (55) in [Mille09]
// To avoid shrinkage, we apply the two-pass method described in
// G. Taubin, "Curve smoothing without shrinkage", ICCV, 1995
void CActiveContourBase::GaussianSmooth()
{
	int iWeight, iVertex, iNbVertices, iGaussianSize, iPass;
	list<CVertex2D>::iterator itVertex, itVertexPrev, itVertexNext;
	CArray1D<float> vectGaussianWeights;
	CArray1D<CCouple<float> > vectForces;
	float f2Sigma2;
	static const float fCoefPass[2] = {1.0f, -1.0f};

	iNbVertices = listVertices.size();

	if (iNbVertices==0 || fStdDeviationGaussianFilter==0.0f)
		return;

	vectForces.Init(iNbVertices);

	// Compute size of gaussian kernel, considering three times the standard derivation
	iGaussianSize = (int)(3.0f*fStdDeviationGaussianFilter);
	if (iGaussianSize==0)
		iGaussianSize = 1;
	vectGaussianWeights.Init(iGaussianSize+1);

	// Precompute gaussian weights
	f2Sigma2 = 2.0f*fStdDeviationGaussianFilter*fStdDeviationGaussianFilter;
	for (iWeight=0; iWeight<vectGaussianWeights.GetSize(); iWeight++)
		vectGaussianWeights[iWeight] = exp(-(float)(iWeight*iWeight)/f2Sigma2)/(fStdDeviationGaussianFilter*2.5066f);

	for (iPass=0; iPass<2; iPass++)
	{
		for (itVertex=listVertices.begin(), iVertex=0; itVertex!=listVertices.end(); itVertex++, iVertex++)
		{
			// Convolve neighboring vertices with the gaussian kernel
			vectForces[iVertex] = itVertex->GetPos() * vectGaussianWeights[0];
			itVertexPrev = itVertex;
			itVertexNext = itVertex;

			for (iWeight=1;iWeight<vectGaussianWeights.GetSize();iWeight++)
			{
				// Get the i^th previous vertex
				if (itVertexPrev!=listVertices.begin())
					itVertexPrev--;
				else
					itVertexPrev = --listVertices.end();

				// Get the i^th next vertex
				itVertexNext++;
				if (itVertexNext==listVertices.end())
					itVertexNext = listVertices.begin();

				vectForces[iVertex] += vectGaussianWeights[iWeight]*(itVertexPrev->GetPos() + itVertexNext->GetPos());
			}

			// At this stage, vectForces[iVertex] holds the smoothed vertex position, resulting from the convolution of
			// its neighboring vertices with the gaussian kernel.
			// Subtract the current vertex position, so that it actually corresponds to a displacement vector
			vectForces[iVertex] -= itVertex->GetPos();
		}

		// Move vertices with computed forces
		// At 1st pass, the force is applied as is
		// At 2nd pass, the force is applied in the opposite direction
		for (itVertex=listVertices.begin(), iVertex=0; itVertex!=listVertices.end(); itVertex++, iVertex++)
			itVertex->SetPos(itVertex->GetPos()+fWeightSmoothness*fCoefPass[iPass]*vectForces[iVertex]);
	}
}

// Update unit inward normal vectors once vertices have been moved or initialized
void CActiveContourBase::ComputeNormals()
{
	CCouple<float> norm, tang;
	list<CVertex2D>::iterator itVertex, itVertexPrev, itVertexNext;

	if (listVertices.size()<3)
		return;

	// Normal vector of first vertex
	itVertex = listVertices.begin();
	itVertexPrev = --listVertices.end();
	itVertexNext = itVertex;
	itVertexNext++;

	tang = itVertexNext->GetPos() - itVertexPrev->GetPos();
	norm.x = -tang.y;
	norm.y = tang.x;
	itVertex->SetNormal(norm.Normalized());

	// Normal vector of other vertices
	for (itVertex=++listVertices.begin(); itVertex!=--listVertices.end(); itVertex++)
	{
		itVertexPrev = itVertex;
		itVertexPrev--;
		itVertexNext = itVertex;
		itVertexNext++;

		tang = itVertexNext->GetPos() - itVertexPrev->GetPos();
		norm.x = -tang.y;
		norm.y = tang.x;
		itVertex->SetNormal(norm.Normalized());
	}

	// Normal vector of last vertex
	itVertex = --listVertices.end();
	itVertexPrev = itVertex;
	itVertexPrev--;
	itVertexNext = listVertices.begin();

	tang = itVertexNext->GetPos() - itVertexPrev->GetPos();
	norm.x = -tang.y;
	norm.y = tang.x;
	itVertex->SetNormal(norm.Normalized());
}

// Return position of approximated centroid of curve (just the average of vertices)
CCouple<float> CActiveContourBase::GetCentroid() const
{
	list<CVertex2D>::const_iterator itVertex;
	CCouple<float> pfCentroid(0.0f, 0.0f);
	int iNbVertices;

	iNbVertices = listVertices.size();

	if (iNbVertices==0)
		return pfCentroid;

	for (itVertex=listVertices.begin(); itVertex!=listVertices.end(); itVertex++)
		pfCentroid += itVertex->GetPos();

	return pfCentroid/(float)iNbVertices;
}

// Return length of curve by summing lengths of edges between vertices
float CActiveContourBase::GetLength() const
{
	list<CVertex2D>::const_iterator itVertex, itVertexNext;
	float fLength;
	int iNbVertices;

	iNbVertices = listVertices.size();
	if (iNbVertices<2)
		return 0.0f;

	fLength = 0.0f;

	// Sum up the lengths of all edges except the last one
	for (itVertex=listVertices.begin(); itVertex!=--listVertices.end(); itVertex++)
	{
		itVertexNext = itVertex;
		itVertexNext++;
		fLength += (itVertex->GetPos()-itVertexNext->GetPos()).L2Norm();
	}

	// Add length of last edge
	itVertex = --listVertices.end();
	itVertexNext = listVertices.begin();
	fLength += (itVertex->GetPos()-itVertexNext->GetPos()).L2Norm();

	return fLength;
}



void CActiveContourBase::InitialModel(const Vertices2DList& list)
{
	CVertex2D vertexNew;
	CCouple<float> pfNew;
	int i, iNbVertices;

	listInitialVertices.clear();
	listInitialVerticesTransformed.clear();

	// Number of vertices depends on sampling and circle perimeter
	iNbVertices = (int)(list.size());
	if (iNbVertices<3)
	{
		cerr << "ERROR in CActiveContourBase::InitCircle(...): cannot initialize the contour: circle is too small to yield enough vertices with current sampling" << endl;
		return;
	}

	// Create vertices
	for (i = 0; i<iNbVertices; i++)
	{
		pfNew.x = list[i].x;
		pfNew.y = list[i].y;
		vertexNew.SetPos(pfNew);
		vertexNew.ResetTotalMotion();

		listInitialVertices.push_back(vertexNew);
		listInitialVerticesTransformed.push_back(vertexNew);
	}
}


void CActiveContourBase::InitModel(const Vertices2DList& list)
{
	CVertex2D vertexNew;
	CCouple<float> ptMin, ptMax, pfNew;
	int i, iNbVertices;

	listVertices.clear();

	if (pInputImage == NULL)
	{
		cerr << "ERROR in CActiveContourBase::InitCircle(...): cannot initialize the contour: not attached to image. Call AttachImage() before" << endl;
		return;
	}

	// Number of vertices depends on sampling and circle perimeter
	iNbVertices = (int)(list.size());
	if (iNbVertices<3)
	{
		cerr << "ERROR in CActiveContourBase::InitCircle(...): cannot initialize the contour: circle is too small to yield enough vertices with current sampling" << endl;
		return;
	}

	ptMin.Set(1.0f, 1.0f);
	ptMax.Set((float)pInputImage->GetWidth() - 2.0f, (float)pInputImage->GetHeight() - 2.0f);

	// Create vertices
	for (i = 0; i<iNbVertices; i++)
	{
		pfNew.x = list[i].x;
		pfNew.y = list[i].y;
		pfNew.LimitWithinRange(ptMin, ptMax);
		vertexNew.SetPos(pfNew);
		vertexNew.ResetTotalMotion();

		listVertices.push_back(vertexNew);
	}

	ComputeNormals();
	UpdateAfterInitialization();
}

void CActiveContourBase::InitCircle(const CCouple<float> &pfCenter, float fRadius)
{
	CVertex2D vertexNew;
	CCouple<float> ptMin, ptMax, pfNew;
	int i, iNbVertices;
	static const float _2pi = 6.28319f;
	float fAngle;

	listVertices.clear();

	if (pInputImage==NULL)
	{
		cerr<<"ERROR in CActiveContourBase::InitCircle(...): cannot initialize the contour: not attached to image. Call AttachImage() before"<<endl;
		return;
	}

	// Number of vertices depends on sampling and circle perimeter
	iNbVertices = (int)((_2pi*fRadius)/fSampling);
	if (iNbVertices<3)
	{
		cerr<<"ERROR in CActiveContourBase::InitCircle(...): cannot initialize the contour: circle is too small to yield enough vertices with current sampling"<<endl;
		return;
	}

	ptMin.Set(1.0f, 1.0f);
	ptMax.Set((float)pInputImage->GetWidth()-2.0f, (float)pInputImage->GetHeight()-2.0f);

	// Create vertices
	for (i=0;i<iNbVertices;i++)
	{
		fAngle = _2pi*(float)i/(float)iNbVertices;

        pfNew.x = pfCenter.x + fRadius*cos(fAngle);
        pfNew.y = pfCenter.y + fRadius*sin(fAngle);
        pfNew.LimitWithinRange(ptMin, ptMax);
		vertexNew.SetPos(pfNew);
		vertexNew.ResetTotalMotion();

		listVertices.push_back(vertexNew);
	}

	ComputeNormals();
	UpdateAfterInitialization();
}

void CActiveContourBase::InitEllipse(const CCouple<float> &pfCenter, float fRadiusX, float fRadiusY)
{
	CVertex2D vertexNew;
	CCouple<float> ptMin, ptMax, pfNew;
	int i, iNbVertices;
	static const float _pi = 3.14159f, _2pi = 6.28319f;
	float fAngle;

	listVertices.clear();

	if (pInputImage==NULL)
	{
		cerr<<"ERROR in CActiveContourBase::InitEllipse(...): cannot initialize the contour: not attached to image. Call AttachImage() before"<<endl;
		return;
	}

	// Number of vertices depends on sampling and approximated ellipse perimeter
	iNbVertices = (int)(_pi*(3.0f*(fRadiusX+fRadiusY)-sqrt((fRadiusX + 3.0f*fRadiusY)*(3.0f*fRadiusX + fRadiusY)))/fSampling);
	if (iNbVertices<3)
	{
		cerr<<"ERROR in CActiveContourBase::InitEllipse(...): cannot initialize the contour: ellipse is too small to yield enough vertices with current sampling"<<endl;
		return;
	}

	ptMin.Set(1.0f, 1.0f);
	ptMax.Set((float)pInputImage->GetWidth()-2.0f, (float)pInputImage->GetHeight()-2.0f);

	// Create vertices
	for (i=0;i<iNbVertices;i++)
	{
		fAngle = _2pi*(float)i/(float)iNbVertices;

		pfNew.x = pfCenter.x + fRadiusX*cos(fAngle);
        pfNew.y = pfCenter.y + fRadiusY*sin(fAngle);
        pfNew.LimitWithinRange(ptMin, ptMax);
		vertexNew.SetPos(pfNew);
		vertexNew.ResetTotalMotion();

		listVertices.push_back(vertexNew);
	}

	ComputeNormals();
	UpdateAfterInitialization();
}

void CActiveContourBase::DrawInImageRGB(CImage2D &im, int iZoom) const
{
	list<CVertex2D>::const_iterator itVertex, itVertexNext;
	list<CVertex2D>::const_iterator itVertexTransformed;
	CArray1D<CCouple<int> > vectIntPos;
	CArray1D<CCouple<int> > vectIntPosInitialTransformed;
	int i, iNbVertices;

	iNbVertices = listVertices.size();
	if (iNbVertices<3)
		return;

	if (im.GetBitsPerPixel()!=24 && im.GetBitsPerPixel()!=32)
	{
		cerr<<"ERROR in CActiveContourBase::DrawInImageRGB(...): image is not RGB"<<endl;
		return;
	}

	vectIntPos.Init(iNbVertices);

	// Compute integer coordinates of vertices with current zooming factor
	for (itVertex = listVertices.begin(), i = 0; itVertex != listVertices.end(); itVertex++, i++)
		vectIntPos[i] = (CCouple<int>)(itVertex->GetPos()*(float)iZoom) + CCouple<int>(iZoom / 2, iZoom / 2);


	// Draw curve
	if (bDisplayEdges==true)
	{
		for (i = 0; i < iNbVertices - 1; i++)
		{
			im.DrawLineRGB(vectIntPos[i], vectIntPos[i + 1], rgbEdgeColor);
		}
//		im.DrawLineRGB(vectIntPos[iNbVertices - 1], vectIntPos[0], rgbEdgeColor);
	}

	vectIntPosInitialTransformed.Init(iNbVertices);

	for (itVertexTransformed = listInitialVerticesTransformed.begin(), i = 0; itVertexTransformed != listInitialVerticesTransformed.end(); itVertexTransformed++, i++)
		vectIntPosInitialTransformed[i] = (CCouple<int>)(itVertexTransformed->GetPos()*(float)iZoom) + CCouple<int>(iZoom / 2, iZoom / 2);

	if (bDisplayEdges == true)
	{
		for (i = 0; i < iNbVertices - 1; i++)
		{
			im.DrawLineRGB(vectIntPosInitialTransformed[i], vectIntPosInitialTransformed[i + 1], rgbEdgeTransformedColor);
		}
	//	im.DrawLineRGB(vectIntPosInitialTransformed[iNbVertices - 1], vectIntPosInitialTransformed[0], rgbEdgeTransformedColor);
	}

	// Draw vertices
	if (bDisplayVertices==true)
	{
		for (i=0; i<iNbVertices; i++)
			im.DrawFilledCircleRGB(vectIntPos[i], iVertexWidth, rgbVertexColor);
	}

	// Draw inner and outer parallel curves
	if (bDisplayParallelCurves==true)
	{
		CCouple<float> pfParallelPoint;
		float fBandThickness = (float)iBandThickness;

		// Compute integer coordinates of vertices on the inner parallel curve with current zooming factor
		for (itVertex=listVertices.begin(), i=0; itVertex!=listVertices.end(); itVertex++, i++)
		{
			pfParallelPoint = itVertex->GetPos() + itVertex->GetNormal()*fBandThickness;
			vectIntPos[i] = (CCouple<int>)(pfParallelPoint*(float)iZoom) + CCouple<int>(iZoom/2, iZoom/2);
		}

		// Draw inner parallel curve
		for (i=0; i<iNbVertices-1; i++)
			im.DrawLineRGB(vectIntPos[i], vectIntPos[i+1], rgbEdgeParallelColor);
		im.DrawLineRGB(vectIntPos[iNbVertices-1], vectIntPos[0], rgbEdgeParallelColor);

		// Compute integer coordinates of vertices on the outer parallel curve with current zooming factor
		for (itVertex=listVertices.begin(), i=0; itVertex!=listVertices.end(); itVertex++, i++)
		{
			pfParallelPoint = itVertex->GetPos() - itVertex->GetNormal()*fBandThickness;
			vectIntPos[i] = (CCouple<int>)(pfParallelPoint*(float)iZoom) + CCouple<int>(iZoom/2, iZoom/2);
		}

		// Draw outer parallel curve
		for (i=0; i<iNbVertices-1; i++)
			im.DrawLineRGB(vectIntPos[i], vectIntPos[i+1], rgbEdgeParallelColor);
		im.DrawLineRGB(vectIntPos[iNbVertices-1], vectIntPos[0], rgbEdgeParallelColor);
	}
}

void CActiveContourBase::MakeBinaryMask(CImage2D &imgMask) const
{
    CCouple<float> pfHalf(0.5f, 0.5f);
    list<CVertex2D>::const_iterator itVertex;
    CArray1D<CCouple<int> > vectIntPos;
    unsigned int i, iNbVertices;

	if (imgMask.GetBitsPerPixel()!=8 || imgMask.GetSize()!=pInputImage->GetSize())
	{
		if (imgMask.Create(pInputImage->GetWidth(), pInputImage->GetHeight(), 8)==false)
			return;
	}
    imgMask.Clear(0);

    iNbVertices = listVertices.size();
    vectIntPos.Init(iNbVertices);

	// Compute integer coordinates of vertices
	for (itVertex=listVertices.begin(), i=0; itVertex!=listVertices.end(); itVertex++, i++)
		vectIntPos[i] = (CCouple<int>)(itVertex->GetPos() + pfHalf);

	// Clear image in black and draw curve
    imgMask.Clear(0);
	for (i=0; i<iNbVertices-1; i++)
        imgMask.DrawLine(vectIntPos[i], vectIntPos[i+1], 255);
    imgMask.DrawLine(vectIntPos[iNbVertices-1], vectIntPos[0], 255);
}

void CActiveContourBase::Empty()
{
	listVertices.clear();
}

void CActiveContourBase::UpdateAfterEvolution()
{
	if (fWeightRegion!=0.0f)
	{
		if (iInsideMode==INSIDEMODE_REGION || iOutsideMode==OUTSIDEMODE_REGION)
			ComputeRegionMeansGreenTheorem();

		if (iInsideMode==INSIDEMODE_BAND || iOutsideMode==OUTSIDEMODE_BAND)
			ComputeBandMeans();
	}
}

/**********************************************************************
*                      CActiveContourGrayscale                        *
**********************************************************************/

CActiveContourGrayscale::CActiveContourGrayscale():CActiveContourBase()
{
	fInnerMean = 0.0f;
	fOuterMean = 0.0f;

	fInnerNarrowBandMean = 0.0f;
	fOuterNarrowBandMean = 0.0f;
}

void CActiveContourGrayscale::AttachImage(const CImage2D *pImage)
{
	int iBitsPerPixel;

	iBitsPerPixel = pImage->GetBitsPerPixel();
	if (iBitsPerPixel!=8)
	{
		cerr<<"ERROR in CActiveContourGrayscale::AttachImage(): instances of CActiveContourGrayscale can be attached to 8-bit images only"<<endl;
		pInputImage = NULL;
		return;
	}

	pInputImage = pImage;
	pInputImage->ConvertToArray2DFloat(arrayImage);

	// If edge information is used, then precompute vector field which will be used for gradient force
	if (fWeightGradient!=0.0f)
	{
		CArray2D<float> arrayGaussian, arrayDiffX, arrayDiffY, arrayGradientNorm;
		float *pDiffX, *pDiffY, *pNorm;
		float fScale;
		int i, iSize;

		fScale = 1.0f;

		// Gaussian mask's half size should be at least 3
		if (fScale<1.0f)
			arrayGaussian.SetGaussianKernel(fScale, 3);
		else
			arrayGaussian.SetGaussianKernel(fScale);

		// Convolve the image with x and y-derivatives of gaussian
		arrayDiffX = arrayImage.Convolve(arrayGaussian.DerivativeX(1, ARRAYNDFLOAT_CENTERED));
		arrayDiffY = arrayImage.Convolve(arrayGaussian.DerivativeY(1, ARRAYNDFLOAT_CENTERED));

		// Compute the magnitude of the obtained "smooth gradient" for every pixel
		arrayGradientNorm.Init(arrayImage.GetSize());
		pDiffX = arrayDiffX.GetBuffer();
		pDiffY = arrayDiffY.GetBuffer();
		pNorm = arrayGradientNorm.GetBuffer();
		iSize = arrayGradientNorm.GetWidth()*arrayGradientNorm.GetHeight();
		for (i=0; i<iSize; i++)
		{
			*pNorm = sqrt((*pDiffX)*(*pDiffX) + (*pDiffY)*(*pDiffY));
			pDiffX++;
			pDiffY++;
			pNorm++;
		}

		// Final vector field is taken as the gradient of the smooth gradient magnitude
		arrayGradientField = arrayGradientNorm.Gradient();
	}

	if (fWeightRegion!=0.0f)
	{
		if (bUseImageIntegrals==true)
			ComputeImageIntegrals();
		else {
			// Compute sum of image intensities
			// Useful to compute average intensity outside the contour if needed
			int x, y, iWidth, iHeight;

			iWidth = pInputImage->GetWidth();
			iHeight = pInputImage->GetHeight();

			fImageSum = 0.0f;

			for (y=0;y<iHeight;y++)
				for (x=0;x<iWidth;x++)
					fImageSum += arrayImage.Element(x,y);
		}
	}
}

void CActiveContourGrayscale::ComputeImageIntegrals()
{
	int x, y, width, height;

	width = pInputImage->GetWidth();
	height = pInputImage->GetHeight();

	arrayImageIntegralX.Init(width, height);
	arrayImageIntegralY.Init(width, height);

	// Compute cumulative intensities in x-direction
	for (y=0;y<height;y++)
	{
		arrayImageIntegralX.Element(0,y) = arrayImage.Element(0,y);
		for (x=1;x<width;x++)
			arrayImageIntegralX.Element(x,y) = arrayImageIntegralX.Element(x-1,y)+arrayImage.Element(x,y);
	}

	// Compute cumulative intensities in y-direction
	for (x=0;x<width;x++)
	{
		arrayImageIntegralY.Element(x,0) = arrayImage.Element(x,0);
		for (y=1;y<height;y++)
			arrayImageIntegralY.Element(x,y) = arrayImageIntegralY.Element(x,y-1)+arrayImage.Element(x,y);
	}

	// Compute sum of image intensities
	fImageSum = 0.0f;
	for (y=0;y<height;y++)
		fImageSum += arrayImageIntegralX.Element(width-1,y);
}

void CActiveContourGrayscale::ComputeRegionMeansGreenTheorem()
{
	list<CVertex2D>::const_iterator itVertex, itVertexPrev, itVertexNext;
	int iWidth, iHeight;
	CCouple<float> vfDerivative1;
	CCouple<int> piPos, piIntegral;
	float fInnerSum, fOuterSum;
	float fSumX, fSumY;

	if (listVertices.size()<3)
		return;

	iWidth = pInputImage->GetWidth();
	iHeight = pInputImage->GetHeight();

	fInnerRegionArea = 0.0f;
	fInnerSum = 0.0f;

	for (itVertex=listVertices.begin(); itVertex!=listVertices.end(); itVertex++)
	{
		if (itVertex==listVertices.begin())
			itVertexPrev = --listVertices.end();
		else {
			itVertexPrev = itVertex;
			itVertexPrev--;
		}

		itVertexNext = itVertex;
		itVertexNext++;
		if (itVertexNext==listVertices.end())
			itVertexNext = listVertices.begin();

		piPos = (CCouple<int>)itVertex->GetPos();

		// Velocity vector at current vertex: centered finite difference approximation
		// of curve first-order derivative
		// Could also use backward finite difference : itVertex->pfPos - itVertexPrev->pfPos ?
		vfDerivative1 = (itVertexNext->GetPos()-itVertexPrev->GetPos())*0.5f;

		fInnerRegionArea += vfDerivative1.y*itVertex->GetPos().x - vfDerivative1.x*itVertex->GetPos().y;

		if (bUseImageIntegrals==true)
			// Arrays arrayImageIntegralX and arrayImageIntegralY implements function 2Q and -2P, respectively,
			// defined in equation (13), so next instruction implements equation (14) of section 2.3 in [Mille09]:
			fInnerSum += vfDerivative1.y*arrayImageIntegralX.Element(piPos) - vfDerivative1.x*arrayImageIntegralY.Element(piPos);
		else {
			// If image integrals have not been computed, apply "brutal" discretization
			// of Green's theorem (see first equation of section 5.6 in [Mille09])
			fSumX = 0.0f;
			piIntegral.y = piPos.y;
			for (piIntegral.x=0; piIntegral.x<=piPos.x; piIntegral.x++)
				fSumX += arrayImage.Element(piIntegral);

			fSumY = 0.0f;
			piIntegral.x = piPos.x;
			for (piIntegral.y=0; piIntegral.y<=piPos.y; piIntegral.y++)
				fSumY += arrayImage.Element(piIntegral);

			fInnerSum += vfDerivative1.y*fSumX - vfDerivative1.x*fSumY;
		}
	}

	fInnerRegionArea*=0.5f;
	fOuterRegionArea = (float)(iWidth*iHeight)-fInnerRegionArea;

	fInnerSum*=0.5f;
	fOuterSum = fImageSum - fInnerSum;
	fInnerMean = fInnerSum/fInnerRegionArea;
	fOuterMean = fOuterSum/fOuterRegionArea;
}

void CActiveContourGrayscale::ComputeBandMeans()
{
	list<CVertex2D>::const_iterator itVertex, itVertexPrev, itVertexNext;
	CCouple<float> vfDerivative1, vfDerivative2;
	float fThickness, fBandThickness = (float)iBandThickness;
	float fLengthElement, fLengthElementParallelCurve, fCurvature;
	float fIntensity;

	if (listVertices.size()<3)
		return;

	fInnerNarrowBandArea = 0.0f;
	fOuterNarrowBandArea = 0.0f;

	fInnerNarrowBandMean = 0.0f;
	fOuterNarrowBandMean = 0.0f;

	// This loop implements the computation of average intensities on the narrow bands,
	// according to the template expression in equation (51) in [Mille09]
	for (itVertex=listVertices.begin(); itVertex!=listVertices.end(); itVertex++)
	{
		if (itVertex==listVertices.begin())
			itVertexPrev = --listVertices.end();
		else {
			itVertexPrev = itVertex;
			itVertexPrev--;
		}

		itVertexNext = itVertex;
		itVertexNext++;
		if (itVertexNext==listVertices.end())
			itVertexNext = listVertices.begin();

		// Velocity vector at current vertex: centered finite difference approximation
		// of curve first-order derivative
		// Could also use backward finite difference : itVertex->pfPos - itVertexPrev->pfPos ?
		vfDerivative1 = (itVertexNext->GetPos()-itVertexPrev->GetPos())*0.5f;

		// Finite difference approximation of curve second-order derivative
		vfDerivative2 = itVertexPrev->GetPos() + itVertexNext->GetPos() - 2.0f*itVertex->GetPos();

		// Discrete length element, denoted \ell_i in [Mille09] (continuous definition is in equation (9) )
		fLengthElement = vfDerivative1.L2Norm();

		// Discrete curvature, denoted \kappa_i in [Mille09] (continuous definition is given shortly after length element)
		fCurvature = (vfDerivative1.x * vfDerivative2.y - vfDerivative1.y * vfDerivative2.x)/(fLengthElement*fLengthElement*fLengthElement);

		for (fThickness=0; fThickness<fBandThickness; fThickness+=1.0f)
		{
			fLengthElementParallelCurve = fLengthElement * (1.0f - fThickness*fCurvature);

			// The following test is related to regularity condition (12) in [Mille09]
			// If it is false, the thickness exceeds the radius of curvature, yielding a singularity on the
			// inner parallel curve. In this case, it seems reasonable not to include the resulting pixel
			// in the inner average color
			if (fLengthElementParallelCurve>0.0f)
			{
				fIntensity = arrayImage.GetElementInterpolate(itVertex->GetPos() + fThickness*itVertex->GetNormal());
				fInnerNarrowBandMean += fIntensity*fLengthElementParallelCurve;
				fInnerNarrowBandArea += fLengthElementParallelCurve;
			}
		}

		for (fThickness=1.0f; fThickness<=fBandThickness; fThickness+=1.0f)
		{
			fLengthElementParallelCurve = fLengthElement * (1.0f + fThickness*fCurvature);

			// The following test is related to regularity condition (12) in [Mille09]
			// If it is false, the thickness exceeds the radius of curvature, yielding a singularity on the
			// outer parallel curve. In this case, it seems reasonable not to include the resulting pixel
			// in the outer average color
			if (fLengthElementParallelCurve>0.0f)
			{
				fIntensity = arrayImage.GetElementInterpolate(itVertex->GetPos() - fThickness*itVertex->GetNormal());
				fOuterNarrowBandMean += fIntensity*fLengthElementParallelCurve;
				fOuterNarrowBandArea += fLengthElementParallelCurve;
			}
		}
	}

	fInnerNarrowBandMean /= fInnerNarrowBandArea;
	fOuterNarrowBandMean /= fOuterNarrowBandArea;
}

CCouple<float> CActiveContourGrayscale::RegionForce(const CVertex2D *pVertexPrev, const CVertex2D *pVertex, const CVertex2D *pVertexNext) const
{
	// Region terms
	float fDiffRegion, fDiffRegionInside, fDiffRegionOutside;

	// Band thickness stored as a real number (make the conversion once)
	float fBandThickness = (float)iBandThickness;

	// Intensity at current vertex
	float fIntensity;

	// Inner and outer intensity region descriptors
	float fInnerMeanCurrent=0.0f, fOuterMeanCurrent=0.0f;

	// For computing the bias force
	float fCoefBias, fForceBias, fDiffInsideOutside;

	fIntensity = arrayImage.GetElementInterpolate(pVertex->GetPos());

	// Choose appropriate inner region descriptor depending on iInsideMode
	if (iInsideMode==INSIDEMODE_REGION)
		fInnerMeanCurrent = fInnerMean;
	else if (iInsideMode==INSIDEMODE_REGIONINIT)
		fInnerMeanCurrent = fInnerInitialMean;
	else if (iInsideMode==INSIDEMODE_BAND)
		fInnerMeanCurrent = fInnerNarrowBandMean;

	// Choose appropriate outer region descriptor depending on iOutsideMode
	if (iOutsideMode==OUTSIDEMODE_REGION)
		fOuterMeanCurrent = fOuterMean;
	else if (iOutsideMode==OUTSIDEMODE_BAND)
		fOuterMeanCurrent = fOuterNarrowBandMean;
	else if (iOutsideMode==OUTSIDEMODE_LINE)
	{
		float fThickness;
		float fLengthElement, fWeightCurvature, fCurvature, fSumWeights;
		CCouple<float> vfDerivative1, vfDerivative2;

		// Velocity vector at current vertex: centered finite difference approximation
		// of curve first-order derivative
		// Could also use backward finite difference : itVertex->pfPos - itVertexPrev->pfPos ?
		vfDerivative1 = (pVertexNext->GetPos()-pVertexPrev->GetPos())*0.5f;

		// Finite difference approximation of curve second-order derivative
		vfDerivative2 = pVertexPrev->GetPos() + pVertexNext->GetPos() - 2.0f*pVertex->GetPos();

		// Discrete length element, denoted \ell_i in [Mille09] (continuous definition is in equation (9) )
		fLengthElement = vfDerivative1.L2Norm();

		// Discrete curvature, denoted \kappa_i in [Mille09] (continuous definition is given shortly after length element)
		fCurvature = (vfDerivative1.x * vfDerivative2.y - vfDerivative1.y * vfDerivative2.x)/(fLengthElement*fLengthElement*fLengthElement);

		// Average weighted intensity along the outward normal line segment of length equal to band thickness
		// Implements the computation of \mu_NL appearing just after equation (54) in [Mille09]
		fOuterMeanCurrent = 0.0f;
		fSumWeights = 0.0f;
		for (fThickness=1.0f; fThickness<=fBandThickness; fThickness+=1.0f)
		{
			fWeightCurvature = 1.0f + fThickness*fCurvature; // Implements (1+b\kappa_i)

			// The following test is related to regularity condition (12) in [Mille09]
			// If it is false, the thickness exceeds the radius of curvature, yielding a singularity on the
			// outer parallel curve. In this case, it seems reasonable not to include the resulting pixel
			if (fWeightCurvature>0.0f)
			{
				fOuterMeanCurrent += arrayImage.GetElementInterpolate(pVertex->GetPos() - fThickness*pVertex->GetNormal())*fWeightCurvature;
				fSumWeights += fWeightCurvature;
			}
		}

		if (fSumWeights==0.0f)
		{
			// This happens if the curve is excessively concave at the current vertex
			// Return a null force, so that only regularization is effective on this particular vertex
			return CCouple<float>(0.0f, 0.0f);
		}
		else
			fOuterMeanCurrent /= fSumWeights;
	}

	// Unlike in [Mille09], deviations with respect to inner and outer descriptors
	// are computed using the L1 norm
	// This yields better behaviour with respect to local intensity differences
	fDiffRegionInside = fabs(fInnerMeanCurrent-fIntensity);
	fDiffRegionOutside = fabs(fOuterMeanCurrent-fIntensity);

	// The narrow band region energies introduced in [Mille09] are symmetric, i.e. inner and outer region terms
	// are identically weighted However, an asymmetric configuration is allowed here, in order to favor
	// minimization of intensity deviation inside or outside. This enables to implement the parametric equivalent
	// of the Chan-Vese model (which is asymmetric) for comparison purpose.
	// The initial configuration is symmetric, since the default value of fWeightRegionInOverOut is 0.5
	fDiffRegion = fWeightRegionInOverOut * fDiffRegionInside - (1.0f-fWeightRegionInOverOut) * fDiffRegionOutside;

	// If bias is enabled, a balloon-like force is added to the region force
	if (bRegionEnergyBias==true)
	{
		// This is slightly different from the bias force described in section 5.5 in [Mille09],
		// as the magnitude of the bias is set by the balloon weight (which should be negative)
		fForceBias = fWeightBalloon + fabs(fInnerMeanCurrent-fIntensity);

		// Compute bias coefficient, denoted \gamma and introduced in equation (56) in [Mille09]
		// It decreases exponentially with respect to the difference between inner and outer region descriptors
		fDiffInsideOutside = fabs(fInnerMeanCurrent-fOuterMeanCurrent);
		fCoefBias = (1.0f-fDiffInsideOutside)/(1.0f+50.0f*fDiffInsideOutside);

		// Linear combination of bias and region forces
		// See second part of equation (56)
		fDiffRegion = fCoefBias*fForceBias + (1.0f-fCoefBias)*fDiffRegion;
	}

	return pVertex->GetNormal()*fDiffRegion;
}

void CActiveContourGrayscale::UpdateAfterInitialization()
{
	if (fWeightRegion!=0.0f)
	{
		if (iInsideMode==INSIDEMODE_REGION || iInsideMode==INSIDEMODE_REGIONINIT ||iOutsideMode==OUTSIDEMODE_REGION)
		{
			ComputeRegionMeansGreenTheorem();
			if (iInsideMode==INSIDEMODE_REGIONINIT)
				fInnerInitialMean = fInnerMean;
		}

		if (iInsideMode==INSIDEMODE_BAND || iOutsideMode==OUTSIDEMODE_BAND)
			ComputeBandMeans();
	}
}



/**********************************************************************
*                      CActiveContourColor                            *
**********************************************************************/

CActiveContourColor::CActiveContourColor():CActiveContourBase()
{
	fcolInnerMean.Set(0.0f, 0.0f, 0.0f);
	fcolOuterMean.Set(0.0f, 0.0f, 0.0f);

	fcolInnerNarrowBandMean.Set(0.0f, 0.0f, 0.0f);
	fcolOuterNarrowBandMean.Set(0.0f, 0.0f, 0.0f);

	// Default color space is RGB
	iColorSpace = COLORSPACE_RGB;
	bIgnoreBrightnessComponent = false;
}

void CActiveContourColor::AttachImage(const CImage2D *pImage)
{
	int iBitsPerPixel;

	iBitsPerPixel = pImage->GetBitsPerPixel();
	if (iBitsPerPixel!=24 && iBitsPerPixel!=32)
	{
		cerr<<"ERROR in CActiveContourColor::AttachImage(): instances of CActiveContourColor can be attached to RGB images only"<<endl;
		pInputImage = NULL;
		return;
	}

	pInputImage = pImage;

	if (iColorSpace==COLORSPACE_RGB)
		pInputImage->ConvertToArray2DTripletFloatRGB(arrayImage);
	else if (iColorSpace==COLORSPACE_YUV)
		pInputImage->ConvertToArray2DTripletFloatYUV(arrayImage);
	else if (iColorSpace==COLORSPACE_LAB)
		pInputImage->ConvertToArray2DTripletFloatLAB(arrayImage);

	// If chosen color space separates brightness from color information (i.e. not RGB)
	// and brightness component should be ignored for illumination invariance purpose,
	// then set brightness component to zero everywhere
	if (iColorSpace!=COLORSPACE_RGB && bIgnoreBrightnessComponent==true)
	{
		CTriplet<float> *pColor;
		int i,iSize;

		iSize = arrayImage.GetWidth()*arrayImage.GetHeight();
		pColor = arrayImage.GetBuffer();
		for (i=0; i<iSize; i++)
		{
			pColor->x = 0.0f;
			pColor++;
		}
	}

	// If edge information is used, then precompute vector field which will be used for gradient force
	if (fWeightGradient!=0.0f)
	{
		CArray2D<CTriplet<float> > arrayDiffX, arrayDiffY;
		CArray2D<float> arrayGaussian, arrayGradientNorm;
		CTriplet<float> *pDiffX, *pDiffY;
		float *pNorm;
		float fScale;
		int i, iSize;

		// Gaussian mask's half size should be at least 3
		fScale = 1.0f;
		if (fScale<1.0f)
			arrayGaussian.SetGaussianKernel(fScale, 3);
		else
			arrayGaussian.SetGaussianKernel(fScale);

		// Convolve the image with x and y-derivatives of gaussian
		arrayDiffX = arrayImage.Convolve(arrayGaussian.DerivativeX(1, ARRAYNDFLOAT_CENTERED));
		arrayDiffY = arrayImage.Convolve(arrayGaussian.DerivativeY(1, ARRAYNDFLOAT_CENTERED));

		// Compute the magnitude of the obtained "smooth gradient" for every pixel
		arrayGradientNorm.Init(arrayImage.GetSize());
		pDiffX = arrayDiffX.GetBuffer();
		pDiffY = arrayDiffY.GetBuffer();
		pNorm = arrayGradientNorm.GetBuffer();
		iSize = arrayGradientNorm.GetWidth()*arrayGradientNorm.GetHeight();
		for (i=0; i<iSize; i++)
		{
			*pNorm = sqrt(pDiffX->L2Norm2() + pDiffY->L2Norm2());
			pDiffX++;
			pDiffY++;
			pNorm++;
		}

		// Final vector field is taken as the gradient of the smooth gradient magnitude
		arrayGradientField = arrayGradientNorm.Gradient();
	}

	if (fWeightRegion!=0.0f)
	{
		if (bUseImageIntegrals==true)
			ComputeImageIntegrals();
		else {
			// Compute sum of image colors
			// Useful to compute average color outside the contour if needed
			int x, y, iWidth, iHeight;
			CTriplet<float> fPixel;

			iWidth = pInputImage->GetWidth();
			iHeight = pInputImage->GetHeight();

			fcolImageSum.Set(0.0f, 0.0f, 0.0f);

			for (y=0;y<iHeight;y++)
				for (x=0;x<iWidth;x++)
					fcolImageSum += arrayImage.Element(x,y);
		}
	}
}

void CActiveContourColor::ComputeImageIntegrals()
{
	int x, y, width, height;

	width = pInputImage->GetWidth();
	height = pInputImage->GetHeight();

	arrayImageIntegralX.Init(width, height);
	arrayImageIntegralY.Init(width, height);

	// Compute cumulative colors in x-direction
	for (y=0;y<height;y++)
	{
		arrayImageIntegralX.Element(0,y) = arrayImage.Element(0,y);
		for (x=1;x<width;x++)
			arrayImageIntegralX.Element(x,y) = arrayImageIntegralX.Element(x-1,y)+arrayImage.Element(x,y);
	}

	// Compute cumulative colors in y-direction
	for (x=0;x<width;x++)
	{
		arrayImageIntegralY.Element(x,0) = arrayImage.Element(x,0);
		for (y=1;y<height;y++)
			arrayImageIntegralY.Element(x,y) = arrayImageIntegralY.Element(x,y-1)+arrayImage.Element(x,y);
	}

	// Compute sum of image colors
	fcolImageSum.Set(0.0f, 0.0f, 0.0f);
	for (y=0;y<height;y++)
		fcolImageSum += arrayImageIntegralX.Element(width-1,y);
}

void CActiveContourColor::ComputeRegionMeansGreenTheorem()
{
	list<CVertex2D>::const_iterator itVertex, itVertexPrev, itVertexNext;
	int iWidth, iHeight;
	CCouple<float> vfDerivative1;
	CCouple<int> piPos, piIntegral;
	CTriplet<float> fPixel, fcolInnerSum, fcolOuterSum;
	CTriplet<float> fcolSumX, fcolSumY;

	if (listVertices.size()<3)
		return;

	iWidth = pInputImage->GetWidth();
	iHeight = pInputImage->GetHeight();

	fInnerRegionArea = 0.0f;
	fcolInnerSum.Set(0.0f, 0.0f, 0.0f);

	for (itVertex=listVertices.begin(); itVertex!=listVertices.end(); itVertex++)
	{
		if (itVertex==listVertices.begin())
			itVertexPrev = --listVertices.end();
		else {
			itVertexPrev = itVertex;
			itVertexPrev--;
		}

		itVertexNext = itVertex;
		itVertexNext++;
		if (itVertexNext==listVertices.end())
			itVertexNext = listVertices.begin();

		piPos = (CCouple<int>)itVertex->GetPos();

		// Velocity vector at current vertex: centered finite difference approximation
		// of curve first-order derivative
		// Could also use backward finite difference : itVertex->GetPos() - itVertexPrev->GetPos() ?
		vfDerivative1 = (itVertexNext->GetPos()-itVertexPrev->GetPos())*0.5f;

		fInnerRegionArea += vfDerivative1.y*itVertex->GetPos().x - vfDerivative1.x*itVertex->GetPos().y;

		if (bUseImageIntegrals==true)
			// Arrays arrayImageIntegralX and arrayImageIntegralY implements function 2Q and -2P, respectively,
			// defined in equation (13), so next instruction implements equation (14) of section 2.3 in [Mille09]:
			fcolInnerSum += vfDerivative1.y*arrayImageIntegralX.Element(piPos) - vfDerivative1.x*arrayImageIntegralY.Element(piPos);
		else {
			// If image integrals have not been computed, apply "brutal" discretization
			// of Green's theorem (see first equation of section 5.6 in [Mille09] for the grayscale equivalent)
			fcolSumX.Set(0.0f, 0.0f, 0.0f);
			piIntegral.y = piPos.y;
			for (piIntegral.x=0; piIntegral.x<=piPos.x; piIntegral.x++)
				fcolSumX += arrayImage.Element(piIntegral);

			fcolSumY.Set(0.0f, 0.0f, 0.0f);
			piIntegral.x = piPos.x;
			for (piIntegral.y=0; piIntegral.y<=piPos.y; piIntegral.y++)
				fcolSumY += arrayImage.Element(piIntegral);

			fcolInnerSum += vfDerivative1.y*fcolSumX - vfDerivative1.x*fcolSumY;
		}
	}

	fInnerRegionArea*=0.5f;
	fOuterRegionArea = (float)(iWidth*iHeight)-fInnerRegionArea;

	fcolInnerSum*=0.5f;
	fcolOuterSum = fcolImageSum - fcolInnerSum;
	fcolInnerMean = fcolInnerSum/fInnerRegionArea;
	fcolOuterMean = fcolOuterSum/fOuterRegionArea;
}

void CActiveContourColor::ComputeBandMeans()
{
	list<CVertex2D>::const_iterator itVertex, itVertexPrev, itVertexNext;
	CCouple<float> vfDerivative1, vfDerivative2;
	float fThickness, fBandThickness = (float)iBandThickness;
	float fLengthElement, fLengthElementParallelCurve, fCurvature;
	CTriplet<float> fPixel;

	if (listVertices.size()<3)
		return;

	fInnerNarrowBandArea = 0.0f;
	fOuterNarrowBandArea = 0.0f;

	fcolInnerNarrowBandMean.Set(0.0f, 0.0f, 0.0f);
	fcolOuterNarrowBandMean.Set(0.0f, 0.0f, 0.0f);

	// This loop implements the computation of average colors on the narrow bands,
	// according to the template expression in equation (51) in [Mille09]
	for (itVertex=listVertices.begin(); itVertex!=listVertices.end(); itVertex++)
	{
		if (itVertex==listVertices.begin())
			itVertexPrev = --listVertices.end();
		else {
			itVertexPrev = itVertex;
			itVertexPrev--;
		}

		itVertexNext = itVertex;
		itVertexNext++;
		if (itVertexNext==listVertices.end())
			itVertexNext = listVertices.begin();

		// Velocity vector at current vertex: centered finite difference approximation
		// of curve first-order derivative
		// Could also use backward finite difference : itVertex->GetPos() - itVertexPrev->GetPos() ?
		vfDerivative1 = (itVertexNext->GetPos()-itVertexPrev->GetPos())*0.5f;

		// Finite difference approximation of curve second-order derivative
		vfDerivative2 = itVertexPrev->GetPos() + itVertexNext->GetPos() - 2.0f*itVertex->GetPos();

		// Discrete length element, denoted \ell_i in [Mille09] (continuous definition is in equation (9) )
		fLengthElement = vfDerivative1.L2Norm();

		// Discrete curvature, denoted \kappa_i in [Mille09] (continuous definition is given shortly after length element)
		fCurvature = (vfDerivative1.x * vfDerivative2.y - vfDerivative1.y * vfDerivative2.x)/(fLengthElement*fLengthElement*fLengthElement);

		for (fThickness=0; fThickness<fBandThickness; fThickness+=1.0f)
		{
			fLengthElementParallelCurve = fLengthElement * (1.0f - fThickness*fCurvature);

			// The following test is related to regularity condition (12) in [Mille09]
			// If it is false, the thickness exceeds the radius of curvature, yielding a singularity on the
			// inner parallel curve. In this case, it seems reasonable not to include the resulting pixel
			// in the inner average color
			if (fLengthElementParallelCurve>0.0f)
			{
				fPixel = arrayImage.GetElementInterpolate(itVertex->GetPos() + fThickness*itVertex->GetNormal());
				fcolInnerNarrowBandMean += fPixel*fLengthElementParallelCurve;
				fInnerNarrowBandArea += fLengthElementParallelCurve;
			}
		}

		for (fThickness=1.0f; fThickness<=fBandThickness; fThickness+=1.0f)
		{
			fLengthElementParallelCurve = fLengthElement * (1.0f + fThickness*fCurvature);

			// The following test is related to regularity condition (12) in [Mille09]
			// If it is false, the thickness exceeds the radius of curvature, yielding a singularity on the
			// outer parallel curve. In this case, it seems reasonable not to include the resulting pixel
			// in the outer average color
			if (fLengthElementParallelCurve>0.0f)
			{
				fPixel = arrayImage.GetElementInterpolate(itVertex->GetPos() - fThickness*itVertex->GetNormal());
				fcolOuterNarrowBandMean += fPixel*fLengthElementParallelCurve;
				fOuterNarrowBandArea += fLengthElementParallelCurve;
			}
		}
	}

	fcolInnerNarrowBandMean /= fInnerNarrowBandArea;
	fcolOuterNarrowBandMean /= fOuterNarrowBandArea;
}

CCouple<float> CActiveContourColor::RegionForce(const CVertex2D *pVertexPrev, const CVertex2D *pVertex, const CVertex2D *pVertexNext) const
{
	// Region terms
	float fDiffRegion, fDiffRegionInside, fDiffRegionOutside;

	// Band thickness stored as a real number (make the conversion once)
	float fBandThickness = (float)iBandThickness;

	// Color at current vertex
	CTriplet<float> fPixel;

	// Inner and outer color region descriptors
	CTriplet<float> fcolInnerMeanCurrent(0.0f, 0.0f, 0.0f), fcolOuterMeanCurrent(0.0f, 0.0f, 0.0f);

	// For computing the bias force
	float fCoefBias, fForceBias, fDiffInsideOutside;

	fPixel = arrayImage.GetElementInterpolate(pVertex->GetPos());

	// Choose appropriate inner region descriptor depending on iInsideMode
	if (iInsideMode==INSIDEMODE_REGION)
		fcolInnerMeanCurrent = fcolInnerMean;
	else if (iInsideMode==INSIDEMODE_REGIONINIT)
		fcolInnerMeanCurrent = fcolInnerInitialMean;
	else if (iInsideMode==INSIDEMODE_BAND)
		fcolInnerMeanCurrent = fcolInnerNarrowBandMean;

	// Choose appropriate outer region descriptor depending on iOutsideMode
	if (iOutsideMode==OUTSIDEMODE_REGION)
		fcolOuterMeanCurrent = fcolOuterMean;
	else if (iOutsideMode==OUTSIDEMODE_BAND)
		fcolOuterMeanCurrent = fcolOuterNarrowBandMean;
	else if (iOutsideMode==OUTSIDEMODE_LINE)
	{
		float fThickness;
		float fLengthElement, fWeightCurvature, fCurvature, fSumWeights;
		CCouple<float> vfDerivative1, vfDerivative2;

		// Velocity vector at current vertex: centered finite difference approximation
		// of curve first-order derivative
		// Could also use backward finite difference : itVertex->GetPos() - itVertexPrev->GetPos() ?
		vfDerivative1 = (pVertexNext->GetPos()-pVertexPrev->GetPos())*0.5f;

		// Finite difference approximation of curve second-order derivative
		vfDerivative2 = pVertexPrev->GetPos() + pVertexNext->GetPos() - 2.0f*pVertex->GetPos();

		// Discrete length element, denoted \ell_i in [Mille09] (continuous definition is in equation (9) )
		fLengthElement = vfDerivative1.L2Norm();

		// Discrete curvature, denoted \kappa_i in [Mille09] (continuous definition is given shortly after length element)
		fCurvature = (vfDerivative1.x * vfDerivative2.y - vfDerivative1.y * vfDerivative2.x)/(fLengthElement*fLengthElement*fLengthElement);

		// Average weighted color along the outward normal line segment of length equal to band thickness
		// Implements the computation of \mu_NL appearing just after equation (54) in [Mille09]
		fcolOuterMeanCurrent.Set(0.0f, 0.0f, 0.0f);
		fSumWeights = 0.0f;
		for (fThickness=1.0f; fThickness<=fBandThickness; fThickness+=1.0f)
		{
			fWeightCurvature = 1.0f + fThickness*fCurvature; // Implements (1+b\kappa_i)

			// The following test is related to regularity condition (12) in [Mille09]
			// If it is false, the thickness exceeds the radius of curvature, yielding a singularity on the
			// outer parallel curve. In this case, it seems reasonable not to include the resulting pixel
			if (fWeightCurvature>0.0f)
			{
				fcolOuterMeanCurrent += arrayImage.GetElementInterpolate(pVertex->GetPos() - fThickness*pVertex->GetNormal())*fWeightCurvature;
				fSumWeights += fWeightCurvature;
			}
		}

		if (fSumWeights==0.0f)
		{
			// This happens if the curve is excessively concave at the current vertex
			// Return a null force, so that only regularization is effective on this particular vertex
			return CCouple<float>(0.0f, 0.0f);
		}
		else
			fcolOuterMeanCurrent /= fSumWeights;
	}

	// Unlike in [Mille09], deviations with respect to inner and outer descriptors
	// are computed using the L2 norm (not the squared one)
	// This yields better behaviour with respect to local color differences
	fDiffRegionInside = (fcolInnerMeanCurrent-fPixel).L2Norm();
	fDiffRegionOutside = (fcolOuterMeanCurrent-fPixel).L2Norm();

	// The narrow band region energies introduced in [Mille09] are symmetric, i.e. inner and outer region terms
	// are identically weighted However, an asymmetric configuration is allowed here, in order to favor
	// minimization of color deviation inside or outside. This enables to implement the parametric equivalent
	// of the Chan-Vese model (which is asymmetric) for comparison purpose.
	// The initial configuration is symmetric, since the default value of fWeightRegionInOverOut is 0.5
	fDiffRegion = fWeightRegionInOverOut * fDiffRegionInside - (1.0f-fWeightRegionInOverOut) * fDiffRegionOutside;

	// If bias is enabled, a balloon-like force is added to the region force
	if (bRegionEnergyBias==true)
	{
		// This is slightly different from the bias force described in section 5.5 in [Mille09],
		// as the magnitude of the bias is set by the balloon weight (which should be negative)
		fForceBias = fWeightBalloon+(fcolInnerMeanCurrent-fPixel).L2Norm();

		// Compute bias coefficient, denoted \gamma and introduced in equation (56) in [Mille09]
		// It decreases exponentially with respect to the difference between inner and outer region descriptors
		fDiffInsideOutside = (fcolInnerMeanCurrent-fcolOuterMeanCurrent).L2Norm();
		fCoefBias = (1.0f-fDiffInsideOutside)/(1.0f+50.0f*fDiffInsideOutside);

		// Linear combination of bias and region forces
		// See second part of equation (56)
		fDiffRegion = fCoefBias*fForceBias + (1.0f-fCoefBias)*fDiffRegion;
	}

	return pVertex->GetNormal()*fDiffRegion;
}

void CActiveContourColor::UpdateAfterInitialization()
{
	if (fWeightRegion!=0.0f)
	{
		if (iInsideMode==INSIDEMODE_REGION || iInsideMode==INSIDEMODE_REGIONINIT ||iOutsideMode==OUTSIDEMODE_REGION)
		{
			ComputeRegionMeansGreenTheorem();
			if (iInsideMode==INSIDEMODE_REGIONINIT)
				fcolInnerInitialMean = fcolInnerMean;
		}

		if (iInsideMode==INSIDEMODE_BAND || iOutsideMode==OUTSIDEMODE_BAND)
			ComputeBandMeans();
	}
}
