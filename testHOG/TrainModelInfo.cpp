#include "TrainModelInfo.h"
#include "opencv2/imgproc/imgproc.hpp"

template<typename T> std::vector<cv::Point_<T> > convertVector(
	const Vertices2DList& list);


bool /*CommonFunctions::*/convertImage(const cv::Mat& srcImage,
	cv::Mat& cvtImage, bool toGrayScale)
{
	cv::Mat tmp;

	assert(srcImage.channels() == 1 || srcImage.channels() == 3);

	if (toGrayScale && srcImage.channels() == 3)
	{
		cv::cvtColor(srcImage, tmp, CV_BGR2GRAY);
	}
	else
	{
		tmp = srcImage;
	}

	cvtImage = tmp;

	/*switch (tmp.type())
	{
	case CV_8UC1:
	cvtImage = RealMatrix(tmp) / 255.0;
	break;
	case CV_8UC3:
	cvtImage = ColorMatrix(tmp) / 255.0;
	break;
	case CV_8UC4:
	break;
	case CV_16UC1:
	cvtImage = RealMatrix(tmp) / 16535.0;
	break;
	case CV_16UC3:
	cvtImage = ColorMatrix(tmp) / 16535.0;
	break;
	case CV_32FC1:
	case CV_64FC1:
	cvtImage = RealMatrix(tmp);
	break;
	case CV_32FC3:
	case CV_64FC3:
	cvtImage = ColorMatrix(tmp);
	break;
	default:
	return false;
	}*/


	return true;
}

TrainModelInfo::TrainModelInfo()
{
}

TrainModelInfo::~TrainModelInfo()
{
}

void TrainModelInfo::setVertices(const Vertices2DList& v)
{
	this->vertices = v;

	std::vector<int> c;
	if (!v.empty())
	{
		cv::convexHull(convertVector<float>(v), c);
	}

	//this->lines.clear();

	//if (c.size() > 1)
	//{
	//	this->lines = c;
	//}
	/*
	if (this->vertices.empty())
	{
	this->triangles.clear();
	}
	else
	{
	CommonFunctions::delaunay(v, this->triangles);
	}*/
}

//std::vector<LineType>& TrainModelInfo::getLines()
//{
//	return this->lines;
//}

Vertices2DList& TrainModelInfo::getVertices()
{
	return this->vertices;
}

void TrainModelInfo::setImage(const cv::Mat& im, bool setGrayScale)
{
	if (!/*CommonFunctions::*/convertImage(im, this->image, setGrayScale))
	{
		throw /*InvalidImageFormat()*/;
	}
}

cv::Mat TrainModelInfo::getImage()
{
	return this->image;
}

const cv::Mat TrainModelInfo::getImage() const
{
	return this->image;
}

//void TrainModelInfo::setTForm(const TForm& tf)
//{
//	this->tform = tf;
//}

//TForm& TrainModelInfo::getTForm()
//{
//	return this->tform;
//}

//const TForm& TrainModelInfo::getTForm() const
//{
//	return this->tform;
//}

void TrainModelInfo::setCVertices(const Vertices2DList& v)
{
	this->cVertices = v;
}

Vertices2DList& TrainModelInfo::getCVertices()
{
	return this->cVertices;
}

const Vertices2DList& TrainModelInfo::getCVertices() const
{
	return this->cVertices;
}

const Vertices2DList& TrainModelInfo::getVertices() const
{
	return this->vertices;
}

const std::vector<cv::Vec3i> TrainModelInfo::getTriangles() const
{
	return this->triangles;
}

