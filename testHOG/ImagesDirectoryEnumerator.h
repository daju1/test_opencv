#include "DirectoryEnumerator.h"


#include <iostream>
#include <cstdio>
#include <fstream>
#include <vector>
#include <map>


class ImagesDirectoryEnumerator 
   : public DirectoryEnumerator

{
   UINT m_files_number;

public:
	ImagesDirectoryEnumerator(const char* fn, const char* ext)
      : DirectoryEnumerator(fn, ext)
      , m_files_number(0)
   {
   }

   virtual bool IsStop()
   {
      return false;
   }


   virtual void called_foo(char * fn_in, LARGE_INTEGER filesize)
   {
      ++m_files_number;
   }

   UINT files_number(){return m_files_number;}
};



