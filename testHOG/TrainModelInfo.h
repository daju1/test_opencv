#include <opencv/cv.h>
#include <vector>
#include <string>
typedef std::vector<cv::Point2d> Vertices2DList;
typedef std::pair<std::string, std::string> ModelPathType;

struct PointInfo
{
	unsigned int pointNumber;
	cv::Point2d point;
};

class TrainModelInfo
{
public:
	TrainModelInfo();
	virtual ~TrainModelInfo();

	void setVertices(const Vertices2DList& v);
	Vertices2DList& getVertices();
	cv::Mat getImage();
	const cv::Mat getImage() const;
	void setImage(const cv::Mat& im, bool setGrayScale = false);
	void setCVertices(const Vertices2DList& v);
	Vertices2DList& getCVertices();
	const Vertices2DList& getCVertices() const;
	const Vertices2DList& getVertices() const;
	const std::vector<cv::Vec3i> getTriangles() const;

private:
	Vertices2DList vertices;
	cv::Mat image;
	Vertices2DList cVertices;
	std::vector<cv::Vec3i> triangles;
};
