#include <opencv/cv.h>
#include <opencv/highgui.h>
#include "opencv2/imgproc/imgproc.hpp"
#include "opencv2/highgui/highgui.hpp"
#if !HAVE_OPENCV_300
#include "opencv2/legacy/legacy.hpp"
#else
#include "opencv2/objdetect/objdetect.hpp"
#endif

#include "mycanny.hpp"
#include "cvplot.h"

#include "TrainModelLoader.h"
#include "activecontour.h"
#include "binaryregion.h"

#include <fstream>
#include <iostream>
#include <stdio.h>



using namespace cv;
using namespace std;


// http://www.pressinganswer.com/1574036/how-to-find-corner-coordinates-of-image-contour-in-opencv-and-c



//int level = 50;
//cvCaptureFromCAM
int smooth_size1 = 1;
int _threshold = 125;


const std::string markupFile = "D:/cpplibs/aam/AAMToolboxWork/AAMToolbox/data/frame_glasses/1.dat";
const std::string modelInfoListFile() { return "D:/cpplibs/aam/AAMToolboxWork/AAMToolbox/data/frame_glasses_list.txt"; }


// ��������� �������� �� �������� �� �������� 
void testMatch(IplImage* original, IplImage* templ);
void testMatch(IplImage* src, IplImage* templ, IplImage* binI, IplImage* binT);


Vertices2DList loadMarkup(const std::string& markupFile)
{
	Vertices2DList result;
	std::ifstream stream(markupFile.c_str());

	if (!stream)
	{
//		return;
	}

	while (!stream.eof())
	{
		std::string line;
		std::getline(stream, line);

		if (!line.empty())
		{
			std::istringstream stringStream(line);
			PointInfo info;

			stringStream >> info.pointNumber >> info.point.x >>
				info.point.y;
			result.push_back(info.point);
		}
	}

	return result;
}

Vertices2DList getFramePart(Vertices2DList& list_all)
{
	Vertices2DList list_frame;

	list_frame.push_back(list_all[25]);

	for (int i = 52; i <= 75; ++i)
	{
		list_frame.push_back(list_all[i]);
	}

	list_frame.push_back(list_all[51]);

	for (int i = 76; i <= 101; ++i)
	{
		list_frame.push_back(list_all[i]);
	}

	return list_frame;
}


Vertices2DList getLeftLenzPart(Vertices2DList& list_all)
{
	Vertices2DList list_left_lenz;

	for (int i = 0; i <= 24; ++i)
	{
		list_left_lenz.push_back(list_all[i]);
	}

	return list_left_lenz;
}


Vertices2DList getRightLenzPart(Vertices2DList& list_all)
{
	Vertices2DList list_right_lenz;

	for (int i = 26; i <= 50; ++i)
	{
		list_right_lenz.push_back(list_all[i]);
	}

	return list_right_lenz;
}




template<typename T> std::vector<cv::Point_<T> > convertVector(
	const Vertices2DList& list)
{
	std::vector<cv::Point_<T> > result(list.size());

	for (size_t i = 0; i < list.size(); i++)
	{
		result[i].x = (T)list[i].x;
		result[i].y = (T)list[i].y;
	}

	return result;
}



bool LoadInputImage(const char* fn, CImage2D& imgInput)
{
	// Load the image file
	if (imgInput.Load(fn) == false)
	{
		cerr << "ERROR: unable to open input image." << endl;
		return false;
	}

	// Check if image is stored in RGB format and contains only grayscale values
	// If this is the case, convert it to 8-bit grayscale format
	if (imgInput.IsGrayScaleRGB() == true)
	{
		cout << "Image has only grayscale values but is actually stored in RGB format. Converting it to 8-bit format..." << endl;
		imgInput = imgInput.GrayScale();
	}

	return true;
}

CDeformableModelBase * CreateDeformableModel(ModelType iModelType, const CImage2D& imgInput, CImage2D& imgOutput, int iZoom)
{
	CDeformableModelBase* pDeformableModel;

	// Allocate the deformable model to a grayscale or color instance depending on image format
	if (imgInput.GetBitsPerPixel() == 8)
	{
		cout << "Grayscale model" << endl;

		// Allocate the deformable model to CActiveContourGrayscale or CBinaryRegionGrayscale
		// depending on model type
		if (iModelType == MODELTYPE_PARAMETRIC)
			pDeformableModel = new CActiveContourGrayscale();
		else
			pDeformableModel = new CBinaryRegionGrayscale();

		// Convert input image to 24-bit RGB format for output
		imgInput.Convert8bitsto24bits(imgOutput);

		// Rescale output image if needed
		if (iZoom>1)
			imgOutput = imgOutput.ResizeRGB(imgOutput.GetWidth()*iZoom, imgOutput.GetHeight()*iZoom);
	}
	else if (imgInput.GetBitsPerPixel() == 24 || imgInput.GetBitsPerPixel() == 32)
	{
		cout << "Color model" << endl;

		// Allocate the deformable model to CActiveContourColor or CBinaryRegionColor
		// depending on model type
		if (iModelType == MODELTYPE_PARAMETRIC)
		{
			CActiveContourColor *pActiveContourColor;

			pDeformableModel = new CActiveContourColor();
			pActiveContourColor = (CActiveContourColor *)pDeformableModel;

			// Set parameters specific to the color binary region model
			pActiveContourColor->iColorSpace = CActiveContourColor::COLORSPACE_YUV; // or COLORSPACE_RGB, COLORSPACE_LAB
			pActiveContourColor->bIgnoreBrightnessComponent = true;
		}
		else {
			CBinaryRegionColor *pBinaryRegionColor;

			pDeformableModel = new CBinaryRegionColor();
			pBinaryRegionColor = (CBinaryRegionColor *)pDeformableModel;

			// Set parameters specific to the color active contour model
			pBinaryRegionColor->iColorSpace = CBinaryRegionColor::COLORSPACE_YUV; // or COLORSPACE_RGB, COLORSPACE_LAB
			pBinaryRegionColor->bIgnoreBrightnessComponent = true;
		}

		// Copy the input image to the output image (with rescaling if needed)
		if (iZoom>1)
			imgOutput = imgInput.ResizeRGB(imgInput.GetWidth()*iZoom, imgInput.GetHeight()*iZoom);
		else
			imgOutput = imgInput;
	}
	else {
		cerr << "ERROR: number of bits/pixel is not supported." << endl;
		return NULL;
	}

	return pDeformableModel;
}

int MainLoop(CDeformableModelBase* pDeformableModel, int iZoom, CImage2D& imgOutput, const char* dir)
{
	CImage2D imgContour, imgMask;
	char strFilename[1024];
	bool bIsEvolving;
	int iIteration, 
		iNbIterationsMax = 500,
		iNbPassesPerIteration = 50;

	// Main loop
	bIsEvolving = true;
	bool bSaveAllIterations = true;
	for (iIteration = 0; iIteration<iNbIterationsMax && bIsEvolving == true; iIteration++)
	{
		cout << "Iteration " << iIteration << endl;

		if (bSaveAllIterations == true)
		{
			// Draw the model in output image at current iteration
			imgContour = imgOutput;
			pDeformableModel->DrawInImageRGB(imgContour, iZoom);

			// Write the output image to BMP file
			sprintf(strFilename, "%s/output_%04d.bmp", dir, iIteration);
			if (imgContour.Save(strFilename, CImage2D::FORMAT_BMP) == false)
			{
				cerr << "ERROR: unable to write to image file " << strFilename << endl;
				return -1;
			}
		}

		// Evolve the model with a few passes of gradient descent
		bIsEvolving = pDeformableModel->EnergyGradientDescent(iNbPassesPerIteration);
	}

	if (bIsEvolving == false)
		cout << endl << "Stopping criterion is met: deformable model has reached stability" << endl;
	else if (iIteration >= iNbIterationsMax)
		cout << endl << "Maximum number of iterations is exceeded, but deformable model has not reached stability" << endl;

	// Draw final model in output image
	imgContour = imgOutput;
	pDeformableModel->DrawInImageRGB(imgContour, iZoom);

	// Write final output image to BMP file
	sprintf(strFilename, "%s/output_final.bmp", dir);
	if (imgContour.Save(strFilename, CImage2D::FORMAT_BMP) == false)
	{
		cerr << "ERROR: unable to write to image file " << strFilename << endl;
		return -1;
	}

	// Make binary mask of final segmentation
	pDeformableModel->MakeBinaryMask(imgMask);

	// Write the mask to BMP file
	sprintf(strFilename, "%s/mask_final.bmp", dir);
	if (imgMask.Save(strFilename, CImage2D::FORMAT_BMP) == false)
	{
		cerr << "ERROR: unable to write to image file " << strFilename << endl;
		return -1;
	}

	return 0;
}

int active_contour_main(const char * fn, const char * dir_out, float fCircleCenterX, float fCircleCenterY, float fCircleRadius, ModelType iModelType, CDeformableModelBase::InsideMode iInsideMode, CDeformableModelBase::OutsideMode iOutsideMode)
{
	CBinaryRegionBase::InitNeighborhood();
	CImage2D::StartImage2D();

	CImage2D imgInput, imgOutput;
	if (!LoadInputImage(fn, imgInput))
		return -1;

	// Choose either to use parametric implementation or the implicit one
	//ModelType iModelType = MODELTYPE_PARAMETRIC;
	//ModelType iModelType = MODELTYPE_IMPLICIT;

	// Set zooming factor for output images
	int iZoom = 1;

	CDeformableModelBase* pDeformableModel = CreateDeformableModel(iModelType, imgInput, imgOutput, iZoom);


	// Set energy configuration
	pDeformableModel->iInsideMode = CDeformableModelBase::INSIDEMODE_REGION; // Other possible values are INSIDEMODE_BAND
	pDeformableModel->iOutsideMode = CDeformableModelBase::OUTSIDEMODE_LINE; // Other possible values are OUTSIDEMODE_REGION, OUTSIDEMODE_BAND

	pDeformableModel->iInsideMode = iInsideMode;
	pDeformableModel->iOutsideMode = iOutsideMode; 

	// To work properly, the second narrow band region energy (the local one)
	// related to equations (6) and (19) in [Mille09] often requires the bias to be enabled
	if (pDeformableModel->iOutsideMode == CDeformableModelBase::OUTSIDEMODE_LINE)
	{
		pDeformableModel->bRegionEnergyBias = true;
		pDeformableModel->fWeightBalloon = -0.3f;
		pDeformableModel->fWeightBalloon = -0.45f;
	}

	// Other parameters may be modified here, before attaching the model to the image
	// ...

	//pDeformableModel->fWeightRegionInOverOut = 0.4;
	pDeformableModel->iBandThickness = 20;

	// Attach model to image data
	pDeformableModel->AttachImage(&imgInput);


	CCouple<float> pfCircleCenter;

	// Initial location for 'brain'
	pfCircleCenter.Set(fCircleCenterX, fCircleCenterY);

	// Initialize the region as a circle with given center and radius
	pDeformableModel->InitCircle(pfCircleCenter, fCircleRadius);

	// Main loop
	MainLoop(pDeformableModel, iZoom, imgOutput, dir_out);

	// Destroy model
	delete pDeformableModel;

	return 0;
}


int active_contour_main(const char * fn, const char * dir_out, Vertices2DList list, ModelType iModelType, CDeformableModelBase::InsideMode iInsideMode, CDeformableModelBase::OutsideMode iOutsideMode)
{
	CBinaryRegionBase::InitNeighborhood();
	CImage2D::StartImage2D();

	CImage2D imgInput, imgOutput;
	if (!LoadInputImage(fn, imgInput))
		return -1;

	// Choose either to use parametric implementation or the implicit one
	///ModelType iModelType = MODELTYPE_PARAMETRIC;

	// Set zooming factor for output images
	int iZoom = 1;

	CDeformableModelBase* pDeformableModel = CreateDeformableModel(iModelType, imgInput, imgOutput, iZoom);


	// Set energy configuration
	pDeformableModel->iInsideMode = CDeformableModelBase::INSIDEMODE_REGION; // Other possible values are INSIDEMODE_BAND
	pDeformableModel->iOutsideMode = CDeformableModelBase::OUTSIDEMODE_LINE; // Other possible values are OUTSIDEMODE_REGION, OUTSIDEMODE_BAND

	pDeformableModel->iInsideMode = iInsideMode;
	pDeformableModel->iOutsideMode = iOutsideMode;


	// To work properly, the second narrow band region energy (the local one)
	// related to equations (6) and (19) in [Mille09] often requires the bias to be enabled
	if (pDeformableModel->iOutsideMode == CDeformableModelBase::OUTSIDEMODE_LINE)
	{
		pDeformableModel->bRegionEnergyBias = true;
		pDeformableModel->fWeightBalloon = -0.3f;
	}

	// Other parameters may be modified here, before attaching the model to the image
	// ...

	pDeformableModel->fWeightInitial = 0.01f;
//	pDeformableModel->bResample = false;

	// Attach model to image data
	pDeformableModel->AttachImage(&imgInput);

	// Initialize the region as a circle with given center and radius
	pDeformableModel->InitialModel(list);
	pDeformableModel->InitModel(list);

	MainLoop(pDeformableModel, iZoom, imgOutput, dir_out);

	// Destroy model
	delete pDeformableModel;

	return 0;
}


void do_snake(cv::Mat& im_canny)
{
	IplImage *canny_image = &(IplImage)im_canny;
	/// Find contours
	CvSeq* contours = 0;
	CvMemStorage* storage = cvCreateMemStorage(0);

	cvFindContours(canny_image, storage, &contours, sizeof(CvContour),
		CV_RETR_TREE, CV_CHAIN_APPROX_SIMPLE, cv::Point(0, 0));

	{

		/// Draw contours

		IplImage* cnt_img = cvCreateImage(im_canny.size(), 8, 3);
		CvSeq* _contours = contours;
		int maxLevel = INT_MAX;

		cvZero(cnt_img);
		cvDrawContours(cnt_img, _contours, CV_RGB(255, 0, 0), CV_RGB(0, 255, 0), maxLevel, 3, CV_AA, cvPoint(0, 0));
		cvShowImage("contours1", cnt_img);
		cvReleaseImage(&cnt_img);

	}

	if (!contours)
		return;
	int length = contours->total;
	if (length < 3)
		return;

	CvPoint* point = new CvPoint[length];

	CvSeqReader reader;
	CvPoint pt = cvPoint(0, 0);;
	CvSeq *contour2 = contours;

	cvStartReadSeq(contour2, &reader);
	for (int i = 0; i < length; i++)
	{
		CV_READ_SEQ_ELEM(pt, reader);
		point[i] = pt;
	}
	cvReleaseMemStorage(&storage);


	float alpha = 0.1f; // Weight of continuity energy
	float beta = 0.5f; // Weight of curvature energy
	float gamma = 0.4f; // Weight of image energy

#if !HAVE_OPENCV_300
	CvSize size;
	size.width = 3;
	size.height = 3;
	CvTermCriteria criteria;
	criteria.type = CV_TERMCRIT_ITER;
	criteria.max_iter = 1000;
	criteria.epsilon = 0.1;
	cvSnakeImage(canny_image, point, length, &alpha, &beta, &gamma, CV_VALUE, size, criteria, 0);
#endif

	cv::Mat drawing = cv::Mat::zeros(im_canny.size(), CV_8UC3);

	for (int i = 0; i<length; i++)
	{
		int j = (i + 1) % length;
		cv::line(drawing, point[i], point[j], CV_RGB(0, 255, 0), 1, 8, 0);
	}
	cv::imshow("do_snake", drawing);
	delete[]point;
}

bool getCentralPoint(const cv::Mat& im, const cv::Rect faceRect, cv::Point2d& cp)
{
	cv::Mat face_im = im(faceRect);
	const char * opencv_sources = getenv("OPENCV_SOURCES");
	if (!opencv_sources)
	{
		printf("Set eviroment variable OPENCV_SOURCES\n");
		return -1;
	}

	String directory = String(opencv_sources) + "/data/haarcascades/";
	cv::CascadeClassifier leye_cascade(directory + "haarcascade_mcs_lefteye.xml");
	cv::CascadeClassifier reye_cascade(directory + "haarcascade_mcs_righteye.xml");
	if (leye_cascade.empty())
	{
		std::cout << "%TEST_FAILED% time=0 testname=testAplyGray (aplymodelTest) message=Can't load leye_cascade haar cascade" << std::endl;
		return false;
	}
	if (reye_cascade.empty())
	{
		std::cout << "%TEST_FAILED% time=0 testname=testAplyGray (aplymodelTest) message=Can't load reye_cascade haar cascade" << std::endl;
		return false;
	}

	std::vector<cv::Rect> leyes;
	leye_cascade.detectMultiScale(face_im, leyes,
		1.1, 2, 0
		| CV_HAAR_FIND_BIGGEST_OBJECT
		//|CV_HAAR_DO_ROUGH_SEARCH
		| CV_HAAR_SCALE_IMAGE
		,
		cv::Size(15, 15));

	std::vector<cv::Rect> reyes;
	reye_cascade.detectMultiScale(face_im, reyes,
		1.1, 2, 0
		| CV_HAAR_FIND_BIGGEST_OBJECT
		//|CV_HAAR_DO_ROUGH_SEARCH
		| CV_HAAR_SCALE_IMAGE
		,
		cv::Size(15, 15));

	if (leyes.empty())
	{
		std::cout << "%TEST_FAILED% time=0 testname=testAplyGray (aplymodelTest) message=No leyes on test picture" << std::endl;
		return false;
	}
	if (reyes.empty())
	{
		std::cout << "%TEST_FAILED% time=0 testname=testAplyGray (aplymodelTest) message=No faces on test picture" << std::endl;
		return false;
	}
	if (leyes.size() > 1)
	{
		return false;
	}
	if (reyes.size() > 1)
	{
		return false;
	}

	cv::Rect leyeRect = leyes[0];
	cv::Rect reyeRect = reyes[0];

	cv::Point2d cp_leye;
	cp_leye.x = cvRound((faceRect.x + leyeRect.x + leyeRect.width * 0.5));
	cp_leye.y = cvRound((faceRect.y + leyeRect.y + leyeRect.height * 0.5));

	cv::Point2d cp_reye;
	cp_reye.x = cvRound((faceRect.x + reyeRect.x + reyeRect.width * 0.5));
	cp_reye.y = cvRound((faceRect.y + reyeRect.y + reyeRect.height * 0.5));

	bool rc = false;
	cp.x = faceRect.x + faceRect.width * 0.5;
	if (cp_leye.x < cp.x && cp_reye.x > cp.x){
		cp.x = (cp_leye.x + cp_reye.x) / 2;
		rc = true;
	}
	cp.y = (cp_leye.y + cp_reye.y) / 2;

#if 0
	cv::rectangle(im, cv::Rect(cv::Point(faceRect.x + leyeRect.x, faceRect.y + leyeRect.y), cv::Size(leyeRect.width, leyeRect.height)), CV_RGB(255, 255, 255));
	cv::rectangle(im, cv::Rect(cv::Point(faceRect.x + reyeRect.x, faceRect.y + reyeRect.y), cv::Size(reyeRect.width, reyeRect.height)), CV_RGB(255, 255, 255));
	cv::rectangle(im, cv::Rect(cv::Point(faceRect.x, faceRect.y), cv::Size(faceRect.width, faceRect.height)), CV_RGB(255, 255, 255));

	cv::circle(im, cp_leye, 3, CV_RGB(255, 255, 255));
	cv::circle(im, cp_reye, 3, CV_RGB(255, 255, 255));
	cv::circle(im, cp, 3, CV_RGB(255, 255, 255));
	//cv::imshow("tmp", im);
	//cv::waitKey(0);
#endif

	return rc;
}
bool getCentralPoint(const cv::Mat& face_im, cv::Point2d& cp, cv::Rect& leyeRect, cv::Rect& reyeRect, cv::Point2d& cp_leye, cv::Point2d& cp_reye)
{
	const char * opencv_sources = getenv("OPENCV_SOURCES");
	if (!opencv_sources)
	{
		printf("Set eviroment variable OPENCV_SOURCES\n");
		return -1;
	}

	String directory = String(opencv_sources) + "/data/haarcascades/";
	cv::CascadeClassifier leye_cascade(directory + "haarcascade_mcs_lefteye.xml");
	cv::CascadeClassifier reye_cascade(directory + "haarcascade_mcs_righteye.xml");
	if (leye_cascade.empty())
	{
		std::cout << "%TEST_FAILED% time=0 testname=testAplyGray (aplymodelTest) message=Can't load leye_cascade haar cascade" << std::endl;
		return false;
	}
	if (reye_cascade.empty())
	{
		std::cout << "%TEST_FAILED% time=0 testname=testAplyGray (aplymodelTest) message=Can't load reye_cascade haar cascade" << std::endl;
		return false;
	}

	std::vector<cv::Rect> leyes;
	leye_cascade.detectMultiScale(face_im, leyes,
		1.1, 2, 0
		| CV_HAAR_FIND_BIGGEST_OBJECT
		//|CV_HAAR_DO_ROUGH_SEARCH
		| CV_HAAR_SCALE_IMAGE
		,
		cv::Size(15, 15));

	std::vector<cv::Rect> reyes;
	reye_cascade.detectMultiScale(face_im, reyes,
		1.1, 2, 0
		| CV_HAAR_FIND_BIGGEST_OBJECT
		//|CV_HAAR_DO_ROUGH_SEARCH
		| CV_HAAR_SCALE_IMAGE
		,
		cv::Size(15, 15));

	if (leyes.empty())
	{
		std::cout << "%TEST_FAILED% time=0 testname=testAplyGray (aplymodelTest) message=No leyes on test picture" << std::endl;
		return false;
	}
	if (reyes.empty())
	{
		std::cout << "%TEST_FAILED% time=0 testname=testAplyGray (aplymodelTest) message=No faces on test picture" << std::endl;
		return false;
	}
	if (leyes.size() > 1)
	{
		return false;
	}
	if (reyes.size() > 1)
	{
		return false;
	}



	leyeRect = leyes[0];
	reyeRect = reyes[0];
	
	cp_leye.x = cvRound((/*faceRect.x +*/ leyeRect.x + leyeRect.width * 0.5));
	cp_leye.y = cvRound((/*faceRect.y +*/ leyeRect.y + leyeRect.height * 0.5));

	cp_reye.x = cvRound((/*faceRect.x +*/ reyeRect.x + reyeRect.width * 0.5));
	cp_reye.y = cvRound((/*faceRect.y +*/ reyeRect.y + reyeRect.height * 0.5));

	bool rc = false;
	cp.x = /*faceRect.x +*/ face_im.size().width * 0.5;
	if (cp_leye.x < cp.x && cp_reye.x > cp.x)
	{
		cp.x = (cp_leye.x + cp_reye.x) / 2;
		rc = true;
	}
	cp.y = (cp_leye.y + cp_reye.y) / 2;

#if 0
	cv::rectangle(im, cv::Rect(cv::Point(faceRect.x + leyeRect.x, faceRect.y + leyeRect.y), cv::Size(leyeRect.width, leyeRect.height)), CV_RGB(255, 255, 255));
	cv::rectangle(im, cv::Rect(cv::Point(faceRect.x + reyeRect.x, faceRect.y + reyeRect.y), cv::Size(reyeRect.width, reyeRect.height)), CV_RGB(255, 255, 255));
	cv::rectangle(im, cv::Rect(cv::Point(faceRect.x, faceRect.y), cv::Size(faceRect.width, faceRect.height)), CV_RGB(255, 255, 255));

	cv::circle(im, cp_leye, 3, CV_RGB(255, 255, 255));
	cv::circle(im, cp_reye, 3, CV_RGB(255, 255, 255));
	cv::circle(im, cp, 3, CV_RGB(255, 255, 255));
	//cv::imshow("tmp", im);
	//cv::waitKey(0);
#endif

	return rc;
}


CvPoint* produceCircle(const float pfCenterX, const float pfCenterY, const float fRadius, int& iNbVertices)
{
	CCouple<float> ptMin, ptMax, pfNew;
	static const float _2pi = 6.28319f;
	float fAngle;
	float fSampling = 2.0f;

	// Number of vertices depends on sampling and circle perimeter
	iNbVertices = (int)((_2pi*fRadius) / fSampling);
	if (iNbVertices<3)
	{
		cerr << "ERROR in CActiveContourBase::InitCircle(...): cannot initialize the contour: circle is too small to yield enough vertices with current sampling" << endl;
		return NULL;
	}

	CvPoint* point = new CvPoint[iNbVertices];

	for (int i = 0; i < iNbVertices; i++)
	{
		fAngle = _2pi*(float)i / (float)iNbVertices;
		point[i].x = pfCenterX + fRadius*cos(fAngle);
		point[i].y = pfCenterY + fRadius*sin(fAngle);
	}

	return point;
}
#if 0
void do_snake2(std::string& fn_in, std::string suffix, cv::Mat& img, cv::Mat& src_gray, ModelType iModelType)
{

	std::vector<ModelPathType> modelPaths;
	if (!loadModelInfo(modelInfoListFile(), modelPaths))
	{
		std::cout << "%TEST_FAILED% time=0 testname=testTrainGray (aamestimatorTest) message=Can' load model info list" << std::endl;
		return;
	}

	Vertices2DList list_all = loadMarkup(markupFile);

	Vertices2DList list_frame = getFramePart(list_all);
	Vertices2DList list_left_lenz = getLeftLenzPart(list_all);
	Vertices2DList list_right_lenz = getRightLenzPart(list_all);

	cv::Point2d cpFace;
	cpFace.x = img.size().width / 2;
	cpFace.y = 0.41*(img.size().height);



	float alpha = 0.1; // Weight of continuity energy
	float beta = 0.9; // Weight of curvature energy
	float gamma = 0.3; // Weight of image energy


	CvSize size;
	size.width = 3;
	size.height = 3;
	CvTermCriteria criteria;
	criteria.type = CV_TERMCRIT_ITER;
	criteria.max_iter = 1000;
	criteria.epsilon = 0.1;

	std::string directory = "D:\\_output_faces";
	CreateDirectory(directory.c_str(), NULL);

	int i1 = fn_in.find_last_of({ '\\','/' });
	int i2 = fn_in.find_last_of('.');
	directory += "\\" + fn_in.substr(i1, i2 - i1 - 1) + suffix;
	CreateDirectory(directory.c_str(), NULL);

	std::string fn = directory + "\\face.bmp";
	cv::imwrite(fn, img);


	cv::Point2d cp_eyes, cp_leye, cp_reye;
	cv::Rect leyeRect, reyeRect;
	float fCircleRadius;
	if (getCentralPoint(src_gray, cp_eyes, leyeRect, reyeRect, cp_leye, cp_reye))
	{
		fCircleRadius = leyeRect.height / 3;
	}
	else
	{
		fCircleRadius = 0.05 * (img.size().width);

		cp_leye.x = cpFace.x - 0.2 * (img.size().width);
		cp_leye.y = cpFace.y;

		cp_reye.x = cpFace.x + 0.15 * (img.size().width);
		cp_reye.y = cpFace.y;
	}


	std::string dir_out = directory + "\\left";
	CreateDirectory(dir_out.c_str(), NULL);
	active_contour_main(fn.c_str(), dir_out.c_str(), cp_leye.x, cp_leye.y, fCircleRadius, iModelType);

	{
		int iNbVertices;
		CvPoint* pts = produceCircle(cp_leye.x, cp_leye.y, fCircleRadius, iNbVertices);
		cvSnakeImage(&(IplImage)img, pts, iNbVertices, &alpha, &beta, &gamma, CV_VALUE, size, criteria, 0);

		cv::Mat drawing = cv::Mat::zeros(img.size(), CV_8UC3);
		cv::Mat gray = src_gray.clone();
		for (int i = 0; i<iNbVertices; i++)
		{
			int j = (i + 1) % iNbVertices;
			cv::line(drawing, pts[i], pts[j], CV_RGB(0, 255, 0), 1, 8, 0);
			cv::line(gray, pts[i], pts[j], CV_RGB(0, 255, 0), 1, 8, 0);
		}

		std::string f_out = dir_out + "\\cvSnakeImage_drawing.jpg";
		cv::imwrite(f_out, drawing);
		f_out = dir_out + "\\cvSnakeImage_gray.jpg";
		cv::imwrite(f_out, gray);
		delete[] pts;
	}
		
	dir_out = directory + "\\right";
	CreateDirectory(dir_out.c_str(), NULL);
	active_contour_main(fn.c_str(), dir_out.c_str(), cp_reye.x, cp_reye.y, fCircleRadius, iModelType);
	{
		int iNbVertices;
		CvPoint* pts = produceCircle(cp_reye.x, cp_reye.y, fCircleRadius, iNbVertices);
		cvSnakeImage(&(IplImage)img, pts, iNbVertices, &alpha, &beta, &gamma, CV_VALUE, size, criteria, 0);
		cv::Mat drawing = cv::Mat::zeros(img.size(), CV_8UC3);
		cv::Mat gray = src_gray.clone();
		for (int i = 0; i<iNbVertices; i++)
		{
			int j = (i + 1) % iNbVertices;
			cv::line(drawing, pts[i], pts[j], CV_RGB(0, 255, 0), 1, 8, 0);
			cv::line(gray, pts[i], pts[j], CV_RGB(0, 255, 0), 1, 8, 0);
		}
		std::string f_out = dir_out + "\\cvSnakeImage_drawing.jpg";
		cv::imwrite(f_out, drawing);
		f_out = dir_out + "\\cvSnakeImage_gray.jpg";
		cv::imwrite(f_out, gray);
		delete[] pts;
	}
	

	{
		cv::Rect shapeRect = cv::boundingRect(convertVector<float>(list_frame));
		cv::Point2d cpShape;
		cpShape.x = shapeRect.tl().x + shapeRect.width / 2;
		cpShape.y = shapeRect.tl().y + shapeRect.height / 2;


		int faceRect_width = img.size().width;

		double k_horz = 0.9 * double(faceRect_width) / shapeRect.width;
		double k_vert = 1.2 * double(faceRect_width) / shapeRect.width;

		int length = list_frame.size();



		CvPoint* point = new CvPoint[length];
		for (int i = 0; i < length; i++)
		{
			list_frame[i].x = cpFace.x + (list_frame[i].x - cpShape.x) * k_horz;
			list_frame[i].y = cpFace.y + (list_frame[i].y - cpShape.y) * k_vert;

			point[i] = list_frame[i];
		}

		//for (int i = 1; i < length; i++)
		//{
		//	cv::line(drawing, point[i], point[i - 1], CV_RGB(255, 0, 0), 1, 8, 0);
		//	cv::line(src_gray, point[i], point[i - 1], CV_RGB(255, 0, 0), 1, 8, 0);		
		//}


		dir_out = directory + "\\both";
		CreateDirectory(dir_out.c_str(), NULL);
		active_contour_main(fn.c_str(), dir_out.c_str(), list_frame, iModelType);

		{
			cvSnakeImage(&(IplImage)img, point, length, &alpha, &beta, &gamma, CV_VALUE, size, criteria, 0);
			cv::Mat drawing = cv::Mat::zeros(img.size(), CV_8UC3);
			cv::Mat gray = src_gray.clone();
			for (int i = 0; i < length; i++)
			{
				int j = (i + 1) % length;
				cv::line(drawing, point[i], point[j], CV_RGB(0, 255, 0), 1, 8, 0);
				cv::line(gray, point[i], point[j], CV_RGB(0, 255, 0), 1, 8, 0);
			}
			std::string f_out = dir_out + "\\cvSnakeImage_drawing.jpg";
			cv::imwrite(f_out, drawing);
			f_out = dir_out + "\\cvSnakeImage_gray.jpg";
			cv::imwrite(f_out, gray);
		}


		//cv::imshow("do_snake", drawing);
		//cv::imshow("do_snake2", gray);
		delete[]point;
	}
}
#endif
void Scale(Vertices2DList& list_frame, cv::Point2d cpFace, double k_horz, double k_vert)
{
	cv::Rect shapeRect = cv::boundingRect(convertVector<float>(list_frame));

	cv::Point2d cpShape;
	cpShape.x = shapeRect.tl().x + shapeRect.width / 2;
	cpShape.y = shapeRect.tl().y + shapeRect.height / 2;

	int length = list_frame.size();

	for (int i = 0; i < length; i++)
	{
		list_frame[i].x = cpFace.x + (list_frame[i].x - cpShape.x) * k_horz;
		list_frame[i].y = cpFace.y + (list_frame[i].y - cpShape.y) * k_vert;
	}
}



void do_snake3(std::string& fn_in, std::string suffix, cv::Mat& img, cv::Mat& src_gray, Vertices2DList& list_all, ModelType iModelType, CDeformableModelBase::InsideMode iInsideMode, CDeformableModelBase::OutsideMode iOutsideMode, bool bCirce)
{
	cv::Point2d cpFace;
	cpFace.x = img.size().width / 2;
	cpFace.y = 0.40*(img.size().height);


	std::string directory = "D:\\_output_faces";
	CreateDirectory(directory.c_str(), NULL);

	int i1 = fn_in.find_last_of({ '\\', '/' });
	int i2 = fn_in.find_last_of('.');
	directory += fn_in.substr(i1, i2 - i1 - 1) + suffix;
	CreateDirectory(directory.c_str(), NULL);

	std::string fn = directory + "\\face.bmp";
	cv::imwrite(fn, img);


	cv::Point2d cp_eyes, cp_leye, cp_reye;
	cv::Rect leyeRect, reyeRect;
	float fCircleRadius;
	if (getCentralPoint(src_gray, cp_eyes, leyeRect, reyeRect, cp_leye, cp_reye))
	{
		fCircleRadius = leyeRect.height / 3;
	}
	else
	{
		fCircleRadius = 0.05 * (img.size().width);

		cp_leye.x = cpFace.x - 0.2 * (img.size().width);
		cp_leye.y = cpFace.y;

		cp_reye.x = cpFace.x + 0.15 * (img.size().width);
		cp_reye.y = cpFace.y;
	}

	double k = 1.0;

	{
		Vertices2DList list_frame = getFramePart(list_all);
		cv::Rect shapeRect = cv::boundingRect(convertVector<float>(list_frame));

		int faceRect_width = img.size().width;

		k = double(faceRect_width) / shapeRect.width;
	}


	{
		Vertices2DList list_left_lenz = getLeftLenzPart(list_all);

		double k_horz = 0.8 * k;
		double k_vert = 0.8 * k;

		Scale(list_left_lenz, cp_leye, k_horz, k_vert);

		std::string dir_out = directory + "\\left";
		CreateDirectory(dir_out.c_str(), NULL);
		if (bCirce)
			active_contour_main(fn.c_str(), dir_out.c_str(), cp_leye.x, cp_leye.y, fCircleRadius, iModelType, iInsideMode, iOutsideMode);
		else
			active_contour_main(fn.c_str(), dir_out.c_str(), list_left_lenz, iModelType, iInsideMode, iOutsideMode);
	}


	{
		Vertices2DList list_right_lenz = getRightLenzPart(list_all);

		double k_horz = 0.8 * k;
		double k_vert = 0.8 * k;

		Scale(list_right_lenz, cp_reye, k_horz, k_vert);

		std::string dir_out = directory + "\\right";
		CreateDirectory(dir_out.c_str(), NULL);
		if (bCirce)
			active_contour_main(fn.c_str(), dir_out.c_str(), cp_reye.x, cp_reye.y, fCircleRadius, iModelType, iInsideMode, iOutsideMode);
		else
			active_contour_main(fn.c_str(), dir_out.c_str(), list_right_lenz, iModelType, iInsideMode, iOutsideMode);
	}


	{
		Vertices2DList list_frame = getFramePart(list_all);

		double k_horz = 0.9 * k;
		double k_vert = 0.9 * k;

		Scale(list_frame, cpFace, k_horz, k_vert);


		//for (int i = 1; i < length; i++)
		//{
		//	cv::line(drawing, point[i], point[i - 1], CV_RGB(255, 0, 0), 1, 8, 0);
		//	cv::line(src_gray, point[i], point[i - 1], CV_RGB(255, 0, 0), 1, 8, 0);		
		//}


		std::string dir_out = directory + "\\both";
		CreateDirectory(dir_out.c_str(), NULL);
		if (bCirce)
			active_contour_main(fn.c_str(), dir_out.c_str(), cpFace.x, cpFace.y, fCircleRadius, iModelType, iInsideMode, iOutsideMode);
		else
			active_contour_main(fn.c_str(), dir_out.c_str(), list_frame, iModelType, iInsideMode, iOutsideMode);

		//cv::imshow("do_snake", drawing);
		//cv::imshow("do_snake2", gray);
	}
}


void adaptiveThreshold_original(InputArray _src, OutputArray _dst, double maxValue,
	int method, int type, int blockSize, double delta)
{
	Mat src = _src.getMat();
	CV_Assert(src.type() == CV_8UC1);
	CV_Assert(blockSize % 2 == 1 && blockSize > 1);
	Size size = src.size();

	_dst.create(size, src.type());
	Mat dst = _dst.getMat();

	if (maxValue < 0)
	{
		dst = Scalar(0);
		return;
	}

	Mat mean;

	if (src.data != dst.data)
		mean = dst;

	if (method == ADAPTIVE_THRESH_MEAN_C)
		boxFilter(src, mean, src.type(), Size(blockSize, blockSize),
		Point(-1, -1), true, BORDER_REPLICATE);
	else if (method == ADAPTIVE_THRESH_GAUSSIAN_C)
		GaussianBlur(src, mean, Size(blockSize, blockSize), 0, 0, BORDER_REPLICATE);
	else
		CV_Error(CV_StsBadFlag, "Unknown/unsupported adaptive threshold method");

	int i, j;
	uchar imaxval = saturate_cast<uchar>(maxValue);
	int idelta = type == THRESH_BINARY ? cvCeil(delta) : cvFloor(delta);
	uchar tab[768];

	if (type == CV_THRESH_BINARY)
	for (i = 0; i < 768; i++)
		tab[i] = (uchar)(i - 255 > -idelta ? imaxval : 0);
	else if (type == CV_THRESH_BINARY_INV)
	for (i = 0; i < 768; i++)
		tab[i] = (uchar)(i - 255 <= -idelta ? imaxval : 0);
	else
		CV_Error(CV_StsBadFlag, "Unknown/unsupported threshold type");

	if (src.isContinuous() && mean.isContinuous() && dst.isContinuous())
	{
		size.width *= size.height;
		size.height = 1;
	}

	for (i = 0; i < size.height; i++)
	{
		const uchar* sdata = src.data + src.step*i;
		const uchar* mdata = mean.data + mean.step*i;
		uchar* ddata = dst.data + dst.step*i;

		for (j = 0; j < size.width; j++)
			ddata[j] = tab[sdata[j] - mdata[j] + 255];
	}
}

void myAdaptiveThreshold(InputArray _src, OutputArray _dst, double maxValue,
	int method, int type, int blockSize, const char * smean, const char * sdelta)
{
	Mat src = _src.getMat();
	CV_Assert(src.type() == CV_8UC1);
	CV_Assert(blockSize % 2 == 1 && blockSize > 1);
	Size size = src.size();

	_dst.create(size, src.type());
	Mat dst = _dst.getMat();

	if (maxValue < 0)
	{
		dst = Scalar(0);
		return;
	}

	Mat mean;

	if (src.data != dst.data)
		mean = dst;

	if (method == ADAPTIVE_THRESH_MEAN_C)
		boxFilter(src, mean, src.type(), Size(blockSize, blockSize),
		Point(-1, -1), true, BORDER_REPLICATE);
	else if (method == ADAPTIVE_THRESH_GAUSSIAN_C)
		GaussianBlur(src, mean, Size(blockSize, blockSize), 0, 0, BORDER_REPLICATE);
	else
		CV_Error(CV_StsBadFlag, "Unknown/unsupported adaptive threshold method");

	cv::imshow(smean, mean);

	if (src.isContinuous() && mean.isContinuous() && dst.isContinuous())
	{
		size.width *= size.height;
		size.height = 1;
	}	

	std::vector<int> hist(768, 0);

	for (int i = 0; i < size.height; i++)
	{
		const uchar* sdata = src.data + src.step*i;
		const uchar* mdata = mean.data + mean.step*i;

		for (int j = 0; j < size.width; j++){
			++hist[sdata[j] - mdata[j] + 255];
		}
	}

	//CvPlot::clear("hist");
	//CvPlot::plot("hist", &hist[0], 768, 1);
	//CvPlot::label("hist");

	int max_hist = 0;
	int index_max_hist = -1;
	for (int i = 0; i < 768; ++i)
	{
		if (max_hist < hist[i])
		{
			max_hist = hist[i];
			index_max_hist = i;
		}
	}

	int idelta = index_max_hist - 255 + 2;
	printf("index_max_hist=%d idelta=%d\n", index_max_hist, idelta);

	uchar imaxval = saturate_cast<uchar>(maxValue);
	//int idelta = type == THRESH_BINARY ? cvCeil(delta) : cvFloor(delta);
	uchar tab[768];

	if (type == CV_THRESH_BINARY)
	for (int i = 0; i < 768; i++)
		tab[i] = (uchar)(i - 255 > -idelta ? imaxval : 0);
	else if (type == CV_THRESH_BINARY_INV)
	for (int i = 0; i < 768; i++)
		tab[i] = (uchar)(i - 255 <= -idelta ? imaxval : 0);
	else
		CV_Error(CV_StsBadFlag, "Unknown/unsupported threshold type");

	//CvPlot::plot("tab", &tab[0], 768, 1);
	//CvPlot::label("tab");


	for (int i = 0; i < size.height; i++)
	{
		const uchar* sdata = src.data + src.step*i;
		const uchar* mdata = mean.data + mean.step*i;
		uchar* ddata = dst.data + dst.step*i;

		for (int j = 0; j < size.width; j++){
			ddata[j] = tab[sdata[j] - mdata[j] + 255];
		}
	}
}


void do_find_countours(std::string& fn_in, cv::Mat& src_gray, cv::RNG& rng, bool debug_plots, std::string& fn_out)
{
	TrainModelLoader loader;
	initAamToolboxPath();

	std::vector<ModelPathType> modelPaths;
	if (!loadModelInfo(modelInfoListFile(), modelPaths))
	{
		std::cout << "%TEST_FAILED% time=0 testname=testTrainGray (aamestimatorTest) message=Can' load model info list" << std::endl;
		return;
	}

	for (size_t i = 0; i < modelPaths.size(); i++)
	{
		loader.load(modelPaths[i].first, modelPaths[i].second);
	}

	for (size_t i = 0; i < loader.getModels().size(); ++i)
	{
		cv::Mat templ = loader.getModels()[i].getImage();
		//cv::imshow("im", im);
		//cv::waitKey();

		// ��������� �������� �� �������� �� �������� 

		if (0)
		{
			testMatch(&(IplImage)src_gray, &(IplImage)templ);
		}
		else
		{
			Mat src = src_gray.clone();

			// �������� ������� ����������� � �������
			cv::Mat binI, binT;
			int aperture_size = 3;
			MyCannyEx(src, binI, 25, 12.5, aperture_size, false);
			MyCannyEx(templ, binT, 25, 12.5, aperture_size, false);

			// ����������
			cvNamedWindow("cannyI", 1);
			cv::imshow("cannyI", binI);

			cvNamedWindow("cannyT", 1);
			cv::imshow("cannyT", binT);

			testMatch(&(IplImage)src, &(IplImage)templ, &(IplImage)binI, &(IplImage)binT);
		}

		/*
		cv::Mat canny_output_i;
		MyCannyEx(templ, canny_output_i, 25, 12.5, aperture_size, false);
		cv::imshow("canny_output_i", canny_output_i);*/

		cv::waitKey();
	}

	return;

	Mat src_gray2 = src_gray.clone();
	int aperture_size = 3;
	cv::Mat canny_output;

	MyCannyEx(src_gray, canny_output, 25, 12.5, aperture_size, false);


	/// Detect edges using adaptive Threshold
	cv::Mat adaptive_threshold_output_gaussian;
	myAdaptiveThreshold(src_gray2, adaptive_threshold_output_gaussian, 255, CV_ADAPTIVE_THRESH_GAUSSIAN_C, CV_THRESH_BINARY_INV, 21, "gaussian", "delta_gaussian");

	//cv::Mat adaptive_threshold_output_mean;
	//myAdaptiveThreshold(src_gray2, adaptive_threshold_output_mean, 255, CV_ADAPTIVE_THRESH_MEAN_C, CV_THRESH_BINARY, 21, 7, "mean", "delta_mean");

	cv::imshow("src_gray", src_gray);
	cv::imshow("canny_output", canny_output);
	cv::imshow("adaptive_threshold_output_gaussian", adaptive_threshold_output_gaussian);
	//cv::imshow("adaptive_threshold_output_mean", adaptive_threshold_output_mean);
	//cv::waitKey();


	Vertices2DList list_all = loadMarkup(markupFile);


	const char* sInsideMode[] = { "INSIDEMODE_REGION", "INSIDEMODE_REGIONINIT", "INSIDEMODE_BAND" };

	const char* sOutsideMode[] = { "OUTSIDEMODE_REGION", "OUTSIDEMODE_BAND", "OUTSIDEMODE_LINE" };

	for (int inside = 0; inside < 3; ++inside)
	{
		for (int outside = 0; outside < 3; ++outside)
		{
			CDeformableModelBase::InsideMode iInsideMode = (CDeformableModelBase::InsideMode)inside;
			CDeformableModelBase::OutsideMode iOutsideMode = (CDeformableModelBase::OutsideMode)outside;
			char suffix2[1024];
			sprintf(suffix2, "_%s_%s", sInsideMode[inside], sOutsideMode[outside]);
//			do_snake3(fn_in, std::string("_adaptive_threshold_implicit_circle") + suffix2, adaptive_threshold_output.clone(), src_gray, list_all, MODELTYPE_IMPLICIT, iInsideMode, iOutsideMode, true);
			//do_snake3(fn_in, std::string("_gray_implicit_circle") + suffix2, src_gray.clone(), src_gray, list_all, MODELTYPE_IMPLICIT, iInsideMode, iOutsideMode, true);
			//do_snake3(fn_in, std::string( "_gray") + suffix2, src_gray.clone(), src_gray, list_all, MODELTYPE_PARAMETRIC, iInsideMode, iOutsideMode, false);
			do_snake3(fn_in, std::string("_canny") + suffix2, canny_output.clone(), src_gray, list_all, MODELTYPE_PARAMETRIC, iInsideMode, iOutsideMode, false);
			//do_snake3(fn_in, std::string("_canny_implicit") + suffix2, canny_output.clone(), src_gray, list_all, MODELTYPE_IMPLICIT, iInsideMode, iOutsideMode, false);
			//do_snake3(fn_in, std::string("_canny_implicit_circle") + suffix2, canny_output.clone(), src_gray, list_all, MODELTYPE_IMPLICIT, iInsideMode, iOutsideMode, true);
		}
	}

#if 0
	cv::imshow("src_gray", src_gray);
	cv::imshow("canny_output", canny_output);
	cv::imshow("adaptive_threshold_output", adaptive_threshold_output);


	/// Find contours
	vector<vector<cv::Point> > contours;
	vector<cv::Vec4i> hierarchy;

	findContours(canny_output, contours, hierarchy, CV_RETR_TREE, CV_CHAIN_APPROX_SIMPLE, cv::Point(0, 0));


	if (debug_plots)
	{
		/// Draw contours
		cv::Mat drawing = cv::Mat::zeros(canny_output.size(), CV_8UC3);
		for (int i = 0; i < contours.size(); i++)
		{
			cv::Scalar color = cv::Scalar(rng.uniform(0, 255), rng.uniform(0, 255), rng.uniform(0, 255));
			drawContours(drawing, contours, i, color, 2, 8, hierarchy, 0, cv::Point());
		}


		/// Show in a window
		cv::namedWindow("Contours", CV_WINDOW_AUTOSIZE);
		cv::imshow("Contours", drawing);
		cv::imwrite(fn_out, drawing);
		cv::waitKey(0);
	}
#endif
}




int find_countours(std::string& fn_in, cv::Mat src, bool debug_plots, std::string& fn_out)
{
	cv::Mat src_gray;
	cv::RNG rng(12345);

	/// Convert image to gray and blur it
	if (src.channels() == 3 || src.channels() == 4)
		cvtColor(src, src_gray, CV_BGR2GRAY);
	else
		src_gray = src.clone();
	equalizeHist(src_gray, src_gray);


	//-- Detect faces
	CascadeClassifier face_cascade;
	const char * opencv_sources = getenv("OPENCV_SOURCES");
	if (!opencv_sources)
	{
		printf("Set eviroment variable OPENCV_SOURCES\n");
		return -1;
	}

	String directory = String(opencv_sources) + "/data/haarcascades/";
	String face_cascade_name = "haarcascade_frontalface_alt.xml";
	if (!face_cascade.load(directory + face_cascade_name))
	{ 
		printf("--(!)Error loading\n"); 
		return -1; 
	}

	std::vector<Rect> faces;
	face_cascade.detectMultiScale(src_gray, faces, 1.1, 2, 0 | CV_HAAR_SCALE_IMAGE, Size(30, 30));

	for (size_t i = 0; i < faces.size(); i++)
	{
		Mat faceROI = src_gray(faces[i]);
		std::vector<Rect> eyes;

		cv::blur(faceROI, faceROI, cv::Size(3, 3));
		/*{
			IplConvKernel* pb = cvCreateStructuringElementEx(3, 3, 1, 1, CV_SHAPE_RECT, NULL);
			cvErode(faceROI, erode, pb, 2);
			cvReleaseStructuringElement(&pb);
		}*/
		do_find_countours(fn_in, faceROI, rng, debug_plots, fn_out);
	}


	return 0;
}

void find_contours2(std::string& fn, std::string& fn_out)
{
	cv::Mat picture = cv::imread(fn);
	bool debug_plots = true;
	find_countours(fn, picture, debug_plots, fn_out);
}

int use_find_contours(IplImage *img)
{
	IplImage *gray;

	IplImage* out_median;
	IplImage* out_threshold;

	gray = cvCreateImage(cvGetSize(img), IPL_DEPTH_8U, 1);
	cvCvtColor(img, gray, CV_BGR2GRAY);

	out_median = cvCreateImage(cvGetSize(gray), IPL_DEPTH_8U, 1);
	cvSmooth(gray, out_median, CV_MEDIAN, 2*smooth_size1+1);

	out_threshold = cvCreateImage(cvGetSize(out_median), out_median->depth, 1);
	cvThreshold(out_median, out_threshold, _threshold, 255, CV_THRESH_BINARY);
	cvShowImage("threshold", out_threshold);

	CvMemStorage* storage = cvCreateMemStorage();
	CvSeq* first_contour = NULL;

	cvFindContours(out_threshold, storage, &first_contour, sizeof(CvContour), CV_RETR_LIST, CV_CHAIN_APPROX_SIMPLE);

	if (!first_contour)
		return 0;

	CvSeq* approx_polygon = NULL;
	approx_polygon = cvApproxPoly(first_contour, sizeof(CvContour), storage, CV_POLY_APPROX_DP, 0.01*cvArcLength(first_contour, CV_WHOLE_SEQ, 1), 0);

	cvDrawContours(out_threshold,approx_polygon,cvScalarAll(255),cvScalarAll(255), 100);
	cvShowImage("Median", out_median);
	cvShowImage("Contours", out_threshold );
	//cvSaveImage("save_approxpoly_contour.jpg",out_threshold);


	return 0;
}

/*
	int borders[4];
	borders[0] = 99999; //left
	borders[1] = 99999; //top
	borders[2] = 0;  //right
	borders[3] = 0;  //bottom

	for (CvBlobs::const_iterator it=blobs.begin(); it!=blobs.end(); ++it)
	{
		if((*it).second->maxx) < borders[0]){
			borders[0] = (*it).second->maxx;
		}
		if(((*it).second->miny) < borders[1]){
			borders[1] = (*it).second->miny;
		}
		if((*it).second->minx) > borders[2]){
			borders[2] = (*it).second->minx;
		}
		if(((*it).second->maxy) > borders[3]){
			borders[3] = (*it).second->maxy;
		}
	}
*/

int find_contours()
{
	IplImage* img;


	if ((img = cvLoadImage("D:/_C++/recognition/inframes/z_c42ef6e0.jpg", 1)) == NULL)
	{
		printf("A Img open error\n");
		return -1;
	}

	return use_find_contours(img);
}

void UpdateThreshold(int k, void* /*z*/)
{
	_threshold = k;
}

void UpdateSmoothSize1(int k, void* /*z*/)
{
	smooth_size1 = k;
}

int find_contours_video()
{
	VideoCapture cap(CV_CAP_ANY);
	cap.set(CV_CAP_PROP_FRAME_WIDTH, 320);
	cap.set(CV_CAP_PROP_FRAME_HEIGHT, 240);
	if (!cap.isOpened())
		return -1;

	Mat img;


	namedWindow("frame", CV_WINDOW_AUTOSIZE);
	namedWindow("Median", CV_WINDOW_AUTOSIZE);
	namedWindow("Contours", CV_WINDOW_AUTOSIZE);
	namedWindow("threshold", CV_WINDOW_AUTOSIZE);


	createTrackbar("threshold", "threshold", &_threshold, 255, UpdateThreshold, NULL);
	createTrackbar("smooth_size1", "Median", &_threshold, 100, UpdateSmoothSize1, NULL);

	
	while (true)
	{
		cap >> img;
		if (!img.data)
			continue;

		cv::imshow("frame", img);

		use_find_contours(&(IplImage(img)));

		if (waitKey(20) >= 0)
			break;
	}
	return 0;
}

int main2()
{
	VideoCapture cap(0);
	cap.set(CV_CAP_PROP_FRAME_WIDTH, 320);
	cap.set(CV_CAP_PROP_FRAME_HEIGHT, 240);
	if (!cap.isOpened())
		return -1;

	Mat img;
	HOGDescriptor hog;
	hog.setSVMDetector(HOGDescriptor::getDefaultPeopleDetector());


	namedWindow("video capture", CV_WINDOW_AUTOSIZE);
	namedWindow("Motion", CV_WINDOW_AUTOSIZE);


	while (true)
	{
		cap >> img;
		if (!img.data)
			continue;

		vector<Rect> found, found_filtered;
		hog.detectMultiScale(img, found, 0, Size(8, 8), Size(32, 32), 1.05, 2);

		size_t i, j;
		for (i = 0; i < found.size(); i++)
		{
			Rect r = found[i];
			for (j = 0; j < found.size(); j++)
			if (j != i && (r & found[j]) == r)
				break;
			if (j == found.size())
				found_filtered.push_back(r);
		}
		for (i = 0; i < found_filtered.size(); i++)
		{
			Rect r = found_filtered[i];
			r.x += cvRound(r.width*0.1);
			r.width = cvRound(r.width*0.8);
			r.y += cvRound(r.height*0.06);
			r.height = cvRound(r.height*0.9);
			rectangle(img, r.tl(), r.br(), cv::Scalar(0, 255, 0), 2);
		}
		imshow("video capture", img);
		IplImage *mat_img;

		mat_img = cvCloneImage(&(IplImage)img);
		cvThreshold(&img, &img, 0, 255, CV_THRESH_BINARY_INV);
		cvDilate(&img, &img, 0, 1);
		cvErode(&img, &img, 0, 0);

		CvMemStorage* storage = cvCreateMemStorage();
		CvSeq* contours = NULL;

		cvFindContours(&img, storage, &contours, sizeof(CvContour),
			CV_RETR_EXTERNAL, CV_CHAIN_APPROX_SIMPLE, cvPoint(0, 0));
	}
}
