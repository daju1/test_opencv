#include "DirectoryEnumerator.h"

#include "opencv2/imgproc/imgproc.hpp"
#include "opencv2/objdetect/objdetect.hpp"
#include "opencv2/highgui/highgui.hpp"

#if !HAVE_OPENCV_300
#include "opencv2/contrib/contrib.hpp"
#endif

#include <stdio.h>
#include <string.h>
#include <ctype.h>

#include <iostream>
#include <iomanip>
#include <algorithm>

using namespace cv;
using namespace std;



int use_hog(Mat& img, HOGDescriptor& hog, double hitThreshold = 0, Size winStride = Size(8, 8),
	Size padding = Size(32, 32), double scale = 1.05,
	double finalThreshold = 2.0, bool useMeanshiftGrouping = false)
{
#if !HAVE_OPENCV_300
	TickMeter tm;
	tm.start();
#endif

	vector<Rect> found, found_filtered;
	hog.detectMultiScale(img, found, hitThreshold, winStride, padding, scale, finalThreshold, useMeanshiftGrouping);

	size_t i, j;
	for (i = 0; i<found.size(); i++)
	{
		Rect r = found[i];
		for (j = 0; j<found.size(); j++)
		if (j != i && (r & found[j]) == r)
			break;
		if (j == found.size())
			found_filtered.push_back(r);
	}
	for (i = 0; i<found_filtered.size(); i++)
	{
		Rect r = found_filtered[i];
		r.x += cvRound(r.width*0.1);
		r.width = cvRound(r.width*0.8);
		r.y += cvRound(r.height*0.06);
		r.height = cvRound(r.height*0.9);
		rectangle(img, r.tl(), r.br(), cv::Scalar(0, 255, 0), 2);
	}

#if !HAVE_OPENCV_300
	tm.stop();
	double detectionTime = tm.getTimeMilli();
	double fps = 1000 / detectionTime;

	//print detections to console
	cout << setfill(' ') << setprecision(2);
	cout << setw(6) << fixed << fps << " FPS, " << found_filtered.size() << " det";
#endif

	if (found_filtered.size() > 0)
	{
		Rect *faceRects = &found_filtered[0];
		for (size_t i = 0; i < std::min<size_t>(found_filtered.size(), 2); ++i)
		{
			cout << ", [" << setw(4) << faceRects[i].x
				<< ", " << setw(4) << faceRects[i].y
				<< ", " << setw(4) << faceRects[i].width
				<< ", " << setw(4) << faceRects[i].height << "]";
		}
	}
	cout << endl;

	imshow("result", img);
	waitKey(5);


	return found_filtered.size();
}


// static void help()
// {
//     printf(
//             "\nDemonstrate the use of the HoG descriptor using\n"
//             "  HOGDescriptor::hog.setSVMDetector(HOGDescriptor::getDefaultPeopleDetector());\n"
//             "Usage:\n"
//             "./peopledetect (<image_filename> | <image_list>.txt)\n\n");
// }

int DemonstrateHoGwithFiles(int argc, const char** argv)
{
	Mat img;
	FILE* f = 0;
	char _filename[1024];

	if (argc == 1)
	{
		printf("Usage: peopledetect (<image_filename> | <image_list>.txt)\n");
		return 0;
	}
	img = imread(argv[1]);

	if (img.data)
	{
		strcpy(_filename, argv[1]);
	}
	else
	{
		f = fopen(argv[1], "rt");
		if (!f)
		{
			fprintf(stderr, "ERROR: the specified file could not be loaded\n");
			return -1;
		}
	}

	int cnt = 0, cnt_success = 0;

	HOGDescriptor hog;
	hog.setSVMDetector(HOGDescriptor::getDefaultPeopleDetector());
	namedWindow("people detector", 1);

	for (;;)
	{
		char* filename = _filename;
		if (f)
		{
			if (!fgets(filename, (int)sizeof(_filename)-2, f))
				break;
			//while(*filename && isspace(*filename))
			//  ++filename;
			if (filename[0] == '#')
				continue;
			int l = (int)strlen(filename);
			while (l > 0 && isspace(filename[l - 1]))
				--l;
			filename[l] = '\0';
			img = imread(filename);
		}
		printf("%s:\n", filename);
		if (!img.data)
			continue;

		fflush(stdout);
		use_hog(img, hog);
#if 0
		vector<Rect> found, found_filtered;
		double t = (double)getTickCount();
		// run the detector with default parameters. to get a higher hit-rate
		// (and more false alarms, respectively), decrease the hitThreshold and
		// groupThreshold (set groupThreshold to 0 to turn off the grouping completely).
		hog.detectMultiScale(img, found, 0, Size(8, 8), Size(32, 32), 1.05, 2);
		t = (double)getTickCount() - t;
		printf("tdetection time = %gms\n", t*1000. / cv::getTickFrequency());
		size_t i, j;
		for (i = 0; i < found.size(); i++)
		{
			Rect r = found[i];
			for (j = 0; j < found.size(); j++)
			if (j != i && (r & found[j]) == r)
				break;
			if (j == found.size())
				found_filtered.push_back(r);
		}
		for (i = 0; i < found_filtered.size(); i++)
		{
			Rect r = found_filtered[i];
			// the HOG detector returns slightly larger rectangles than the real objects.
			// so we slightly shrink the rectangles to get a nicer output.
			r.x += cvRound(r.width*0.1);
			r.width = cvRound(r.width*0.8);
			r.y += cvRound(r.height*0.07);
			r.height = cvRound(r.height*0.8);
			rectangle(img, r.tl(), r.br(), cv::Scalar(0, 255, 0), 3);
		}

		if (found_filtered.size())
			++cnt_success;
		++cnt;
#endif
#if 0
		imshow("people detector", img);
		int c = waitKey(0) & 255;
		if (c == 'q' || c == 'Q' || !f)
			break;
#endif

	}

	printf("cnt = %d cnt_success=%d\n", cnt, cnt_success);

	if (f)
		fclose(f);
	return 0;
}


int DemonstrateHoGwithVideoCapture(int argc, const char * argv[])
{
	VideoCapture cap(CV_CAP_ANY);
	cap.set(CV_CAP_PROP_FRAME_WIDTH, 320);
	cap.set(CV_CAP_PROP_FRAME_HEIGHT, 240);
	if (!cap.isOpened())
		return -1;

	Mat img;
	HOGDescriptor hog;
	hog.setSVMDetector(HOGDescriptor::getDefaultPeopleDetector());

	namedWindow("video capture", CV_WINDOW_AUTOSIZE);
	while (true)
	{
		cap >> img;
		if (!img.data)
			continue;

		use_hog(img, hog);

		if (waitKey(20) >= 0)
			break;
	}
	return 0;
}

