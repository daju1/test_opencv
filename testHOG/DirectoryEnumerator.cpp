#include "stdafx.h"
#include "DirectoryEnumerator.h"
#include "tchar.h"


DirectoryEnumerator::DirectoryEnumerator(const char* fn, 
                                         const char* ext)
{
   m_fn  = fn;
   m_ext = ext;
}



void DirectoryEnumerator::DisplayErrorBox(LPTSTR lpszFunction, BOOL bShowMsgBox)
{
   // Retrieve the system error message for the last-error code
   
   LPVOID lpMsgBuf;
   LPVOID lpDisplayBuf;
   DWORD dw = GetLastError(); 
   
   FormatMessage(
      FORMAT_MESSAGE_ALLOCATE_BUFFER | 
      FORMAT_MESSAGE_FROM_SYSTEM |
      FORMAT_MESSAGE_IGNORE_INSERTS,
      NULL,
      dw,
      MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT),
      (LPTSTR) &lpMsgBuf,
      0, NULL );
   
   // Display the error message and clean up
   
   lpDisplayBuf = (LPVOID)LocalAlloc(LMEM_ZEROINIT, 
      (lstrlen((LPCTSTR)lpMsgBuf)+lstrlen((LPCTSTR)lpszFunction)+40)*sizeof(TCHAR)); 
   
   sprintf((char*)lpDisplayBuf, 
      "%s failed with error %d: %s", 
      lpszFunction, dw, lpMsgBuf); 
   if (bShowMsgBox)
      MessageBox(NULL, (LPCTSTR)lpDisplayBuf, TEXT("Error"), MB_OK); 

   printf ("%s failed with error %d: %s", 
      lpszFunction, dw, lpMsgBuf);
   
   LocalFree(lpMsgBuf);
   LocalFree(lpDisplayBuf);
}


#if 1
int DirectoryEnumerator::enum_files(const char * dir)
{
   WIN32_FIND_DATA ffd;
   LARGE_INTEGER filesize;
   TCHAR szDir[MAX_PATH];
   HANDLE hFind = INVALID_HANDLE_VALUE;
   DWORD dwError=0;



   //_tprintf(TEXT("\nTarget directory is %s\n\n"), dir);

   wsprintf(szDir, _T("%s/%s.%s"), dir, m_fn.c_str(), m_ext.c_str());

   hFind = FindFirstFile(szDir, &ffd);

   if (INVALID_HANDLE_VALUE == hFind) 
   {
      //DisplayErrorBox(TEXT("FindFirstFile"));
      return dwError;
   }


   do
   {
      char fn[MAX_PATH];
      sprintf(fn, "%s/%s", dir, ffd.cFileName);

      if (ffd.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY)
      {
         _tprintf(TEXT("  %s   <DIR>\n"), ffd.cFileName);
      }
      else
      {
         filesize.LowPart = ffd.nFileSizeLow;
         filesize.HighPart = ffd.nFileSizeHigh;
         _tprintf(TEXT("  %s   %ld bytes\n"), ffd.cFileName, filesize.QuadPart);

		 called_foo(fn, filesize);
		 if (IsStop())
			 return 0;
	  }
   }
   while (FindNextFile(hFind, &ffd) != 0);

   dwError = GetLastError();
   if (dwError != ERROR_NO_MORE_FILES) 
   {
	   DisplayErrorBox(_T("FindFirstFile"), FALSE);
   }

   FindClose(hFind);
   return dwError;
}
#endif


int DirectoryEnumerator::enum_dir(const char * dir)
{
   WIN32_FIND_DATA ffd;
   TCHAR szDir[MAX_PATH];
   HANDLE hFind = INVALID_HANDLE_VALUE;
   DWORD dwError=0;

   LARGE_INTEGER filesize;


   //_tprintf(TEXT("\nTarget directory is %s\n\n"), dir);

   wsprintf(szDir, _T("%s\\*.*"), dir);
   //sprintf(szDir, "%s\\%s.%s", dir, m_fn.c_str(), m_ext.c_str());

   hFind = FindFirstFile(szDir, &ffd);

   if (INVALID_HANDLE_VALUE == hFind) 
   {
      DisplayErrorBox(TEXT("FindFirstFile"), FALSE);
      return dwError;
   }


   do
   {
      char fn[MAX_PATH];
      sprintf(fn, "%s\\%s", dir, ffd.cFileName);

      if (ffd.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY)
      {
         _tprintf(TEXT("  %s   <DIR>\n"), ffd.cFileName);

         if (ffd.cFileName[0] != '.')
         {
            enum_dir  (fn);
            enum_files(fn);
         }
      }
	  /*else
	  {
		  filesize.LowPart = ffd.nFileSizeLow;
		  filesize.HighPart = ffd.nFileSizeHigh;
		  _tprintf(TEXT("  %s   %ld bytes\n"), ffd.cFileName, filesize.QuadPart);

		  called_foo(fn, filesize);
        if (IsStop())
           return 0;
	  }*/
   }
   while (FindNextFile(hFind, &ffd) != 0);

   dwError = GetLastError();
   if (dwError != ERROR_NO_MORE_FILES) 
   {
      DisplayErrorBox(TEXT("FindFirstFile"), FALSE);
   }

   FindClose(hFind);
   return dwError;
}
