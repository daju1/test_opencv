#include <opencv/cv.h>
#include <opencv/highgui.h>
#include "opencv2/imgproc/imgproc.hpp"
#include "opencv2/highgui/highgui.hpp"
#if !HAVE_OPENCV_300
#include "opencv2/legacy/legacy.hpp"
#endif


#include "activecontour.h"
#include "binaryregion.h"

int active_contour_main(const char * fn, const char * dir_out, float fCircleCenterX, float fCircleCenterY, float fCircleRadius, ModelType iModelType, CDeformableModelBase::InsideMode iInsideMode, CDeformableModelBase::OutsideMode iOutsideMode);



void myConvexityDefects(cv::InputArray _points, cv::InputArray _hull, cv::OutputArray _defects)
{
	cv::Mat points = _points.getMat();
	int ptnum = points.checkVector(2, CV_32S);
	CV_Assert(ptnum > 3);
	cv::Mat hull = _hull.getMat();
//	CV_Assert(hull.checkVector(1, CV_32S) > 2);
	cv::Ptr<CvMemStorage> storage = cvCreateMemStorage();

	CvMat c_points = points, c_hull = hull;
	CvSeq* seq = cvConvexityDefects(&c_points, &c_hull, storage);
	int i, n = seq->total;

	if (n == 0)
	{
		_defects.release();
		return;
	}

	_defects.create(n, 1, CV_32SC4);
	cv::Mat defects = _defects.getMat();

	cv::SeqIterator<CvConvexityDefect> it = cv::Seq<CvConvexityDefect>(seq).begin();
	CvPoint* ptorg = (CvPoint*)points.data;

	for (i = 0; i < n; i++, ++it)
	{
		CvConvexityDefect& d = *it;
		int idx0 = (int)(d.start - ptorg);
		int idx1 = (int)(d.end - ptorg);
		int idx2 = (int)(d.depth_point - ptorg);
		CV_Assert(0 <= idx0 && idx0 < ptnum);
		CV_Assert(0 <= idx1 && idx1 < ptnum);
		CV_Assert(0 <= idx2 && idx2 < ptnum);
		CV_Assert(d.depth >= 0);
		int idepth = cvRound(d.depth * 256);
		defects.at<cv::Vec4i>(i) = cv::Vec4i(idx0, idx1, idx2, idepth);
	}
}

void do_convex_hull(std::vector<cv::Point>& c, cv::Size size)
{
	vector < cv::Point > hull;
	cv::convexHull(cv::Mat(c), hull);

	std::vector<int> ihull;
	cv::convexHull(cv::Mat(c), ihull);


	//double area = contourArea(Mat(msers[i]));
	//double hullArea = contourArea(Mat(hull));
	//double ratio = area / hullArea;

	//printf("area=%f hullArea=%f ratio=%f\n", area, hullArea, ratio);

	/*for (size_t j = 0; j < hull.size(); j++)
	{
		size_t j_1 = j == 0 ? hull.size() - 1 : j - 1;
		cv::Point pt1 = hull[j_1];
		cv::Point pt2 = hull[j];
		cv::line(drawing, pt1, pt2, CV_RGB(0, 0, 255));
	}*/

	cv::Mat drawing = cv::Mat::zeros(size, CV_8UC3);
	cv::polylines(drawing, c, true, cv::Scalar(0xCC, 0xCC, 0xCC), 1);
	cv::polylines(drawing, hull, true, cv::Scalar(0xFF, 0x20, 0x20), 1);

	/*for (size_t j = 0; j < ihull.size(); j++)
	{
		size_t j_1 = j == 0 ? ihull.size() - 1 : j - 1;
		cv::Point pt1 = c[ihull[j_1]];
		cv::Point pt2 = c[ihull[j]];
		cv::line(drawing, pt1, pt2, CV_RGB(0, 0, 255));
	}*/

	int ptnum = c.size();
	int nn = cv::Mat(ihull).checkVector(1, CV_32S);
	if (nn > 2 && ptnum > 3)
	{
		std::vector<cv::Vec4i> defects;
		convexityDefects(c, ihull, defects);

		std::vector<cv::Vec4i>::iterator d = defects.begin();
		while (d != defects.end())
		{
			cv::Vec4i& v = (*d); d++;
			int startidx = v[0]; cv::Point ptStart(c[startidx]);
			int endidx = v[1]; cv::Point ptEnd(c[endidx]);
			int faridx = v[2]; cv::Point ptFar(c[faridx]);

			cv::circle(drawing, ptStart, 1, cv::Scalar(0x02, 0x60, 0xFF), 2);
			cv::circle(drawing, ptEnd, 1, cv::Scalar(0xFF, 0x60, 0x02), 2);
			cv::circle(drawing, ptFar, 1, cv::Scalar(0x60, 0xFF, 0x02), 2);

			cv::Mat drawing2 = cv::Mat::zeros(size, CV_8UC3);
			cv::polylines(drawing2, c, true, cv::Scalar(0xCC, 0xCC, 0xCC), 1);
			cv::polylines(drawing2, hull, true, cv::Scalar(0xFF, 0x20, 0x20), 1);

			cv::circle(drawing2, ptStart, 1, cv::Scalar(0x02, 0x60, 0xFF), 2);
			cv::circle(drawing2, ptEnd, 1, cv::Scalar(0xFF, 0x60, 0x02), 2);
			cv::circle(drawing2, ptFar, 1, cv::Scalar(0x60, 0xFF, 0x02), 2);

			cv::imshow("hull2", drawing2);
			cv::waitKey();

		}
	}

	cv::imshow("hull", drawing);
	cv::waitKey();
}



void do_find_captcha(std::string& fn_in, cv::Mat& src_gray, cv::RNG& rng, bool debug_plots, std::string& fn_out)
{
	/// Find contours
	std::vector<std::vector<cv::Point> > contours;
	std::vector<std::vector<cv::Point> > contours2;
	std::vector<cv::Vec4i> hierarchy;

	findContours(src_gray, contours, hierarchy, CV_RETR_TREE, CV_CHAIN_APPROX_SIMPLE, cv::Point(0, 0));

	std::vector<cv::Point> contours_all;

	for (int i = 0; i < contours.size(); i++)
	{
		cv::Rect shapeRect = cv::boundingRect(contours[i]);
		if (shapeRect.tl().x <= 1)
			continue;
		if (shapeRect.tl().y <= 1)
			continue;

		contours2.push_back(contours[i]);

		for (int j = 0; j < contours[i].size(); ++j)
			contours_all.push_back(contours[i][j]);
	}

	cv::Rect allShapeRect = cv::boundingRect(contours_all);
	for (int i = 0; i < contours2.size(); i++)
	{
		do_convex_hull(contours2[i], src_gray.size());
	}
	//do_convex_hull(contours_all, src_gray.size());

	cv::imshow("hull", src_gray);
	cv::waitKey();

	std::vector<std::vector<std::vector<cv::Point> > > contours6;
	contours6.resize(6);

	for (int i = 0; i < contours2.size(); i++)
	{
		int prev_p = -1;

		for (int j = 0; j < contours2[i].size(); ++j)
		{
			int p = 6 * double(contours2[i][j].x - allShapeRect.tl().x) / allShapeRect.width;
			if (p > 5)
				continue;

			if (p != prev_p)
				contours6[p].push_back(std::vector<cv::Point> ());

			cv::Point pt = contours2[i][j];

			pt.x -= allShapeRect.tl().x + int (double(p) / 6 * allShapeRect.width);
			pt.y -= allShapeRect.tl().y;

			contours6[p].back().push_back(pt);

			prev_p = p;
		}
	}

	for (int p = 0; p < 6; ++p)
	{
		std::vector<cv::Point> contours_all_p;

		for (int i = 0; i < contours6[p].size(); i++)
		{
			for (int j = 0; j < contours6[p][i].size(); ++j)
				contours_all_p.push_back(contours6[p][i][j]);
		}

		cv::Rect allShapeRect_p = cv::boundingRect(contours_all_p);

		cv::Mat drawing = cv::Mat::zeros(allShapeRect_p.size(), CV_8UC3);

		for (int i = 0; i < contours6[p].size(); i++)
		{
			drawContours(drawing, contours6[p], i, CV_RGB(255,255,255), 1, 8, hierarchy, 0, cv::Point());
		}

		char str[255];
		sprintf(str, "%d", p);

		cv::namedWindow(str, CV_WINDOW_AUTOSIZE);
		cv::imshow(str, drawing);
		//cv::imwrite(fn_out, drawing);
		//cv::waitKey(0);

	}




	if (debug_plots)
	{
		/// Draw contours
		cv::Mat drawing = cv::Mat::zeros(src_gray.size(), CV_8UC3);
		for (int i = 0; i < contours.size(); i++)
		{
			cv::Scalar color = cv::Scalar(rng.uniform(0, 255), rng.uniform(0, 255), rng.uniform(0, 255));
			drawContours(drawing, contours, i, color, 1, 8, hierarchy, 0, cv::Point());
		}


		/// Show in a window
		cv::namedWindow("Contours", CV_WINDOW_AUTOSIZE);
		cv::imshow("Contours", drawing);
		//cv::imwrite(fn_out, drawing);
		cv::waitKey(0);
	}
}

bool doThis(const cv::Mat image, cv::Mat& result, int rBright, int tLetter)
{
	result = image.clone();
	for (int x = 0; x < result.size().width; ++x)
	{
		for (int y = 0; y < result.size().height; ++y)
		{
			if (result.at<unsigned char>(cv::Point(x,y)) == 0)
				continue;
			if (result.at<unsigned char>(cv::Point(x, y)) < tLetter){
				cv::floodFill(result, cv::Point(x, y), 0, 0, rBright, rBright, 8);
				if (result.at<unsigned char>(cv::Point(0, 0)) != image.at<unsigned char>(cv::Point(0, 0)) ||
					result.at<unsigned char>(cv::Point(0, result.size().height - 1)) != image.at<unsigned char>(cv::Point(0, image.size().height - 1)) ||
					result.at<unsigned char>(cv::Point(result.size().width - 1, 0)) != image.at<unsigned char>(cv::Point(image.size().width - 1, 0)) ||
					result.at<unsigned char>(cv::Point(result.size().width - 1, result.size().height - 1)) != image.at<unsigned char>(cv::Point(image.size().width - 1, image.size().height - 1))
					)
					return false;
			}
		}
	}
	return true;
}

cv::Mat clearNoise(const cv::Mat image)
{
	cv::Mat result;
	int tLetter = 64, rBright = 30;
	while (!doThis(image, result, rBright, tLetter))
	{
		rBright -= 1;
	}
	return result;	
}

/*, tLetter = 64, rBright = 30
    if tLetter <= 0:
        raise ValueError("tLetter = " + str(tLetter))
    if rBright <= 0:
        raise ValueError("rBright = " + str(rBright))
    def doThis():
        result = cv.CloneImage(image)
        for x in xrange(result.width):
            for y in xrange(result.height):
                if result[y, x] == 0:
                    continue
                if result[y, x] < tLetter:
                    cv.FloodFill(result, (x, y), 0, rBright, rBright, 8, None)
                    if result[0, 0] != image[0, 0] or \
                       result[result.height - 1, 0] != image[image.height - 1, 0] or \
                       result[0, result.width - 1] != image[0, image.width - 1] or \
                       result[result.height - 1, result.width - 1] != image[image.height - 1, image.width - 1]:
                        return None
        return result
    result = doThis()
    while result is None:
        rBright -= 1
        result = doThis()
    cv.Threshold(result, result, 1, 255, cv.CV_THRESH_BINARY_INV)
    return result
	*/





/*def smoothNoise1(image, bgcolor=255):
    temp = cv.CreateImage((image.width + 2, image.height + 2), image.depth, image.nChannels)
    result = cv.CreateImage(cv.GetSize(image), image.depth, image.nChannels)
    cv.CopyMakeBorder(image, temp, (1, 1), 0, bgcolor)
    for x in xrange(1, image.width + 1):
        for y in xrange(1, image.height + 1):
            if   temp[y + 1, x] == temp[y - 1, x] and temp[y, x] != temp[y + 1, x]:
                result[y - 1, x - 1] = temp[y + 1, x]
            elif temp[y, x + 1] == temp[y, x - 1] and temp[y, x] != temp[y, x + 1]:
                result[y - 1, x - 1] = temp[y, x + 1]
            else:
                result[y - 1, x - 1] = temp[y, x]
    return result
	*/

cv::Mat smoothNoise1(const cv::Mat image, cv::Scalar bgcolor = 255)
{
	cv::Mat temp = cv::Mat(cv::Size(image.size().width + 2, image.size().height + 2), image.type());
	cv::Mat result = cv::Mat(cv::Size(image.size().width, image.size().height), image.type());

	cv::copyMakeBorder(image, temp, 1, 1, 1, 1, 0, bgcolor);

	for (int x = 1; x < result.size().width + 1; ++x)
	{
		for (int y = 1; y < result.size().height + 1; ++y)
		{
			if (temp.at<unsigned char>(cv::Point(x, y + 1)) == temp.at<unsigned char>(cv::Point(x, y - 1)) && temp.at<unsigned char>(cv::Point(x, y + 1)) != temp.at<unsigned char>(cv::Point(x, y)))
				result.at<unsigned char>(cv::Point(x, y - 1)) = temp.at<unsigned char>(cv::Point(x, y + 1));
			else if (temp.at<unsigned char>(cv::Point(x + 1, y)) == temp.at<unsigned char>(cv::Point(x - 1, y)) && temp.at<unsigned char>(cv::Point(x + 1, y)) != temp.at<unsigned char>(cv::Point(x, y)))
				result.at<unsigned char>(cv::Point(x - 1, y)) = temp.at<unsigned char>(cv::Point(x + 1, y));
			else 
				result.at<unsigned char>(cv::Point(x - 1, y - 1)) = temp.at<unsigned char>(cv::Point(x, y));
		}
	}

	return result;
}

cv::Mat smoothNoise2(const cv::Mat image, cv::Scalar bgcolor = 255)
{
	cv::Mat temp = cv::Mat(cv::Size(image.size().width + 2, image.size().height + 2), image.type());
	cv::Mat result = cv::Mat(cv::Size(image.size().width, image.size().height), image.type());

	cv::copyMakeBorder(image, temp, 1, 1, 1, 1, 0, bgcolor);

	for (int x = 1; x < result.size().width + 1; ++x)
	{
		for (int y = 1; y < result.size().height + 1; ++y)
		{
			if (temp.at<unsigned char>(cv::Point(x, y + 1)) == temp.at<unsigned char>(cv::Point(x, y - 1)) && temp.at<unsigned char>(cv::Point(x, y + 1)) != temp.at<unsigned char>(cv::Point(x, y)) && temp.at<unsigned char>(cv::Point(x, y + 1)) == 0)
			{
				unsigned char v = temp.at<unsigned char>(cv::Point(x, y + 1));
				result.at<unsigned char>(cv::Point(x - 1, y - 1)) = v;
			}
			else if (temp.at<unsigned char>(cv::Point(x + 1, y)) == temp.at<unsigned char>(cv::Point(x - 1, y)) && temp.at<unsigned char>(cv::Point(x + 1, y)) != temp.at<unsigned char>(cv::Point(x, y)) && temp.at<unsigned char>(cv::Point(x + 1, y)) == 0)
			{
				unsigned char v = temp.at<unsigned char>(cv::Point(x + 1, y));
				result.at<unsigned char>(cv::Point(x - 1, y - 1)) = v;
			}
			else
				result.at<unsigned char>(cv::Point(x - 1, y - 1)) = temp.at<unsigned char>(cv::Point(x, y));
		}
	}

	return result;
}

int find_captcha(std::string& fn_in, cv::Mat src, bool debug_plots, std::string& fn_out)
{
	cv::Mat src_gray;
	cv::RNG rng(12345);

	/// Convert image to gray and blur it
	if (src.channels() == 3 || src.channels() == 4)
		cvtColor(src, src_gray, CV_BGR2GRAY);
	else
		src_gray = src.clone();
	equalizeHist(src_gray, src_gray);

	cv::imshow("src src_gray", src_gray);
	cv::waitKey(0);

	src_gray = smoothNoise2(src_gray);
	cv::imshow("clearNoise", src_gray);
	src_gray = smoothNoise2(src_gray);
	cv::imshow("clearNoise2", src_gray);
	cv::waitKey(0);

	//cv::threshold(src_gray, src_gray, 0, 255, CV_THRESH_BINARY_INV);
	cv::dilate(src_gray, src_gray, 0);
	cv::erode(src_gray, src_gray, 0);

	cv::imshow("src_gray", src_gray);

	do_find_captcha(fn_in, src_gray, rng, debug_plots, fn_out);


#if 0
	std::string directory = "D:\\_output_captcha";
	CreateDirectory(directory.c_str(), NULL);

	std::string fn = directory + "\\face.bmp";
	cv::imwrite(fn, src);





	const char* sInsideMode[] = { "INSIDEMODE_REGION", "INSIDEMODE_REGIONINIT", "INSIDEMODE_BAND" };

	const char* sOutsideMode[] = { "OUTSIDEMODE_REGION", "OUTSIDEMODE_BAND", "OUTSIDEMODE_LINE" };


	for (int inside = 0; inside < 3; ++inside)
	{
		for (int outside = 0; outside < 3; ++outside)
		{
			CDeformableModelBase::InsideMode iInsideMode = (CDeformableModelBase::InsideMode)inside;
			CDeformableModelBase::OutsideMode iOutsideMode = (CDeformableModelBase::OutsideMode)outside;
			char suffix2[1024];
			sprintf(suffix2, "_%s_%s", sInsideMode[inside], sOutsideMode[outside]);

			int i1 = fn_in.find_last_of({ '\\', '/' });
			int i2 = fn_in.find_last_of('.');
			directory += fn_in.substr(i1, i2 - i1 - 1) + suffix2;
			CreateDirectory(directory.c_str(), NULL);

			ModelType iModelType = ModelType::MODELTYPE_IMPLICIT;
			float fCircleRadius = 10;
			active_contour_main(fn_in.c_str(), directory.c_str(), src.size().width / 2, src.size().height / 2, fCircleRadius, iModelType, iInsideMode, iOutsideMode);
		}
	}

#endif
	return 0;
}

void captcha(std::string& fn, std::string& fn_out)
{
	cv::Mat picture = cv::imread(fn);
	bool debug_plots = true;
	find_captcha(fn, picture, debug_plots, fn_out);
}