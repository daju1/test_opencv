#include "DirectoryEnumerator.h"
#include "contour_recognition/ContourRecognitionDirectoryEnumerator.h"


#include "opencv2/imgproc/imgproc.hpp"
#include "opencv2/objdetect/objdetect.hpp"
#include "opencv2/highgui/highgui.hpp"

#if !HAVE_OPENCV_300
#include <opencv2/nonfree/nonfree.hpp>
#endif

#include "TrainModelLoader.h"


#include "MultiOtsu.h"
#include "mycanny.hpp"
#include "cascadeclassifier.h"

#if !HAVE_OPENCV_300
#include <stdio.h>
#include <ctype.h>
#endif
#include <string.h>
#include <algorithm>
#include <list>

using namespace cv;
using namespace std;

#include "fann.h"

#include "contour_recognition\SuperSegment.h"
#include "contour_recognition\ClusterTableEntry.h"


const std::string modelInfoListFile();

int use_hog(Mat& img, HOGDescriptor& hog, double hitThreshold = 0, Size winStride = Size(8, 8),
	Size padding = Size(32, 32), double scale = 1.05,
	double finalThreshold = 2.0, bool useMeanshiftGrouping = false);


int DemonstrateHoGwithVideoCapture(int argc, const char * argv[]);
int DemonstrateHoGwithFiles(int argc, const char** argv);

void CreateGHTTemplate(const char* fn_input_img, const char* fn_detected_edges);
void runGHT(char c);

int use_cascadeclassifier_cpu(string cascadeName, string inputName, bool isInputImage, bool isInputVideo, bool isInputCamera, bool toWriteImageBack, double detectScaleFactor, Size minSize, Size maxSize);
int use_cascadeclassifier2(int argc, const char** argv);

int dense_optical_flow_video();
int farneback_optical_flow_video();

#if !HAVE_OPENCV_300
void CV_MserTest_run();
int mser_video();

void use_orb(const char * filename);
int orb_video();
#endif

void CV_BackgroundSubtractorTest_run();
int background_subtractor_GMG_video();
int background_subtractor_MOG_video();
int background_subtractor_MOG2_video();

int find_contours_video();
int find_contours();
void find_contours2(std::string& fn, std::string& fn_out);

void test_cornerEigenValsAndVecs();
void test_superres();

int main_lineFittingTest();
int main_eyeLike();
void main_slic(const char * fn, int nr_superpixels, int nc);
void test_getAffineTransformOverdetermined();
void simplify_converter_test();




class GHTDirectoryEnumerator
	: public DirectoryEnumerator

{
	UINT m_files_number;
	UINT m_total_files_number;

public:
	GHTDirectoryEnumerator(const char* fn, const char* ext)
		: DirectoryEnumerator(fn, ext)
		, m_files_number(0)
		, m_total_files_number(0)
	{
	}

	virtual bool IsStop()
	{
		return false;
	}

	virtual void called_foo(char * fn_in, LARGE_INTEGER filesize)
	{
		CreateGHTTemplate(fn_in, NULL);

		//if (rc > 0)
		++m_files_number;
		++m_total_files_number;
	}

	UINT files_number(){ return m_files_number; }
	UINT total_files_number(){ return m_total_files_number; }
};



#if !HAVE_OPENCV_300
class OrbDirectoryEnumerator
	: public DirectoryEnumerator

{
	UINT m_files_number;
	UINT m_total_files_number;

public:
	OrbDirectoryEnumerator(const char* fn, const char* ext)
		: DirectoryEnumerator(fn, ext)
		, m_files_number(0)
		, m_total_files_number(0)
	{
	}

	virtual bool IsStop()
	{
		return false;
	}

	virtual void called_foo(char * fn_in, LARGE_INTEGER filesize)
	{
		use_orb(fn_in);

		//if (rc > 0)
			++m_files_number;
		++m_total_files_number;
	}

	UINT files_number(){ return m_files_number; }
	UINT total_files_number(){ return m_total_files_number; }
};
#endif



class HogDirectoryEnumerator
	: public DirectoryEnumerator

{
	UINT m_files_number;
	UINT m_total_files_number;
	HOGDescriptor hog;

public:
	HogDirectoryEnumerator(const char* fn, const char* ext)
		: DirectoryEnumerator(fn, ext)
		, m_files_number(0)
		, m_total_files_number(0)
		, hog(Size(64, 128), Size(16, 16), Size(8, 8), Size(8, 8), 9, 1, -1, HOGDescriptor::L2Hys, 0.2, false, cv::HOGDescriptor::DEFAULT_NLEVELS)
	{
		hog.setSVMDetector(HOGDescriptor::getDefaultPeopleDetector());
	}

	virtual bool IsStop()
	{
		return false;
	}

	virtual void called_foo(char * fn_in, LARGE_INTEGER filesize)
	{
		Mat img = imread(fn_in);
		if (!img.data)
			return;

		double hitThreshold = 0;
		Size winStride = Size(8, 8);
		Size padding = Size(64, 128);
		double scale = 1.05;
		double finalThreshold = 2.0;
		bool useMeanshiftGrouping = false;

		int rc = use_hog(img, hog, hitThreshold, winStride,
			padding, scale, finalThreshold, useMeanshiftGrouping);

		if (rc > 0)
			++m_files_number;
		++m_total_files_number;
	}

	UINT files_number(){ return m_files_number; }
	UINT total_files_number(){ return m_total_files_number; }
};




class CascadeDirectoryEnumerator
	: public DirectoryEnumerator

{
	UINT m_files_number;
	UINT m_total_files_number;
	string directory;
	string cascadeName;
	bool toWriteImageBack;

	bool isInputImage;
	bool isInputVideo;
	bool isInputCamera;


public:
	CascadeDirectoryEnumerator(const char* fn, const char* ext)
		: DirectoryEnumerator(fn, ext)
		, m_files_number(0)
		, m_total_files_number(0)

		, toWriteImageBack (false)

		, isInputImage (true)
		, isInputVideo (false)
		, isInputCamera (false)

	{
		const char * opencv_sources = getenv("OPENCV_SOURCES");
		if (!opencv_sources)
		{
			printf("Set eviroment variable OPENCV_SOURCES\n");
		}

		directory = string(opencv_sources) + "/data/";
		cascadeName = "hogcascades/hogcascade_pedestrians.xml";
	}

	void SetToWriteImageBack(){ toWriteImageBack = true; }

	void SetCascadName(const char* _cascadeName)
	{ 
		cascadeName = _cascadeName; 
	}

	void SetIsInputImage(bool _isInputImage)
	{
		isInputImage = _isInputImage;
	}

	void SetIsInputVideo(bool _isInputVideo)
	{
		isInputVideo = _isInputVideo;
	}

	virtual bool IsStop()
	{
		return false;
	}


	virtual void called_foo(char * fn_in, LARGE_INTEGER filesize)
	{
		string inputName = fn_in;

		double detectScaleFactor = 1.1;
		//Size minSize = Size(48, 96);
		Size minSize = Size(24, 48);
		Size maxSize = Size();


		int rc = use_cascadeclassifier_cpu(directory + cascadeName, inputName, isInputImage, isInputVideo, isInputCamera, toWriteImageBack, detectScaleFactor, minSize, maxSize);
		//if (rc == 2 || rc == 4)
		if (rc > 0)
			++m_files_number;
		++m_total_files_number;
	}

	UINT files_number(){ return m_files_number; }
	UINT total_files_number(){ return m_total_files_number; }
};



std::string GetFnOut(const std::string& fn, const std::string& output_directory)
{
	int ind1 = fn.find_last_of('/');
	int ind2 = fn.find_last_of('\\');
	int ind = ind1 > ind2 ? ind1 : ind2;
	std::string fn_out = output_directory + fn.substr(ind);
	return fn_out;
}



class FindContoursDirectoryEnumerator
	: public DirectoryEnumerator
{
	std::string output_directory;
public:

	void setOutputDirectory(std::string dir)
	{
		output_directory = dir;
		CreateDirectoryA(dir.c_str(), NULL);
	}

	FindContoursDirectoryEnumerator(const char* fn, const char* ext)
		: DirectoryEnumerator(fn, ext)
	{
	}

	virtual bool IsStop()
	{
		return false;
	}

	virtual void called_foo(char * fn_in, LARGE_INTEGER filesize)
	{
		std::string fn = fn_in;
		find_contours2(fn, GetFnOut(fn, output_directory));
	}
};

void captcha(std::string& fn, std::string& fn_out);

class CaptchaDirectoryEnumerator
	: public DirectoryEnumerator
{
	std::string output_directory;
public:

	void setOutputDirectory(std::string dir)
	{
		output_directory = dir;
		CreateDirectoryA(dir.c_str(), NULL);
	}

	CaptchaDirectoryEnumerator(const char* fn, const char* ext)
		: DirectoryEnumerator(fn, ext)
	{
	}

	virtual bool IsStop()
	{
		return false;
	}

	virtual void called_foo(char * fn_in, LARGE_INTEGER filesize)
	{
		std::string fn = fn_in;
		captcha(fn, GetFnOut(fn, output_directory));
	}
};


inline bool FileExists(const std::string& name)
{
	HANDLE hFile = CreateFile(name.c_str(), GENERIC_READ, 0, NULL, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, NULL);
	if (hFile != NULL && hFile != INVALID_HANDLE_VALUE)
	{
		CloseHandle(hFile);
		return true;
	}
	return false;
}


class TrainCascadeDirectoryEnumerator
	: public DirectoryEnumerator
{
	std::string output_directory;
	std::string output_directory_good;
	std::string output_directory_bad;

	std::string good_dat;
	std::string bad_dat;
	std::string train_bat;

	char m_chLetterToTrain;

	int numGood, numBad;
	int mean_w, mean_h;

public:

	bool bSaveBad;

	void setLetterToTrain(char ch)
	{
		m_chLetterToTrain = ch;
	}

	void setOutputDirectory(std::string dir)
	{
		output_directory = dir;
		CreateDirectoryA(dir.c_str(), NULL);
		output_directory_good = output_directory + "/Good";
		output_directory_bad = output_directory + "/Bad";

		CreateDirectoryA(output_directory_good.c_str(), NULL);
		CreateDirectoryA(output_directory_bad.c_str(), NULL);

		CreateDirectoryA((output_directory + "/haarcascade").c_str(), NULL);
	}

	void save()
	{
		FILE * stream_good = fopen((output_directory + "/Good.dat").c_str(), "wt");
		if (stream_good)
		{
			fwrite(good_dat.c_str(), good_dat.size(), 1, stream_good);
			fclose(stream_good);
		}

		FILE * stream_bad = fopen((output_directory + "/Bad.dat").c_str(), "wt");
		if (stream_bad)
		{
			fwrite(bad_dat.c_str(), bad_dat.size(), 1, stream_bad);
			fclose(stream_bad);
		}


		mean_w /= numGood;
		mean_h /= numGood;

		int w = mean_w;
		int h = mean_h;

		int numNeg = numBad;

		int numStages = 14;
		double minHitRate = 0.995;

		//http://answers.opencv.org/question/4368/traincascade-error-bad-argument-can-not-get-new-positive-sample-the-most-possible-reason-is-insufficient-count-of-samples-in-given-vec-file/
		//numGood = numPos + (numStages - 1) * (1 - minHitRate) * numPos + S
		//numGood = numPos * (1.0 + (numStages - 1) * (1 - minHitRate) ) + S
		//numGood - S = numPos * (1.0 + (numStages - 1) * (1 - minHitRate) )

		int S = 20;
		int numPos = int(double(numGood - S) / (1.0 + double(numStages - 1) * (1.0 - minHitRate)));

		FILE * stream_bat = fopen((output_directory + "/create_examples_and_train.bat").c_str(), "wt");
		if (stream_bat)
		{
			fprintf(stream_bat, "opencv_createsamples.exe -info Good.dat -num %d -vec samples.vec -w %d -h %d\n", numGood, w, h);
			fprintf(stream_bat, "D:/cpplibs/opencv/git/opencv/build2013_x64/bin/Release/opencv_traincascade.exe -featureType %s -data haarcascade -vec samples.vec -bg Bad.dat -numStages %d -minHitRate %f -maxFalseAlarmRate 0.4 -numPos %d -numNeg %d -w %d -h %d -mode ALL -precalcValBufSize 128 -precalcIdxBufSize 128\n", 
				"LBP", numStages, minHitRate, numPos, numNeg, w, h);
			fclose(stream_bat);
		}

	}

	TrainCascadeDirectoryEnumerator(const char* fn, const char* ext)
		: DirectoryEnumerator(fn, ext)
	{
		numGood = 0;
		numBad = 0;

		mean_w = 0;
		mean_h = 0;

		bSaveBad = true;
	}

	virtual bool IsStop()
	{
		return false;
	}

	virtual void called_foo(char * fn_in, LARGE_INTEGER filesize)
	{
		std::string fn = fn_in;
		std::string _f = fn.substr(fn.find_last_of('/') + 1);
		std::string _n = _f.substr(0, 1);

		char letter = _n[0];

		std::string new_fn;
		if (letter == m_chLetterToTrain)
		{
			new_fn = output_directory_good + "/" + _f;
		}
		else if (bSaveBad)
		{
			new_fn = output_directory_bad + "/" + _f;
		}
		else
			return;

		if (FileExists(new_fn))
		{
			int n_pt = _f.find_last_of('.');_f = _f.substr(0, n_pt) + "_1" + _f.substr(n_pt);
			if (letter == m_chLetterToTrain)
			{
				new_fn = output_directory_good + "/" + _f;
			}
			else if (bSaveBad)
			{
				new_fn = output_directory_bad + "/" + _f;
			}
			else
				return;
		}

		bool copied = CopyFile(fn_in, new_fn.c_str(), FALSE) != FALSE;
		if (copied)
		{
			if (letter == m_chLetterToTrain)
			{
				cv::Mat picture = cv::imread(fn);
				char str[256];
				sprintf(str, " 1 0 0 %d %d\n\0", picture.size().width, picture.size().height);
				good_dat += "Good\\" + _f + str;

				numGood++;
				mean_w += picture.size().width;
				mean_h += picture.size().height;
			}
			else if (bSaveBad)
			{
				bad_dat += "Bad\\" + _f + "\n";
				numBad++;
			}
		}
	}
};


class TrainDigitsFannDirectoryEnumerator
	: public DirectoryEnumerator
{
	std::string output_directory;
	std::string output_directory_train;

	std::string good_dat;
	std::string bad_dat;
	std::string train_bat;

	int numTrain;
	int numGood;
	int mean_w, mean_h;

public:

	bool bSaveBad;

	void setOutputDirectory(std::string dir)
	{
		output_directory = dir;
		CreateDirectoryA(dir.c_str(), NULL);
		output_directory_train = output_directory + "/TrainDigits";

		CreateDirectoryA(output_directory_train.c_str(), NULL);
	}

	void save()
	{
		FILE * stream_good = fopen((output_directory + "/TrainDigits.dat").c_str(), "wt");
		if (stream_good)
		{
			fwrite(good_dat.c_str(), good_dat.size(), 1, stream_good);
			fclose(stream_good);
		}

		mean_w /= numGood;
		mean_h /= numGood;

		int w = mean_w;
		int h = mean_h;

		FILE * stream_bat = fopen((output_directory + "/createdigitsfanndataset.bat").c_str(), "wt");
		if (stream_bat)
		{
			fprintf(stream_bat, "D:/cpplibs/opencv/git/opencv/build2013_x64/bin/Release/opencv_createfanndataset.exe -info TrainDigits.dat -num %d -vec samplesDigits.train -w %d -h %d\n", numGood, w, h);
			fclose(stream_bat);
		}
	}

	TrainDigitsFannDirectoryEnumerator(const char* fn, const char* ext)
		: DirectoryEnumerator(fn, ext)
	{
		numTrain = 0;
		numGood = 0;

		mean_w = 0;
		mean_h = 0;

		bSaveBad = true;
	}

	virtual bool IsStop()
	{
		return false;
	}

	virtual void called_foo(char * fn_in, LARGE_INTEGER filesize)
	{
		std::string fn = fn_in;
		std::string _f = fn.substr(fn.find_last_of('/') + 1);
		std::string _n = _f.substr(0, 1);

		char letter = _n[0];

		bool isDigit = letter >= '0' && letter <= '9';

		std::string new_fn;
		if (isDigit || bSaveBad)
		{
			new_fn = output_directory_train + "/" + _f;
		}
		else if (bSaveBad)
		{
			new_fn = output_directory_train + "/" + _f;
		}
		else
			return;

		if (FileExists(new_fn))
		{
			int n_pt = _f.find_last_of('.'); _f = _f.substr(0, n_pt) + "_1" + _f.substr(n_pt);

			if (isDigit || bSaveBad)
			{
				new_fn = output_directory_train + "/" + _f;
			}
			else if (bSaveBad)
			{
				new_fn = output_directory_train + "/" + _f;
			}
			else
				return;
		}

		bool copied = CopyFile(fn_in, new_fn.c_str(), FALSE) != FALSE;
		if (copied)
		{
			if (isDigit || bSaveBad)
			{
				cv::Mat picture = cv::imread(fn);
				char str[256];
				sprintf(str, " 1 0 0 %d %d %d\n\0", picture.size().width, picture.size().height, isDigit ? int(letter - '0') : -9);
				good_dat += "TrainDigits\\" + _f + str;

				numTrain++;
				if (isDigit)
				{
					numGood++;
					mean_w += picture.size().width;
					mean_h += picture.size().height;
				}
			}
		}
	}
};


class TrainFannDirectoryEnumerator
	: public DirectoryEnumerator
{
	std::string output_directory;
	std::string output_directory_train;

	std::string good_dat;
	std::string bad_dat;
	std::string train_bat;

	char m_chLetterToTrain;

	int numTrain;
	int numGood;
	int mean_w, mean_h;

public:

	bool bSaveBad;

	void setLetterToTrain(char ch)
	{
		m_chLetterToTrain = ch;
	}

	void setOutputDirectory(std::string dir)
	{
		output_directory = dir;
		CreateDirectoryA(dir.c_str(), NULL);
		output_directory_train = output_directory + "/Train";

		CreateDirectoryA(output_directory_train.c_str(), NULL);
	}

	void save()
	{
		FILE * stream_good = fopen((output_directory + "/Train.dat").c_str(), "wt");
		if (stream_good)
		{
			fwrite(good_dat.c_str(), good_dat.size(), 1, stream_good);
			fclose(stream_good);
		}

		mean_w /= numGood;
		mean_h /= numGood;

		int w = mean_w;
		int h = mean_h;

		FILE * stream_bat1 = fopen((output_directory + "/createfanndataset.bat").c_str(), "wt");
		if (stream_bat1)
		{
			fprintf(stream_bat1, "D:/cpplibs/opencv/git/opencv/build2013_x64/bin/Release/opencv_createfanndataset.exe -info Train.dat -num %d -vec samples.train -w %d -h %d\n", numTrain, w, h);
			fclose(stream_bat1);
		}

		FILE * stream_bat2 = fopen((output_directory + "/trainfann.bat").c_str(), "wt");
		if (stream_bat2)
		{
			fprintf(stream_bat2, "D:/cpplibs/fann/git/code/bin/simple_train_double.exe samples.train %c.net\n", m_chLetterToTrain);
			fclose(stream_bat2);
		}
	}

	TrainFannDirectoryEnumerator(const char* fn, const char* ext)
		: DirectoryEnumerator(fn, ext)
	{
		numTrain = 0;
		numGood = 0;

		mean_w = 0;
		mean_h = 0;

		bSaveBad = true;
	}

	virtual bool IsStop()
	{
		return false;
	}

	virtual void called_foo(char * fn_in, LARGE_INTEGER filesize)
	{
		std::string fn = fn_in;
		std::string _f = fn.substr(fn.find_last_of('/') + 1);
		std::string _n = _f.substr(0, 1);

		char letter = _n[0];

		std::string new_fn;
		if (letter == m_chLetterToTrain)
		{
			new_fn = output_directory_train + "/" + _f;
		}
		else if (bSaveBad)
		{
			new_fn = output_directory_train + "/" + _f;
		}
		else
			return;

		if (FileExists(new_fn))
		{
			int n_pt = _f.find_last_of('.'); _f = _f.substr(0, n_pt) + "_1" + _f.substr(n_pt);

			if (letter == m_chLetterToTrain)
			{
				new_fn = output_directory_train + "/" + _f;
			}
			else if (bSaveBad)
			{
				new_fn = output_directory_train + "/" + _f;
			}
			else
				return;
		}

		bool copied = CopyFile(fn_in, new_fn.c_str(), FALSE) != FALSE;
		if (copied)
		{
			if (letter == m_chLetterToTrain || bSaveBad)
			{
				cv::Mat picture = cv::imread(fn);
				char str[256];
				sprintf(str, " 1 0 0 %d %d %d\n\0", picture.size().width, picture.size().height, int(letter == m_chLetterToTrain));
				good_dat += "Train\\" + _f + str;

				numTrain++;
				if (letter == m_chLetterToTrain)
				{
					numGood++;
					mean_w += picture.size().width;
					mean_h += picture.size().height;
				}
			}
		}
	}
};



cv::Mat smoothNoise2(const cv::Mat image, cv::Scalar bgcolor = 255);
void do_find_captcha(std::string& fn_in, cv::Mat& src_gray, cv::RNG& rng, bool debug_plots, std::string& fn_out);

class CascadeClassifierDirectoryEnumerator
	: public DirectoryEnumerator

{
	UINT m_files_number;
	UINT m_total_files_number;
	vector<string> chars;
	vector<string> cascadeNames;
	vector<string> annNames;
	vector<cv::Size> annImgSizes;
	bool toWriteImageBack = false;

	int m_max_width, m_max_height;
	int m_ncols, m_nrows;


	std::vector<struct fann *> anns;

public:

	cv::Mat common_picture;

	CascadeClassifierDirectoryEnumerator(const char* fn, const char* ext)
		: DirectoryEnumerator(fn, ext)
		, m_files_number(0)
		, m_total_files_number(0)
		, toWriteImageBack(false)
	{
	}

	void loadAnns()
	{
		anns.resize(annNames.size());
		for (size_t n = 0; n < annNames.size(); n++)
		{
			anns[n] = fann_create_from_file(annNames[n].c_str());
			if (!anns[n])
			{
				printf("Error creating ann --- ABORTING.\n");
				return;
			}
		}
	}

	void freeAnns()
	{
		for (size_t n = 0; n < annNames.size(); n++)
		{
			fann_destroy(anns[n]);
		}
	}


	void set(UINT total_files, int max_width, int max_height)
	{
		m_max_width = max_width;
		m_max_height = max_height;

		//m_ncols = m_nrows * double(max_height) / max_width;
		//m_ncols * m_nrows = total_files
		//m_nrows * m_nrows * double(max_height) / max_width = total_files
		//m_nrows * m_nrows = double (total_files * max_width) / max_height

		m_nrows = (int)ceil(sqrt(double(total_files * max_width) / max_height));
		m_ncols = (int)ceil(double(total_files) / m_nrows);

		int width = m_ncols * m_max_width;
		int height = m_nrows * m_max_height;

		common_picture = cv::Mat::zeros(cv::Size(width, height), CV_8UC3);
	}

	void SetToWriteImageBack(){ toWriteImageBack = true; }

	void AddCascadName(const char* _cascadeName)
	{
		cascadeNames.push_back(_cascadeName);
	}

	void AddAnnName(const char* _annName)
	{
		annNames.push_back(_annName);
	}

	void AddAnnNameImgSize(cv::Size& _annImgSize)
	{
		annImgSizes.push_back(_annImgSize);
	}

	void AddChar(const char* _char)
	{
		chars.push_back(_char);
	}

	virtual bool IsStop()
	{
		return false;
	}

	virtual void called_foo(char * fn_in, LARGE_INTEGER filesize)
	{
		cv::Mat src = cv::imread(fn_in);
		if (src.empty())
			return;

		int t = src.type();

		cv::Mat src_gray;

		if (src.channels() == 3 || src.channels() == 4)
			cvtColor(src, src_gray, CV_BGR2GRAY);
		else
			src_gray = src.clone();

		cv::Mat src3 = src.clone();

#if 0
		cv::Mat src_gray2 = src_gray.clone();

		//equalizeHist(src_gray2, src_gray2);


		src_gray2 = smoothNoise2(src_gray2);
		src_gray2 = smoothNoise2(src_gray2);


		cv::threshold(src_gray2, src_gray2, 0, 255, CV_THRESH_BINARY_INV);
		cv::dilate(src_gray2, src_gray2, 0);
		cv::erode(src_gray2, src_gray2, 0);

		//cv::imshow("src_gray", src_gray);
		//cv::waitKey();

		std::vector<std::vector<cv::Point> > contours;
		std::vector<std::vector<cv::Point> > contours2;
		std::vector<cv::Vec4i> hierarchy;

		findContours(src_gray2.clone(), contours, hierarchy, CV_RETR_TREE, CV_CHAIN_APPROX_SIMPLE, cv::Point(0, 0));

		std::vector<cv::Point> contours_all;

		cv::RNG rng;

		for (size_t i = 0; i < contours.size(); i++)
		{
			cv::Rect shapeRect = cv::boundingRect(contours[i]);
			if (shapeRect.tl().x <= 1)
				continue;
			if (shapeRect.tl().y <= 1)
				continue;

			contours2.push_back(contours[i]);

			for (size_t j = 0; j < contours[i].size(); ++j)
				contours_all.push_back(contours[i][j]);
		}


		cv::Rect allShapeRect = cv::boundingRect(contours_all);
		for (size_t i = 0; i < contours2.size(); i++)
		{
			do_convex_hull(contours2[i], src3);
		}
//		do_convex_hull(contours_all, src3);

		cv::rectangle(src3, allShapeRect, cv::Scalar(200, 200, 200));
		for (int i = 1; i < 6; ++i)
		{
			int x = int(i * (double(allShapeRect.width) / 6));
			cv::line(src3, cv::Point(allShapeRect.x + x, allShapeRect.tl().y), cv::Point(allShapeRect.x + x, allShapeRect.br().y - 1), cv::Scalar(200, 200, 200));
		}

		apply_cascade(fn_in, allShapeRect, src_gray, src3);
#else
		int margx = 5;
		int margy = 5;
		cv::Rect allShapeRect = cv::Rect(margx, margy, src3.cols - 2 * margx, src3.rows - 2 * margy);
		apply_cascade(fn_in, allShapeRect, src_gray, src3);
#endif
		int n_col = m_files_number / m_nrows;
		int n_row = m_files_number - n_col * m_nrows;

		cv::Rect ROI;
		ROI.x = n_col * m_max_width;
		ROI.y = n_row * m_max_height;

		ROI.width = src3.size().width;
		ROI.height = src3.size().height;

		src3.copyTo(common_picture(ROI));
		


			++m_files_number;


		++m_total_files_number;
	}


	void apply_cascade(char * fn_in, const cv::Rect& allShapeRect, cv::Mat& src_gray, cv::Mat& src3)
	{
		bool cropByallShapeRect = true;
		cv::Rect allShapeRect4Crop = allShapeRect;

		int marg = 3;
		allShapeRect4Crop.x -= marg;
		allShapeRect4Crop.y -= marg;
		allShapeRect4Crop.width += 2 * marg;
		allShapeRect4Crop.height += 2 * marg;



		char str[1024];



		if (cropByallShapeRect)
			src_gray = src_gray(allShapeRect4Crop);
		int jj = 0;
		for (size_t n = 0; n < cascadeNames.size(); n++)
		{
			CascadeClassifier cascade;
			if (!cascade.load(cascadeNames[n]))
			{
				printf("--(!)Error loading\n");
				return;
			}

			cv::Size szCascade = cascade.getOriginalWindowSize();

			int h = allShapeRect4Crop.height;
			double dw = double(szCascade.width) / szCascade.height;
			int w = int(h * dw);			
			double p = 0.4;

			//p = 0.1;
			//w = 50;
			//h = 100;

			cv::Size minSize = Size(int(p*w), int(p*h));
			cv::Size maxSize = Size(w, h);

			std::vector<Rect> digit_rects;
			cascade.detectMultiScale(src_gray, digit_rects, 1.05, 3, 0 | CV_HAAR_SCALE_IMAGE | CV_HAAR_DO_CANNY_PRUNING, minSize, maxSize);

			unsigned int num_input, num_output, num_data, i, j;
			unsigned int line = 1;

			num_data = digit_rects.size();

			if (num_data == 0)
				continue;

			fann_type *calc_out;
			int ret = 0;

			struct fann_train_data * data;


			num_input = anns[n]->num_input;
			num_output = 1;

			data = fann_create_train(num_data, num_input, num_output);
			if (!data)
			{
				printf("Error creating data --- ABORTING.\n");
				return;
			}


			IplImage* sample;
			sample = cvCreateImage(this->annImgSizes[n], IPL_DEPTH_8U, 1);

			for (i = 0; i != num_data; i++)
			{
				Mat digit = src_gray(digit_rects[i]);

				cvResize(&(IplImage)digit, sample, digit_rects[i].width >= sample->width &&
					digit_rects[i].height >= sample->height ? CV_INTER_AREA : CV_INTER_LINEAR);

				CvMat* mat, stub;
				int r, c;
				short tmp;
#if 1
				mat = cvGetMat(sample, &stub);
				//printf("mat->rows=%d, mat->cols=%d %d\n", mat->rows, mat->cols, mat->rows * mat->cols);
				//TODO test sizes here
				j = 0;
				for (r = 0; r < mat->rows; r++)
				{
					for (c = 0; c < mat->cols; c++)
					{
						tmp = (short)(CV_MAT_ELEM(*mat, uchar, r, c));
						//data->input[i][j] = tmp;
						data->input[i][j] = double(255 - tmp) / 255;

						++j;
					}
				}
#endif
				//


				//sprintf(str, "%s", chars[n].c_str());
				cv::putText(src3, chars[n].c_str(), allShapeRect4Crop.tl() + digit_rects[i].tl(), cv::FONT_HERSHEY_SIMPLEX, 0.5, cv::Scalar(0, 0, 255));



				if (!cropByallShapeRect)
				{
					int max_shift_x = int( double(digit_rects[i].width) / 3);
					int max_shift_y = int( double(digit_rects[i].height) / 3);

					if (allShapeRect.tl().x - digit_rects[i].tl().x > max_shift_x)
					{
						cv::rectangle(src3, digit_rects[i], cv::Scalar(0, 0, 255));
						continue;
					}
					if (digit_rects[i].tl().y - allShapeRect.tl().y > max_shift_y)
					{
						cv::rectangle(src3, digit_rects[i], cv::Scalar(0, 0, 255));
						continue;
					}

					if (digit_rects[i].br().x - allShapeRect.br().x > max_shift_x)
					{
						cv::rectangle(src3, digit_rects[i], cv::Scalar(0, 0, 255));
						continue;
					}
					if (allShapeRect.br().y - digit_rects[i].br().y > max_shift_y)
					{
						cv::rectangle(src3, digit_rects[i], cv::Scalar(0, 0, 255));
						continue;
					}
				}
				else
				{
					if (digit_rects[i].tl().y > marg+2)
					{
						cv::rectangle(src3, cv::Rect(allShapeRect4Crop.tl() + digit_rects[i].tl(), digit_rects[i].size()), cv::Scalar(0, 0, 255));
						//continue;
					}
					if (allShapeRect4Crop.height - digit_rects[i].br().y > marg+2)
					{
						cv::rectangle(src3, cv::Rect(allShapeRect4Crop.tl() + digit_rects[i].tl(), digit_rects[i].size()), cv::Scalar(0, 0, 125));
						//continue;
					}
				}


				/*}
				for (i = 0; i < fann_length_train_data(data); i++)
				{*/

				fann_reset_MSE(anns[n]);
				calc_out = fann_test(anns[n], data->input[i], data->output[i]);
				printf("%s -> %f\n", fn_in, calc_out[0]);

				double epsilon = 0.85;
				if (fabs(calc_out[0] - 1) > epsilon)
				{
					if (!cropByallShapeRect)
						cv::rectangle(src3, digit_rects[i], cv::Scalar(255, 0, 255));
					else
						cv::rectangle(src3, cv::Rect(allShapeRect4Crop.tl() + digit_rects[i].tl(), digit_rects[i].size()), cv::Scalar(255, 0, 0));
				}
				else
				{
					if (!cropByallShapeRect)
						cv::rectangle(src3, digit_rects[i], cv::Scalar(0, 255, 0));
					else
						cv::rectangle(src3, cv::Rect(allShapeRect4Crop.tl() + digit_rects[i].tl(), digit_rects[i].size()), cv::Scalar(0, 255, 0));
				}

				int pw = int(floor(log10(calc_out[0])));
				double m = calc_out[0] / pow(10, pw);
				int M = int(m);

				//sprintf(str, "%de%d", M, pw);
				sprintf_s(str, 1024, "%1.1f", log10(calc_out[0]));
				cv::putText(src3, str, cv::Point(allShapeRect4Crop.tl().x + digit_rects[i].tl().x, src3.size().height - 1 - jj*3), cv::FONT_HERSHEY_SIMPLEX, 0.3, cv::Scalar(0, 0, 255));
				++jj;
			}


			printf("Cleaning up.\n");

			fann_destroy_train(data);


			cvReleaseImage(&sample);

			//cv::imshow("digits", src3);
			//cv::waitKey();
		}
	}

	void do_convex_hull(std::vector<cv::Point>& c, cv::Mat& drawing)
	{
		vector < cv::Point > hull;
		cv::convexHull(cv::Mat(c), hull);

		std::vector<int> ihull;
		cv::convexHull(cv::Mat(c), ihull);


		//double area = contourArea(Mat(msers[i]));
		//double hullArea = contourArea(Mat(hull));
		//double ratio = area / hullArea;

		//printf("area=%f hullArea=%f ratio=%f\n", area, hullArea, ratio);


		//cv::Mat drawing = cv::Mat::zeros(size, CV_8UC3);
//		cv::polylines(drawing, c, true, cv::Scalar(0xCC, 0xCC, 0xCC), 1);
		cv::polylines(drawing, hull, true, cv::Scalar(0xFF, 0x20, 0x20), 1);


		int ptnum = c.size();
		int nn = cv::Mat(ihull).checkVector(1, CV_32S);
		if (nn > 2 && ptnum > 3)
		{
			std::vector<cv::Vec4i> defects;
			convexityDefects(c, ihull, defects);

			std::vector<cv::Vec4i>::iterator d = defects.begin();
			while (d != defects.end())
			{
				cv::Vec4i& v = (*d); d++;
				int startidx = v[0]; cv::Point ptStart(c[startidx]);
				int endidx = v[1]; cv::Point ptEnd(c[endidx]);
				int faridx = v[2]; cv::Point ptFar(c[faridx]);

//				cv::circle(drawing, ptStart, 1, cv::Scalar(0x02, 0x60, 0xFF), 2);
//				cv::circle(drawing, ptEnd, 1, cv::Scalar(0xFF, 0x60, 0x02), 2);
//				cv::circle(drawing, ptFar, 1, cv::Scalar(0x60, 0xFF, 0x02), 2);

				//cv::Mat drawing2 = cv::Mat::zeros(size, CV_8UC3);
				/*cv::polylines(drawing2, c, true, cv::Scalar(0xCC, 0xCC, 0xCC), 1);
				cv::polylines(drawing2, hull, true, cv::Scalar(0xFF, 0x20, 0x20), 1);

				cv::circle(drawing2, ptStart, 1, cv::Scalar(0x02, 0x60, 0xFF), 2);
				cv::circle(drawing2, ptEnd, 1, cv::Scalar(0xFF, 0x60, 0x02), 2);
				cv::circle(drawing2, ptFar, 1, cv::Scalar(0x60, 0xFF, 0x02), 2);*/

				//cv::imshow("hull2", drawing2);
				//cv::waitKey();

			}
		}

		//cv::imshow("hull", drawing);
		//cv::waitKey();
	}

	UINT files_number(){ return m_files_number; }
	UINT total_files_number(){ return m_total_files_number; }
};



class CalcFilesDirectoryEnumerator
	: public DirectoryEnumerator

{
	UINT m_files_number;
	UINT m_total_files_number;
	int m_max_width, m_max_height;

public:
	CalcFilesDirectoryEnumerator(const char* fn, const char* ext)
		: DirectoryEnumerator(fn, ext)
		, m_files_number(0)
		, m_total_files_number(0)
		, m_max_width(0)
		, m_max_height(0)
	{
	}


	virtual bool IsStop()
	{
		return false;
	}

	virtual void called_foo(char * fn_in, LARGE_INTEGER filesize)
	{
		cv::Mat src = cv::imread(fn_in);
		if (src.empty())
			return;

		if (m_max_width < src.size().width)
			m_max_width = src.size().width;

		if (m_max_height < src.size().height)
			m_max_height = src.size().height;

		++m_files_number;
		++m_total_files_number;
	}

	int max_width() { return m_max_width; }
	int max_height() { return m_max_height; }

	UINT files_number(){ return m_files_number; }
	UINT total_files_number(){ return m_total_files_number; }
};



#if 0
//int _integral1 = 75;// 250;
//int _integral2 = 56;// 125;
int _integral1 = 250;
int _integral2 = 125;
cv::Mat model_image;
cv::Mat canny_output_i;



void ApplyMyCannyOnModelImage()
{
	double integral1 = double(_integral1) / 10;
	double integral2 = double(_integral2) / 10;

	int aperture_size = 3;
	cv::MyCannyEx(model_image, canny_output_i, integral1/*25*/, integral2/*12.5*/, aperture_size, false);

	cv::imshow("model_image", model_image);
	cv::imshow("canny_output_i", canny_output_i);


	/// Find contours
	vector<vector<cv::Point> > contours;
	vector<cv::Vec4i> hierarchy;

	findContours(canny_output_i, contours, hierarchy, CV_RETR_TREE, CV_CHAIN_APPROX_SIMPLE, cv::Point(0, 0));

	if (1)//(debug_plots)
	{
		cv::RNG rng;

		/// Draw contours
		cv::Mat drawing = cv::Mat::zeros(canny_output_i.size(), CV_8UC3);
		for (int i = 0; i < contours.size(); i++)
		{
			cv::Scalar color = cv::Scalar(rng.uniform(0, 255), rng.uniform(0, 255), rng.uniform(0, 255));
			drawContours(drawing, contours, i, color, 2, 8, hierarchy, 0, cv::Point());
		}


		/// Show in a window
		cv::namedWindow("Contours", CV_WINDOW_AUTOSIZE);
		cv::imshow("Contours", drawing);
	}

}


void UpdateIntegral1(int k, void* /*z*/)
{
	_integral1 = k;
	ApplyMyCannyOnModelImage();
}

void UpdateIntegral2(int k, void* /*z*/)
{
	_integral2 = k;
	ApplyMyCannyOnModelImage();
}
#endif

void myAdaptiveThreshold(InputArray _src, OutputArray _dst, double maxValue,
	int method, int type, int blockSize, const char * smean, const char * sdelta);


class ApplyMyCannyDirectoryEnumerator
	: public DirectoryEnumerator
{
	std::string output_directory_my_canny;
	std::string output_directory_canny;
public:

	bool build_models;

	int threshold1;
	int threshold2;

	double integral1;
	double integral2;

	void setOutputDirectoryMyCanny(std::string dir)
	{
		output_directory_my_canny = dir;
		CreateDirectoryA(dir.c_str(), NULL);
	}

	void setOutputDirectoryCanny(std::string dir)
	{
		output_directory_canny = dir;
		CreateDirectoryA(dir.c_str(), NULL);
	}

	ApplyMyCannyDirectoryEnumerator(const char* fn, const char* ext)
		: DirectoryEnumerator(fn, ext)
	{
		threshold1 = 106;
		threshold2 = 206;

		integral1 = 25.0;
		integral2 = 12.5;

		build_models = true;
	}

	virtual bool IsStop()
	{
		return false;
	}

	void MyCannyVsCanny(const char * fn_in)
	{
		cv::Mat input_image;
		cv::Mat canny_output_i;
		cv::Mat my_canny_output_image;
		input_image = cv::imread(fn_in);

		cv::Mat my_adaptive_canny_output_image;
		cv::Mat my_canny_tests_output_image;

		int aperture_size = 3;
		cv::Canny(input_image, canny_output_i, threshold1, threshold2, aperture_size, false);
		cv::MyCannyEx(input_image, my_canny_output_image, integral1/*25*/, integral2/*12.5*/, aperture_size, false);

		if (!build_models)
		{
			cv::MyCannyTests(input_image, my_canny_tests_output_image, aperture_size, false);
			cv::MyAdaptiveCanny(input_image, my_adaptive_canny_output_image, aperture_size, false);
			cv::imshow("my_adaptive_canny_output_image", my_adaptive_canny_output_image);
		}

		cv::Mat src_gray;
		if (input_image.channels() == 3 || input_image.channels() == 4)
			cvtColor(input_image, src_gray, CV_BGR2GRAY);
		else
			src_gray = input_image.clone();
#if 1


		/// Detect edges using adaptive Threshold
		cv::Mat adaptive_threshold_output_gaussian;
		cv::Mat my_adaptive_threshold_output_gaussian;
		adaptiveThreshold(src_gray, adaptive_threshold_output_gaussian, 255, CV_ADAPTIVE_THRESH_GAUSSIAN_C, CV_THRESH_BINARY_INV, 21, 7);
		myAdaptiveThreshold(src_gray, my_adaptive_threshold_output_gaussian, 255, CV_ADAPTIVE_THRESH_GAUSSIAN_C, CV_THRESH_BINARY_INV, 21, "gaussian", "delta_gaussian");

		Mat adaptive_delta;
		adaptiveDelta(src_gray, adaptive_delta,
			CV_ADAPTIVE_THRESH_GAUSSIAN_C, 41,
			"gaussian2", "delta_gaussian2");

		cv::imshow("adaptive_threshold_output_gaussian", adaptive_threshold_output_gaussian);
		cv::imshow("my_adaptive_threshold_output_gaussian", my_adaptive_threshold_output_gaussian);

		//cv::Mat img_thresh_otsu;
		//cv::threshold(src_gray, img_thresh_otsu, 0, 255, CV_THRESH_BINARY | CV_THRESH_OTSU);
		//cv::imshow("img_thresh_otsu", img_thresh_otsu);
#endif

		cv::imshow("canny_output_i", canny_output_i);
		cv::imshow("my_canny_output_image", my_canny_output_image);


		std::string fn = fn_in;
		std::string _f = fn.substr(fn.find_last_of('/') + 1);
		std::string _n = _f.substr(0, 1);

		/*if (output_directory_my_canny.size()) {
			std::string path_my_canny = output_directory_my_canny + "\\" + _f;
			cv::imwrite(path_my_canny, my_canny_output_image);
		}*/

		/*if (output_directory_canny.size()) {
			std::string path_canny = output_directory_canny + "\\" + _f;
			cv::imwrite(path_canny, canny_output_i);
		}*/

		//if (input_image.channels() == 3 || input_image.channels() == 4)
		//	cvtColor(input_image, src_gray, CV_BGR2GRAY);
		//else
		//	src_gray = input_image.clone();

		int mlevel = 4;
		if (1)
		{
			cv::Mat regions;
			MultiOtsu(src_gray, regions, mlevel);
			cv::imshow("otsu_regions", regions);
		}

		if (1)
		{
			cv::Mat otsu_edges;
			cv::Mat src_gray_blur;
			//cv::GaussianBlur(src_gray, src_gray_blur, 3, 1);
			//cv::medianBlur(src_gray, src_gray_blur, 3);
			MultiOtsuEdges(src_gray, otsu_edges, mlevel);
			cv::imshow("otsu_edges", otsu_edges);
		}



		cv::Mat bin;
		if (build_models)
		{
			//threshold(src_gray, bin, 0, 255, THRESH_BINARY | THRESH_OTSU);

			cv::Mat multi_otso_thresholds;
			MultiOtsuThresholds(src_gray, multi_otso_thresholds, mlevel);
			int multi_otso_back_thresh = multi_otso_thresholds.ptr<int>()[mlevel - 1];
			threshold(src_gray, bin, multi_otso_back_thresh + 1, 255, THRESH_BINARY);
			cv::imshow("bin", bin);
		}



		/// Find contours
		vector<vector<cv::Point> > contours;
		vector<cv::Vec4i> hierarchy;

		if (build_models)
			findContours(bin, contours, hierarchy, CV_RETR_TREE, CV_CHAIN_APPROX_SIMPLE, cv::Point(0, 0));
		else
			findContours(my_adaptive_canny_output_image, contours, hierarchy, CV_RETR_TREE, CV_CHAIN_APPROX_SIMPLE, cv::Point(0, 0));

		cv::RNG rng;
		cv::Mat drawing = cv::Mat::zeros(input_image.size(), CV_8UC3);

		cv::imshow("Contours", drawing);

		if (0)//(debug_plots)
		{
			cv::RNG rng;

			/// Draw contours
			cv::Mat drawing = cv::Mat::zeros(my_adaptive_canny_output_image.size(), CV_8UC3);
			for (int i = 0; i < contours.size(); i++)
			{
				cv::Scalar color = cv::Scalar(rng.uniform(0, 255), rng.uniform(0, 255), rng.uniform(0, 255));
				drawContours(drawing, contours, i, color, 2, 8, hierarchy, 0, cv::Point());
			}


			/// Show in a window
			cv::namedWindow("Contours", CV_WINDOW_AUTOSIZE);
			cv::imshow("Contours", drawing);
		}

		if (!build_models)
			printf("");


		cv::imshow("input_image", input_image);
	}



	virtual void called_foo(char * fn_in, LARGE_INTEGER filesize)
	{
		MyCannyVsCanny(fn_in);
		if (!build_models)
			cv::waitKey();
	}
};



#if !HAVE_OPENCV_300

class ApplySiftDirectoryEnumerator
	: public DirectoryEnumerator
{
	std::string output_directory;
public:

	void setOutputDirectory(std::string dir)
	{
		output_directory = dir;
		CreateDirectoryA(dir.c_str(), NULL);
	}


	ApplySiftDirectoryEnumerator(const char* fn, const char* ext)
		: DirectoryEnumerator(fn, ext)
	{

	}

	virtual bool IsStop()
	{
		return false;
	}

	//http://stackoverflow.com/questions/5461148/sift-implementation-with-opencv-2-2

	void useSift(const char * fn_in)
	{
		Mat image = imread(fn_in);

		// Create smart pointer for SIFT feature detector.
		Ptr<FeatureDetector> featureDetector = FeatureDetector::create("SIFT");
		vector<KeyPoint> keypoints;

		// Detect the keypoints
		featureDetector->detect(image, keypoints); // NOTE: featureDetector is a pointer hence the '->'.

		//Similarly, we create a smart pointer to the SIFT extractor.
		Ptr<DescriptorExtractor> featureExtractor = DescriptorExtractor::create("SIFT");

		// Compute the 128 dimension SIFT descriptor at each keypoint.
		// Each row in "descriptors" correspond to the SIFT descriptor for each keypoint
		Mat descriptors;
		featureExtractor->compute(image, keypoints, descriptors);

		// If you would like to draw the detected keypoint just to check
		Mat outputImage;
		Scalar keypointColor = Scalar(255, 0, 0);     // Blue keypoints.
		drawKeypoints(image, keypoints, outputImage, keypointColor, DrawMatchesFlags::DEFAULT);

		namedWindow("Output");
		imshow("Output", outputImage);

	}

	void useSift2(const char * fn_in)
	{
		cv::Mat img = cv::imread(fn_in);
		cv::Size sz = cv::Size(img.cols, img.rows);

		sz.width /= 5;
		sz.height /= 5;

		cv::resize(img, img, sz);

		cv::SIFT sift;  //(10); //number of keypoints

		std::vector<cv::KeyPoint> key_points;

		cv::Mat descriptors, mascara;
		cv::Mat output_img;

		sift(img, mascara, key_points, descriptors);
		cv::drawKeypoints(img, key_points, output_img, Scalar::all(-1), 1|4);

		string winname = string("Image ") + fn_in;
		cv::namedWindow(winname);
		cv::imshow(winname, output_img);
	}




	virtual void called_foo(char * fn_in, LARGE_INTEGER filesize)
	{
		useSift2(fn_in);
		cv::waitKey();
	}
};

#endif

extern bool build_models;

int  main(int argc, const char * argv[])
{
	//orb_video();
	//mser_video();
	//CV_MserTest_run();

	//dense_optical_flow_video();
	//farneback_optical_flow_video();

	//CV_BackgroundSubtractorTest_run();
	//background_subtractor_GMG_video();
	//background_subtractor_MOG_video();
	//background_subtractor_MOG2_video();

	//find_contours_video();
	//find_contours();
	//find_contours2();

	//test_cornerEigenValsAndVecs();

#if HAVE_OPENCV_300
//	test_superres();
#endif
	//main_lineFittingTest();
	//main_eyeLike();

	//test_getAffineTransformOverdetermined();

	//int nr_superpixels = 300;
	//int nc = 3;
	//main_slic("D:\\_C++\\testHOG_workcopy\\testHOG\\SLIC-Superpixels\\dog.png", nr_superpixels, nc);
	//main_slic("D:\\__files\\eye_roi_a.png", nr_superpixels, nc);
	//simplify_converter_test();

	cv::Point2d a = cv::Point2d(-1,0);
	cv::Point2d b = cv::Point2d(0, 0);
	cv::Point2d c = cv::Point2d(1, -1);

	double angle = 180 * getAngleABC(a, b, c) / pi;
	printf("a(%lf %lf) b(%lf %lf) c(%lf %lf) angle=%lf\n", a.x, a.y, b.x, b.y, c.x, c.y, angle);

	//return 0;


	if (0)
	{
		CascadeDirectoryEnumerator work_enumerator("*", "MTS");
		work_enumerator.SetIsInputImage(false);
		work_enumerator.SetIsInputVideo(true);
		work_enumerator.SetToWriteImageBack();
		work_enumerator.SetCascadName("haarcascades/cars3.xml");
		//work_enumerator.SetCascadName("hogcascades/hogcascade_pedestrians.xml");
		work_enumerator.enumerate("D:/_C++/stereo/testvideo2");
		int success = work_enumerator.files_number();
		int total = work_enumerator.total_files_number();

		printf("total = %d success=%d\n", total, success);
	}

	/*if (0)
	{
		ApplySiftDirectoryEnumerator worker4("*", "jpg");
		worker4.enumerate("N:\\OLD\\cam\\xx\\40");
	}*/


	if (0)
	{
		ApplyMyCannyDirectoryEnumerator worker2("*", "jpg");
		worker2.build_models = false;
		worker2.enumerate("D:\\_C++\\contour_recognition\\hvostatiki");

		ApplyMyCannyDirectoryEnumerator worker3("*", "png");
		worker3.enumerate("D:\\_C++\\contour_recognition\\hvostatiki");

		return 0;
	}
//#if !HAVE_OPENCV_300
	if (1)
	{

		ContourRecognitionDirectoryEnumerator worker4("*", "jpg");
		/*worker4.*/build_models = true;
		//worker4.enumerate("D:\\_C++\\recognition\\������ � �������\\������ ���� � ����������� 640�171");
		worker4.enumerate("D:\\_C++\\recognition\\������ � �������\\new");

		initClusterisator();
		initPlots();

		ContourRecognitionDirectoryEnumerator worker("*", "jpg");
		/*worker.*/build_models = false;
		//worker.setOutputDirectoryMyCanny(std::string("D:\\_C++\\recognition\\inframes_mycanny_250_125_output"));
		//worker.setOutputDirectoryCanny(std::string("D:\\_C++\\recognition\\inframes_canny_106_206_output"));
		worker.enumerate("D:\\_C++\\recognition\\inframes");	

		return 0;
	}
//#endif

#if 0
	if (0)
	{
		namedWindow("model_image", CV_WINDOW_AUTOSIZE);
		namedWindow("canny_output_i", CV_WINDOW_AUTOSIZE);

		createTrackbar("integral1", "canny_output_i", &_integral1, 500, UpdateIntegral1, NULL);
		createTrackbar("integral2", "canny_output_i", &_integral2, 500, UpdateIntegral2, NULL);


		TrainModelLoader loader;
		initAamToolboxPath();

		std::vector<ModelPathType> modelPaths;
		if (!loadModelInfo(modelInfoListFile(), modelPaths))
		{
			std::cout << "%TEST_FAILED% time=0 testname=testTrainGray (aamestimatorTest) message=Can' load model info list" << std::endl;
			return -1;
		}

		for (size_t i = 0; i < modelPaths.size(); i++)
		{
			loader.load(modelPaths[i].first, modelPaths[i].second);
		}

		for (size_t i = 0; i < loader.getModels().size(); ++i)
		{
			model_image = loader.getModels()[i].getImage();
			ApplyMyCannyOnModelImage();
			cv::waitKey();
		}
	}
#endif


	if (0)
	{
		for (char ch = '0'; ch <= '9'; ++ch)
		{
			TrainFannDirectoryEnumerator       work_enumerator2("*", "bmp");
			TrainCascadeDirectoryEnumerator    work_enumerator3("*", "bmp");
			work_enumerator2.setOutputDirectory(std::string("D:/_C++/captcha/Captchure_pics/images_egrul/train2/_0_train_") + ch);
			work_enumerator3.setOutputDirectory(std::string("D:/_C++/captcha/Captchure_pics/images_egrul/train2/_0_train_") + ch);
			work_enumerator2.setLetterToTrain(ch);
			work_enumerator3.setLetterToTrain(ch);
			for (int i = 0; i < 50; ++i)
			{
				char str[1028];
				sprintf(str, "D:/_C++/captcha/Captchure_pics/images_egrul/images/%d/Cropper", i);
				work_enumerator2.enum_files(str);
				work_enumerator3.enum_files(str);

				sprintf(str, "D:/_C++/captcha/Captchure_pics/images_egrul/bad/%d/Cropper", i);
				work_enumerator2.enumerate(str);
				work_enumerator3.enumerate(str);

				if (i >= 25){
					work_enumerator2.bSaveBad = false;
					work_enumerator3.bSaveBad = false;
				}
			}
			work_enumerator2.save();
			work_enumerator3.save();
		}
	}

	if (0)
	{

		{
			TrainDigitsFannDirectoryEnumerator work_enumerator1("*", "bmp");
			work_enumerator1.setOutputDirectory(std::string("D:/_C++/captcha/Captchure_pics/images_egrul/train2/_0_train_digits"));
			for (int i = 0; i < 50; ++i)
			{
				char str[1028];
				sprintf(str, "D:/_C++/captcha/Captchure_pics/images_egrul/images/%d/Cropper", i);
				work_enumerator1.enum_files(str);

				sprintf(str, "D:/_C++/captcha/Captchure_pics/images_egrul/bad/%d/Cropper", i);
				work_enumerator1.enumerate(str);

				if (i >= 10){
					work_enumerator1.bSaveBad = false;
				}
			}
			work_enumerator1.save();
		}
	}

	if (0)
	{
		for (char ch = '0'; ch <= '9'; ++ch)
		{
			TrainFannDirectoryEnumerator work_enumerator("*", "bmp");
			work_enumerator.setOutputDirectory(std::string("D:/_C++/captcha/Captchure_pics/images_egrul/train3/train_") + ch);
			work_enumerator.setLetterToTrain(ch);
			for (int i = 0; i < 50; ++i)
			{
				char str[1028];
				sprintf(str, "D:/_C++/captcha/Captchure_pics/images_egrul/images/%d/Cropper", i);
				work_enumerator.enum_files(str);
				sprintf(str, "D:/_C++/captcha/Captchure_pics/images_egrul/images/%d/Cropper/Cropper", i);
				work_enumerator.enum_files(str);
				sprintf(str, "D:/_C++/captcha/Captchure_pics/images_egrul/bad/%d/Cropper", i);
				work_enumerator.enumerate(str);

				//if (i >= 10)
				//	work_enumerator.bSaveBad = false;
			}
			work_enumerator.save();
		}
	}

	if (0)
	{
		for (char ch = '0'; ch <= '9'; ++ch)
		{
			TrainCascadeDirectoryEnumerator work_enumerator("*", "bmp");
			work_enumerator.setOutputDirectory(std::string("D:/_C++/captcha/Captchure_pics/images_egrul/train3/train_") + ch);
			work_enumerator.setLetterToTrain(ch);
			for (int i = 0; i < 50; ++i)
			{
				char str[1028];
				sprintf(str, "D:/_C++/captcha/Captchure_pics/images_egrul/images/%d/Cropper", i);
				work_enumerator.enum_files(str);
				sprintf(str, "D:/_C++/captcha/Captchure_pics/images_egrul/images/%d/Cropper/Cropper", i);
				work_enumerator.enum_files(str);
				sprintf(str, "D:/_C++/captcha/Captchure_pics/images_egrul/bad/%d/Cropper", i);
				work_enumerator.enumerate(str);

				//if (i >= 10)
				//	work_enumerator.bSaveBad = false;
			}
			work_enumerator.save();
		}
	}


	if (0)
	{
		for (char ch = '0'; ch <= '9'; ++ch)
		{
			TrainCascadeDirectoryEnumerator work_enumerator("*", "png");
			work_enumerator.setOutputDirectory(std::string("D:/_C++/captcha/_8_train_") + ch);
			work_enumerator.setLetterToTrain(ch);
			work_enumerator.enumerate("D:/_C++/captcha/Captchure_pics/blue/segmented");
			work_enumerator.enumerate("D:/_C++/captcha/Captchure_pics/green/segmented");
			work_enumerator.enumerate("D:/_C++/captcha/Captchure_pics/red/segmented");
			work_enumerator.enumerate("D:/_C++/captcha/Captchure_pics/yellow/segmented");
			work_enumerator.enumerate("D:/_C++/captcha/Captchure_pics/yellow/segments_validset");
			work_enumerator.save();
		}
	}

	if (1)
	{
		//std::string work_directory = "D:/_C++/captcha/Captchure_pics/images_egrul/digitized_bmp";
		std::string work_directory = "D:\\_C++\\avto_nomera_recognition\\velo";

		//CalcFilesDirectoryEnumerator calc_enumerator("*", "bmp");
		CalcFilesDirectoryEnumerator calc_enumerator("*", "jpg");
		calc_enumerator.enumerate(work_directory.c_str());
		int total = calc_enumerator.total_files_number();
		int max_width = calc_enumerator.max_width();
		int max_height = calc_enumerator.max_height();

		//CascadeClassifierDirectoryEnumerator work_enumerator("*", "bmp");
		CascadeClassifierDirectoryEnumerator work_enumerator("*", "jpg");
		work_enumerator.set(total, max_width, max_height);


		work_enumerator.AddCascadName("D:/_C++/captcha/Captchure_pics/images_egrul/train3/train_0/haarcascade/cascade.xml");
		work_enumerator.AddAnnName("D:/_C++/captcha/Captchure_pics/images_egrul/train3/train_0/___0.net");
		work_enumerator.AddAnnNameImgSize(cv::Size(30, 39));
		work_enumerator.AddChar("0");

		work_enumerator.AddCascadName("D:/_C++/captcha/Captchure_pics/images_egrul/train3/train_1/haarcascade/cascade.xml");
		work_enumerator.AddAnnName("D:/_C++/captcha/Captchure_pics/images_egrul/train3/train_1/___1.net");
		work_enumerator.AddAnnNameImgSize(cv::Size(29, 39));
		work_enumerator.AddChar("1");


		work_enumerator.AddCascadName("D:/_C++/captcha/Captchure_pics/images_egrul/train3/train_2/haarcascade/cascade.xml");
		work_enumerator.AddAnnName("D:/_C++/captcha/Captchure_pics/images_egrul/train3/train_2/___2.net");
		work_enumerator.AddAnnNameImgSize(cv::Size(32, 40));
		work_enumerator.AddChar("2");


		work_enumerator.AddCascadName("D:/_C++/captcha/Captchure_pics/images_egrul/train2/_0_train_3/haarcascade/cascade.xml");
		work_enumerator.AddAnnName("D:/_C++/captcha/Captchure_pics/images_egrul/train3/train_3/___3.net");
		work_enumerator.AddAnnNameImgSize(cv::Size(31, 40));
		work_enumerator.AddChar("3");

/*		work_enumerator.AddCascadName("D:/_C++/captcha/Captchure_pics/images_egrul/train/_2_train_3/haarcascade/cascade.xml");
		work_enumerator.AddAnnName("D:/_C++/captcha/Captchure_pics/images_egrul/train3/train_3/3.net");
		work_enumerator.AddAnnNameImgSize(cv::Size(31, 40));
		work_enumerator.AddChar("3'");

		work_enumerator.AddCascadName("D:/_C++/captcha/Captchure_pics/images_egrul/train/_2_train_4/haarcascade/cascade.xml");
		work_enumerator.AddAnnName("D:/_C++/captcha/Captchure_pics/images_egrul/train3/train_4/4.net");
		work_enumerator.AddAnnNameImgSize(cv::Size(29, 39));
		work_enumerator.AddChar("4");*/

		work_enumerator.AddCascadName("D:/_C++/captcha/Captchure_pics/images_egrul/train2/_0_train_4/haarcascade/cascade.xml");
		work_enumerator.AddAnnName("D:/_C++/captcha/Captchure_pics/images_egrul/train3/train_4/___4.net");
		work_enumerator.AddAnnNameImgSize(cv::Size(29, 39));
		work_enumerator.AddChar("4");

		work_enumerator.AddCascadName("D:/_C++/captcha/Captchure_pics/images_egrul/train3/train_5/haarcascade/cascade_.xml");
		work_enumerator.AddAnnName("D:/_C++/captcha/Captchure_pics/images_egrul/train3/train_5/___5.net");
		work_enumerator.AddAnnNameImgSize(cv::Size(31, 40));
		work_enumerator.AddChar("6");

		work_enumerator.AddCascadName("D:/_C++/captcha/Captchure_pics/images_egrul/train3/train_6/haarcascade/cascade.xml");
		work_enumerator.AddAnnName("D:/_C++/captcha/Captchure_pics/images_egrul/train3/train_6/___6.net");
		work_enumerator.AddAnnNameImgSize(cv::Size(30, 40));
		work_enumerator.AddChar("6");

		work_enumerator.AddCascadName("D:/_C++/captcha/Captchure_pics/images_egrul/train2/_0_train_7/haarcascade/cascade.xml");
		work_enumerator.AddAnnName("D:/_C++/captcha/Captchure_pics/images_egrul/train3/train_7/___7.net");
		work_enumerator.AddAnnNameImgSize(cv::Size(28, 39));
		work_enumerator.AddChar("7");

		work_enumerator.AddCascadName("D:/_C++/captcha/Captchure_pics/images_egrul/train3/train_8/haarcascade/cascade.xml");
		work_enumerator.AddAnnName("D:/_C++/captcha/Captchure_pics/images_egrul/train3/train_8/___8.net");
		work_enumerator.AddAnnNameImgSize(cv::Size(31, 40));
		work_enumerator.AddChar("8");

		work_enumerator.AddCascadName("D:/_C++/captcha/Captchure_pics/images_egrul/train3/train_9/haarcascade/cascade_.xml");
		work_enumerator.AddAnnName("D:/_C++/captcha/Captchure_pics/images_egrul/train3/train_9/___9.net");
		work_enumerator.AddAnnNameImgSize(cv::Size(30, 39));
		work_enumerator.AddChar("9");

		work_enumerator.loadAnns();

		work_enumerator.enumerate(work_directory.c_str());
		int success = work_enumerator.files_number();
		//int total = work_enumerator.total_files_number();

		//cv::imwrite("D:/_C++/captcha/Captchure_pics/images_egrul/all.bmp", work_enumerator.common_picture);
		cv::imwrite("D:\\_C++\\avto_nomera_recognition\\velo_all.bmp", work_enumerator.common_picture);

		printf("total = %d success=%d\n", total, success);
		work_enumerator.freeAnns();
	}

	if (0)
	{
		CascadeClassifierDirectoryEnumerator work_enumerator("*", "png");
		//
		//work_enumerator.SetCascadName("D:/_C++/captcha/_8_train_3/haarcascade/cascade.xml");
		//work_enumerator.SetCascadName("D:/_C++/captcha/_7_train_1/haarcascade/cascade.xml");
		work_enumerator.AddCascadName("D:/_C++/captcha/_train_0/haarcascade/cascade.xml");
		work_enumerator.AddCascadName("D:/_C++/captcha/_train_1/haarcascade/cascade.xml");
		work_enumerator.AddCascadName("D:/_C++/captcha/_train_2/haarcascade/cascade.xml");
		work_enumerator.AddCascadName("D:/_C++/captcha/_train_3/haarcascade/cascade.xml");
		work_enumerator.AddCascadName("D:/_C++/captcha/_train_4/haarcascade/cascade.xml");
		work_enumerator.AddCascadName("D:/_C++/captcha/_train_5/haarcascade/cascade.xml");
		work_enumerator.AddCascadName("D:/_C++/captcha/_train_6/haarcascade/cascade.xml");
		work_enumerator.AddCascadName("D:/_C++/captcha/_train_7/haarcascade/cascade.xml");
		//work_enumerator.AddCascadName("D:/_C++/captcha/_train_8/haarcascade/cascade.xml");
		work_enumerator.AddCascadName("D:/_C++/captcha/_train_9/haarcascade/cascade.xml");
		work_enumerator.enumerate("D:/_C++/captcha/Captchure_pics/new/2");
		//work_enumerator.enumerate("D:/_C++/captcha/Captchure_pics/blue/testset");
		int success = work_enumerator.files_number();
		int total = work_enumerator.total_files_number();

		printf("total = %d success=%d\n", total, success);
	}

	if (0)
	{
		CaptchaDirectoryEnumerator work_enumerator("*", "png");
		work_enumerator.enumerate("D:/_C++/captcha/Captchure_pics/new");
	}

	if (0)
	{
		FindContoursDirectoryEnumerator work_enumerator("*", "jpg");
		work_enumerator.setOutputDirectory("D:/_C++/recognition/inframes_test_find_contours");
		work_enumerator.enumerate("D:/_C++/recognition/inframes");
	}

	if (0)
	{
		GHTDirectoryEnumerator work_enumerator("*", "jpg");
		work_enumerator.enumerate("D:/_C++/recognition/inframes");
		int success = work_enumerator.files_number();
		int total = work_enumerator.total_files_number();

		printf("total = %d success=%d\n", total, success);
	}


	if (0)
	{
		CascadeDirectoryEnumerator work_enumerator("*", "pgm");		//
		work_enumerator.SetCascadName("haarcascades/haarcascade_eye_tree_eyeglasses.xml");
		work_enumerator.enumerate("D:/cpplibs/aam/_datasets/att_faces");
		int success = work_enumerator.files_number();
		int total = work_enumerator.total_files_number();

		printf("total = %d success=%d\n", total, success);
	}

	if (0)
	{
		CascadeDirectoryEnumerator work_enumerator("*", "jpg");		//
		work_enumerator.SetCascadName("haarcascades/haarcascade_eye_tree_eyeglasses.xml");
		work_enumerator.SetToWriteImageBack();
		work_enumerator.enumerate("D:/_C++/recognition/inframes");
		int success = work_enumerator.files_number();
		int total = work_enumerator.total_files_number();

		printf("total = %d success=%d\n", total, success);
	}
#if !HAVE_OPENCV_300
	if (0)
	{
		OrbDirectoryEnumerator work_enumerator("*", "jpg");
		work_enumerator.enumerate("D:/_C++/recognition/inframes");
		int success = work_enumerator.files_number();
		int total = work_enumerator.total_files_number();

		printf("total = %d success=%d\n", total, success);
	}

	if (0)
	{
		OrbDirectoryEnumerator work_enumerator("*", "ppm");
		work_enumerator.enumerate("D:/_C++/stereo/pedestrians128x64");
		int success = work_enumerator.files_number();
		int total = work_enumerator.total_files_number();

		printf("total = %d success=%d\n", total, success);
	}
#endif

	if (0)
	{
		namedWindow("result", CV_WINDOW_AUTOSIZE);

		HogDirectoryEnumerator work_enumerator("*", "jpg");
		work_enumerator.enumerate("D:/_C++/stereo/dctracking-v1.0/demo/img");
		int success = work_enumerator.files_number();
		int total = work_enumerator.total_files_number();

		printf("total = %d success=%d\n", total, success);
	}


	if (0)
	{
		HogDirectoryEnumerator work_enumerator("*", "ppm");
		work_enumerator.enumerate("D:/_C++/stereo/pedestrians128x64");
		int success = work_enumerator.files_number();
		int total = work_enumerator.total_files_number();

		printf("total = %d success=%d\n", total, success);
	}

	if (0)
	{
		CascadeDirectoryEnumerator work_enumerator("*", "ppm");
		work_enumerator.SetCascadName("hogcascades/hogcascade_pedestrians.xml");
		work_enumerator.enumerate("D:/_C++/stereo/pedestrians128x64");
		int success = work_enumerator.files_number();
		int total = work_enumerator.total_files_number();

		printf("total = %d success=%d\n", total, success);
	}

	if (0)
	{
		namedWindow("result", CV_WINDOW_AUTOSIZE);

		CascadeDirectoryEnumerator work_enumerator("*", "jpg");
		work_enumerator.enumerate("D:/_C++/stereo/dctracking-v1.0/demo/img");
		int success = work_enumerator.files_number();
		int total = work_enumerator.total_files_number();

		printf("total = %d success=%d\n", total, success);
	}

	if (0)
	{
		CascadeDirectoryEnumerator work_enumerator("*", "png");
		work_enumerator.enumerate("D:/_C++/stereo/INRIAPerson");
		int success = work_enumerator.files_number();
		int total = work_enumerator.total_files_number();

		printf("total = %d success=%d\n", total, success);
	}


	if (0)
	{
		const char * opencv_sources = getenv("OPENCV_SOURCES");
		if (!opencv_sources)
		{
			printf("Set eviroment variable OPENCV_SOURCES\n");
			return -1;
		}


		string directory = string(opencv_sources) + "/data/haarcascades/";
		string cascadeName = "haarcascade_frontalface_alt.xml";

		//string directory = string(opencv_sources) + "/data/hogcascades/";
		//string cascadeName = "hogcascade_pedestrians.xml";

		//string directory = string(opencv_sources) + "/data/haarcascades/";
		//string cascadeName = "haarcascade_fullbody.xml";

		//string directory = string(opencv_sources) + "/data/lbpcascades/";
		//string cascadeName = "lbpcascade_profileface.xml";

		//string directory = string(opencv_sources) + "/data/lbpcascades/";
		//string cascadeName = "lbpcascade_frontalface.xml";


		string inputName = "";
		bool isInputImage = false;
		bool isInputVideo = false;
		bool isInputCamera = true;

		//use_cascadeclassifier_cpu(directory + cascadeName, inputName, isInputImage, isInputVideo, isInputCamera);
		use_cascadeclassifier((directory + cascadeName).c_str(), inputName.c_str(), isInputImage, isInputVideo, isInputCamera, false, ProcessType::PT_OCL);
	}

	if (0)
	{
		runGHT('t');
		runGHT('r');
	}

	//use_cascadeclassifier2(argc, argv);

	//DemonstrateHoGwithFiles(argc, argv);
	//DemonstrateHoGwithVideoCapture(argc, argv);

	return 0;
}