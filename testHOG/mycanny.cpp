/*M///////////////////////////////////////////////////////////////////////////////////////
//
//  IMPORTANT: READ BEFORE DOWNLOADING, COPYING, INSTALLING OR USING.
//
//  By downloading, copying, installing or using the software you agree to this license.
//  If you do not agree to this license, do not download, install,
//  copy or use the software.
//
//
//                        Intel License Agreement
//                For Open Source Computer Vision Library
//
// Copyright (C) 2000, Intel Corporation, all rights reserved.
// Third party copyrights are property of their respective owners.
//
// Redistribution and use in source and binary forms, with or without modification,
// are permitted provided that the following conditions are met:
//
//   * Redistribution's of source code must retain the above copyright notice,
//     this list of conditions and the following disclaimer.
//
//   * Redistribution's in binary form must reproduce the above copyright notice,
//     this list of conditions and the following disclaimer in the documentation
//     and/or other materials provided with the distribution.
//
//   * The name of Intel Corporation may not be used to endorse or promote products
//     derived from this software without specific prior written permission.
//
// This software is provided by the copyright holders and contributors "as is" and
// any express or implied warranties, including, but not limited to, the implied
// warranties of merchantability and fitness for a particular purpose are disclaimed.
// In no event shall the Intel Corporation or contributors be liable for any direct,
// indirect, incidental, special, exemplary, or consequential damages
// (including, but not limited to, procurement of substitute goods or services;
// loss of use, data, or profits; or business interruption) however caused
// and on any theory of liability, whether in contract, strict liability,
// or tort (including negligence or otherwise) arising in any way out of
// the use of this software, even if advised of the possibility of such damage.
//
//M*/


#ifdef HAVE_VDOUBLE
#include "Vdouble.h"
#include "systemnnet.h"
#endif

#include "mycanny.hpp"
#include "opencv2/highgui/highgui.hpp"
#include "cvplot.h"


/*
#if defined (HAVE_IPP) && (IPP_VERSION_MAJOR >= 7)
#define USE_IPP_CANNY 1
#else
#undef USE_IPP_CANNY
#endif
*/
#ifdef USE_IPP_CANNY
namespace cv
{
	static bool ippCanny(const Mat& _src, Mat& _dst, float low, float high)
	{
		int size = 0, size1 = 0;
		IppiSize roi = { _src.cols, _src.rows };

		ippiFilterSobelNegVertGetBufferSize_8u16s_C1R(roi, ippMskSize3x3, &size);
		ippiFilterSobelHorizGetBufferSize_8u16s_C1R(roi, ippMskSize3x3, &size1);
		size = std::max(size, size1);
		ippiCannyGetSize(roi, &size1);
		size = std::max(size, size1);

		AutoBuffer<uchar> buf(size + 64);
		uchar* buffer = alignPtr((uchar*)buf, 32);

		Mat _dx(_src.rows, _src.cols, CV_16S);
		if (ippiFilterSobelNegVertBorder_8u16s_C1R(_src.data, (int)_src.step,
			_dx.ptr<short>(), (int)_dx.step, roi,
			ippMskSize3x3, ippBorderRepl, 0, buffer) < 0)
			return false;

		Mat _dy(_src.rows, _src.cols, CV_16S);
		if (ippiFilterSobelHorizBorder_8u16s_C1R(_src.data, (int)_src.step,
			_dy.ptr<short>(), (int)_dy.step, roi,
			ippMskSize3x3, ippBorderRepl, 0, buffer) < 0)
			return false;

		if (ippiCanny_16s8u_C1R(_dx.ptr<short>(), (int)_dx.step,
			_dy.ptr<short>(), (int)_dy.step,
			_dst.data, (int)_dst.step, roi, low, high, buffer) < 0)
			return false;
		return true;
	}
}
#endif

#if 0
// itseez
void cv::Canny(InputArray _src, OutputArray _dst,
	double low_thresh, double high_thresh,
	int aperture_size, bool L2gradient)
{
	const int type = _src.type(), depth = CV_MAT_DEPTH(type), cn = CV_MAT_CN(type);
	const Size size = _src.size();

	CV_Assert(depth == CV_8U);
	_dst.create(size, CV_8U);

	if (!L2gradient && (aperture_size & CV_CANNY_L2_GRADIENT) == CV_CANNY_L2_GRADIENT)
	{
		// backward compatibility
		aperture_size &= ~CV_CANNY_L2_GRADIENT;
		L2gradient = true;
	}

	if ((aperture_size & 1) == 0 || (aperture_size != -1 && (aperture_size < 3 || aperture_size > 7)))
		CV_Error(CV_StsBadFlag, "Aperture size should be odd");

	if (low_thresh > high_thresh)
		std::swap(low_thresh, high_thresh);

	CV_OCL_RUN(_dst.isUMat() && (cn == 1 || cn == 3),
		ocl_Canny(_src, _dst, (float)low_thresh, (float)high_thresh, aperture_size, L2gradient, cn, size))

		Mat src = _src.getMat(), dst = _dst.getMat();

#ifdef HAVE_TEGRA_OPTIMIZATION
	if (tegra::useTegra() && tegra::canny(src, dst, low_thresh, high_thresh, aperture_size, L2gradient))
		return;
#endif

#ifdef USE_IPP_CANNY
	CV_IPP_CHECK()
	{
		if (aperture_size == 3 && !L2gradient && 1 == cn)
		{
			if (ippCanny(src, dst, (float)low_thresh, (float)high_thresh))
			{
				CV_IMPL_ADD(CV_IMPL_IPP);
				return;
			}
			setIppErrorStatus();
		}
	}
#endif

#ifdef HAVE_TBB

	if (L2gradient)
	{
		low_thresh = std::min(32767.0, low_thresh);
		high_thresh = std::min(32767.0, high_thresh);

		if (low_thresh > 0) low_thresh *= low_thresh;
		if (high_thresh > 0) high_thresh *= high_thresh;
	}
	int low = cvFloor(low_thresh);
	int high = cvFloor(high_thresh);

	ptrdiff_t mapstep = src.cols + 2;
	AutoBuffer<uchar> buffer((src.cols + 2)*(src.rows + 2));

	uchar* map = (uchar*)buffer;
	memset(map, 1, mapstep);
	memset(map + mapstep*(src.rows + 1), 1, mapstep);

	int threadsNumber = tbb::task_scheduler_init::default_num_threads();
	int grainSize = src.rows / threadsNumber;

	// Make a fallback for pictures with too few rows.
	uchar ksize2 = aperture_size / 2;
	int minGrainSize = 1 + ksize2;
	int maxGrainSize = src.rows - 2 - 2 * ksize2;
	if (!(minGrainSize <= grainSize && grainSize <= maxGrainSize))
	{
		threadsNumber = 1;
		grainSize = src.rows;
	}

	tbb::task_group g;

	for (int i = 0; i < threadsNumber; ++i)
	{
		if (i < threadsNumber - 1)
			g.run(tbbCanny(Range(i * grainSize, (i + 1) * grainSize), src, map, low, high, aperture_size, L2gradient));
		else
			g.run(tbbCanny(Range(i * grainSize, src.rows), src, map, low, high, aperture_size, L2gradient));
	}

	g.wait();

#define CANNY_PUSH_SERIAL(d)    *(d) = uchar(2), borderPeaks.push(d)

	// now track the edges (hysteresis thresholding)
	uchar* m;
	while (borderPeaks.try_pop(m))
	{
		if (!m[-1])         CANNY_PUSH_SERIAL(m - 1);
		if (!m[1])          CANNY_PUSH_SERIAL(m + 1);
		if (!m[-mapstep - 1]) CANNY_PUSH_SERIAL(m - mapstep - 1);
		if (!m[-mapstep])   CANNY_PUSH_SERIAL(m - mapstep);
		if (!m[-mapstep + 1]) CANNY_PUSH_SERIAL(m - mapstep + 1);
		if (!m[mapstep - 1])  CANNY_PUSH_SERIAL(m + mapstep - 1);
		if (!m[mapstep])    CANNY_PUSH_SERIAL(m + mapstep);
		if (!m[mapstep + 1])  CANNY_PUSH_SERIAL(m + mapstep + 1);
	}

#else

	Mat dx(src.rows, src.cols, CV_16SC(cn));
	Mat dy(src.rows, src.cols, CV_16SC(cn));

	Sobel(src, dx, CV_16S, 1, 0, aperture_size, 1, 0, BORDER_REPLICATE);
	Sobel(src, dy, CV_16S, 0, 1, aperture_size, 1, 0, BORDER_REPLICATE);

	if (L2gradient)
	{
		low_thresh = std::min(32767.0, low_thresh);
		high_thresh = std::min(32767.0, high_thresh);

		if (low_thresh > 0) low_thresh *= low_thresh;
		if (high_thresh > 0) high_thresh *= high_thresh;
	}
	int low = cvFloor(low_thresh);
	int high = cvFloor(high_thresh);

	ptrdiff_t mapstep = src.cols + 2;
	AutoBuffer<uchar> buffer((src.cols + 2)*(src.rows + 2) + cn * mapstep * 3 * sizeof(int));

	int* mag_buf[3];
	mag_buf[0] = (int*)(uchar*)buffer;
	mag_buf[1] = mag_buf[0] + mapstep*cn;
	mag_buf[2] = mag_buf[1] + mapstep*cn;
	memset(mag_buf[0], 0, /* cn* */mapstep*sizeof(int));

	uchar* map = (uchar*)(mag_buf[2] + mapstep*cn);
	memset(map, 1, mapstep);
	memset(map + mapstep*(src.rows + 1), 1, mapstep);

	int maxsize = std::max(1 << 10, src.cols * src.rows / 10);
	std::vector<uchar*> stack(maxsize);
	uchar **stack_top = &stack[0];
	uchar **stack_bottom = &stack[0];

	/* sector numbers
	(Top-Left Origin)

	1   2   3
	*  *  *
	* * *
	0*******0
	* * *
	*  *  *
	3   2   1
	*/

#define CANNY_PUSH(d)    *(d) = uchar(2), *stack_top++ = (d)
#define CANNY_POP(d)     (d) = *--stack_top

#if CV_SSE2
	bool haveSSE2 = checkHardwareSupport(CV_CPU_SSE2);
#endif

	// calculate magnitude and angle of gradient, perform non-maxima suppression.
	// fill the map with one of the following values:
	//   0 - the pixel might belong to an edge
	//   1 - the pixel can not belong to an edge
	//   2 - the pixel does belong to an edge
	for (int i = 0; i <= src.rows; i++)
	{
		int* _norm = mag_buf[(i > 0) + 1] + 1;
		if (i < src.rows)
		{
			short* _dx = dx.ptr<short>(i);
			short* _dy = dy.ptr<short>(i);

			if (!L2gradient)
			{
				int j = 0, width = src.cols * cn;
#if CV_SSE2
				if (haveSSE2)
				{
					__m128i v_zero = _mm_setzero_si128();
					for (; j <= width - 8; j += 8)
					{
						__m128i v_dx = _mm_loadu_si128((const __m128i *)(_dx + j));
						__m128i v_dy = _mm_loadu_si128((const __m128i *)(_dy + j));
						v_dx = _mm_max_epi16(v_dx, _mm_sub_epi16(v_zero, v_dx));
						v_dy = _mm_max_epi16(v_dy, _mm_sub_epi16(v_zero, v_dy));

						__m128i v_norm = _mm_add_epi32(_mm_unpacklo_epi16(v_dx, v_zero), _mm_unpacklo_epi16(v_dy, v_zero));
						_mm_storeu_si128((__m128i *)(_norm + j), v_norm);

						v_norm = _mm_add_epi32(_mm_unpackhi_epi16(v_dx, v_zero), _mm_unpackhi_epi16(v_dy, v_zero));
						_mm_storeu_si128((__m128i *)(_norm + j + 4), v_norm);
					}
				}
#elif CV_NEON
				for (; j <= width - 8; j += 8)
				{
					int16x8_t v_dx = vld1q_s16(_dx + j), v_dy = vld1q_s16(_dy + j);
					vst1q_s32(_norm + j, vaddq_s32(vabsq_s32(vmovl_s16(vget_low_s16(v_dx))),
						vabsq_s32(vmovl_s16(vget_low_s16(v_dy)))));
					vst1q_s32(_norm + j + 4, vaddq_s32(vabsq_s32(vmovl_s16(vget_high_s16(v_dx))),
						vabsq_s32(vmovl_s16(vget_high_s16(v_dy)))));
				}
#endif
				for (; j < width; ++j)
					_norm[j] = std::abs(int(_dx[j])) + std::abs(int(_dy[j]));
			}
			else
			{
				int j = 0, width = src.cols * cn;
#if CV_SSE2
				if (haveSSE2)
				{
					for (; j <= width - 8; j += 8)
					{
						__m128i v_dx = _mm_loadu_si128((const __m128i *)(_dx + j));
						__m128i v_dy = _mm_loadu_si128((const __m128i *)(_dy + j));

						__m128i v_dx_ml = _mm_mullo_epi16(v_dx, v_dx), v_dx_mh = _mm_mulhi_epi16(v_dx, v_dx);
						__m128i v_dy_ml = _mm_mullo_epi16(v_dy, v_dy), v_dy_mh = _mm_mulhi_epi16(v_dy, v_dy);

						__m128i v_norm = _mm_add_epi32(_mm_unpacklo_epi16(v_dx_ml, v_dx_mh), _mm_unpacklo_epi16(v_dy_ml, v_dy_mh));
						_mm_storeu_si128((__m128i *)(_norm + j), v_norm);

						v_norm = _mm_add_epi32(_mm_unpackhi_epi16(v_dx_ml, v_dx_mh), _mm_unpackhi_epi16(v_dy_ml, v_dy_mh));
						_mm_storeu_si128((__m128i *)(_norm + j + 4), v_norm);
					}
				}
#elif CV_NEON
				for (; j <= width - 8; j += 8)
				{
					int16x8_t v_dx = vld1q_s16(_dx + j), v_dy = vld1q_s16(_dy + j);
					int16x4_t v_dxp = vget_low_s16(v_dx), v_dyp = vget_low_s16(v_dy);
					int32x4_t v_dst = vmlal_s16(vmull_s16(v_dxp, v_dxp), v_dyp, v_dyp);
					vst1q_s32(_norm + j, v_dst);

					v_dxp = vget_high_s16(v_dx), v_dyp = vget_high_s16(v_dy);
					v_dst = vmlal_s16(vmull_s16(v_dxp, v_dxp), v_dyp, v_dyp);
					vst1q_s32(_norm + j + 4, v_dst);
				}
#endif
				for (; j < width; ++j)
					_norm[j] = int(_dx[j])*_dx[j] + int(_dy[j])*_dy[j];
			}

			if (cn > 1)
			{
				for (int j = 0, jn = 0; j < src.cols; ++j, jn += cn)
				{
					int maxIdx = jn;
					for (int k = 1; k < cn; ++k)
					if (_norm[jn + k] > _norm[maxIdx]) maxIdx = jn + k;
					_norm[j] = _norm[maxIdx];
					_dx[j] = _dx[maxIdx];
					_dy[j] = _dy[maxIdx];
				}
			}
			_norm[-1] = _norm[src.cols] = 0;
		}
		else
			memset(_norm - 1, 0, /* cn* */mapstep*sizeof(int));

		// at the very beginning we do not have a complete ring
		// buffer of 3 magnitude rows for non-maxima suppression
		if (i == 0)
			continue;

		uchar* _map = map + mapstep*i + 1;
		_map[-1] = _map[src.cols] = 1;

		int* _mag = mag_buf[1] + 1; // take the central row
		ptrdiff_t magstep1 = mag_buf[2] - mag_buf[1];
		ptrdiff_t magstep2 = mag_buf[0] - mag_buf[1];

		const short* _x = dx.ptr<short>(i - 1);
		const short* _y = dy.ptr<short>(i - 1);

		if ((stack_top - stack_bottom) + src.cols > maxsize)
		{
			int sz = (int)(stack_top - stack_bottom);
			maxsize = std::max(maxsize * 3 / 2, sz + src.cols);
			stack.resize(maxsize);
			stack_bottom = &stack[0];
			stack_top = stack_bottom + sz;
		}

		int prev_flag = 0;
		for (int j = 0; j < src.cols; j++)
		{
#define CANNY_SHIFT 15
			const int TG22 = (int)(0.4142135623730950488016887242097*(1 << CANNY_SHIFT) + 0.5);

			int m = _mag[j];

			if (m > low)
			{
				int xs = _x[j];
				int ys = _y[j];
				int x = std::abs(xs);
				int y = std::abs(ys) << CANNY_SHIFT;

				int tg22x = x * TG22;

				if (y < tg22x)
				{
					if (m > _mag[j - 1] && m >= _mag[j + 1]) goto __ocv_canny_push;
				}
				else
				{
					int tg67x = tg22x + (x << (CANNY_SHIFT + 1));
					if (y > tg67x)
					{
						if (m > _mag[j + magstep2] && m >= _mag[j + magstep1]) goto __ocv_canny_push;
					}
					else
					{
						int s = (xs ^ ys) < 0 ? -1 : 1;
						if (m > _mag[j + magstep2 - s] && m > _mag[j + magstep1 + s]) goto __ocv_canny_push;
					}
				}
			}
			prev_flag = 0;
			_map[j] = uchar(1);
			continue;
		__ocv_canny_push:
			if (!prev_flag && m > high && _map[j - mapstep] != 2)
			{
				CANNY_PUSH(_map + j);
				prev_flag = 1;
			}
			else
				_map[j] = 0;
		}

		// scroll the ring buffer
		_mag = mag_buf[0];
		mag_buf[0] = mag_buf[1];
		mag_buf[1] = mag_buf[2];
		mag_buf[2] = _mag;
	}

	// now track the edges (hysteresis thresholding)
	while (stack_top > stack_bottom)
	{
		uchar* m;
		if ((stack_top - stack_bottom) + 8 > maxsize)
		{
			int sz = (int)(stack_top - stack_bottom);
			maxsize = maxsize * 3 / 2;
			stack.resize(maxsize);
			stack_bottom = &stack[0];
			stack_top = stack_bottom + sz;
		}

		CANNY_POP(m);

		if (!m[-1])         CANNY_PUSH(m - 1);
		if (!m[1])          CANNY_PUSH(m + 1);
		if (!m[-mapstep - 1]) CANNY_PUSH(m - mapstep - 1);
		if (!m[-mapstep])   CANNY_PUSH(m - mapstep);
		if (!m[-mapstep + 1]) CANNY_PUSH(m - mapstep + 1);
		if (!m[mapstep - 1])  CANNY_PUSH(m + mapstep - 1);
		if (!m[mapstep])    CANNY_PUSH(m + mapstep);
		if (!m[mapstep + 1])  CANNY_PUSH(m + mapstep + 1);
	}

#endif

	// the final pass, form the final image
	const uchar* pmap = map + mapstep + 1;
	uchar* pdst = dst.ptr();
	for (int i = 0; i < src.rows; i++, pmap += mapstep, pdst += dst.step)
	{
		for (int j = 0; j < src.cols; j++)
			pdst[j] = (uchar)-(pmap[j] >> 1);
	}
}

#endif

// original
void cv::Canny_original(InputArray _src, OutputArray _dst,
	double low_thresh, double high_thresh,
	int aperture_size, bool L2gradient)
{
	Mat src = _src.getMat();
	CV_Assert(src.depth() == CV_8U);

	_dst.create(src.size(), CV_8U);
	Mat dst = _dst.getMat();

	if (!L2gradient && (aperture_size & CV_CANNY_L2_GRADIENT) == CV_CANNY_L2_GRADIENT)
	{
		//backward compatibility
		aperture_size &= ~CV_CANNY_L2_GRADIENT;
		L2gradient = true;
	}

	if ((aperture_size & 1) == 0 || (aperture_size != -1 && (aperture_size < 3 || aperture_size > 7)))
		CV_Error(CV_StsBadFlag, "");

	if (low_thresh > high_thresh)
		std::swap(low_thresh, high_thresh);

#ifdef HAVE_TEGRA_OPTIMIZATION
	if (tegra::canny(src, dst, low_thresh, high_thresh, aperture_size, L2gradient))
		return;
#endif

#ifdef USE_IPP_CANNY
	if (aperture_size == 3 && !L2gradient &&
		ippCanny(src, dst, (float)low_thresh, (float)high_thresh))
		return;
#endif

	const int cn = src.channels();
	Mat dx(src.rows, src.cols, CV_16SC(cn));
	Mat dy(src.rows, src.cols, CV_16SC(cn));

	Sobel(src, dx, CV_16S, 1, 0, aperture_size, 1, 0, cv::BORDER_REPLICATE);
	Sobel(src, dy, CV_16S, 0, 1, aperture_size, 1, 0, cv::BORDER_REPLICATE);

	if (L2gradient)
	{
		low_thresh = std::min(32767.0, low_thresh);
		high_thresh = std::min(32767.0, high_thresh);

		if (low_thresh > 0) low_thresh *= low_thresh;
		if (high_thresh > 0) high_thresh *= high_thresh;
	}
	int low = cvFloor(low_thresh);
	int high = cvFloor(high_thresh);

	ptrdiff_t mapstep = src.cols + 2;
	AutoBuffer<uchar> buffer((src.cols + 2)*(src.rows + 2) + cn * mapstep * 3 * sizeof(int));

	int* mag_buf[3];
	mag_buf[0] = (int*)(uchar*)buffer;
	mag_buf[1] = mag_buf[0] + mapstep*cn;
	mag_buf[2] = mag_buf[1] + mapstep*cn;
	memset(mag_buf[0], 0, /* cn* */mapstep*sizeof(int));

	uchar* map = (uchar*)(mag_buf[2] + mapstep*cn);
	memset(map, 1, mapstep);
	memset(map + mapstep*(src.rows + 1), 1, mapstep);

	int maxsize = std::max(1 << 10, src.cols * src.rows / 10);
	std::vector<uchar*> stack(maxsize);
	uchar **stack_top = &stack[0];
	uchar **stack_bottom = &stack[0];

	/* sector numbers
	(Top-Left Origin)

	1   2   3
	*  *  *
	* * *
	0*******0
	* * *
	*  *  *
	3   2   1
	*/

#define CANNY_PUSH(d)    *(d) = uchar(2), *stack_top++ = (d)
#define CANNY_POP(d)     (d) = *--stack_top

	// calculate magnitude and angle of gradient, perform non-maxima suppression.
	// fill the map with one of the following values:
	//   0 - the pixel might belong to an edge
	//   1 - the pixel can not belong to an edge
	//   2 - the pixel does belong to an edge
	for (int i = 0; i <= src.rows; i++)
	{
		int* _norm = mag_buf[(i > 0) + 1] + 1;
		if (i < src.rows)
		{
			short* _dx = dx.ptr<short>(i);
			short* _dy = dy.ptr<short>(i);

			if (!L2gradient)
			{
				for (int j = 0; j < src.cols*cn; j++)
					_norm[j] = std::abs(int(_dx[j])) + std::abs(int(_dy[j]));
			}
			else
			{
				for (int j = 0; j < src.cols*cn; j++)
					_norm[j] = int(_dx[j])*_dx[j] + int(_dy[j])*_dy[j];
			}

			if (cn > 1)
			{
				for (int j = 0, jn = 0; j < src.cols; ++j, jn += cn)
				{
					int maxIdx = jn;
					for (int k = 1; k < cn; ++k)
					if (_norm[jn + k] > _norm[maxIdx]) maxIdx = jn + k;
					_norm[j] = _norm[maxIdx];
					_dx[j] = _dx[maxIdx];
					_dy[j] = _dy[maxIdx];
				}
			}
			_norm[-1] = _norm[src.cols] = 0;
		}
		else
			memset(_norm - 1, 0, /* cn* */mapstep*sizeof(int));

		// at the very beginning we do not have a complete ring
		// buffer of 3 magnitude rows for non-maxima suppression
		if (i == 0)
			continue;

		uchar* _map = map + mapstep*i + 1;
		_map[-1] = _map[src.cols] = 1;

		int* _mag = mag_buf[1] + 1; // take the central row
		ptrdiff_t magstep1 = mag_buf[2] - mag_buf[1];
		ptrdiff_t magstep2 = mag_buf[0] - mag_buf[1];

		const short* _x = dx.ptr<short>(i - 1);
		const short* _y = dy.ptr<short>(i - 1);

		if ((stack_top - stack_bottom) + src.cols > maxsize)
		{
			int sz = (int)(stack_top - stack_bottom);
			maxsize = maxsize * 3 / 2;
			stack.resize(maxsize);
			stack_bottom = &stack[0];
			stack_top = stack_bottom + sz;
		}

		int prev_flag = 0;
		for (int j = 0; j < src.cols; j++)
		{
#define CANNY_SHIFT 15
			const int TG22 = (int)(0.4142135623730950488016887242097*(1 << CANNY_SHIFT) + 0.5);

			int m = _mag[j];

			if (m > low)
			{
				int xs = _x[j];
				int ys = _y[j];
				int x = std::abs(xs);
				int y = std::abs(ys) << CANNY_SHIFT;

				int tg22x = x * TG22;

				if (y < tg22x)
				{
					if (m > _mag[j - 1] && m >= _mag[j + 1]) goto __ocv_canny_push;
				}
				else
				{
					int tg67x = tg22x + (x << (CANNY_SHIFT + 1));
					if (y > tg67x)
					{
						if (m > _mag[j + magstep2] && m >= _mag[j + magstep1]) goto __ocv_canny_push;
					}
					else
					{
						int s = (xs ^ ys) < 0 ? -1 : 1;
						if (m > _mag[j + magstep2 - s] && m > _mag[j + magstep1 + s]) goto __ocv_canny_push;
					}
				}
			}
			prev_flag = 0;
			_map[j] = uchar(1);
			continue;
		__ocv_canny_push:
			if (!prev_flag && m > high && _map[j - mapstep] != 2)
			{
				CANNY_PUSH(_map + j);
				prev_flag = 1;
			}
			else
				_map[j] = 0;
		}

		// scroll the ring buffer
		_mag = mag_buf[0];
		mag_buf[0] = mag_buf[1];
		mag_buf[1] = mag_buf[2];
		mag_buf[2] = _mag;
	}

	// now track the edges (hysteresis thresholding)
	while (stack_top > stack_bottom)
	{
		uchar* m;
		if ((stack_top - stack_bottom) + 8 > maxsize)
		{
			int sz = (int)(stack_top - stack_bottom);
			maxsize = maxsize * 3 / 2;
			stack.resize(maxsize);
			stack_bottom = &stack[0];
			stack_top = stack_bottom + sz;
		}

		CANNY_POP(m);

		if (!m[-1])         CANNY_PUSH(m - 1);
		if (!m[1])          CANNY_PUSH(m + 1);
		if (!m[-mapstep - 1]) CANNY_PUSH(m - mapstep - 1);
		if (!m[-mapstep])   CANNY_PUSH(m - mapstep);
		if (!m[-mapstep + 1]) CANNY_PUSH(m - mapstep + 1);
		if (!m[mapstep - 1])  CANNY_PUSH(m + mapstep - 1);
		if (!m[mapstep])    CANNY_PUSH(m + mapstep);
		if (!m[mapstep + 1])  CANNY_PUSH(m + mapstep + 1);
	}

	// the final pass, form the final image
	const uchar* pmap = map + mapstep + 1;
	uchar* pdst = dst.ptr();
	for (int i = 0; i < src.rows; i++, pmap += mapstep, pdst += dst.step)
	{
		for (int j = 0; j < src.cols; j++)
			pdst[j] = (uchar)-(pmap[j] >> 1);
	}
}



double otsu(int * histogram, int total) {
	int sum = 0;
	for (int i = 1; i < 256; ++i)
		sum += i * histogram[i];

	int sumB = 0;
	int wB = 0;
	int wF = 0;
	double mB;
	double mF;
	double max = 0.0;
	double between = 0.0;
	double threshold1 = 0.0;
	double threshold2 = 0.0;
	for (int i = 0; i < 256; ++i) {
		wB += histogram[i];
		if (wB == 0)
			continue;
		wF = total - wB;
		if (wF == 0)
			break;
		sumB += i * histogram[i];
		mB = double(sumB) / wB;
		mF = double(sum - sumB) / wF;
		between = wB * wF * (mB - mF) * (mB - mF);
		if (between >= max) {
			threshold1 = i;
			if (between > max) {
				threshold2 = i;
			}
			max = between;
		}
	}
	printf("threshold1=%f + threshold2=%f\n", threshold1, threshold2);
	return (threshold1 + threshold2) / 2.0;
}



/* ������� ���������� ����� ����������� ��� ������������ ����������� image � ����� ������ �������� size */
template < class T >
int otsuThreshold(const cv::Mat& _src)
{
	const int size = _src.rows * _src.cols;

	const T* image = _src.ptr<T>();

	int min = image[0], max = image[0];
	int i, temp, temp1;
	int *hist;
	int histSize;

	int alpha, beta, threshold = 0;
	double sigma, maxSigma = -1;
	double w1, a;

	/**** ���������� ����������� ****/
	/* ������ ���������� � ���������� ������� */
	for (i = 1; i<size; i++)
	{
		temp = image[i];
		if (temp<min)   min = temp;
		if (temp>max)   max = temp;
	}

	histSize = max - min + 1;
	if ((hist = (int*)malloc(sizeof(int)*histSize)) == NULL) return -1;

	for (i = 0; i<histSize; i++)
		hist[i] = 0;

	/* ������� ������� ����� ��������� */
	for (i = 0; i<size; i++)
		hist[image[i] - min]++;

	/**** ����������� ��������� ****/

	CvPlot::plot("otsu_hist", &hist[0], histSize, 1);
	CvPlot::label("o");

	/*{
		int total = 0;
		for (i = 0; i < histSize; i++)
			total += i * hist[i];


		double thresh = otsu(hist, total);

		return thresh;
	}*/

	temp = temp1 = 0;
	alpha = beta = 0;
	/* ��� ������� ��������������� �������� ������� ������ */
	for (i = 0; i <= (max - min); i++)
	{
		temp += i*hist[i]; 
		temp1 += hist[i]; 
	}

	/* �������� ���� ������ ������
	����������� �� ���� ��������� ��� ������ ������, ��� ������� ��������������� ��������� ���������� */
	for (i = 0; i<(max - min); i++)
	{
		alpha += i*hist[i];//sumB
		beta += hist[i];//wB

		w1 = (double)beta / temp1;
		a = (double)alpha / beta - (double)(temp - alpha) / (temp1 - beta);
		sigma = w1*(1 - w1)*a*a;

		if (sigma>maxSigma)
		{
			maxSigma = sigma;
			threshold = i;
		}
	}
	free(hist);
	return threshold + min;
}


static double
cv::getThreshVal_Otsu_8u(const Mat& _src)
{
	Size size = _src.size();
	int step = (int)_src.step;
	if (_src.isContinuous())
	{
		size.width *= size.height;
		size.height = 1;
		step = size.width;
	}

#ifdef HAVE_IPP
	unsigned char thresh;
	CV_IPP_RUN(IPP_VERSION_X100 >= 801 && !HAVE_ICV, ipp_getThreshVal_Otsu_8u(_src.ptr(), step, size, thresh), thresh);
#endif

	const int N = 256;
	int i, j, h[N] = { 0 };
	for (i = 0; i < size.height; i++)
	{
		const uchar* src = _src.ptr() + step*i;
		j = 0;
#if CV_ENABLE_UNROLLED
		for (; j <= size.width - 4; j += 4)
		{
			int v0 = src[j], v1 = src[j + 1];
			h[v0]++; h[v1]++;
			v0 = src[j + 2]; v1 = src[j + 3];
			h[v0]++; h[v1]++;
		}
#endif
		for (; j < size.width; j++)
			h[src[j]]++;
	}

	CvPlot::plot("otsu_h", &h[0], 256, 1);
	CvPlot::label("o");


	double mu = 0, scale = 1. / (size.width*size.height);
	for (i = 0; i < N; i++)
		mu += i*(double)h[i];

	mu *= scale;
	double mu1 = 0, q1 = 0;
	double max_sigma = 0, max_val = 0;

	for (i = 0; i < N; i++)
	{
		double p_i, q2, mu2, sigma;

		p_i = h[i] * scale; // pixel intensity probality
		mu1 *= q1;
		q1 += p_i; // probality of class occurence (omega)
		q2 = 1. - q1;

		if (std::min(q1, q2) < FLT_EPSILON || std::max(q1, q2) > 1. - FLT_EPSILON)
			continue;

		mu1 = (mu1 + i*p_i) / q1;
		mu2 = (mu - q1*mu1) / q2;
		sigma = q1*q2*(mu1 - mu2)*(mu1 - mu2);
		if (sigma > max_sigma)
		{
			max_sigma = sigma;
			max_val = i;
		}
	}

	return max_val;
}

static double
cv::getThreshVal_Triangle_8u(const Mat& _src)
{
	Size size = _src.size();
	int step = (int)_src.step;
	if (_src.isContinuous())
	{
		size.width *= size.height;
		size.height = 1;
		step = size.width;
	}

	const int N = 256;
	int i, j, h[N] = { 0 };
	for (i = 0; i < size.height; i++)
	{
		const uchar* src = _src.ptr() + step*i;
		j = 0;
#if CV_ENABLE_UNROLLED
		for (; j <= size.width - 4; j += 4)
		{
			int v0 = src[j], v1 = src[j + 1];
			h[v0]++; h[v1]++;
			v0 = src[j + 2]; v1 = src[j + 3];
			h[v0]++; h[v1]++;
		}
#endif
		for (; j < size.width; j++)
			h[src[j]]++;
	}

	CvPlot::plot("tria_h", &h[0], 256, 1);
	CvPlot::label("o");


	int left_bound = 0, right_bound = 0, max_ind = 0, max = 0;
	int temp;
	bool isflipped = false;

	for (i = 0; i < N; i++)
	{
		if (h[i] > 0)
		{
			left_bound = i;
			break;
		}
	}
	if (left_bound > 0)
		left_bound--;

	for (i = N - 1; i > 0; i--)
	{
		if (h[i] > 0)
		{
			right_bound = i;
			break;
		}
	}
	if (right_bound < N - 1)
		right_bound++;

	for (i = 0; i < N; i++)
	{
		if (h[i] > max)
		{
			max = h[i];
			max_ind = i;
		}
	}

	if (max_ind - left_bound < right_bound - max_ind)
	{
		isflipped = true;
		i = 0, j = N - 1;
		while (i < j)
		{
			temp = h[i]; h[i] = h[j]; h[j] = temp;
			i++; j--;
		}
		left_bound = N - 1 - right_bound;
		max_ind = N - 1 - max_ind;
	}

	CvPlot::plot("tria_h2", &h[0], 256, 1);
	CvPlot::label("o");


	double thresh = left_bound;
	double a, b, dist = 0, tempdist;

	/*
	* We do not need to compute precise distance here. Distance is maximized, so some constants can
	* be omitted. This speeds up a computation a bit.
	*/
	a = max; b = left_bound - max_ind;
	for (i = left_bound + 1; i <= max_ind; i++)
	{
		tempdist = a*i + b*h[i];
		if (tempdist > dist)
		{
			dist = tempdist;
			thresh = i;
		}
	}
	thresh--;

	if (isflipped)
		thresh = N - 1 - thresh;

	return thresh;
}




// https://github.com/subokita/Sandbox/blob/master/otsu.py

void otsu_multi(int * histogram
	, int L
	, int N //total number of pixels
	, double& optimalThresh1
	, double& optimalThresh2)
{
	//Here is my C# implementation of Otsu Multi for 2 thresholds:
	//http://stackoverflow.com/questions/22706742/multi-otsumulti-thresholding-with-opencv
	/* Otsu (1979) - multi */

	//image histogram
	//int[] histogram = new int[256];

	//accumulate image histogram and total number of pixels
	/*foreach(int intensity in image.Data) {
		if (intensity != 0) {
			histogram[intensity] += 1;
			N++;
		}
	}*/

	double W0K, W1K, W2K, M0, M1, M2, currVarB, maxBetweenVar, M0K, M1K, M2K, MT;

	optimalThresh1 = 0;
	optimalThresh2 = 0;

	W0K = 0;
	W1K = 0;

	M0K = 0;
	M1K = 0;

	MT = 0;
	maxBetweenVar = 0;
	for (int k = 0; k < L; k++) {
		MT += k * (histogram[k] / (double)N);
	}


	for (int t1 = 0; t1 < L; t1++) {
		W0K += histogram[t1] / (double)N; //Pi
		M0K += t1 * (histogram[t1] / (double)N); //i * Pi
		M0 = M0K / W0K; //(i * Pi)/Pi

		W1K = 0;
		M1K = 0;

		for (int t2 = t1 + 1; t2 < L; t2++) {
			W1K += histogram[t2] / (double)N; //Pi
			M1K += t2 * (histogram[t2] / (double)N); //i * Pi
			M1 = M1K / W1K; //(i * Pi)/Pi

			W2K = 1 - (W0K + W1K);
			M2K = MT - (M0K + M1K);

			if (W2K <= 0) break;

			M2 = M2K / W2K;

			currVarB = W0K * (M0 - MT) * (M0 - MT) + W1K * (M1 - MT) * (M1 - MT) + W2K * (M2 - MT) * (M2 - MT);

			if (maxBetweenVar < currVarB) {
				maxBetweenVar = currVarB;
				optimalThresh1 = t1;
				optimalThresh2 = t2;
			}
		}
	}
}


void fill_magnitude_original_histogram(cv::Mat& magnitude, int* mag_origin_hist, const int N)
{
	memset(mag_origin_hist, 0, N * sizeof(int));

	for (int i = 0; i < magnitude.rows; i++)
	{
		int* _mag = magnitude.ptr<int>(i);
		for (int j = 0; j < magnitude.cols; j++)
		{
			int m = _mag[j];

			if (m >= N || m < 0)
				printf("i = %d, j = %d m=%d\n", i, j, m);

			if (m >= 0 && m < N)
				mag_origin_hist[m]++;
		}
	}
}

void fill_magnitude_histogram(cv::Mat& magnitude, int* mag_hist, const int N)
{
	memset(mag_hist, 0, N * sizeof(int));

	for (int i = 0; i < magnitude.rows; i++)
	{
		int* _mag = magnitude.ptr<int>(i);
		for (int j = 0; j < magnitude.cols; j++)
		{
			int m = _mag[j];

			if (m >= N || m < 0)
				printf("i = %d, j = %d m=%d\n", i, j, m);

			if (m / 2 >= 0 && m / 2 < N)
				mag_hist[m / 2]++;
		}
	}
}


void fill_gray_histogram(cv::Mat& gray, int* hist, const int N)
{
	memset(hist, 0, N * sizeof(int));

	for (int i = 0; i < gray.rows; i++)
	{
		uchar* _mag = gray.ptr<uchar>(i);
		for (int j = 0; j < gray.cols; j++)
		{
			int m = _mag[j];

			if (m >= N || m < 0)
				printf("i = %d, j = %d m=%d\n", i, j, m);

			if (m >= 0 && m < N)
				hist[m / 2]++;
		}
	}
}



void fill_magnitude_integral_histogram(cv::Mat& magnitude, int* integral_hist, const int N)
{
	memset(integral_hist, 0, N * sizeof(int));

	for (int i = 0; i < magnitude.rows; i++)
	{
		int* _mag = magnitude.ptr<int>(i);

		for (int j = 0; j < magnitude.cols; j++)
		{
			int m = _mag[j];

			if (m >= N || m < 0)
				printf("i = %d, j = %d m=%d\n", i, j, m);

			for (int ii = 0; ii < m; ii += 2)
			{
				if (ii / 2 < N)
					integral_hist[ii / 2]++;
			}
		}
	}
}


void fill_magnitude_im(cv::Mat& magnitude, cv::Mat& im_magnitude)
{
	im_magnitude.create(magnitude.size(), CV_8UC1);
	for (int i = 0; i < magnitude.rows; i++)
	{
		int* _mag = magnitude.ptr<int>(i);
		uchar* im_row = im_magnitude.ptr<uchar>(i);
		for (int j = 0; j < magnitude.cols; j++)
		{
			int m = _mag[j];
			im_row[j] = m <= 255 ? m : 255;
		}
	}
}


int calc_max_index(int* hist, const int N)
{
	int max_hist = -INT_MAX;
	int index_max_hist = -1;
	for (int i = 1; i < N; ++i)
	{
		if (max_hist < hist[i])
		{
			max_hist = hist[i];
			index_max_hist = i * 2;
		}
	}

	return index_max_hist;
}


int calc_min_index(int* hist, const int N)
{
	int min_hist = INT_MAX;
	int index_min_hist = -1;
	for (int i = 1; i < N; ++i)
	{
		if (min_hist > hist[i])
		{
			min_hist = hist[i];
			index_min_hist = i * 2;
		}
	}

	return index_min_hist;
}

void diff(const int* hist, const int N, int * d)
{
	for (int i = 1; i < N; ++i)
	{
		d[i - 1] = hist[i] - hist[i - 1];
	}
}

void diff2(const int* hist, const int N, int * d2)
{
	for (int i = 1; i < N - 1; ++i)
	{
		d2[i - 1] = hist[i - 1] - 2 * hist[i] + hist[i - 1];
	}
}

void diff3(const int* hist, const int N, int * d3)
{
	for (int i = 2; i < N - 2; ++i)
	{
		d3[i - 2] = hist[i + 2] - 2 * hist[i + 1] + 2 * hist[i - 1] - hist[i - 2];
	}
}


void integral(const int* hist, const int N, int * I)
{
	memset(I, 0, N * sizeof(int));
	for (int i = 0; i < N; ++i)
	{
		int m = hist[i];
		for (int ii = 0; ii < i; ii += 1)
		{
			I[ii] += m;
		}
	}
}

#ifdef HAVE_VDOUBLE

int LagerInitialization(vdouble& m_T, vdouble& m_t, int i0, LagerStruct& lgstr, int flag_of_Inf, char* sNumDen, char* sAnaliticEquation, char* sRoots, bool bIncludeRealRoots, Vector<vdouble>& koefs)
{
	char s[1024];
	sprintf(sAnaliticEquation, "\n\nLager Initialization:\n");

	int type, ParamCount;
	double h;
	vdouble Q, koef, param;

	Q = m_T.LagerQuotients(i0, m_t, lgstr, flag_of_Inf, koefs, sNumDen, sRoots);
	int N_lagers = koefs.Size();

	for (int iLag = 0; iLag < N_lagers; iLag++)
	{

		koef = koefs[iLag];
		if (koef.Length() == 4)
		{
			// koef[0] = xk*2.0;  // ����������� ����� ���������
			// koef[1] = - yk*2.0; // ������������� ����������� ����� �������
			// koef[2] = x; // ����������� ���������� ���������� (Real part of root)
			// koef[3] = y; // �������� ������� (Imag part of root)
			type = 3;
			ParamCount = 3;
			param = vdouble(3);
			h = sqrt(koef[0] * koef[0] + koef[1] * koef[1]);
			param[0] = koef[2];
			param[1] = koef[3];
			param[2] = atan2(koef[0], koef[1]);
			if (param[2] > 0)
			{
				sprintf(s, "%g * exp(%g * t) * sin(%g * t + %g)\n", h, param[0], param[1], param[2]);
			}
			if (param[2] < 0)
			{
				sprintf(s, "%g * exp(%g * t) * sin(%g * t - %g)\n", h, param[0], param[1], fabs(param[2]));
			}
			if (param[2] == 0)
			{
				sprintf(s, "%g * exp(%g * t) * sin(%g * t)\n", h, param[0], param[1]);
			}
			strcat(sAnaliticEquation, s);
		}
		if (bIncludeRealRoots && koef.Length() == 2)
		{
			// koef[0] = xk;// ����������� ����� �����������
			// koef[1] = x;// �������������� ������
			type = 1;
			ParamCount = 1;
			param = vdouble(1);
			h = koef[0];
			param[0] = koef[1];

			sprintf(s, "%g * exp(%g * t)\n", h, param[0]);
			strcat(sAnaliticEquation, s);
		}
		if (
			(bIncludeRealRoots && koef.Length() == 2)
			||
			(koef.Length() == 4)
			)
		{

			//pLM = AddLager(i0, type, ParamCount, h);
			//pLM->m_par = param;
		}
	}
	//SimulNnet();
	//ErrorNnet();
	//lgstr.error = m_E;
	/*	fprintf(stderr, "(m_dEdY * m_dEdY).dispWithIndex();");
	(m_dEdY * m_dEdY).dispWithIndex();
	fprintf(stderr, "m_dEdY.CumMeanSquareSumFrom(0).dispWithIndex();");
	m_dEdY.CumMeanSquareSumFrom(0).dispWithIndex();
	//	fprintf(stderr, "m_dEdY.CumMeanSquareSumFrom(0).DiffVector().dispWithIndex();");
	//	m_dEdY.CumMeanSquareSumFrom(0).DiffVector().dispWithIndex();
	fprintf(stderr, " sqrt(m_E) = %g\n", sqrt(m_E));*/
	//WriteRaporto("LagerInitialization end\n");
	return 0;
}

#endif

void cv::adaptiveDelta(InputArray _src, OutputArray _dst,
	int method, int blockSize, const char * smean, const char * sdelta)
{
	cv::Mat src = _src.getMat();
	CV_Assert(src.type() == CV_8UC1);
	CV_Assert(blockSize % 2 == 1 && blockSize > 1);
	cv::Size size = src.size();

	_dst.create(size, src.type());
	cv::Mat mdelta = _dst.getMat();

	cv::Mat mean;
	if (src.data != mdelta.data)
		mean = mdelta;

	if (method == cv::ADAPTIVE_THRESH_MEAN_C)
		boxFilter(src, mean, src.type(), cv::Size(blockSize, blockSize),
		cv::Point(-1, -1), true, cv::BORDER_REPLICATE);
	else if (method == cv::ADAPTIVE_THRESH_GAUSSIAN_C)
		GaussianBlur(src, mean, cv::Size(blockSize, blockSize), 0, 0, cv::BORDER_REPLICATE);
	else
		CV_Error(CV_StsBadFlag, "Unknown/unsupported adaptive threshold method");

	cv::imshow(smean, mean);

	if (src.isContinuous() && mean.isContinuous() && mdelta.isContinuous())
	{
		size.width *= size.height;
		size.height = 1;
	}

	for (int i = 0; i < size.height; i++)
	{
		const uchar* sdata = src.data + src.step*i;
		const uchar* mdata = mean.data + mean.step*i;
		uchar* delta_data = mdelta.data + mdelta.step*i;

		for (int j = 0; j < size.width; j++){
			delta_data[j] = sdata[j] - mdata[j];
		}
	}

	cv::imshow(sdelta, mdelta);
}

#ifdef HAVE_VDOUBLE
void interpolation(int i0
	, int maxOrder
	, double t_end
	, int N
	, int * hist
	, int * hist_rek
	, const char * plot_name
	, int plotInd
	, int plotLen
	, int * hist_rek2 = NULL
	, int * m = NULL
	, const char * plot_name2 = NULL
	)
{
	char * sAnaliticEquation = new char[1024 * 32];
	char * sRoots = new char[1024 * 32];
	char * sNumDen = new char[1024 * 32];


	vdouble m_T;
	vdouble m_t;

	m_T.resize(N);
	m_t.Sequence(0, 1, N - 1);

	for (int i = 0; i < N; ++i)
	{
		m_T[i] = hist[i];
	}

	LagerStruct lgstr;

	lgstr.beta = 0.0;
	lgstr.epsilon = 0.00001;
	lgstr.maxOrder = maxOrder;

	double _alpha_det = m_t.LagerAlphaDeterm(lgstr.maxOrder, t_end, .999);
	lgstr.alpha = _alpha_det;

	printf("_alpha_det = %f\n", _alpha_det);

	int flag_of_Inf = 0;
	bool bIncludeRealRoots = true;


	SystemNnet m_Nnet;
	m_Nnet.Init(m_t, m_T);
	m_Nnet.LagerInitialization(i0, lgstr, flag_of_Inf, sNumDen, sAnaliticEquation, sRoots, bIncludeRealRoots);

	double error = lgstr.error;
	double fund_det = lgstr.fund_det;

	vdouble vQ = lgstr.vQ;
	vdouble alphai = lgstr.alphai;
	vdouble flip_betai = lgstr.flip_betai;
	vdouble num = lgstr.num;
	vdouble den = lgstr.den;

	for (int t = 0; t < N; ++t)
	{
		if (hist_rek2)
			hist_rek2[t] = m[t] * m_Nnet.m_Y[t];
		hist_rek[t] = m_Nnet.m_Y[t];
	}

	if (hist_rek2)
	{
		CvPlot::plot(plot_name2, &hist_rek2[plotInd], plotLen, 1);
		CvPlot::label("rek");
	}

	CvPlot::plot(plot_name, &hist_rek[plotInd], plotLen, 1);
	CvPlot::label("rek");

	TrainParam m_tp;

	m_Nnet.Default();

	m_tp.epochs = 10000;

	m_tp.goalE = 0.0000001;
	m_tp.goalQ = 0.0000001;

	m_tp.min_grad = 1e-14;
	m_tp.show = 100;
	m_tp.full_turn = 5;
	m_Nnet.Train(m_tp, sNumDen, sAnaliticEquation, sRoots, true);
	m_Nnet.SimulNnet();
	m_Nnet.ErrorNnet();


	for (int t = 0; t < N; ++t)
	{
		if (hist_rek2)
			hist_rek2[t] = m[t] * m_Nnet.m_Y[t];
		hist_rek[t] = m_Nnet.m_Y[t];
	}

	if (hist_rek2)
	{
		CvPlot::plot(plot_name2, &hist_rek2[plotInd], plotLen, 1);
		CvPlot::label("trained");
	}

	CvPlot::plot(plot_name, &hist_rek[plotInd], plotLen, 1);
	CvPlot::label("trained");

	printf(sAnaliticEquation);

	delete[] sAnaliticEquation;
	delete[] sRoots;
	delete[] sNumDen;
}

#endif

void cv::MyCannyTests(InputArray _image, OutputArray edges, double integral1, double integral2, int apertureSize, bool L2gradient)
{
	printf("MyCannyEx (integral1=%f, integral2=%f)\n", integral1, integral2);

	const int myCannyPlotLen = 72;
	const int myCannyPlotInd = 0;



	Mat image = _image.getMat();
	int len = image.cols * image.rows;

	// calculate magnitude of gradient
	Mat magnitude;
	MyCanny(_image, magnitude, apertureSize, L2gradient);

	Mat im_magnitude;
	fill_magnitude_im(magnitude, im_magnitude);
	cv::imshow("magnitude of gradient", im_magnitude);

	// (1.0 + k1 * t) * exp (- k2 - k1 * t)
	int integral_hist[myCannyHistBuffSize];
	fill_magnitude_integral_histogram(magnitude, integral_hist, myCannyHistBuffSize);

	CvPlot::clear("integral_hist");
	CvPlot::plot("integral_hist", &integral_hist[myCannyPlotInd], myCannyPlotLen, 1);
	CvPlot::label("s");

	int integral_integral_hist[myCannyHistBuffSize];
	::integral(integral_hist, myCannyHistBuffSize, integral_integral_hist);


	int mag_origin_hist[myCannyHistBuffSize];
	fill_magnitude_original_histogram(magnitude, mag_origin_hist, myCannyHistBuffSize);

	{
		double optimalThresh1;
		double optimalThresh2;

		otsu_multi(mag_origin_hist
			, myCannyHistBuffSize
			, len //total number of pixels
			, optimalThresh1
			, optimalThresh2);
		printf("optimalThresh1=%f, optimalThresh2=%f\n", optimalThresh1, optimalThresh2);

		Mat canny_otsu_multy;
		Canny(_image, canny_otsu_multy, optimalThresh1, optimalThresh2, apertureSize, L2gradient);
		cv::imshow("canny_otsu_multy_origin", canny_otsu_multy);

		CvPlot::clear("mag_origin_hist");
		CvPlot::plot("mag_origin_hist", &mag_origin_hist[myCannyPlotInd], optimalThresh1, 1);
		CvPlot::label("s");
	}


	int mag_hist[myCannyHistBuffSize];
	fill_magnitude_histogram(magnitude, mag_hist, myCannyHistBuffSize);


	{
		double optimalThresh1;
		double optimalThresh2;

		otsu_multi(mag_hist
			, myCannyHistBuffSize
			, len //total number of pixels
			, optimalThresh1
			, optimalThresh2);
		printf("optimalThresh1=%f, optimalThresh2=%f\n", optimalThresh1, optimalThresh2);

		Mat canny_otsu_multy;
		Canny(_image, canny_otsu_multy, optimalThresh1, optimalThresh2, apertureSize, L2gradient);
		cv::imshow("canny_otsu_multy", canny_otsu_multy);


		CvPlot::clear("mag_hist");
		CvPlot::plot("mag_hist", &mag_hist[myCannyPlotInd], optimalThresh1, 1);
		CvPlot::label("m");

	}




#if 1
	{
		cv::Mat src_gray;
		if (image.channels() == 3 || image.channels() == 4)
			cvtColor(image, src_gray, CV_BGR2GRAY);
		else
			src_gray = image.clone();

		int hist[256];
		fill_gray_histogram(src_gray, hist, 256);

		double optimalThresh1;
		double optimalThresh2;

		otsu_multi(hist
			, myCannyHistBuffSize
			, len //total number of pixels
			, optimalThresh1
			, optimalThresh2);
		printf("optimalThresh1=%f, optimalThresh2=%f\n", optimalThresh1, optimalThresh2);

		Mat canny_otsu_multy;
		Canny(src_gray, canny_otsu_multy, optimalThresh1, optimalThresh2, apertureSize, L2gradient);
		cv::imshow("canny_otsu_multy_gray", canny_otsu_multy);


		int otsu_val = otsuThreshold<uchar>(src_gray);


		double otsu_thresh_val = getThreshVal_Otsu_8u(src_gray);
		double triangele_thresh_val = getThreshVal_Triangle_8u(src_gray);
		printf("otsu_val = %d otsu_thresh_val=%f triangele_thresh_val=%f\n", otsu_val, otsu_thresh_val, triangele_thresh_val);

		Mat edges_otsu_canny;
		Canny(src_gray, edges_otsu_canny, otsu_thresh_val / 2, otsu_thresh_val, apertureSize, L2gradient);
		cv::imshow("edges_otsu_canny", edges_otsu_canny);

		//Mat edges_otsu_max_d2_canny;
		//Canny(src_gray, edges_otsu_max_d2_canny, index_max_d2_integral_hist, otsu_thresh_val, apertureSize, L2gradient);
		//cv::imshow("edges_otsu_max_d2_canny", edges_otsu_max_d2_canny);
	}
#endif


	int index_max_mag_hist = calc_max_index(mag_hist, myCannyHistBuffSize);
	printf("index_max_mag_hist=%d\n", index_max_mag_hist);

	int d_mag_hist[myCannyHistBuffSize - 1];
	diff(mag_hist, myCannyHistBuffSize, d_mag_hist);
	int d2_mag_hist[myCannyHistBuffSize - 2];
	diff2(mag_hist, myCannyHistBuffSize, d2_mag_hist);
	int d3_mag_hist[myCannyHistBuffSize - 4];
	diff3(mag_hist, myCannyHistBuffSize, d3_mag_hist);

	CvPlot::clear("d_mag_hist");
	CvPlot::plot("d_mag_hist", &d_mag_hist[myCannyPlotInd], myCannyPlotLen, 1);
	CvPlot::label("d");

	CvPlot::clear("d2_mag_hist");
	CvPlot::plot("d2_mag_hist", &d2_mag_hist[myCannyPlotInd], myCannyPlotLen, 1);
	CvPlot::label("d2");

	CvPlot::clear("d3_mag_hist");
	CvPlot::plot("d3_mag_hist", &d3_mag_hist[myCannyPlotInd], myCannyPlotLen, 1);
	CvPlot::label("d3");

	int index_min_d_mag_hist = calc_min_index(d_mag_hist, myCannyHistBuffSize - 1);
	printf("index_min_d_mag_hist=%d\n", index_min_d_mag_hist);
	int index_max_d_mag_hist = calc_max_index(d_mag_hist, myCannyHistBuffSize - 1);
	printf("index_max_d_mag_hist=%d\n", index_max_d_mag_hist);

	int index_min_d2_mag_hist = calc_min_index(d2_mag_hist, myCannyHistBuffSize - 2);;
	printf("index_min_d2_mag_hist=%d\n", index_min_d2_mag_hist);
	int index_max_d2_mag_hist = calc_max_index(d2_mag_hist, myCannyHistBuffSize - 2);;
	printf("index_max_d2_mag_hist=%d\n", index_max_d2_mag_hist);

#ifdef HAVE_VDOUBLE
	{
		CvPlot::clear("integral_integral_hist");
		CvPlot::plot("integral_integral_hist", &integral_integral_hist[myCannyPlotInd], /*myCannyPlotLen*/256, 1);
		CvPlot::label("s");

		int integral_integral_hist_rek[myCannyHistBuffSize];
		int i0 = 0;
		int maxOrder = 2;
		double t_end = 256;
		int plotLen = 256;

		interpolation(i0
			, maxOrder
			, t_end
			, myCannyHistBuffSize
			, integral_integral_hist
			, integral_integral_hist_rek
			, "integral_integral_hist"
			, myCannyPlotInd
			, plotLen);
	}
#endif

	{
		const double pw = 2;
		double a = 0.0;
		int i0 = 1;
		int maxOrder = 1;
		double t_end = index_min_d_mag_hist * 4;

		int integral_hist_rek_m[myCannyHistBuffSize];
		int integral_hist_per_i[myCannyHistBuffSize];
		for (int i = 1; i < myCannyHistBuffSize; ++i)
		{
			integral_hist_rek_m[i] = (a + pow(i, pw));
			integral_hist_per_i[i] = integral_hist[i] / integral_hist_rek_m[i];
		}


		CvPlot::clear("integral_hist_per_t");
		CvPlot::plot("integral_hist_per_t", &integral_hist_per_i[1], myCannyPlotLen, 1);
		CvPlot::label("m");

#ifdef HAVE_VDOUBLE
		int integral_hist_rek[myCannyHistBuffSize];
		int integral_hist_per_i_rek[myCannyHistBuffSize];
		interpolation(i0
			, maxOrder
			, t_end
			, myCannyHistBuffSize
			, integral_hist_per_i
			, integral_hist_per_i_rek
			, "integral_hist_per_t"
			, myCannyPlotInd
			, myCannyPlotLen
			, integral_hist_rek
			, integral_hist_rek_m
			, "integral_hist"
			);
#endif
	}

	{
		const double pw = 2;
		double a = 0.0;
		int i0 = 3;
		int maxOrder = 1;
		double t_end = index_min_d_mag_hist * 4;

		int mag_hist_rek_m[myCannyHistBuffSize];
		int mag_hist_per_i[myCannyHistBuffSize];
		for (int i = 1; i < myCannyHistBuffSize; ++i)
		{
			mag_hist_rek_m[i] = (a + pow(i, pw));
			mag_hist_per_i[i] = mag_hist[i] / mag_hist_rek_m[i];
		}


		CvPlot::clear("mag_hist_per_t");
		CvPlot::plot("mag_hist_per_t", &mag_hist_per_i[1], myCannyPlotLen, 1);
		CvPlot::label("m");

#ifdef HAVE_VDOUBLE
		int mag_hist_rek[myCannyHistBuffSize];
		int mag_hist_per_i_rek[myCannyHistBuffSize];
		interpolation(3
			, maxOrder
			, t_end
			, myCannyHistBuffSize
			, mag_hist_per_i
			, mag_hist_per_i_rek
			, "mag_hist_per_t"
			, myCannyPlotInd
			, myCannyPlotLen
			, mag_hist_rek
			, mag_hist_rek_m
			, "mag_hist"
			);
#endif
	}


	/*int d_integral_hist[myCannyHistBuffSize - 1];
	diff(integral_hist, myCannyHistBuffSize, d_integral_hist);
	int d2_integral_hist[myCannyHistBuffSize - 2];
	diff2(integral_hist, myCannyHistBuffSize, d2_integral_hist);

	CvPlot::plot("d_integral_hist", &d_integral_hist[0], 400, 1);
	CvPlot::label("d");
	CvPlot::plot("d2_integral_hist", &d2_integral_hist[0], 400, 1);
	CvPlot::label("d2");

	int index_min_d_integral_hist = calc_min_index(d_integral_hist, myCannyHistBuffSize - 1);
	printf("index_min_d_integral_hist=%d\n", index_min_d_integral_hist);

	int index_max_d2_integral_hist = calc_max_index(d2_integral_hist, myCannyHistBuffSize - 2);;
	printf("index_max_d2_integral_hist=%d\n", index_max_d2_integral_hist);
	*/

	{
		double otsu_magnitude_thresh_val_int = otsuThreshold<int>(magnitude);
		double otsu_im_magnitude_thresh_val_uchar = otsuThreshold<uchar>(im_magnitude);
		double otsu_im_magnitude_thresh_val = getThreshVal_Otsu_8u(im_magnitude);
		printf("otsu_magnitude_thresh_val_int = %f otsu_im_magnitude_thresh_val_uchar = %f otsu_im_magnitude_thresh_val = %f\n"
			, otsu_magnitude_thresh_val_int
			, otsu_im_magnitude_thresh_val_uchar, otsu_im_magnitude_thresh_val);




		Mat canny_int_otsu_mag__int_otsu_mag;
		Canny(_image, canny_int_otsu_mag__int_otsu_mag, otsu_magnitude_thresh_val_int, otsu_magnitude_thresh_val_int, apertureSize, L2gradient);
		cv::imshow("canny_int_otsu_mag__int_otsu_mag", canny_int_otsu_mag__int_otsu_mag);

		Mat canny_int_otsu_mag_2__int_otsu_mag;
		Canny(_image, canny_int_otsu_mag_2__int_otsu_mag, otsu_magnitude_thresh_val_int / 2, otsu_magnitude_thresh_val_int, apertureSize, L2gradient);
		cv::imshow("canny_int_otsu_mag_2__int_otsu_mag", canny_int_otsu_mag_2__int_otsu_mag);



		Mat canny_uchar_otsu_mag__uchar_otsu_mag;
		Canny(_image, canny_uchar_otsu_mag__uchar_otsu_mag, otsu_im_magnitude_thresh_val_uchar, otsu_im_magnitude_thresh_val_uchar, apertureSize, L2gradient);
		cv::imshow("canny_uchar_otsu_mag__uchar_otsu_mag", canny_uchar_otsu_mag__uchar_otsu_mag);

		Mat canny_uchar_otsu_mag_2__uchar_otsu_mag;
		Canny(_image, canny_uchar_otsu_mag_2__uchar_otsu_mag, otsu_im_magnitude_thresh_val_uchar / 2, otsu_im_magnitude_thresh_val_uchar, apertureSize, L2gradient);
		cv::imshow("canny_uchar_otsu_mag_2__uchar_otsu_mag", canny_uchar_otsu_mag_2__uchar_otsu_mag);





		Mat canny_max_d_mag__uchar_otsu_mag;
		Canny(_image, canny_max_d_mag__uchar_otsu_mag, index_max_d_mag_hist, otsu_im_magnitude_thresh_val_uchar, apertureSize, L2gradient);
		cv::imshow("canny_max_d_mag__uchar_otsu_mag", canny_max_d_mag__uchar_otsu_mag);

		Mat canny_min_d_mag__uchar_otsu_mag;
		Canny(_image, canny_min_d_mag__uchar_otsu_mag, index_min_d_mag_hist, otsu_im_magnitude_thresh_val_uchar, apertureSize, L2gradient);
		cv::imshow("canny_min_d_mag__uchar_otsu_mag", canny_min_d_mag__uchar_otsu_mag);

		Mat canny_max_mag__uchar_otsu_mag;
		Canny(_image, canny_max_mag__uchar_otsu_mag, index_max_mag_hist, otsu_im_magnitude_thresh_val_uchar, apertureSize, L2gradient);
		cv::imshow("canny_max_mag__uchar_otsu_mag", canny_max_mag__uchar_otsu_mag);



		Mat canny_max_d_mag__int_otsu_mag;
		Canny(_image, canny_max_d_mag__int_otsu_mag, index_max_d_mag_hist, otsu_magnitude_thresh_val_int, apertureSize, L2gradient);
		cv::imshow("canny_max_d_mag__int_otsu_mag", canny_max_d_mag__int_otsu_mag);

		Mat canny_min_d_mag__int_otsu_mag;
		Canny(_image, canny_min_d_mag__int_otsu_mag, index_min_d_mag_hist, otsu_magnitude_thresh_val_int, apertureSize, L2gradient);
		cv::imshow("canny_min_d_mag__int_otsu_mag", canny_min_d_mag__int_otsu_mag);

		Mat canny_max_mag__int_otsu_mag;
		Canny(_image, canny_max_mag__int_otsu_mag, index_max_mag_hist, otsu_magnitude_thresh_val_int, apertureSize, L2gradient);
		cv::imshow("canny_max_mag__int_otsu_mag", canny_max_mag__int_otsu_mag);


	}



	//Mat edges_index_max_d2_integral_hist;
	//Canny(image, edges_index_max_d2_integral_hist, index_max_d2_integral_hist, index_max_d2_integral_hist * 2, apertureSize, L2gradient);
	//cv::imshow("edges_index_max_d2_integral_hist", edges_index_max_d2_integral_hist);

	//Mat edges_index_max_mag_hist;
	//Canny(image, edges_index_max_mag_hist, index_max_mag_hist, index_max_mag_hist * 2, apertureSize, L2gradient);
	//cv::imshow("edges_index_max_mag_hist", edges_index_max_mag_hist);

	double mean_hist[myCannyHistBuffSize];
	for (int i = 0; i < myCannyHistBuffSize; ++i)
	{
		mean_hist[i] = 100 * ((double)integral_hist[i]) / len;
	}

	int thresh1 = 0;
	for (int i = 0; i < myCannyHistBuffSize; ++i)
	{
		if (mean_hist[i] < integral1)
		{
			thresh1 = i * 2;
			break;
		}
	}

	int thresh2 = thresh1 * 2;
	for (int i = 0; i < myCannyHistBuffSize; ++i)
	{
		if (mean_hist[i] < integral2)
		{
			thresh2 = i * 2;
			break;
		}
	}

	printf("thresh1 = %d thresh2 = %d\n", thresh1, thresh2);

	/// Detect edges using canny
	Canny(image, edges, thresh1, thresh2, apertureSize, L2gradient);
}



void cv::MyAdaptiveCanny(InputArray _image, OutputArray edges, int apertureSize, bool L2gradient)
{
	const int myCannyPlotLen = 72;
	const int myCannyPlotInd = 0;

	Mat image = _image.getMat();
	int len = image.cols * image.rows;

	// calculate magnitude of gradient
	Mat magnitude;
	MyCanny(_image, magnitude, apertureSize, L2gradient);

	int mag_origin_hist[myCannyHistBuffSize];
	fill_magnitude_original_histogram(magnitude, mag_origin_hist, myCannyHistBuffSize);

	{
		double optimalThresh1;
		double optimalThresh2;

		otsu_multi(mag_origin_hist
			, myCannyHistBuffSize
			, len //total number of pixels
			, optimalThresh1
			, optimalThresh2);
		printf("optimalThresh1=%f, optimalThresh2=%f\n", optimalThresh1, optimalThresh2);

		//Mat canny_otsu_multy;
		Canny(_image, edges, optimalThresh1, optimalThresh2, apertureSize, L2gradient);
		//cv::imshow("canny_otsu_multy_origin", edges);

		//CvPlot::clear("mag_origin_hist");
		//CvPlot::plot("mag_origin_hist", &mag_origin_hist[myCannyPlotInd], optimalThresh1, 1);
		//CvPlot::label("s");
	}


#if 0
	int mag_hist[myCannyHistBuffSize];
	fill_magnitude_histogram(magnitude, mag_hist, myCannyHistBuffSize);


	{
		double optimalThresh1;
		double optimalThresh2;

		otsu_multi(mag_hist
			, myCannyHistBuffSize
			, len //total number of pixels
			, optimalThresh1
			, optimalThresh2);
		printf("optimalThresh1=%f, optimalThresh2=%f\n", optimalThresh1, optimalThresh2);

		Mat canny_otsu_multy;
		Canny(_image, canny_otsu_multy, optimalThresh1, optimalThresh2, apertureSize, L2gradient);
		cv::imshow("canny_otsu_multy", canny_otsu_multy);


		CvPlot::clear("mag_hist");
		CvPlot::plot("mag_hist", &mag_hist[myCannyPlotInd], optimalThresh1, 1);
		CvPlot::label("m");

	}

#endif


#if 0
	{
		cv::Mat src_gray;
		if (image.channels() == 3 || image.channels() == 4)
			cvtColor(image, src_gray, CV_BGR2GRAY);
		else
			src_gray = image.clone();

		int hist[256];
		fill_gray_histogram(src_gray, hist, 256);

		double optimalThresh1;
		double optimalThresh2;

		otsu_multi(hist
			, myCannyHistBuffSize
			, len //total number of pixels
			, optimalThresh1
			, optimalThresh2);
		printf("optimalThresh1=%f, optimalThresh2=%f\n", optimalThresh1, optimalThresh2);

		Mat canny_otsu_multy;
		Canny(src_gray, canny_otsu_multy, optimalThresh1, optimalThresh2, apertureSize, L2gradient);
		cv::imshow("canny_otsu_multy_gray", canny_otsu_multy);


		int otsu_val = otsuThreshold<uchar>(src_gray);


		double otsu_thresh_val = getThreshVal_Otsu_8u(src_gray);
		double triangele_thresh_val = getThreshVal_Triangle_8u(src_gray);
		printf("otsu_val = %d otsu_thresh_val=%f triangele_thresh_val=%f\n", otsu_val, otsu_thresh_val, triangele_thresh_val);

		Mat edges_otsu_canny;
		Canny(src_gray, edges_otsu_canny, otsu_thresh_val / 2, otsu_thresh_val, apertureSize, L2gradient);
		cv::imshow("edges_otsu_canny", edges_otsu_canny);

		//Mat edges_otsu_max_d2_canny;
		//Canny(src_gray, edges_otsu_max_d2_canny, index_max_d2_integral_hist, otsu_thresh_val, apertureSize, L2gradient);
		//cv::imshow("edges_otsu_max_d2_canny", edges_otsu_max_d2_canny);
	}
#endif


}



void cv::MyCannyEx(InputArray _image, OutputArray edges, double integral1, double integral2, int apertureSize, bool L2gradient)
{
	printf("MyCannyEx (integral1=%f, integral2=%f)\n", integral1, integral2);

	const int myCannyPlotLen = 72;
	const int myCannyPlotInd = 0;

	Mat image = _image.getMat();
	int len = image.cols * image.rows;

	// calculate magnitude of gradient
	Mat magnitude;
	MyCanny(_image, magnitude, apertureSize, L2gradient);


	// (1.0 + k1 * t) * exp (- k2 - k1 * t)
	int integral_hist[myCannyHistBuffSize];
	fill_magnitude_integral_histogram(magnitude, integral_hist, myCannyHistBuffSize);

	CvPlot::clear("integral_hist");
	CvPlot::plot("integral_hist", &integral_hist[myCannyPlotInd], myCannyPlotLen, 1);
	CvPlot::label("s");


	double mean_hist[myCannyHistBuffSize];
	for (int i = 0; i < myCannyHistBuffSize; ++i)
	{
		mean_hist[i] = 100 * ((double)integral_hist[i]) / len;
	}

	int thresh1 = 0;
	for (int i = 0; i < myCannyHistBuffSize; ++i)
	{
		if (mean_hist[i] < integral1)
		{
			thresh1 = i * 2;
			break;
		}
	}

	int thresh2 = thresh1 * 2;
	for (int i = 0; i < myCannyHistBuffSize; ++i)
	{
		if (mean_hist[i] < integral2)
		{
			thresh2 = i * 2;
			break;
		}
	}

	printf("thresh1 = %d thresh2 = %d\n", thresh1, thresh2);

	/// Detect edges using canny
	Canny(image, edges, thresh1, thresh2, apertureSize, L2gradient);
}


void cv::MyCanny(InputArray _src, OutputArray _magnitude,
	int aperture_size, bool L2gradient)
{
	Mat src = _src.getMat();
	CV_Assert(src.depth() == CV_8U);

	_magnitude.create(cv::Size(src.cols, src.rows), CV_32S);
	Mat magnitude = _magnitude.getMat();


	if (!L2gradient && (aperture_size & CV_CANNY_L2_GRADIENT) == CV_CANNY_L2_GRADIENT)
	{
		//backward compatibility
		aperture_size &= ~CV_CANNY_L2_GRADIENT;
		L2gradient = true;
	}

	if ((aperture_size & 1) == 0 || (aperture_size != -1 && (aperture_size < 3 || aperture_size > 7)))
		CV_Error(CV_StsBadFlag, "");

#ifdef HAVE_TEGRA_OPTIMIZATION
	if (tegra::canny(src, dst, low_thresh, high_thresh, aperture_size, L2gradient))
		return;
#endif

#ifdef USE_IPP_CANNY
	if (aperture_size == 3 && !L2gradient &&
		ippCanny(src, dst, (float)low_thresh, (float)high_thresh))
		return;
#endif

	const int cn = src.channels();
	Mat dx(src.rows, src.cols, CV_16SC(cn));
	Mat dy(src.rows, src.cols, CV_16SC(cn));

	Sobel(src, dx, CV_16S, 1, 0, aperture_size, 1, 0, cv::BORDER_REPLICATE);
	Sobel(src, dy, CV_16S, 0, 1, aperture_size, 1, 0, cv::BORDER_REPLICATE);

	ptrdiff_t mapstep = src.cols + 2;
	AutoBuffer<uchar> buffer((src.cols + 2)*(src.rows + 2) + cn * mapstep * 3 * sizeof(int));

	int* mag_buf[3];
	mag_buf[0] = (int*)(uchar*)buffer;
	mag_buf[1] = mag_buf[0] + mapstep*cn;
	mag_buf[2] = mag_buf[1] + mapstep*cn;
	memset(mag_buf[0], 0, /* cn* */mapstep*sizeof(int));

	uchar* map = (uchar*)(mag_buf[2] + mapstep*cn);
	memset(map, 1, mapstep);
	memset(map + mapstep*(src.rows + 1), 1, mapstep);

	int maxsize = std::max(1 << 10, src.cols * src.rows / 10);
	std::vector<uchar*> stack(maxsize);
	uchar **stack_top = &stack[0];
	uchar **stack_bottom = &stack[0];


	/* sector numbers
	(Top-Left Origin)

	1   2   3
	*  *  *
	* * *
	0*******0
	* * *
	*  *  *
	3   2   1
	*/

#define CANNY_PUSH(d)    *(d) = uchar(2), *stack_top++ = (d)
#define CANNY_POP(d)     (d) = *--stack_top

	// calculate magnitude and angle of gradient, perform non-maxima suppression.
	// fill the map with one of the following values:
	//   0 - the pixel might belong to an edge
	//   1 - the pixel can not belong to an edge
	//   2 - the pixel does belong to an edge
	for (int i = 0; i <= src.rows; i++)
	{
		int* _norm = mag_buf[(i > 0) + 1] + 1;
		if (i < src.rows)
		{
			short* _dx = dx.ptr<short>(i);
			short* _dy = dy.ptr<short>(i);

			if (!L2gradient)
			{
				for (int j = 0; j < src.cols*cn; j++)
					_norm[j] = std::abs(int(_dx[j])) + std::abs(int(_dy[j]));
			}
			else
			{
				for (int j = 0; j < src.cols*cn; j++)
					_norm[j] = int(_dx[j])*_dx[j] + int(_dy[j])*_dy[j];
			}

			if (cn > 1)
			{
				for (int j = 0, jn = 0; j < src.cols; ++j, jn += cn)
				{
					int maxIdx = jn;
					for (int k = 1; k < cn; ++k)
					if (_norm[jn + k] > _norm[maxIdx]) maxIdx = jn + k;
					_norm[j] = _norm[maxIdx];
					_dx[j] = _dx[maxIdx];
					_dy[j] = _dy[maxIdx];
				}
			}
			_norm[-1] = _norm[src.cols] = 0;
		}
		else
			memset(_norm - 1, 0, /* cn* */mapstep*sizeof(int));

		// at the very beginning we do not have a complete ring
		// buffer of 3 magnitude rows for non-maxima suppression
		if (i == 0)
			continue;

		uchar* _map = map + mapstep*i + 1;
		_map[-1] = _map[src.cols] = 1;

		int* _mag = mag_buf[1] + 1; // take the central row
		ptrdiff_t magstep1 = mag_buf[2] - mag_buf[1];
		ptrdiff_t magstep2 = mag_buf[0] - mag_buf[1];

		const short* _x = dx.ptr<short>(i - 1);
		const short* _y = dy.ptr<short>(i - 1);

		if ((stack_top - stack_bottom) + src.cols > maxsize)
		{
			int sz = (int)(stack_top - stack_bottom);
			maxsize = maxsize * 3 / 2;
			stack.resize(maxsize);
			stack_bottom = &stack[0];
			stack_top = stack_bottom + sz;
		}

		int* pmagnitude = magnitude.ptr<int>(i-1);

		int prev_flag = 0;
		for (int j = 0; j < src.cols; j++)
		{
			int m = _mag[j];
			pmagnitude[j] = m;
		}

		// scroll the ring buffer
		_mag = mag_buf[0];
		mag_buf[0] = mag_buf[1];
		mag_buf[1] = mag_buf[2];
		mag_buf[2] = _mag;
	}
}


#if 0
void cv::MyCanny(InputArray _src, int *sum_hist,
	int aperture_size, bool L2gradient)
{
	memset(sum_hist, 0, myCannyHistBuffSize * sizeof(int));

	Mat src = _src.getMat();
	CV_Assert(src.depth() == CV_8U);


	if (!L2gradient && (aperture_size & CV_CANNY_L2_GRADIENT) == CV_CANNY_L2_GRADIENT)
	{
		//backward compatibility
		aperture_size &= ~CV_CANNY_L2_GRADIENT;
		L2gradient = true;
	}

	if ((aperture_size & 1) == 0 || (aperture_size != -1 && (aperture_size < 3 || aperture_size > 7)))
		CV_Error(CV_StsBadFlag, "");

#ifdef HAVE_TEGRA_OPTIMIZATION
	if (tegra::canny(src, dst, low_thresh, high_thresh, aperture_size, L2gradient))
		return;
#endif

#ifdef USE_IPP_CANNY
	if (aperture_size == 3 && !L2gradient &&
		ippCanny(src, dst, (float)low_thresh, (float)high_thresh))
		return;
#endif

	const int cn = src.channels();
	Mat dx(src.rows, src.cols, CV_16SC(cn));
	Mat dy(src.rows, src.cols, CV_16SC(cn));

	Sobel(src, dx, CV_16S, 1, 0, aperture_size, 1, 0, cv::BORDER_REPLICATE);
	Sobel(src, dy, CV_16S, 0, 1, aperture_size, 1, 0, cv::BORDER_REPLICATE);

	ptrdiff_t mapstep = src.cols + 2;
	AutoBuffer<uchar> buffer((src.cols + 2)*(src.rows + 2) + cn * mapstep * 3 * sizeof(int));

	int* mag_buf[3];
	mag_buf[0] = (int*)(uchar*)buffer;
	mag_buf[1] = mag_buf[0] + mapstep*cn;
	mag_buf[2] = mag_buf[1] + mapstep*cn;
	memset(mag_buf[0], 0, /* cn* */mapstep*sizeof(int));

	uchar* map = (uchar*)(mag_buf[2] + mapstep*cn);
	memset(map, 1, mapstep);
	memset(map + mapstep*(src.rows + 1), 1, mapstep);

	int maxsize = std::max(1 << 10, src.cols * src.rows / 10);
	std::vector<uchar*> stack(maxsize);
	uchar **stack_top = &stack[0];
	uchar **stack_bottom = &stack[0];


	/* sector numbers
	(Top-Left Origin)

	1   2   3
	 *  *  *
	  * * *
	0*******0
	  * * *
	 *  *  *
	3   2   1
	*/

#define CANNY_PUSH(d)    *(d) = uchar(2), *stack_top++ = (d)
#define CANNY_POP(d)     (d) = *--stack_top

	// calculate magnitude and angle of gradient, perform non-maxima suppression.
	// fill the map with one of the following values:
	//   0 - the pixel might belong to an edge
	//   1 - the pixel can not belong to an edge
	//   2 - the pixel does belong to an edge
	for (int i = 0; i <= src.rows; i++)
	{
		int* _norm = mag_buf[(i > 0) + 1] + 1;
		if (i < src.rows)
		{
			short* _dx = dx.ptr<short>(i);
			short* _dy = dy.ptr<short>(i);

			if (!L2gradient)
			{
				for (int j = 0; j < src.cols*cn; j++)
					_norm[j] = std::abs(int(_dx[j])) + std::abs(int(_dy[j]));
			}
			else
			{
				for (int j = 0; j < src.cols*cn; j++)
					_norm[j] = int(_dx[j])*_dx[j] + int(_dy[j])*_dy[j];
			}

			if (cn > 1)
			{
				for (int j = 0, jn = 0; j < src.cols; ++j, jn += cn)
				{
					int maxIdx = jn;
					for (int k = 1; k < cn; ++k)
					if (_norm[jn + k] > _norm[maxIdx]) maxIdx = jn + k;
					_norm[j] = _norm[maxIdx];
					_dx[j] = _dx[maxIdx];
					_dy[j] = _dy[maxIdx];
				}
			}
			_norm[-1] = _norm[src.cols] = 0;
		}
		else
			memset(_norm - 1, 0, /* cn* */mapstep*sizeof(int));

		// at the very beginning we do not have a complete ring
		// buffer of 3 magnitude rows for non-maxima suppression
		if (i == 0)
			continue;

		uchar* _map = map + mapstep*i + 1;
		_map[-1] = _map[src.cols] = 1;

		int* _mag = mag_buf[1] + 1; // take the central row
		ptrdiff_t magstep1 = mag_buf[2] - mag_buf[1];
		ptrdiff_t magstep2 = mag_buf[0] - mag_buf[1];

		const short* _x = dx.ptr<short>(i - 1);
		const short* _y = dy.ptr<short>(i - 1);

		if ((stack_top - stack_bottom) + src.cols > maxsize)
		{
			int sz = (int)(stack_top - stack_bottom);
			maxsize = maxsize * 3 / 2;
			stack.resize(maxsize);
			stack_bottom = &stack[0];
			stack_top = stack_bottom + sz;
		}

		int prev_flag = 0;
		for (int j = 0; j < src.cols; j++)
		{
			int m = _mag[j];

			if (m >= myCannyHistBuffSize)
				printf("m=%d\n", m);

			for (int ii = 0; ii < m; ++ii)
			{
				if (ii < myCannyHistBuffSize)
					sum_hist[ii]++;
			}
		}

		// scroll the ring buffer
		_mag = mag_buf[0];
		mag_buf[0] = mag_buf[1];
		mag_buf[1] = mag_buf[2];
		mag_buf[2] = _mag;


	}
}
#endif

#if 0
void cv::MyCanny(InputArray _src, OutputArray _dst,
	double low_thresh, double high_thresh,
	int aperture_size, bool L2gradient)
{
	Mat src = _src.getMat();
	CV_Assert(src.depth() == CV_8U);

	_dst.create(src.size(), CV_8U);
	Mat dst = _dst.getMat();

	if (!L2gradient && (aperture_size & CV_CANNY_L2_GRADIENT) == CV_CANNY_L2_GRADIENT)
	{
		//backward compatibility
		aperture_size &= ~CV_CANNY_L2_GRADIENT;
		L2gradient = true;
	}

	if ((aperture_size & 1) == 0 || (aperture_size != -1 && (aperture_size < 3 || aperture_size > 7)))
		CV_Error(CV_StsBadFlag, "");

	if (low_thresh > high_thresh)
		std::swap(low_thresh, high_thresh);

#ifdef HAVE_TEGRA_OPTIMIZATION
	if (tegra::canny(src, dst, low_thresh, high_thresh, aperture_size, L2gradient))
		return;
#endif

#ifdef USE_IPP_CANNY
	if (aperture_size == 3 && !L2gradient &&
		ippCanny(src, dst, (float)low_thresh, (float)high_thresh))
		return;
#endif

	const int cn = src.channels();
	Mat dx(src.rows, src.cols, CV_16SC(cn));
	Mat dy(src.rows, src.cols, CV_16SC(cn));

	Sobel(src, dx, CV_16S, 1, 0, aperture_size, 1, 0, cv::BORDER_REPLICATE);
	Sobel(src, dy, CV_16S, 0, 1, aperture_size, 1, 0, cv::BORDER_REPLICATE);

	cv::imshow("src", src);
	cv::imshow("dy", dy);
	cv::imshow("dx", dx);
	cv::waitKey();

	if (L2gradient)
	{
		low_thresh = std::min(32767.0, low_thresh);
		high_thresh = std::min(32767.0, high_thresh);

		if (low_thresh > 0) low_thresh *= low_thresh;
		if (high_thresh > 0) high_thresh *= high_thresh;
	}
	int low = cvFloor(low_thresh);
	int high = cvFloor(high_thresh);

	ptrdiff_t mapstep = src.cols + 2;
	AutoBuffer<uchar> buffer((src.cols + 2)*(src.rows + 2) + cn * mapstep * 3 * sizeof(int));

	int* mag_buf[3];
	mag_buf[0] = (int*)(uchar*)buffer;
	mag_buf[1] = mag_buf[0] + mapstep*cn;
	mag_buf[2] = mag_buf[1] + mapstep*cn;
	memset(mag_buf[0], 0, /* cn* */mapstep*sizeof(int));

	printf("%d %d %d\n", mag_buf[0], mag_buf[1], mag_buf[2]);
	printf("%d %d %d\n", (*(mag_buf[0])), (*(mag_buf[1])), (*(mag_buf[2])));

	uchar* map = (uchar*)(mag_buf[2] + mapstep*cn);
	memset(map, 1, mapstep);
	memset(map + mapstep*(src.rows + 1), 1, mapstep);

	int maxsize = std::max(1 << 10, src.cols * src.rows / 10);
	std::vector<uchar*> stack(maxsize);
	uchar **stack_top = &stack[0];
	uchar **stack_bottom = &stack[0];

	int hist[myCannyHistBuffSize];
	memset(hist, 0, myCannyHistBuffSize * sizeof(int));
	int sum_hist[myCannyHistBuffSize];
	memset(sum_hist, 0, myCannyHistBuffSize * sizeof(int));

	/* sector numbers
	(Top-Left Origin)

	1   2   3
	 *  *  *
	  * * *
	0*******0
	  * * *
	 *  *  *
	3   2   1
	*/

#define CANNY_PUSH(d)    *(d) = uchar(2), *stack_top++ = (d)
#define CANNY_POP(d)     (d) = *--stack_top

	// calculate magnitude and angle of gradient, perform non-maxima suppression.
	// fill the map with one of the following values:
	//   0 - the pixel might belong to an edge
	//   1 - the pixel can not belong to an edge
	//   2 - the pixel does belong to an edge
	for (int i = 0; i <= src.rows; i++)
	{
		int* _norm = mag_buf[(i > 0) + 1] + 1;
		if (i < src.rows)
		{
			short* _dx = dx.ptr<short>(i);
			short* _dy = dy.ptr<short>(i);
			/*
			CvPlot::plot("Sobel", _dx, src.cols, 1);
			CvPlot::label("dx0");
			CvPlot::plot("Sobel", _dy, src.cols, 1);
			CvPlot::label("dy0");*/
			//cv::waitKey();

			if (!L2gradient)
			{
				for (int j = 0; j < src.cols*cn; j++)
					_norm[j] = std::abs(int(_dx[j])) + std::abs(int(_dy[j]));
			}
			else
			{
				for (int j = 0; j < src.cols*cn; j++)
					_norm[j] = int(_dx[j])*_dx[j] + int(_dy[j])*_dy[j];
			}

			if (cn > 1)
			{
				for (int j = 0, jn = 0; j < src.cols; ++j, jn += cn)
				{
					int maxIdx = jn;
					for (int k = 1; k < cn; ++k)
					if (_norm[jn + k] > _norm[maxIdx]) maxIdx = jn + k;
					_norm[j] = _norm[maxIdx];
					_dx[j] = _dx[maxIdx];
					_dy[j] = _dy[maxIdx];
				}
			}
			_norm[-1] = _norm[src.cols] = 0;
		}
		else
			memset(_norm - 1, 0, /* cn* */mapstep*sizeof(int));

		// at the very beginning we do not have a complete ring
		// buffer of 3 magnitude rows for non-maxima suppression
		if (i == 0)
			continue;

		uchar* _map = map + mapstep*i + 1;
		_map[-1] = _map[src.cols] = 1;

		int* _mag = mag_buf[1] + 1; // take the central row
		ptrdiff_t magstep1 = mag_buf[2] - mag_buf[1];
		ptrdiff_t magstep2 = mag_buf[0] - mag_buf[1];

		//printf("magstep1 = %d magstep2 = %d\n", magstep1, magstep2);

		const short* _x = dx.ptr<short>(i - 1);
		const short* _y = dy.ptr<short>(i - 1);

		/*CvPlot::plot("Sobel", _x, src.cols, 1);
		CvPlot::label("dx");
		CvPlot::plot("Sobel", _y, src.cols, 1);
		CvPlot::label("dy");
		cv::waitKey();*/


		if ((stack_top - stack_bottom) + src.cols > maxsize)
		{
			int sz = (int)(stack_top - stack_bottom);
			maxsize = maxsize * 3 / 2;
			stack.resize(maxsize);
			stack_bottom = &stack[0];
			stack_top = stack_bottom + sz;
		}

		int prev_flag = 0;
		for (int j = 0; j < src.cols; j++)
		{
#define CANNY_SHIFT 15
			const int TG22 = (int)(0.4142135623730950488016887242097*(1 << CANNY_SHIFT) + 0.5);

			int m = _mag[j];

			for (int i = 0; i < m; ++i)
			{
				sum_hist[i]++;
			}

			if (m/2 >= 0 && m/2 < myCannyHistBuffSize)
				hist[m/2]++;
			else
				printf("extremum = %d\n", m);

			if (m > low)
			{
				int xs = _x[j];
				int ys = _y[j];
				int x = std::abs(xs);
				int y = std::abs(ys) << CANNY_SHIFT;

				int tg22x = x * TG22;

				if (y < tg22x)
				{
					if (m > _mag[j - 1] && m >= _mag[j + 1])
					{
//						printf("if (y %d < tg22x %d) if (m %d > _mag[j - 1] %d && m %d >= _mag[j + 1] %d)\n",
//							y, tg22x, m, _mag[j - 1], m, _mag[j + 1]);
						goto __ocv_canny_push;
					}
				}
				else
				{
					int tg67x = tg22x + (x << (CANNY_SHIFT + 1));
					if (y > tg67x)
					{
						if (m > _mag[j + magstep2] && m >= _mag[j + magstep1])
						{
//							printf("if (y %d > tg67x %d ) if (m %d > _mag[j + magstep2] %d && m %d >= _mag[j + magstep1] %d)\n",
//								y, tg67x, m, _mag[j + magstep2], m, _mag[j + magstep1]);
							goto __ocv_canny_push;
						}
					}
					else
					{
						int s = (xs ^ ys) < 0 ? -1 : 1;
						if (m > _mag[j + magstep2 - s] && m > _mag[j + magstep1 + s])
						{
//							printf("if (m > _mag[j + magstep2 - s] && m > _mag[j + magstep1 + s])\n",
//								m, _mag[j + magstep2 - s], m, _mag[j + magstep1 + s]);
							goto __ocv_canny_push;
						}
					}
				}
			}
			prev_flag = 0;
			_map[j] = uchar(1);//   1 - the pixel can not belong to an edge
			continue;
		__ocv_canny_push:
			if (!prev_flag && m > high && _map[j - mapstep] != 2)
			{
				CANNY_PUSH(_map + j);//   2 - the pixel does belong to an edge
				prev_flag = 1;
			}
			else //   0 - the pixel might belong to an edge
				_map[j] = 0;
		}

		// scroll the ring buffer
		_mag = mag_buf[0];
		mag_buf[0] = mag_buf[1];
		mag_buf[1] = mag_buf[2];
		mag_buf[2] = _mag;
	}

	// now track the edges (hysteresis thresholding)
	while (stack_top > stack_bottom)
	{
		uchar* m;
		if ((stack_top - stack_bottom) + 8 > maxsize)
		{
			int sz = (int)(stack_top - stack_bottom);
			maxsize = maxsize * 3 / 2;
			stack.resize(maxsize);
			stack_bottom = &stack[0];
			stack_top = stack_bottom + sz;
		}

		CANNY_POP(m);

		if (!m[-1])         CANNY_PUSH(m - 1);
		if (!m[1])          CANNY_PUSH(m + 1);
		if (!m[-mapstep - 1]) CANNY_PUSH(m - mapstep - 1);
		if (!m[-mapstep])   CANNY_PUSH(m - mapstep);
		if (!m[-mapstep + 1]) CANNY_PUSH(m - mapstep + 1);
		if (!m[mapstep - 1])  CANNY_PUSH(m + mapstep - 1);
		if (!m[mapstep])    CANNY_PUSH(m + mapstep);
		if (!m[mapstep + 1])  CANNY_PUSH(m + mapstep + 1);
	}

	// the final pass, form the final image
	const uchar* pmap = map + mapstep + 1;
	uchar* pdst = dst.ptr();
	for (int i = 0; i < src.rows; i++, pmap += mapstep, pdst += dst.step)
	{
		for (int j = 0; j < src.cols; j++)
			pdst[j] = (uchar)-(pmap[j] >> 1);
	}



	CvPlot::plot("sum_hist", &sum_hist[0], 400, 1);
	CvPlot::label("m");

	CvPlot::plot("hist", &hist[25], 200, 1);
	CvPlot::label("m");

	cv::imshow("dst", dst);
	cv::waitKey();

}
#endif
void cvMyCanny(const CvArr* image, CvArr* edges, double threshold1,
	double threshold2, int aperture_size)
{
	cv::Mat src = cv::cvarrToMat(image), dst = cv::cvarrToMat(edges);
	CV_Assert(src.size == dst.size && src.depth() == CV_8U && dst.type() == CV_8U);

	cv::Canny(src, dst, threshold1, threshold2, aperture_size & 255,
		(aperture_size & CV_CANNY_L2_GRADIENT) != 0);
}

/* End of file. */
