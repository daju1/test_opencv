#include <opencv/cv.h>
#include <opencv/highgui.h>
#include "MultiOtsu.h"


template <class T> T **Alloc2DMat(size_t size1, size_t size2)
{
	T **v;
	size_t k;
	v = new T*[size1];
	v[0] = new T[size1 * size2];

	for (k = 1; k < size1; k++)
		v[k] = v[k - 1] + size2;
	return (v);
}

template <class T> void Free2DMat(T ** v)
{
	if (v[0])
		delete[] v[0];
	if (v)
		delete[] v;
}

class Multi_OtsuThreshold
{
	static int NGRAY;

	float** P;
	float** S;
	float** H;

public:

	Multi_OtsuThreshold()
	{
		P = Alloc2DMat<float>(NGRAY, NGRAY);
		S = Alloc2DMat<float>(NGRAY, NGRAY);
		H = Alloc2DMat<float>(NGRAY, NGRAY);
	}

	virtual ~Multi_OtsuThreshold()
	{
		Free2DMat<float>(P);
		Free2DMat<float>(S);
		Free2DMat<float>(H);
	}


	void calc_thresholds(const cv::Mat& src, int MLEVEL, cv::Mat& thresholds)
	{
		int width = src.cols;
		int height = src.rows;
		////////////////////////////////////////////
		// Build Histogram
		////////////////////////////////////////////
		float* h = new float[NGRAY];
		const unsigned char*  pixels = src.ptr<unsigned char>();
		buildHistogram(h, pixels, width, height);

		/////////////////////////////////////////////
		// Build lookup tables from h
		////////////////////////////////////////////
		buildLookupTables(P, S, H, h);

		////////////////////////////////////////////////////////
		// now M level loop   MLEVEL dependent term
		////////////////////////////////////////////////////////

		float maxSig = findMaxSigma(MLEVEL, H, thresholds.ptr<int>());


		//CvPlot::clear("Histogram");
		//CvPlot::plot("Histogram", &h[threshold[1]], NGRAY - threshold[1], 1);

		printf("MultiOtsu thresholds: ");
		for (int i = 0; i < MLEVEL; ++i)
			printf("%d ", thresholds.ptr<int>()[i]);
		printf("\n");

		delete[] h;
	}

	void run(const cv::Mat& src, cv::Mat& regions, int mlevel)
	{
		cv::Mat thresholds;
		thresholds.create(1, mlevel, CV_32S);
		calc_thresholds(src, mlevel, thresholds);

		///////////////////////////////////////////////////////////////
		// calc regions works for any MLEVEL
		///////////////////////////////////////////////////////////////
		int width = src.cols;
		int height = src.rows;
		const unsigned char*  pixels = src.ptr<unsigned char>();
		calcRegions(regions, mlevel, thresholds.ptr<int>(), pixels, width, height);
	}

	void edges(const cv::Mat& src, cv::Mat& edges, int mlevel)
	{
		cv::Mat thresholds;
		thresholds.create(1, mlevel, CV_32S);
		calc_thresholds(src, mlevel, thresholds);

		uchar * p = edges.ptr();
		int N = src.size().width * src.size().height;
		for (int i = 0; i < N; ++i)
			p[i] = 0;

		///////////////////////////////////////////////////////////////
		// calc regions works for any MLEVEL
		///////////////////////////////////////////////////////////////
		calcEdges(src, edges, mlevel, thresholds.ptr<int>());
	}

private:
	void buildHistogram(float* h, const unsigned char* pixels, int width, int height)
	{
		for (int i = 0; i < NGRAY; ++i)
			h[i] = 0.0;

		// note Java unsigned char is signed. in order to make it 0 to 255 you have to
		// do int pix = 0xff & pixels[i];
		for (int i = 0; i < width*height; ++i)
			h[(int)(pixels[i] & 0xff)]++;

		// note the probability of grey i is h[i]/(width*height)
		float hmax = 0.f;
		for (int i = 0; i < NGRAY; ++i)
		{
			h[i] /= ((float)(width*height));
			if (hmax < h[i])
				hmax = h[i];
		}

	}

	void buildLookupTables(float** P, float** S, float** H, float* h)
	{
		// initialize
		for (int j = 0; j < NGRAY; j++)
		for (int i = 0; i < NGRAY; ++i)
		{
			P[i][j] = 0.f;
			S[i][j] = 0.f;
			H[i][j] = 0.f;
		}
		// diagonal 
		for (int i = 1; i < NGRAY; ++i)
		{
			P[i][i] = h[i];
			S[i][i] = ((float)i)*h[i];

		}
		// calculate first row (row 0 is all zero)
		for (int i = 1; i < NGRAY - 1; ++i)
		{
			P[1][i + 1] = P[1][i] + h[i + 1];
			S[1][i + 1] = S[1][i] + ((float)(i + 1))*h[i + 1];
		}
		// using row 1 to calculate others
		for (int i = 2; i < NGRAY; i++)
		for (int j = i + 1; j < NGRAY; j++)
		{
			P[i][j] = P[1][j] - P[1][i - 1];
			S[i][j] = S[1][j] - S[1][i - 1];

		}
		// now calculate H[i][j]
		for (int i = 1; i < NGRAY; ++i)
		for (int j = i + 1; j < NGRAY; j++)
		{
			if (P[i][j] != 0){
				H[i][j] = (S[i][j] * S[i][j]) / P[i][j];
			}
			else
				H[i][j] = 0.f;
		}

	}

	float findMaxSigma(int mlevel, float** H, int* t)
	{
		t[0] = 0;
		float maxSig = 0.f;
		switch (mlevel)
		{
		case 2:
			for (int i = 1; i < NGRAY - mlevel; i++) // t1
			{
				float Sq = H[1][i] + H[i + 1][255];
				if (maxSig < Sq)
				{
					t[1] = i;
					maxSig = Sq;
				}
			}
			break;
		case 3:
			for (int i = 1; i < NGRAY - mlevel; i++) // t1
			for (int j = i + 1; j < NGRAY - mlevel + 1; j++) // t2
			{
				float Sq = H[1][i] + H[i + 1][j] + H[j + 1][255];
				if (maxSig < Sq)
				{
					t[1] = i;
					t[2] = j;
					maxSig = Sq;
				}
			}
			break;
		case 4:
			for (int i = 1; i < NGRAY - mlevel; i++) // t1
			for (int j = i + 1; j < NGRAY - mlevel + 1; j++) // t2
			for (int k = j + 1; k < NGRAY - mlevel + 2; k++) // t3
			{
				float Sq = H[1][i] + H[i + 1][j] + H[j + 1][k] + H[k + 1][255];
				if (maxSig < Sq)
				{
					t[1] = i;
					t[2] = j;
					t[3] = k;
					maxSig = Sq;
				}
			}
			break;
		case 5:
			for (int i = 1; i < NGRAY - mlevel; i++) // t1
			for (int j = i + 1; j < NGRAY - mlevel + 1; j++) // t2
			for (int k = j + 1; k < NGRAY - mlevel + 2; k++) // t3
			for (int m = k + 1; m < NGRAY - mlevel + 3; m++) // t4
			{
				float Sq = H[1][i] + H[i + 1][j] + H[j + 1][k] + H[k + 1][m] + H[m + 1][255];
				if (maxSig < Sq)
				{
					t[1] = i;
					t[2] = j;
					t[3] = k;
					t[4] = m;
					maxSig = Sq;
				}
			}
			break;
		}
		return maxSig;
	}
public:
	void calcRegions(cv::Mat& result, int mlevel, int* t, const unsigned char* pixels, int width, int height)
	{
		for (int i = 0; i < width*height; ++i)
		{
			int val = 0xff & pixels[i];
			for (int k = 0; k < mlevel; k++)
			{
				if (k < mlevel - 1)
				{
					if (t[k] <= val && val < t[k + 1]) // k-1 region
						result.at<unsigned char>(i / width, i % width) = (t[k] + t[k + 1]) / 2;
				}
				else // k= mlevel-1 last region
				{
					if (t[k] <= val)
						result.at<unsigned char>(i / width, i % width) = (t[k] + 255) / 2;
				}
			}
		}
	}

	void calcEdges(const cv::Mat& src, cv::Mat& result, int mlevel, int* t)
	{
		for (int i = 1; i < src.rows - 1; i++)
		{
			const unsigned char* row0 = src.ptr<unsigned char>(i - 1);
			const unsigned char* row1 = src.ptr<unsigned char>(i);
			const unsigned char* row2 = src.ptr<unsigned char>(i + 1);

			unsigned char* row = result.ptr<unsigned char>(i);

			for (int j = 1; j < src.cols - 1; j++)
			{
				unsigned char val = row1[j];

				int nSwitchedIntervals = 0;
				if (switchedInterval(row1[j - 1], row1[j + 1], mlevel, t))
					++nSwitchedIntervals;

				if (switchedInterval(row0[j], row2[j], mlevel, t))
					++nSwitchedIntervals;

				if (switchedInterval(row0[j - 1], row2[j + 1], mlevel, t))
					++nSwitchedIntervals;

				if (switchedInterval(row0[j + 1], row2[j - 1], mlevel, t))
					++nSwitchedIntervals;

				if (nSwitchedIntervals > 2 /*&& row1[j] <= t[nInterval(row1[j], mlevel, t)]*/)
					row[j] = 0xff;

			}
		}
	}

	bool switchedInterval(int v1, int v2, int mlevel, int* t)
	{
		return nInterval(v1, mlevel, t) != nInterval(v2, mlevel, t);
	}

	int nInterval(int val, int mlevel, int* t)
	{
		for (int k = 0; k < mlevel; k++)
		{
			if (k < mlevel - 1)
			{
				if (t[k] <= val && val < t[k + 1]) // k-1 region
				{
					return k;
				}
			}
			else // k= mlevel-1 last region
			{
				if (t[k] <= val)
				{
					return k;
				}
			}
		}

		return -1;
	}
};

int Multi_OtsuThreshold::NGRAY = 256;

void cv::MultiOtsu(InputArray _src, OutputArray _dst, int mlevel)
{
	const int type = _src.type(), depth = CV_MAT_DEPTH(type), cn = CV_MAT_CN(type);
	const Size size = _src.size();

	CV_Assert(depth == CV_8U);
	CV_Assert(cn == 1);

	Mat src = _src.getMat();

	_dst.create(src.size(), CV_8U);
	Mat dst = _dst.getMat();

	Multi_OtsuThreshold multi_otso;
	multi_otso.run(src, dst, mlevel);
}


void cv::MultiOtsuEdges(InputArray _src, OutputArray _dst, int mlevel)
{
	const int type = _src.type(), depth = CV_MAT_DEPTH(type), cn = CV_MAT_CN(type);
	const Size size = _src.size();

	CV_Assert(depth == CV_8U);
	CV_Assert(cn == 1);

	Mat src = _src.getMat();

	_dst.create(src.size(), CV_8U);
	Mat dst = _dst.getMat();

	Multi_OtsuThreshold multi_otso;
	multi_otso.edges(src, dst, mlevel);
}


void cv::MultiOtsu2(InputArray _src, OutputArray _dst, OutputArray _thresholds, int mlevel)
{
	const int type = _src.type(), depth = CV_MAT_DEPTH(type), cn = CV_MAT_CN(type);
	const Size size = _src.size();

	CV_Assert(depth == CV_8U);
	CV_Assert(cn == 1);

	Mat src = _src.getMat();

	_dst.create(src.size(), CV_8U);
	Mat dst = _dst.getMat();


	_thresholds.create(1, mlevel, CV_32S);
	Mat thresholds = _thresholds.getMat();

	Multi_OtsuThreshold multi_otso;
	multi_otso.calc_thresholds(src, mlevel, thresholds);



	///////////////////////////////////////////////////////////////
	// calc regions works for any MLEVEL
	///////////////////////////////////////////////////////////////
	int width = src.cols;
	int height = src.rows;
	const unsigned char*  pixels = src.ptr<unsigned char>();
	multi_otso.calcRegions(dst, mlevel, thresholds.ptr<int>(), pixels, width, height);

}


void cv::MultiOtsuThresholds(InputArray _src, OutputArray _thresholds, int mlevel)
{
	const int type = _src.type(), depth = CV_MAT_DEPTH(type), cn = CV_MAT_CN(type);
	const Size size = _src.size();

	CV_Assert(depth == CV_8U);
	CV_Assert(cn == 1);

	Mat src = _src.getMat();

	_thresholds.create(1, mlevel, CV_32S);
	Mat thresholds = _thresholds.getMat();

	Multi_OtsuThreshold multi_otso;
	multi_otso.calc_thresholds(src, mlevel, thresholds);
}

