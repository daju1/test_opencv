#pragma once



#include "SuperSegment.h"
#include "Hypothes.h"
#include "GrayCode.h"
#include "CorrespondenceTableEntry.h"
#include "ClusterTableEntry.h"

#include <vector>
#include <string>
#include <map>
#include <list>



struct SuperSegments;
struct HashTable;

class Model
{
public:

	Model();

	virtual ~Model();

	std::string get_super_segment_with_corresponding_gray_code_winname()
	{
		std::string winname = fn.substr(fn.find_last_of("/\\") + 1)
			+ " super_segment_with_corresponding_gray_code";

		return winname;
	}

	void fill(std::list<SuperSegments*> & super_segments);
	void print_clusters();
	void build_cluster_table_entry();
	void do_epoch();
	void init_learning_step();
	void draw_on_scene_with_translation(double M[6], cv::Mat& drawing, cv::Scalar color);

	//void draw_by_hash_table_index();
	void draw_by_correspondence_table_index();
	void draw_by_cluster_table_index();

	void destroyOtherModelsWindows();

	std::vector<std::vector<cv::Point> > contours;
	//std::vector<std::vector<cv::Point2d> > line_fittings;
	std::map<int, HashTable*> hash_tables;
	CorrespondenceTableEntry correspondence_table_entry;
	ClusterTableEntry cluster_table_entry;

	std::string fn;
	cv::Mat image;
	cv::Mat scene_drawing;
	cv::Mat scene_image;

	//int hash_table_index;
	int correspondence_table_index;
	int cluster_table_index;

	double learning_step;
	double max_error;
	double max_sigma_angle;



	cv::Rect shapeRect;
};

//void update_hash_table_index(int k, void* p); 
void update_correspondence_table_index(int k, void* p); 
void update_cluster_table_index(int k, void* p);