#include "mycanny.hpp"
#include "opencv2/highgui/highgui.hpp"
#include "cvplot.h"

/* ������� ���������� ����� ����������� ��� ������������ ����������� image � ����� ������ �������� size */
template < class T >
int otsuThreshold(const cv::Mat& _src)
{
	const int size = _src.rows * _src.cols;
	const T* image = _src.ptr<T>();

	int min = image[0], max = image[0];
	int i, temp, temp1;
	int *hist;
	int histSize;

	int alpha, beta, threshold = 0;
	double sigma, maxSigma = -1;
	double w1, a;

	/**** ���������� ����������� ****/
	/* ������ ���������� � ���������� ������� */
	for (i = 1; i<size; i++)
	{
		temp = image[i];
		if (temp<min)   min = temp;
		if (temp>max)   max = temp;
	}

	histSize = max - min + 1;
	if ((hist = (int*)malloc(sizeof(int)*histSize)) == NULL) return -1;

	for (i = 0; i<histSize; i++)
		hist[i] = 0;

	/* ������� ������� ����� ��������� */
	for (i = 0; i<size; i++)
		hist[image[i] - min]++;
	/**** ����������� ��������� ****/

	CvPlot::plot("otsu_hist", &hist[0], histSize, 1);
	CvPlot::label("o");

	temp = temp1 = 0;
	alpha = beta = 0;
	/* ��� ������� ��������������� �������� ������� ������ */
	for (i = 0; i <= (max - min); i++)
	{
		temp += i*hist[i]; 
		temp1 += hist[i]; 
	}
	/* �������� ���� ������ ������
	����������� �� ���� ��������� ��� ������ ������, ��� ������� ��������������� ��������� ���������� */
	for (i = 0; i<(max - min); i++)
	{
		alpha += i*hist[i]; // sumB
		beta += hist[i]; // wB

		w1 = (double)beta / temp1;
		a = (double)alpha / beta - (double)(temp - alpha) / (temp1 - beta);
		sigma = w1*(1 - w1)*a*a;

		if (sigma>maxSigma)
		{
			maxSigma = sigma;
			threshold = i;
		}
	}
	free(hist);
	return threshold + min;
}

static double
cv::getThreshVal_Otsu_8u(const Mat& _src)
{
	Size size = _src.size();
	int step = (int)_src.step;
	if (_src.isContinuous())
	{
		size.width *= size.height;
		size.height = 1;
		step = size.width;
	}

	const int N = 256;
	int i, j, h[N] = { 0 };
	for (i = 0; i < size.height; i++)
	{
		const uchar* src = _src.ptr() + step*i;
		j = 0;
#if CV_ENABLE_UNROLLED
		for (; j <= size.width - 4; j += 4)
		{
			int v0 = src[j], v1 = src[j + 1];
			h[v0]++; h[v1]++;
			v0 = src[j + 2]; v1 = src[j + 3];
			h[v0]++; h[v1]++;
		}
#endif
		for (; j < size.width; j++)
			h[src[j]]++;
	}

	CvPlot::plot("otsu_h", &h[0], 256, 1);
	CvPlot::label("o");

	double mu = 0, scale = 1. / (size.width*size.height);
	for (i = 0; i < N; i++)
		mu += i*(double)h[i];

	mu *= scale;

	double mu1 = 0, q1 = 0;
	double max_sigma = 0, max_val = 0;

	for (i = 0; i < N; i++)
	{
		double p_i, q2, mu2, sigma;

		p_i = h[i] * scale; // pixel intensity probality
		mu1 *= q1;
		q1 += p_i; // probality of class occurence (omega)
		q2 = 1. - q1;

		if (std::min(q1, q2) < FLT_EPSILON || std::max(q1, q2) > 1. - FLT_EPSILON)
			continue;

		mu1 = (mu1 + i*p_i) / q1;
		mu2 = (mu - q1*mu1) / q2;
		sigma = q1*q2*(mu1 - mu2)*(mu1 - mu2);
		if (sigma > max_sigma)
		{
			max_sigma = sigma;
			max_val = i;
		}
	}
	return max_val;
}

static double
cv::getThreshVal_Triangle_8u(const Mat& _src)
{
	Size size = _src.size();
	int step = (int)_src.step;
	if (_src.isContinuous())
	{
		size.width *= size.height;
		size.height = 1;
		step = size.width;
	}

	const int N = 256;
	int i, j, h[N] = { 0 };
	for (i = 0; i < size.height; i++)
	{
		const uchar* src = _src.ptr() + step*i;
		j = 0;

		for (; j < size.width; j++)
			h[src[j]]++;
	}

	CvPlot::plot("tria_h", &h[0], 256, 1);
	CvPlot::label("o");

	int left_bound = 0, right_bound = 0, max_ind = 0, max = 0;
	int temp;
	bool isflipped = false;

	for (i = 0; i < N; i++)
	{
		if (h[i] > 0)
		{
			left_bound = i;
			break;
		}
	}
	if (left_bound > 0)
		left_bound--;

	for (i = N - 1; i > 0; i--)
	{
		if (h[i] > 0)
		{
			right_bound = i;
			break;
		}
	}
	if (right_bound < N - 1)
		right_bound++;

	for (i = 0; i < N; i++)
	{
		if (h[i] > max)
		{
			max = h[i];
			max_ind = i;
		}
	}

	if (max_ind - left_bound < right_bound - max_ind)
	{
		isflipped = true;
		i = 0, j = N - 1;
		while (i < j)
		{
			temp = h[i]; h[i] = h[j]; h[j] = temp;
			i++; j--;
		}
		left_bound = N - 1 - right_bound;
		max_ind = N - 1 - max_ind;
	}

	CvPlot::plot("tria_h2", &h[0], 256, 1);
	CvPlot::label("o");

	double thresh = left_bound;
	double a, b, dist = 0, tempdist;
	/*
	* We do not need to compute precise distance here. Distance is maximized, so some constants can
	* be omitted. This speeds up a computation a bit.
	*/
	a = max; b = left_bound - max_ind;
	for (i = left_bound + 1; i <= max_ind; i++)
	{
		tempdist = a*i + b*h[i];
		if (tempdist > dist)
		{
			dist = tempdist;
			thresh = i;
		}
	}
	thresh--;

	if (isflipped)
		thresh = N - 1 - thresh;

	return thresh;
}

// https://github.com/subokita/Sandbox/blob/master/otsu.py
void otsu_multi(int * histogram
	, int L
	, int N // total number of pixels
	, double& optimalThresh1
	, double& optimalThresh2)
{
	// Here is my C# implementation of Otsu Multi for 2 thresholds:
	// http://stackoverflow.com/questions/22706742/multi-otsumulti-thresholding-with-opencv

	double W0K, W1K, W2K, M0, M1, M2, currVarB, maxBetweenVar, M0K, M1K, M2K, MT;

	optimalThresh1 = 0;
	optimalThresh2 = 0;

	W0K = 0;
	W1K = 0;

	M0K = 0;
	M1K = 0;

	MT = 0;
	maxBetweenVar = 0;

	for (int k = 0; k < L; k++) {
		MT += k * (histogram[k] / (double)N);
	}

	for (int t1 = 0; t1 < L; t1++) {
		W0K += histogram[t1] / (double)N; //Pi
		M0K += t1 * (histogram[t1] / (double)N); //i * Pi
		M0 = M0K / W0K; //(i * Pi)/Pi

		W1K = 0;
		M1K = 0;

		for (int t2 = t1 + 1; t2 < L; t2++) {
			W1K += histogram[t2] / (double)N; //Pi
			M1K += t2 * (histogram[t2] / (double)N); //i * Pi
			M1 = M1K / W1K; //(i * Pi)/Pi

			W2K = 1 - (W0K + W1K);
			M2K = MT - (M0K + M1K);

			if (W2K <= 0) break;

			M2 = M2K / W2K;

			currVarB = W0K * (M0 - MT) * (M0 - MT) + W1K * (M1 - MT) * (M1 - MT) + W2K * (M2 - MT) * (M2 - MT);

			if (maxBetweenVar < currVarB) {
				maxBetweenVar = currVarB;
				optimalThresh1 = t1;
				optimalThresh2 = t2;
			}
		}
	}
}
// ���������� ��������� ������������ �����������
void fill_magnitude_original_histogram(cv::Mat& magnitude, int* mag_origin_hist, const int N)
{
	memset(mag_origin_hist, 0, N * sizeof(int));

	for (int i = 0; i < magnitude.rows; i++)
	{
		int* _mag = magnitude.ptr<int>(i);
		for (int j = 0; j < magnitude.cols; j++)
		{
			int m = _mag[j];

			if (m >= N || m < 0)
				printf("i = %d, j = %d m=%d\n", i, j, m);

			if (m >= 0 && m < N)
				mag_origin_hist[m]++;
		}
	}
}

void fill_magnitude_histogram(cv::Mat& magnitude, int* mag_hist, const int N)
{
	memset(mag_hist, 0, N * sizeof(int));

	for (int i = 0; i < magnitude.rows; i++)
	{
		int* _mag = magnitude.ptr<int>(i);
		for (int j = 0; j < magnitude.cols; j++)
		{
			int m = _mag[j];

			if (m >= N || m < 0)
				printf("i = %d, j = %d m=%d\n", i, j, m);

			if (m / 2 >= 0 && m / 2 < N)
				mag_hist[m / 2]++;
		}
	}
}

void fill_gray_histogram(cv::Mat& gray, int* hist, const int N)
{
	memset(hist, 0, N * sizeof(int));

	for (int i = 0; i < gray.rows; i++)
	{
		uchar* _mag = gray.ptr<uchar>(i);
		for (int j = 0; j < gray.cols; j++)
		{
			int m = _mag[j];

			if (m >= N || m < 0)
				printf("i = %d, j = %d m=%d\n", i, j, m);

			if (m >= 0 && m < N)
				hist[m / 2]++;
		}
	}
}

void fill_magnitude_integral_histogram(cv::Mat& magnitude, int* integral_hist, const int N)
{
	memset(integral_hist, 0, N * sizeof(int));

	for (int i = 0; i < magnitude.rows; i++)
	{
		int* _mag = magnitude.ptr<int>(i);

		for (int j = 0; j < magnitude.cols; j++)
		{
			int m = _mag[j];

			if (m >= N || m < 0)
				printf("i = %d, j = %d m=%d\n", i, j, m);

			for (int ii = 0; ii < m; ii += 2)
			{
				if (ii / 2 < N)
					integral_hist[ii / 2]++;
			}
		}
	}
}

void integral(const int* hist, const int N, int * I)
{
	memset(I, 0, N * sizeof(int));
	for (int i = 0; i < N; ++i)
	{
		int m = hist[i];
		for (int ii = 0; ii < i; ii += 1)
		{
			I[ii] += m;
		}
	}
}

void cv::adaptiveDelta(InputArray _src, OutputArray _dst,
	int method, int blockSize, const char * smean, const char * sdelta)
{
	cv::Mat src = _src.getMat();
	CV_Assert(src.type() == CV_8UC1);
	CV_Assert(blockSize % 2 == 1 && blockSize > 1);
	cv::Size size = src.size();

	_dst.create(size, src.type());
	cv::Mat mdelta = _dst.getMat();

	cv::Mat mean;
	if (src.data != mdelta.data)
		mean = mdelta;

	if (method == cv::ADAPTIVE_THRESH_MEAN_C)
		boxFilter(src, mean, src.type(), cv::Size(blockSize, blockSize),
		cv::Point(-1, -1), true, cv::BORDER_REPLICATE);
	else if (method == cv::ADAPTIVE_THRESH_GAUSSIAN_C)
		GaussianBlur(src, mean, cv::Size(blockSize, blockSize), 0, 0, cv::BORDER_REPLICATE);
	else
		CV_Error(CV_StsBadFlag, "Unknown/unsupported adaptive threshold method");

	cv::imshow(smean, mean);

	if (src.isContinuous() && mean.isContinuous() && mdelta.isContinuous())
	{
		size.width *= size.height;
		size.height = 1;
	}

	for (int i = 0; i < size.height; i++)
	{
		const uchar* sdata = src.data + src.step*i;
		const uchar* mdata = mean.data + mean.step*i;
		uchar* delta_data = mdelta.data + mdelta.step*i;

		for (int j = 0; j < size.width; j++){
			delta_data[j] = sdata[j] - mdata[j];
		}
	}

	cv::imshow(sdelta, mdelta);
}

void cv::MyAdaptiveCanny(InputArray _image, OutputArray edges, int apertureSize, bool L2gradient)
{
	//const int myCannyPlotLen = 72;
	//const int myCannyPlotInd = 0;

	Mat image = _image.getMat();
	int len = image.cols * image.rows;

	// calculate magnitude of gradient
	Mat magnitude;
	MyCanny(_image, magnitude, apertureSize, L2gradient);

	int mag_origin_hist[myCannyHistBuffSize];

	fill_magnitude_original_histogram(magnitude, mag_origin_hist, myCannyHistBuffSize);

	{
		double optimalThresh1;
		double optimalThresh2;

		otsu_multi(mag_origin_hist
			, myCannyHistBuffSize
			, len // total number of pixels
			, optimalThresh1
			, optimalThresh2);
		printf("optimalThresh1=%f, optimalThresh2=%f\n", optimalThresh1, optimalThresh2);

		// Mat canny_otsu_multy;
		Canny(_image, edges, optimalThresh1, optimalThresh2, apertureSize, L2gradient);
	}

}

void cv::MyCanny(InputArray _src, OutputArray _magnitude, int aperture_size, bool L2gradient)
{
	Mat src = _src.getMat();
	CV_Assert(src.depth() == CV_8U);

	_magnitude.create(cv::Size(src.cols, src.rows), CV_32S);
	Mat magnitude = _magnitude.getMat();

	if (!L2gradient && (aperture_size & CV_CANNY_L2_GRADIENT) == CV_CANNY_L2_GRADIENT)
	{
		// backward compatibility
		aperture_size &= ~CV_CANNY_L2_GRADIENT;
		L2gradient = true;
	}

	if ((aperture_size & 1) == 0 || (aperture_size != -1 && (aperture_size < 3 || aperture_size > 7)))
		CV_Error(CV_StsBadFlag, "");

	const int cn = src.channels();
	Mat dx(src.rows, src.cols, CV_16SC(cn));
	Mat dy(src.rows, src.cols, CV_16SC(cn));

	Sobel(src, dx, CV_16S, 1, 0, aperture_size, 1, 0, cv::BORDER_REPLICATE);
	Sobel(src, dy, CV_16S, 0, 1, aperture_size, 1, 0, cv::BORDER_REPLICATE);

	ptrdiff_t mapstep = src.cols + 2;
	AutoBuffer<uchar> buffer((src.cols + 2)*(src.rows + 2) + cn * mapstep * 3 * sizeof(int));

	int* mag_buf[3];
	mag_buf[0] = (int*)(uchar*)buffer;
	mag_buf[1] = mag_buf[0] + mapstep*cn;
	mag_buf[2] = mag_buf[1] + mapstep*cn;
	memset(mag_buf[0], 0, mapstep*sizeof(int));

	uchar* map = (uchar*)(mag_buf[2] + mapstep*cn);
	memset(map, 1, mapstep);
	memset(map + mapstep*(src.rows + 1), 1, mapstep);

	int maxsize = std::max(1 << 10, src.cols * src.rows / 10);
	std::vector<uchar*> stack(maxsize);
	uchar **stack_top = &stack[0];
	uchar **stack_bottom = &stack[0];

#define CANNY_PUSH(d)    *(d) = uchar(2), *stack_top++ = (d)
#define CANNY_POP(d)     (d) = *--stack_top

	// calculate magnitude and angle of gradient, perform non-maxima suppression.
	// fill the map with one of the following values:
	//   0 - the pixel might belong to an edge
	//   1 - the pixel can not belong to an edge
	//   2 - the pixel does belong to an edge
	for (int i = 0; i <= src.rows; i++)
	{
		int* _norm = mag_buf[(i > 0) + 1] + 1;
		if (i < src.rows)
		{
			short* _dx = dx.ptr<short>(i);
			short* _dy = dy.ptr<short>(i);

			if (!L2gradient)
			{
				for (int j = 0; j < src.cols*cn; j++)
					_norm[j] = std::abs(int(_dx[j])) + std::abs(int(_dy[j]));
			}
			else
			{
				for (int j = 0; j < src.cols*cn; j++)
					_norm[j] = int(_dx[j])*_dx[j] + int(_dy[j])*_dy[j];
			}

			if (cn > 1)
			{
				for (int j = 0, jn = 0; j < src.cols; ++j, jn += cn)
				{
					int maxIdx = jn;
					for (int k = 1; k < cn; ++k)
					if (_norm[jn + k] > _norm[maxIdx]) maxIdx = jn + k;
					_norm[j] = _norm[maxIdx];
					_dx[j] = _dx[maxIdx];
					_dy[j] = _dy[maxIdx];
				}
			}
			_norm[-1] = _norm[src.cols] = 0;
		}
		else
			memset(_norm - 1, 0, mapstep*sizeof(int));

		// at the very beginning we do not have a complete ring
		// buffer of 3 magnitude rows for non-maxima suppression
		if (i == 0)
			continue;

		uchar* _map = map + mapstep*i + 1;
		_map[-1] = _map[src.cols] = 1;

		int* _mag = mag_buf[1] + 1; // take the central row
		ptrdiff_t magstep1 = mag_buf[2] - mag_buf[1];
		ptrdiff_t magstep2 = mag_buf[0] - mag_buf[1];

		const short* _x = dx.ptr<short>(i - 1);
		const short* _y = dy.ptr<short>(i - 1);

		if ((stack_top - stack_bottom) + src.cols > maxsize)
		{
			int sz = (int)(stack_top - stack_bottom);
			maxsize = maxsize * 3 / 2;
			stack.resize(maxsize);
			stack_bottom = &stack[0];
			stack_top = stack_bottom + sz;
		}

		int* pmagnitude = magnitude.ptr<int>(i-1);

		int prev_flag = 0;
		for (int j = 0; j < src.cols; j++)
		{
			int m = _mag[j];
			pmagnitude[j] = m;
		}

		// scroll the ring buffer
		_mag = mag_buf[0];
		mag_buf[0] = mag_buf[1];
		mag_buf[1] = mag_buf[2];
		mag_buf[2] = _mag;
	}
}

void cvMyCanny(const CvArr* image, CvArr* edges, double threshold1, double threshold2, int aperture_size)
{
	cv::Mat src = cv::cvarrToMat(image), dst = cv::cvarrToMat(edges);
	CV_Assert(src.size == dst.size && src.depth() == CV_8U && dst.type() == CV_8U);

	cv::Canny(src, dst, threshold1, threshold2, aperture_size & 255,
		(aperture_size & CV_CANNY_L2_GRADIENT) != 0);
}
/* End of file. */