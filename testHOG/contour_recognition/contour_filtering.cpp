#include "opencv2/imgproc/imgproc.hpp"
#include "opencv2/imgproc/imgproc_c.h"
#if !HAVE_OPENCV_300
#include "opencv2/core/internal.hpp"
#endif

template <class T> T **Alloc2DMat(size_t size1, size_t size2)
{
	T **v;
	size_t k;
	v = new T*[size1];
	v[0] = new T[size1 * size2];

	for (k = 1; k < size1; k++)
		v[k] = v[k - 1] + size2;
	return (v);
}

template <class T> void Free2DMat(T ** v)
{
	if (v[0])
		delete[] v[0];
	if (v)
		delete[] v;
}

static inline double Norm2D(double x[2])
{
	return sqrt(x[0] * x[0] + x[1] * x[1]);
}

#define space_n_dims 2
#define Norm Norm2D

double get_line_equation_old(std::vector<cv::Point>& contour, size_t j0, size_t j1, double x[2])
{
	double** M = Alloc2DMat<double>(space_n_dims, space_n_dims);
	for (int j = 0; j < space_n_dims; ++j){
		for (int i = 0; i < space_n_dims; ++i){
			M[j][i] = 0.0;
		}
	}

	double b[2];

	cv::Mat A(2, 2, CV_64F, M[0]);
	cv::Mat B(2, 1, CV_64F, b);
	cv::Mat X(2, 1, CV_64F, x);

	for (int j = 0; j < space_n_dims; ++j)
		b[j] = 0.0;

	double summ_abs_y = 0.0;
	for (int j = j0; j < j1 && j < contour.size(); ++j)
	{
		cv::Point& pt = contour[j];
		summ_abs_y += abs(pt.y);
	}

	bool shift_y = summ_abs_y == 0;
	if (shift_y)
	{
		x[0] = 0.0;
		x[1] = 1.0;
		return 0.0;
	}

	for (int j = j0; j < j1 && j < contour.size(); ++j)
	{
		cv::Point& pt = contour[j];

		M[0][0] += pt.x * pt.x;
		M[1][1] += pt.y * pt.y;

		M[0][1] += pt.x * pt.y;

		M[1][0] += pt.y * pt.x;

		b[0] += pt.x;
		b[1] += pt.y;
	}


	// �������� ������������ ��������� ������
	solve(A, B, X, cv::DECOMP_SVD);


	Free2DMat<double>(M);

	double C = -1.0;
	double norm = Norm(x);
	for (int j = 0; j < space_n_dims; ++j)
		x[j] /= norm;

	C /= norm;

	return C;
}

template <class T>
T SquareSum(std::vector<T>& v) // ���������� ����� ��������� ��������� �������
{
	T _SquareSum = 0;
	for (std::vector<T>::iterator it = v.begin(); it != v.end(); it++)
	{
		_SquareSum += (*it) * (*it);
	}
	return _SquareSum;
}

template <class T>
T Sum(std::vector<T>& v) // ���������� ����� ��������� �������
{
	T _Sum = 0;
	for (std::vector<T>::iterator it = v.begin(); it != v.end(); it++)
	{
		_Sum += (*it);
	}
	return _Sum;
}

bool Tichonov(std::vector<std::vector<double> > & A,
	size_t cols, double alpha, // ����������������� �����������
	bool rand_not_zero_init,
	int maxq,
	std::vector<double> & b,
	std::vector<double> & x,
	std::vector<double> & E
	)
{
	size_t
		r, c,
		rows = A.size(),
		//		cols = m_size[1],
		//		max_epochs = 2,
		//		maxq = 10000 / max_epochs,
		//
		MaxNotTurned = 10;// ����� �������� �� ������� ��������� ������ ���������
	double delta, // ����������� - ����� �������� �������
		alpha_old, // 
		xS, //
		part = 0.01, // ���� �� ����� ������� �������, ����� ������ ���������� ����������������� �����������
		part_down = 0.1, //
		mindFdx = 1.0e-12, // ����������� �������� ��������� ����������� - ���� �������� ���������� ��������� ��� �������� ������� �� �������� ����� ��������
		mindFdx_down = 0.1, //
		inertio = 0.004, // ����������� �����������
		minetha = 0.4, // ����������� �������� ������������ �������� ��������
		maxetha = 1.3, // ������������ �������� ������������ �������� ��������
		little_up = 1.3, // ��������� ���������� ������������ �������� ��������
		little_down = 0.82, //
		much_down = 0.49, //
		etha0 = 1.2, // ��������� �������� ������������ �������� ��������
		F, // ����������
		local_min_F = DBL_MAX;

	std::vector<double>
//		E(rows), // ������ �������
		local_min_E(rows),
		dFdx(cols), // ��������� ����������� ����������� �� �������� ������� �������
		d2Fdx2(cols), // ������ ����������������� ����������� �� ��������� �������
		dx(cols), // ������ �������� ���������� ����� �� ������� ��������
		etha(cols, etha0), // ������ ������������� �������� ��������
		dF(cols), // ������ ��������� ����������� ��� ���������� ������� �������� �������
		vF(cols); // ������ �����������

	// ���� ��������� ������� ��������
	std::vector<bool> ToTurn;
	ToTurn.resize(cols, true);

	double norm;
	int nNotFailed = 0;
	bool AlphaNonZero = false,
		NotFailed, preNotFailed, // ���� ���� ��� ��������� ���� ��������� ��������� �������
		ToTurnAnyElement; // ���� ������������� ��������� ���� ������ �������� ������� �������

	//	if (rows >= cols)
	//	{
	/*
	vdouble M;
	M.resize(cols, cols); // �������� ������ ��� ���������� �������
	vdouble b0;
	b0.resize(cols); // �������� ������ ���  ��������������� ������
	pb0 = b0.pD;
	pM = M.pD;
	int len = cols * cols; // ����� ����� ����� ����� ��������
	// ��������� ���������� ������� �� ������� ����� �������� �������
	for (i = 0; i < len; i++)
	*(pM + i) = *(pD + i);
	// ��������������� ������� ������� ������ ������
	for (i = 0; i < cols; i++)
	*(pb0 + i) = *(pb + i);

	// ���������� ������� �������� ��������� ������ ������� ������ - �������� ������ ����������� �������
	double det;
	M.sls_det(b0,x,det);
	px = x.pD;
	//return x;
	// ������ �������
	for (r = 0; r < rows; r++)
	{
	int rcols = r * cols;
	*(pE + r) = - *(pb + r); // ������� ������� ������ ������ �� �����������
	for (c = 0; c < cols; c++)
	*(pE + r) += *(px + c) * *(pD + c + rcols); // ��������� ����������� ������������ �������� ������� �� ������
	}
	// �����������
	delta = sqrt(E.SquareSum());
	// ����������������� �����������
	alpha = 0.0;*/

	//	}
	//	else // ���� ������� ��������������
	//	{
	//x = ones(cols); // �������������� ������ ������� ���������
	//px = x.pD;

	if (x.size() != cols)
	{
		x.resize(cols, 1.0); // ������ �������
		for (c = 0; c < cols; c++)
		{
			if (rand_not_zero_init)
				x[c] = double(rand() - (RAND_MAX / 2)) / double(RAND_MAX);
			else
				x[c] = 0.0;
		}
		double norm = Norm(&x[0]);
		for (size_t j = 0; j < cols; j++)
			x[j] /= norm;
	}
	std::vector<double> local_min(cols);

	// ������ �������
	for (r = 0; r < rows; r++)
	{
		E[r] = -b[r];
		for (c = 0; c < cols; c++)
			E[r] += x[c] * A[r][c];
	}
	// �����������
	delta = sqrt(SquareSum(E));
	// ����������������� �����������
	if (AlphaNonZero)
		alpha = delta;
	else
		alpha = 0.0;
	//	}
	//���� ����������
	//	bool AlphaNonZero = true;
	//	for (int epoch = 1; epoch <= max_epochs; epoch++)
	//	{
	//		alpha = 0.0;
	//	p2 = d2Fdx2.pD;
	//���������� ������ �����������
	for (c = 0; c < cols; c++)
	{
		d2Fdx2[c] = alpha;
		for (r = 0; r < rows; r++)
		{
			d2Fdx2[c] += A[r][c] * A[r][c];
		}
		d2Fdx2[c] *= 2.0;
	}

	ToTurn.resize(cols, true);
	int q = 0;
	preNotFailed = true;
	do
	{
		q++;
		NotFailed = true; // ���� ���� ��� ��������� ���� ��������� ��������� �������
		++nNotFailed;
		ToTurnAnyElement = false; // ���� ������������� ��������� ���� ������ �������� ������� �������
		alpha_old = alpha;
		// �����������
		// ����������������� �����������
		if (AlphaNonZero)
		{
			xS = SquareSum(E) / SquareSum(x);
			xS = xS < .1 ? xS : .1;
			alpha = part *  xS;
		}
		else
			alpha = 0.;
		// �������� ������ �����������
		for (c = 0; c < cols; c++)
			d2Fdx2[c] += 2.0*(alpha - alpha_old);

		// ���������� ��� �������� x
		for (c = 0; c < cols; c++)
		{
			if (ToTurn[c] || q % MaxNotTurned == 0)
			{
				ToTurnAnyElement = true;
				// ��������� ������ �����������
				dFdx[c] = alpha * x[c];
				for (r = 0; r < rows; r++)
				{
					dFdx[c] += E[r] * A[r][c];
				}
				dFdx[c] *= 2.0 / d2Fdx2[c];
				//
				if (q > 1)
				{
					//
					if (fabs(dFdx[c]) < mindFdx)
						ToTurn[c] = false;
					else
						ToTurn[c] = true;
				}
				//
				if (q > 1)
				{
					// ��������� �������� � ������� ������ ��������� ������ �� ������ ����������� ����������� �� ��������� ������� �������
					bool first = true;
					double min, max, cur;
					int TurnedElements = 0;
					for (size_t i = 0; i < cols; i++)
					{
						if (ToTurn[i])
						{
							TurnedElements++;
							if (first)
							{
								first = false;
								min = fabs(dFdx[i]);
								max = min;
							}
							else
							{
								cur = fabs(dFdx[i]);
								min = cur < min ? cur : min;
								max = cur > max ? cur : max;
							}
						}
					}
					if (TurnedElements > 2)
					{
						cur = fabs(dFdx[c]);
						if (cur == min)	// ��������� ����������� (<0) ���������� �� ���������� �������� - �.�. ���������� �� ������
						{
							// ������� ��������� ����������� �������� ����������
							etha[c] *= little_down;
							// �� ����� �� �� ������� ������ �������?
							etha[c] = etha[c] < minetha ? minetha : etha[c];
						}
						else if (cur == max) // ��������� ����������� (<0) ���������� �� ���������� �������� - �.�. ����������  �� ������
						{
							// ������� ����������� ����������� �������� ����������
							etha[c] *= little_up;
							// �� ����� �� �� ������� ������� �������?
							etha[c] = etha[c] > maxetha ? maxetha : etha[c];
						}
					}
				}


				// �������� ���������� � �������������� ������������ �����
				if (q == 1)
					dx[c] = etha[c] * dFdx[c];
				else // ���������� ����������� ����
				{
					// ���������� ������� �������� �� ����������� �����������
					dx[c] *= inertio;
					// ��������� ��������� � ������� ��������� �������� ������� �������
					dx[c] += etha[c] * dFdx[c];
				}
				//������������ ������� ������� x
				x[c] -= dx[c];
				if (c < space_n_dims){
					norm = Norm(&x[0]);
					for (size_t j = 0; j < cols; j++)
						x[j] /= norm;
				}
				// ������ �������
				for (r = 0; r < rows; r++)
				{
					//int rcols = r * cols;
					E[r] = -b[r];
					for (size_t C = 0; C < cols; C++)
						E[r] += x[C] * A[r][C];
				}

				// ��������� ���������� ����� ���������� ���������� �������� ������� �������
				F = SquareSum(E) + alpha * SquareSum(x);
				if (q > 1)
				{
					// ������ ��������� ����������� �� ������� ��������
					dF[c] = F - vF[c];
					if (dF[c] > 0) // ���������� ����� - ������� ���������� ���������
					{
						if (preNotFailed)
						{
							for (size_t j = 0; j < cols; j++)
								local_min[j] = x[j];

							// �������� ������� ��������� �������� ������� �������
							if (c < space_n_dims){
								for (size_t j = 0; j < cols; j++)
									local_min[j] *= norm;
							}

							local_min[c] += dx[c];

							// ������ �������
							for (r = 0; r < rows; r++)
							{
								local_min_E[r] = - b[r];
								for (size_t C = 0; C < cols; C++)
									local_min_E[r] += local_min[C] * A[r][C];
							}

							local_min_F = SquareSum(local_min_E) + alpha * SquareSum(local_min);
							preNotFailed = false;
						}
#if 0

						// �������� ������� ��������� �������� ������� �������
						if (c < space_n_dims){
							for (int j = 0; j < cols; j++)
								x[j] *= norm;
						}
						x[c] += dx[c];
						// ��������������� ���������� �������� ������������ �����
						dx[c] -= etha[c] * dFdx[c];
						dx[c] /= inertio;

						// ������ ��������� ������ �������
						for (r = 0; r < rows; r++)
						{
							//int rcols = r * cols;
							E[r] = -b[r];
							for (int C = 0; C < cols; C++)
								E[r] += x[C] * A[r][C];
						}
						// ��������������� ������� �������� �����������
						F = vF[c];
						// ��������� ����������� = 0
						dF[c] = 0;
#endif
						ToTurn[c] = true;
						NotFailed = false;
						nNotFailed = 0;
						if (etha[c] == minetha) // minetha ��������
						{
							minetha *= little_down;
						}
						if (etha[c] == maxetha) // maxetha ��������
						{
							maxetha *= little_down;
						}
						// ���������� ��������� ����������� �������� ����������
						etha[c] *= much_down;

						// �� ����� �� �� ������� ������ �������?
						etha[c] = etha[c] < minetha ? minetha : etha[c];
					}
				}
				// ���������� ����������� ���������� � ������
				vF[c] = F;
			}
		}
		// �� ���������� ����������� ����������� ��� �������� ����� �������� �������� ����������������� �����������
		if (AlphaNonZero && (!ToTurnAnyElement || q > maxq / 2))
		{
			AlphaNonZero = false;
			ToTurnAnyElement = true;
			ToTurn.resize(cols, true);
		}


		/*FILE * stream = fopen("d://_.txt","at");
		if (stream)
		{
			fprintf(stream, "%d %lf %e %e %d %d", q, F, SquareSum<double>(dF), Sum<double>(dF), NotFailed == true ? 1 : 0, nNotFailed);
			for (size_t j = 0; j < cols; ++j)
			fprintf(stream, " %f", x[j]);
			for (size_t j = 0; j < cols; ++j)
			fprintf(stream, " %d", ToTurn[j] == true ? 1 : 0);
			for (size_t j = 0; j < cols; ++j)
			fprintf(stream, " %e", etha[j]);
			fprintf(stream, "\n");
			fclose(stream);
		}*/

		preNotFailed = NotFailed;

		if (q > 2 && NotFailed && nNotFailed > 2 && SquareSum<double>(dF) < 1.0e-12)
			ToTurnAnyElement = false;

	}
	while (ToTurnAnyElement && q < maxq);

	if (local_min_F < F)
	{
		for (size_t j = 0; j < cols; j++)
			x[j] = local_min[j];


		/*FILE * stream = fopen("d://_.txt","at");
		if (stream)
		{
			fprintf(stream, "\n");
			fprintf(stream, "%e", local_min_F - F);

			for (size_t j = 0; j < cols; ++j)
				fprintf(stream, " %f", x[j]);


			fprintf(stream, "\n");
			fclose(stream);
		}*/

	}
	//

	//CString s;	s.Format("alpha = %g", alpha);	AfxMessageBox(s);

	/*	if (epoch == max_epochs)
	{
		AlphaNonZero = false;
		part = 0.0;
	}
	else
		part *= part_down;
	//
	mindFdx *= mindFdx_down;
	}*/

	printf("q = %d ", q);

	return true;

}

struct errsum
{
	double err;
	size_t sum;

	errsum()
		: err(0.0)
		, sum(0)
	{}
};

double get_line_equation_tichonov(std::vector<cv::Point>& contour, size_t j0, size_t j1, double* ab, double C, std::vector<errsum>& errs)
{
	size_t numPoints = std::min(contour.size(), j1) - j0;

	std::vector<std::vector<double> >  A;
	A.resize(numPoints);

	std::vector<double> b, E;
	b.resize(numPoints, 0.0);
	E.resize(numPoints, 0.0);

	std::vector<double> x;
	x.resize(3);
	for (int j = 0; j < space_n_dims; ++j)
		x[j] = ab[j];

	x[2] = C;

	for (int j = 0; j < numPoints; ++j){
		A[j].resize(3);
	}

	for (int j = j0; j < j1 && j < contour.size(); ++j)
	{
		cv::Point& pt = contour[j];

		A[j - j0][0] = pt.x;
		A[j - j0][1] = pt.y;

		A[j - j0][2] = 1.0;
	}

	// ����������������� �����������
	double alpha = 0.00;
	bool rand_not_zero_init = true;
	int maxq = 100;

	Tichonov(A, space_n_dims+1, alpha, rand_not_zero_init, maxq, b, x, E);
	//solve(A, b, x, cv::DECOMP_SVD);

	for (int j = 0; j < space_n_dims; ++j)
		ab[j] = x[j];

	double sqrsumE = SquareSum(E) / numPoints;
	printf("sqrsumE=%f\n", sqrsumE);

	for (int j = j0; j < j1 && j < errs.size(); ++j)
	{
		cv::Point& pt = contour[j];
		if (sqrsumE <= 4.0)
			errs[j].err += E[j - j0] * E[j - j0] / sqrsumE;
		errs[j].sum += 1;
	}

	C = x[2];

	return C;
}

double get_line_equation(std::vector<cv::Point>& contour, size_t j0, size_t j1, double* ab, std::vector<errsum>& errs)
{
	double C = get_line_equation_old(contour, j0, j1, ab);
	//Visualize(d, abc);
	C = get_line_equation_tichonov(contour, j0, j1, ab, C, errs);
	//Visualize(d, abc);
	return C;
}


void apply_contour_filtering(std::vector<std::vector<cv::Point> >& contours, std::vector<std::vector<cv::Point> >& contours_out)
{
	int w = 10;

	for (int i = 0; i < contours.size(); i++)
	{
		std::vector<errsum> errs;
		errs.resize(contours[i].size(), errsum());

		for (int j0 = 0, j1 = w; j1 <= contours[i].size(); ++j0, ++j1)
		{
			double ab[2];
			double C = get_line_equation(contours[i], j0, j1, ab, errs);
		}

		for (int j = 0; j < contours[i].size(); ++j)
		{
			//printf("errs[%d].err = %e errs[%d].sum=%d\n", j, errs[j].err, j, errs[j].sum);
			errs[j].err /= errs[j].sum;
		}

		double mean_err = 0.0;
		for (int j = 0; j < contours[i].size(); ++j)
		{
			mean_err += errs[j].err;
		}
		//printf("mean_err = %f\n", mean_err);
		mean_err /= contours[i].size();

		printf("mean_err = %f\n", mean_err);

		contours_out.push_back(std::vector<cv::Point>());

		for (int j = 0; j < contours[i].size(); ++j)
		{
			cv::Point& pt = contours[i][j];
			if (errs[j].err <= mean_err)
				contours_out.back().push_back(pt);
		}
	}

}