#pragma once

#include "CorrespondenceTableEntry.h"

struct SuperSegments
{
	int n_points;

	SuperSegments(int n_points_)
	{
		n_points = n_points_;
	}

	void build_correspondence_table(
		std::vector<Model*>& models
		, cv::Mat& drawing2);

	std::list<SuperSegment> super_segments_list;
};

void draw_super_segments(cv::Mat& drawing, std::list<SuperSegments*>& super_segments, int thickness);
