#pragma once

#include "super_segment_effective_measure.h"

#include <vector>

#define Sqw(n) (n)*(n)

struct Cluster;

#define USE_HIST_DISPERSION 0

#include "Hypothes.h"
#include "Histogram.h"

struct ClPoint
{
   std::vector<double> crd; // ���������� ����� �������� � ndims-������ ������������
   struct Cluster* cluster; // ������� �������� ��������� ������� ���������
   // �����������
   ClPoint(int ndims, const Hypothes* _hypothes) : cluster(NULL), hypothes(_hypothes)
   {
      crd.resize(ndims);
   }
   const Hypothes* hypothes; // ��������
   // ���������� ����� �������������
   void extract_super_segment_points(
         std::vector<cv::Point2f>& src
       , std::vector<cv::Point2f>& dst);
   // ���������� ���� ����� ������������ ������������� ����� � ������ 
   void extract_angle_between_model_and_scene_super_segment_directions(std::vector<double>& angles);
};

// ��������� ��������
struct Cluster
{
   // static int n_dimentions_for_clasterisation;
   // ������ �������
   std::vector<double> center;
   // ���� ����������� ��������� ��������
#if CALC_CLUSTER_DISPERSION
   std::vector<double> dispersions;
#endif
   // ������ �����
   std::list<struct ClPoint*> points;
   // �����������
   Cluster(int nvars)
   {
      center.resize(nvars);
#if CALC_CLUSTER_DISPERSION
      dispersions.resize(nvars, 0.0);
#endif
   }
   // ����������
   virtual ~Cluster()
   {
   }

   bool test(struct ClPoint* point, double max_error, double max_sigma_angle);
   // ���������� ����� � �������
   void add(struct ClPoint* point, double learning_step);
   // ����� ����� ��������
   static const int clasterColorsNumber;
   // ��������� ����� �� ������� ��������
   static CvScalar getClusterColor(int clusterIdx);
   // �������� ��������
   bool isAsumed(Model * model);
   // �������� ����������� ������ ��������
   std::map<int, super_segment_effective_measures_list> get_effective_cluster_size();
   //size_t get_effective_cluster_size_max();
   //size_t get_effective_cluster_size_sum();
   size_t get_effective_cluster_size_merge();
   std::string get_effective_cluster_size_str();
   // �������� ����� �������������
   static void extract_super_segment_points(std::list<struct ClPoint*>& points, std::vector<cv::Point2f>& src, std::vector<cv::Point2f>& dst);
   // ���������� ���� ����� ������������ ������������� ����� � ������ 
   void extract_angle_between_model_and_scene_super_segment_directions(std::vector<double>& angles);
   // �������� ������� ��������� �������������� ������ ����� � ����� ����� 
   static double GetAffineMatrix(double m[6], std::vector<cv::Point2f>& src, std::vector<cv::Point2f>& dst);
   // �������� ������� ��������� �������������� ������ ����� 
   static double GetAffineMatrix(double m[6], std::list<struct ClPoint*>& points);
   // ��������� ����������� �����, ��� ����� ����� ��������� �������������� ������ � ����� 
   static void calc_angles_sigma(std::vector<double>& angles, double& mean_angle, double& sigma_angle);
   // ��������� ���� �����  A, B, ������������ ��������������� �� ������ � ������ W, H 
   static void calc_shear_angles(double m[6], double mean_angle, double& A, double& B, double& W, double& H);
   // �������� ������� ��������� ��������������
   double GetAffineMatrix(double m[6]);
   // ������������ ������� ��������� ��������������
   double m[6];
   // ������
   double _error;
   // ����������� ������� ��������������
   double determinant_R;
   // ���� �� �
   double shear_angle_x;
   // ���� �� Y
   double shear_angle_y;
   // ��������� ��������� �������� ��������������
   double A, B, W, H;
   // ����������� ����� �������� - ����� ����� ��������� �������������� ������ � ����� 
   double mean_angle;
   // ������-������������ ���������� ����� ����� ��������� �������������� ������ � �����
   double sigma_angle;
   // ������������� �������������� ������
   cv::Rect modelSupersegmentsShapeRect;
   // �������������
   double modelSupersegmentsShapeRectRelation;
   // ���������� ��������������
   void calcShapeRect();
   // ���������� ��������������
   void calcAffineMatrix();
};
// ��������� �������������
struct Clusterisator
{
   int m_ndims;
   // ������ ����������
   std::vector<Histogram*> histograms;
   // ������ ��������� ����������
   std::vector<double> hist_clustering_dispersion;
   // ������ ���� ��������� ���������
   std::vector<double> all_clusters_dispersion;
   // ����� ����� ���������
   int all_clusters_number;
   // ������ �����
   std::vector<double> delta;  
   // �����������
   Clusterisator()
   {
   }
   // ����������
   virtual ~Clusterisator()
   {
	   // ���� �� �������� �����������
	   for (int i = 0; i < m_ndims; ++i){
		   // ������� �����������
		   if (histograms[i])
			   delete histograms[i];
	   }
	   histograms.clear();
   }
   // �������� ��� ���������
   void zero_all_clusters_dispersion()
   {
	  // ������ � 0
	  all_clusters_dispersion.resize(m_ndims, 0.0);
	  // ����� ����� ��������� � 0
      all_clusters_number = 0;
   }
   // �������������
   void init(int ndims)
   {
      // �����������
      m_ndims = ndims;
	  histograms.resize(m_ndims);
	  for (int i = 0; i < m_ndims; ++i)
		  histograms[i] = new Histogram(true);

	  init_hists();
   }
   // ������������� �����������
   void init_hists()
   {
	  // ������ � 0
	  hist_clustering_dispersion.resize(m_ndims, 0.0);
	  zero_all_clusters_dispersion();
	  delta.resize(m_ndims, 0.0);
   }
   // ���������� ���������� �� ����� �� ��������
   double Clusterisator::calc_dist2(struct ClPoint* point, Cluster* cluster);
   // �������� ��������� �������
   struct Cluster* Clusterisator::get_nearest_cluster(
          struct ClPoint* point
        , std::list<Cluster*>& clusters);
   // ������������� �������������
   void process(
       ClPoint* point // �����
       , std::list<ClPoint*>& points // ������ �����
       , std::list<Cluster*>& clusters // ������ ���������
       , double max_error // ������������ ������
       , double max_sigma_angle // ������������ ��� ����
       , double learning_step // ��� ��������
       , bool& calc_all_clasters_dispersion // ��������� ��� ��������� ���������
       )
   {
      calc_all_clasters_dispersion = false;
	  // ���� ��������� ���
      if (clusters.size() == 0)
      {
         // ������� ����� �������
         struct Cluster* cluster = new struct Cluster(m_ndims);
		 // ��������� ����� � ��� ��������
		 cluster->add(point, learning_step);
		 // ??? ����� �� ��� ��������� � ��������, ����� ��� �������� � ����� �������
         // �������� ����� �������� ��� ��������
         point->cluster = cluster;
         clusters.push_back(cluster);

		 for (int ndim = 0; ndim < m_ndims; ++ndim)
			 delta[ndim] = 0.0;
      }
      else
      {
         // �������� ��������� ������� 
		 struct Cluster* nearest = get_nearest_cluster(point, clusters);
		 // ��������� ���������� �������
		 if (nearest && nearest->test(point, max_error, max_sigma_angle))
         {
            // ��������� ����� � ��������� �������
            nearest->add(point, learning_step);
			// ���������� ����� ������ � ������� ���������� ������
			for (int ndim = 0; ndim < m_ndims; ++ndim)
				delta[ndim] = point->crd[ndim] - nearest->center[ndim];
         }
         else
         {
			//  ������� ����� �������   
            struct Cluster* cluster = new struct Cluster(m_ndims);

			cluster->add(point, learning_step);
            point->cluster = cluster;
            clusters.push_back(cluster);

			for (int ndim = 0; ndim < m_ndims; ++ndim)
				delta[ndim] = 0.0;

            calc_all_clasters_dispersion = true;
         }
      }
	  // �� ������ ����������� ��������� �����������
	  for (int ndim = 0; ndim < m_ndims; ++ndim){
		  histograms[ndim]->add(delta[ndim]);
		  hist_clustering_dispersion[ndim] = histograms[ndim]->get_dispersion();
	  }
   }
   // ����-�������
   void post_process(std::list<Cluster*>& clusters)
   {
	  // �������� �� ���� ���������   
      std::list<Cluster*>::const_iterator it_cluster = clusters.begin();
      std::list<Cluster*>::const_iterator end_cluster = clusters.end();
	  // ���� �� ���������
      for(int i_cluster = 0; it_cluster != end_cluster; ++it_cluster, ++i_cluster)
      {
		 // �������� �� ������ ����� ��������
         std::list<ClPoint*>::const_iterator it_point = (*it_cluster)->points.begin();
         std::list<ClPoint*>::const_iterator end_point = (*it_cluster)->points.end();
		 // ���� �� ������
         for (; it_point != end_point; ++it_point)
         {
			// ������������ ��������� 
            for (size_t i = 0; i < (*it_point)->crd.size(); ++i)
            {
               //double relative_dist = fabs ((*it_point)->crd[i] - (*it_cluster)->center[i]) / (*it_cluster)->dispersions[i];
               //printf("relative_dist[%d] = %f ", i, relative_dist);
               all_clusters_dispersion[i] += Sqw((*it_point)->crd[i] - (*it_cluster)->center[i]);
            }
            ++all_clusters_number;
          }
      }
   }
   // ���������� ���������
   void norm_all_clusters_dispersion()
   {
	   for (int ndim = 0; ndim < m_ndims; ++ndim)
		   all_clusters_dispersion[ndim] /= all_clusters_number;
   }
};