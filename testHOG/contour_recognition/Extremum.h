#pragma once 
// ��������� ��������� ����������
struct Extremum
{
	double maximum;
	double minimum;

	Extremum()
	{
		reset();
	}
	// ���������� �����������
	void put(double val)
	{
		if (maximum < val)
			maximum = val;

		if (minimum > val)
			minimum = val;
	}
	// ���������� �������� �� �����������
	double normal(double val) const
	{
		return (val - minimum) / (maximum - minimum);
	}
	// ����� ���������, ��������� ������������
	double delta() const
	{
		return maximum - minimum;
	}
	// ���������� �� ���������
	void reset()
	{
		maximum = -DBL_MAX;
		minimum = DBL_MAX;
	}

};

