#pragma once

#include "cluster.h" 

// ��������� ��� ���������� ���������, ������� ������������� ���� ��������
struct ClusterItem
{
	Model* model;
	Cluster * cluster;
	double value; // �������� �������� ���������� ����������
	// �����������
	ClusterItem(Model* model_, Cluster * cluster_);

	static int cluster_items_index; // ������ �������� ��� ����������� � ������� ��������
};
//  ��������������� ������� ����������
bool operator< (const ClusterItem& item1, const ClusterItem& item2);

struct my_cluster_data
{
	Model * model; // ������
	cv::Mat *scene_image; // ����������� �����
	cv::Mat *input_image; // 
};

void my_cluster_callback_fun(Cluster * cluster, void * p, int i);

struct my_cluster_point_data
{
	cv::RNG *rng;
	cv::Mat *input_image;
	cv::Mat *scene_drawing;
};

void my_cluster_point_callback_fun(ClPoint * point, void * p, int i_point);

void update_cluster_items_index(int k, void* p);