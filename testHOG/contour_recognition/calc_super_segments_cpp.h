// ������� ������������ ��������������
void calc_super_segments_func(
	int n_points // ����� ����� � �������������
	, std::vector<cv::Point2d>& line_fitting_out // ������� ����� ������������������� ������� - ����� �� ��������� ������������ ����� ���� ��� ������� ��������
	, int j0 // ����� ����� � ������� �������� ������� ������������ �������������
	, std::list<SuperSegment>& super_segments //�������� ������ ��������������
	, size_t i_contour // ����� �������
	, int i_scale) // ����� ��������
{
	if ((int) line_fitting_out.size() < n_points + j0)
		return;

	// �������������� ���������. � ���������� ��������� ���������� ����� ��������� �����
	std::vector<cv::Point2d>::calc_super_segments_iterator it0 = line_fitting_out.calc_super_segments_iterator_begin() + j0;
	std::vector<cv::Point2d>::calc_super_segments_iterator end = line_fitting_out.calc_super_segments_iterator_end();

	// ����� ������������ ������������� ��� ������� �������� ������������� ����� ���������� �����
	bool common_edge = false;

	int start_line_index = 0;
	// � �������� ������ ����� ������ ������������ � ������� ����� ������ �����
	super_segments.push_back(SuperSegment(i_contour, i_sign*start_line_index, i_scale));
	super_segments.back().points.push_back(*it0);

	// ���� ����� ����� �����, �� ����� � ��������� ����� � ������������
	if (common_edge){
		super_segments.back().points.push_back(*(it0 + 1));
	}

	// ����� ����� � �������������
	int sum_points = super_segments.back().points.size();

	// ����� �������� ����� � ������� ����� ������������������� �������
	int j_end
		= common_edge
		? line_fitting_out.size() - 1
		: line_fitting_out.size();

	//�������������� ������� �������� �� ��������� ����� � �������� ����
	std::vector<cv::Point2d>::calc_super_segments_iterator it = it0 + 1;
	for (int j = j0 + 1; j < j_end; ++j, ++it){
		// ����� � ������������ ��������� �����
		if (!common_edge){
			super_segments.back().points.push_back(*it);
		}
		if (common_edge){
			super_segments.back().points.push_back(*(it + 1));
		}
		// ��������� ����� ����� � �������������
		++sum_points;

		// ���� ������������ �����������
		if (sum_points >= n_points)
		{
			//��������� ����� ������������ � ����� � ���� �����/ ��� ��� ����� � ������ ������ ����� ����
			start_line_index = j;
			super_segments.push_back(SuperSegment(i_contour, i_sign*start_line_index, i_scale));
			if (common_edge){
				super_segments.back().points.push_back(*it);
				super_segments.back().points.push_back(*(it + 1));
			}
			else{
				super_segments.back().points.push_back(*it);
			}
			// ��������� �������� ����� ����� � ����������� �������������
			sum_points = super_segments.back().points.size();
		}
	}
}

