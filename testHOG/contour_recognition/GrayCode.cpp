#include "GrayCode.h"
#include "assert.h"
#include "Constants.h"

// ������������ ������� ���� �����, ���������� ��������
int difference(const std::string& code1, const std::string& code2)
{
	assert(code1.size() == code2.size());

	int diff = 0;
	for (size_t i = 0; i < code1.size(); ++i)
	{
		char c1 = code1[i];
		char c2 = code2[i];

		int d = abs(c1 - c2);

		diff += d;
	}

	return diff;
}

int get_segment_relative_length_maximal_difference(const GrayCode& gc1, const GrayCode& gc2)
{
	int i = 0;
	int maximal_difference = 0;
	// ���� �� ����� ����������� ������� ������� ������
	assert(gc1.n_points == gc2.n_points);

#if USE_SEGMENT_RELATIVE_LENGTH
	for (int j = 0; j < gc1.n_points - 1; ++j)
	{
		std::string code1 = gc1.code.substr(i, N_PREDICATES_RELATIVE_LENGTH);
		std::string code2 = gc2.code.substr(i, N_PREDICATES_RELATIVE_LENGTH);
		i += N_PREDICATES_RELATIVE_LENGTH;

		int diff = difference(code1, code2);

		if (maximal_difference < diff)
			maximal_difference = diff;
	}
#endif
	return maximal_difference;
}


int get_angles_maximal_difference(const GrayCode& gc1, const GrayCode& gc2)
{
	int i = 0;
	int maximal_difference = 0;

	assert(gc1.n_points == gc2.n_points);

#if USE_SEGMENT_RELATIVE_LENGTH
	for (int j = 0; j < gc1.n_points - 1; ++j)
	{
		std::string code1 = gc1.code.substr(i, N_PREDICATES_RELATIVE_LENGTH);
		std::string code2 = gc2.code.substr(i, N_PREDICATES_RELATIVE_LENGTH);
		i += N_PREDICATES_RELATIVE_LENGTH;
	}
#endif
	for (int j = 1; j < gc1.n_points - 1; ++j)
	{
		std::string code1 = gc1.code.substr(i, N_PREDICATES_ANGLES);
		std::string code2 = gc2.code.substr(i, N_PREDICATES_ANGLES);
		i += N_PREDICATES_ANGLES;

		int diff = difference(code1, code2);

		if (maximal_difference < diff)
			maximal_difference = diff;
	}

	return maximal_difference;
}

extern bool build_models;
// ��������� 2 ����-�����
bool operator < (const GrayCode& gc1, const GrayCode& gc2)
{
	if (build_models)
	{
		return gc1.code < gc2.code;
	}

	int segment_relative_length_maximal_difference = get_segment_relative_length_maximal_difference(gc1, gc2);
	int angles_maximal_difference = get_angles_maximal_difference(gc1, gc2);
	// ����-���� ��������� �������
	if (segment_relative_length_maximal_difference <= GREY_CODE_MAXIMAL_DIFFERENCE_TO_BE_EQUIAL_RELATIVE_LENGTH && angles_maximal_difference <= GREY_CODE_MAXIMAL_DIFFERENCE_TO_BE_EQUIAL_ANGLES)
		return false;

	return gc1.code < gc2.code;
}

