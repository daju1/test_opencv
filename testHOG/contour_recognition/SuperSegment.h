#pragma once

#include "opencv2/imgproc/imgproc.hpp"
#include "opencv2/objdetect/objdetect.hpp"
#include "opencv2/highgui/highgui.hpp"
#if !HAVE_OPENCV_300
#include <opencv2/nonfree/nonfree.hpp>
#endif

#include <vector>
#include <list>

#include "GrayCode.h"

class SuperSegment
{
public:

	// �����������
	SuperSegment(size_t i_contour_, int start_line_index_, int i_scale_)
	{
		i_contour = i_contour_;  
		start_line_index = start_line_index_;
		i_scale = i_scale_;
	}

	// ���c������� �����������
	SuperSegment(const SuperSegment& super_segment)
	{
		this->i_contour        = super_segment.i_contour;
		this->start_line_index = super_segment.start_line_index;
		this->i_scale            = super_segment.i_scale;
		this->points           = super_segment.points;
		this->gray_code        = super_segment.gray_code;
	}

	virtual ~SuperSegment()
	{
	}

	// ���������� ��� ����
	GrayCode getGrayCode()
	{
		return GrayCode(gray_code, i_contour, start_line_index, this->points.size());
	}

	void print() const;
	
	// ����� �������
	size_t i_contour; 
	// ������ ��������� �����
	int start_line_index; 
	// ����� ��������
	int i_scale; 
	// ��� ����� ������������� 
	std::vector<cv::Point2f> points;
	// ��� ���� �������������
	std::string gray_code;
};

// ���������� ���� ����� ����� �������, ������������� �� ������ 3 �����
double getAngleABC(cv::Point2d a, cv::Point2d b, cv::Point2d c);

// ���������� ����� 2 �������
double distance2(cv::Point2d pt0, cv::Point2d pt1);

// �������������� ����������� ��� ������ �����
bool calc_fitting(double& d_pre, double dist, double d, cv::Point2d& current, const cv::Point2d& pt_pre, const cv::Point2d& pt, std::vector<cv::Point2d>& out);

// ��������� ��������� ����� �� ��������� �������������� �����: ������� �����, ����������, �������� �����
void line_fitting_algorithm_equial_segment_length(std::vector<cv::Point>& contour, double dist, std::vector<cv::Point2d>& out);
// ������������� ������������� �� ������ ������������ ������� �� ����
// ?? ��������, ����� ������ ������������ ����� ��������?
void line_fitting_algorithm_sum_alpha_tolerance(double max_sum_alpha, std::vector<cv::Point>& contour, std::vector<cv::Point2d>& simplified);
// ???
void line_fitting_algorithm_epsilon_sector_tolerance(double tolerance, std::vector<cv::Point>& contour, std::vector<cv::Point2d>& simplified);
// ���������� ����� �����: ����, ����, ��������� ����
void update_summ_alpha(const double angle, double& alpha, double& sum_alpha);
// ???
void calc_super_segments(int n_points, std::vector<cv::Point2d>& line_fitting_out, int j0, std::list<SuperSegment>& super_segments, size_t i_contour, int i_scale);
// ���������� ���� ���� �����
// ?? ������������� ������ ��� ���������, ��� �������� �������� �����������?
std::string calc_gray_code(double val, int n_predicates);
// ���������� �������������: �����������, ������������, ����, �������, ���� ���������� �����
void draw_super_segment(cv::Mat& drawing, const SuperSegment& super_segment, cv::Scalar color, int thickness, bool draw_points = false);
// ���������� ������������������ ��������������: �����������, ������������������ ��������������, �������
void draw_super_segments(cv::Mat& drawing, std::list<SuperSegment>& super_segments, int thickness);
// ���������� ���� ���� ������������
// ?? ������ ������� �������� sum_alpha ��� ���
std::string calc_super_segment_gray_code(const SuperSegment& super_segment, double& sum_alpha);
// ���������� ���� �� ������� ������
// ?? ������������� ������
double getAngleABCD(cv::Point2d a, cv::Point2d b, cv::Point2d c, cv::Point2d d);

const double pi = 3.141592653589793238462643383279502884197169399375105820974944592307816406286208998628034825342117067982148086513;