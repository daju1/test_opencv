#include "ContourRecognitionDirectoryEnumerator.h"
#if HAVE_OPENCV_300
#if HAVE_VIZ
#include <opencv2/viz/vizcore.hpp>
#endif
#endif
// �������������� �����
#include "../mycanny.hpp"
#include "./my_contours.hpp"
// ����������
#include "../MultiOtsu.h"

#include "SuperSegment.h"
#include "SuperSegments.h"
#include "Hypothes.h"
#include "Model.h"
#include "Cluster.h"
#include "../cvplot.h"
#include "Extremum.h"
#include "CorrespondenceTableEntry.h"
#include "ClusterTableEntry.h"
#include "HashTable.h"
#include "ClusterItem.h"

#if (_MSC_VER > 1500)
#include <mapnik/simplify.hpp>
#include <mapnik/simplify_converter.hpp>
#include <mapnik/vertex_adapters.hpp>
#endif

#include <set>

#include "Constants.h"

bool build_models;

#if (_MSC_VER > 1500)
template <class T>
void mapnik_simplify(
	std::vector<cv::Point_<T> >& input_contour
	, double tolerance
	, std::string const& method
	, std::vector<cv::Point2d >& line_fitting)
{
	mapnik::geometry::line_string<double> line;

	std::vector<cv::Point_<T> >::iterator it = input_contour.begin();
	std::vector<cv::Point_<T> >::iterator end = input_contour.end();

	for (; it != end; ++it)
		line.push_back(mapnik::geometry::point<double>((*it).x, (*it).y));

	mapnik::geometry::line_string_vertex_adapter<double> va(line);
	mapnik::simplify_converter<mapnik::geometry::line_string_vertex_adapter<double> > generalizer(va);
	generalizer.set_simplify_algorithm(mapnik::simplify_algorithm_from_string(method).get());
	generalizer.set_simplify_tolerance(tolerance);

	//suck the vertices back out of it
	mapnik::CommandType cmd;
	double x, y;
	while ((cmd = (mapnik::CommandType)generalizer.vertex(&x, &y)) != mapnik::SEG_END)
	{
		line_fitting.push_back(cv::Point_<T>((int) x, (int) y));
	}
}
#endif


bool do_not_display_rejected_clusters = true;

#if HAVE_OPENCV_300
#if HAVE_VIZ
cv::viz::Viz3d viz;
#endif
#endif

IplImage** plots;
IplImage* img_composite;
CvSize plot_size = cvSize(320, 240);
CvSize composite_size = cvSize(3 * plot_size.width, 2 * plot_size.height);

void initPlots()
{
#if HAVE_OPENCV_300
#if HAVE_VIZ
	viz.showWidget("coo", cv::viz::WCoordinateSystem());
#endif
#endif

	img_composite = cvCreateImage(composite_size, IPL_DEPTH_8U, 3);
	plots = new IplImage*[6];
	for (int i = 0; i < 6; ++i)
	{
		plots[i] = cvCreateImage(plot_size, IPL_DEPTH_8U, 3);
		cvRectangle(plots[i], cvPoint(0, 0), cvPoint(plot_size.width, plot_size.height), CV_RGB(0, 0, 0), CV_FILLED);
	}
}





std::vector<Model*> models;
std::set<ClusterItem> cluster_items;
cv::Mat g_scene_image;

void build_cluster_table(std::vector<Model*>& models)
{
	printf("build_cluster_table\n");

	std::vector<Model*>::iterator it_model = models.begin();
	std::vector<Model*>::iterator end_model = models.end();

	for (int i_model = 0; it_model != end_model; ++it_model, ++i_model)
	{
		Model * model = (*it_model);

		printf("i_model %d %d\n", i_model, models.size());

		model->build_cluster_table_entry();
	}

	printf("build_cluster_table end\n");
}



// ���� ����� �������� ��������� - ������������������ ����� ��������
void do_epoch(std::vector<Model*>& models)
{
	printf("do_epoch\n");

	std::vector<Model*>::iterator it_model = models.begin();
	std::vector<Model*>::iterator end_model = models.end();

	for (int i_model = 0; it_model != end_model; ++it_model, ++i_model)
	{
		Model * model = (*it_model);

		printf("i_model %d %d\n", i_model, models.size());

		model->do_epoch();
	}

	printf("do_epoch end\n");
}



void build_histogram_of_error_of_overdetermined_get_affine_matrix()
{
	Histogram histogram(false);
	IplImage * plot = cvCreateImage(plot_size, IPL_DEPTH_8U, 3);
	cvRectangle(plot, cvPoint(0, 0), cvPoint(plot_size.width, plot_size.height), CV_RGB(0, 0, 0), CV_FILLED);


	std::vector<Model*>::iterator it_model = models.begin();
	std::vector<Model*>::iterator end_model = models.end();
	for (; it_model != end_model; ++it_model)
	{
		Model * model = (*it_model);

		Extremum extremum;
		model->correspondence_table_entry.calcExtremumOfErrorOfOverdeterminedGetAffineMatrix(extremum);
		model->correspondence_table_entry.buildHistogramOfErrorOfOverdeterminedGetAffineMatrix(histogram, &extremum);
	}

	int plot_marg = 10;
	double fontscale = 0.6;
	char plot_name[128];

	sprintf(plot_name, "error_of_overdetermined_get_affine_matrix");
	histogram.plotting(plot, plot_size, plot_marg, fontscale, plot_name);
	sprintf(plot_name, "error of overdetermined get affine matrix");
	histogram.plot(plot_name);
	cvShowImage("error_of_overdetermined_get_affine_matrix", plot);

	cvReleaseImage(&plot);
}

void build_histogram_of_determinant_R()
{
	Histogram histogram(false);
	IplImage * plot = cvCreateImage(plot_size, IPL_DEPTH_8U, 3);
	cvRectangle(plot, cvPoint(0, 0), cvPoint(plot_size.width, plot_size.height), CV_RGB(0, 0, 0), CV_FILLED);


	std::vector<Model*>::iterator it_model = models.begin();
	std::vector<Model*>::iterator end_model = models.end();
	for (; it_model != end_model; ++it_model)
	{
		Model * model = (*it_model);

		Extremum extremum;
		model->correspondence_table_entry.calcExtremumOfDeterminantR(extremum);
		model->correspondence_table_entry.buildHistogramOfDeterminantR(histogram, &extremum);
	}

	int plot_marg = 10;
	double fontscale = 0.6;
	char plot_name[128];

	sprintf(plot_name, "determinant_R");
	histogram.plotting(plot, plot_size, plot_marg, fontscale, plot_name);
	sprintf(plot_name, "determinant R");
	histogram.plot(plot_name);
	cvShowImage("determinant_R", plot);

	cvReleaseImage(&plot);
}

void build_histogram_of_angle_between_model_and_scene_super_segment_directions()
{
	Histogram histogram(false);
	IplImage * plot = cvCreateImage(plot_size, IPL_DEPTH_8U, 3);
	cvRectangle(plot, cvPoint(0, 0), cvPoint(plot_size.width, plot_size.height), CV_RGB(0, 0, 0), CV_FILLED);


	std::vector<Model*>::iterator it_model = models.begin();
	std::vector<Model*>::iterator end_model = models.end();
	for (; it_model != end_model; ++it_model)
	{
		Model * model = (*it_model);

		Extremum extremum;
		model->correspondence_table_entry.calcExtremumOfAngleBetweenModelAndSceneSuperSegmentDirections(extremum);
		model->correspondence_table_entry.buildHistogramOfAngleBetweenModelAndSceneSuperSegmentDirections(histogram, &extremum);
	}

	int plot_marg = 10;
	double fontscale = 0.6;
	char plot_name[128];

	sprintf(plot_name, "angle_between_model_and_scene_super_segment_directions");
	histogram.plotting(plot, plot_size, plot_marg, fontscale, plot_name);
	sprintf(plot_name, "angle between model and scene super segment directions");
	histogram.plot(plot_name);
	cvShowImage("angle_between_model_and_scene_super_segment_directions", plot);

	cvReleaseImage(&plot);
}

void build_histograms_of_hypotheses_affine_matrixes()
{
	std::vector<Histogram*> histograms;
	histograms.resize(6);
	for (int i = 0; i < 6; ++i)
	{
		histograms[i] = new Histogram(true);
		cvRectangle(plots[i], cvPoint(0, 0), cvPoint(plot_size.width, plot_size.height), CV_RGB(0, 0, 0), CV_FILLED);
	}


	std::vector<Model*>::iterator it_model = models.begin();
	std::vector<Model*>::iterator end_model = models.end();
	for (; it_model != end_model; ++it_model)
	{
		Model * model = (*it_model);

		std::vector<Extremum> extremums;
		model->correspondence_table_entry.calcExtremumsOfHypothesesAffineMatrixes(extremums);
		model->correspondence_table_entry.buildHistogramOfHypothesesAffineMatrixes(histograms, &extremums);
	}

	int plot_marg = 10;
	double fontscale = 0.6;
	char plot_name[128];
	for (int i = 0; i < 6; ++i)
	{
		sprintf(plot_name, "M%d%d", i / 3, i % 3);
		histograms[i]->plotting(plots[i], plot_size, plot_marg, fontscale, plot_name);
		histograms[i]->plot(plot_name);

		cvSetImageROI(img_composite, cvRect((i % 3) * plot_size.width, (i / 3) * plot_size.height, plot_size.width, plot_size.height));
		cvCopy(plots[i], img_composite);
		cvResetImageROI(img_composite);
	}

	for (int i = 0; i < 6; ++i)
	{
		delete histograms[i];
	}


	cvShowImage("img_composite", img_composite);
}




void calc_clusters_items()
{
	std::vector<Model*>::iterator it_model = models.begin();
	std::vector<Model*>::iterator end_model = models.end();

	for (; it_model != end_model; ++it_model)
	{
		cv::destroyWindow((*it_model)->fn);

		std::list<Cluster*>::iterator it_cluster = (*it_model)->cluster_table_entry.clusters.begin();
		std::list<Cluster*>::iterator end_cluster = (*it_model)->cluster_table_entry.clusters.end();
		for (; it_cluster != end_cluster; ++it_cluster)
		{
			bool is_rejected_cluster = !(*it_cluster)->isAsumed((*it_model));
			if (is_rejected_cluster)
				continue;


			cluster_items.insert(ClusterItem((*it_model), (*it_cluster)));
		}
	}
}


void draw_clusters(cv::Mat input_image)
{
	std::vector<Model*>::iterator it_model = models.begin();
	std::vector<Model*>::iterator end_model = models.end();

	for (; it_model != end_model; ++it_model)
	{
		cv::destroyWindow((*it_model)->fn);

		std::list<Cluster*>::iterator it_cluster = (*it_model)->cluster_table_entry.clusters.begin();
		std::list<Cluster*>::iterator end_cluster = (*it_model)->cluster_table_entry.clusters.end();
		for (; it_cluster != end_cluster; ++it_cluster)
		{
			bool is_rejected_cluster = !(*it_cluster)->isAsumed((*it_model));
			if (is_rejected_cluster)
				continue;

			double m[6];
			(*it_cluster)->GetAffineMatrix(m);

			(*it_model)->draw_on_scene_with_translation(m, input_image, CV_RGB(0, 255, 0));
		}
	}
}

int calcCloudSize(std::list<Cluster*>& clusters, Model * model)
{
	int cloud_size = 0;

	std::list<Cluster* >::const_reverse_iterator it_cluster = clusters.rbegin();
	std::list<Cluster* >::const_reverse_iterator end_cluster = clusters.rend();

	for (; it_cluster != end_cluster; ++it_cluster)
	{
		bool is_rejected_cluster = !(*it_cluster)->isAsumed(model);
		if (do_not_display_rejected_clusters && is_rejected_cluster)
			continue;

		cloud_size += (*it_cluster)->points.size();
	}

	return cloud_size;
}



void calcCloudExtremums(std::list<Cluster*>& clusters, std::vector<Extremum>& extremums, Model * model)
{
	if (6 != extremums.size())
		extremums.resize(6);

	std::list<Cluster* >::const_reverse_iterator it_cluster = clusters.rbegin();
	std::list<Cluster* >::const_reverse_iterator end_cluster = clusters.rend();

	for (; it_cluster != end_cluster; ++it_cluster)
	{
		bool is_rejected_cluster = !(*it_cluster)->isAsumed(model);
		if (do_not_display_rejected_clusters && is_rejected_cluster)
			continue;

		std::list<struct ClPoint*>::const_iterator it_point = (*it_cluster)->points.begin();
		std::list<struct ClPoint*>::const_iterator end_point = (*it_cluster)->points.end();

		for (; it_point != end_point; ++it_point)
		{
			for (int i = 0; i < 6; ++i)
			{
				double affine_matrix_i = (*it_point)->crd[i];
				extremums[i].put(affine_matrix_i);
			}
		}

	}
}

void calcCloudExtremums(std::vector<Extremum>& extremums)
{
	std::vector<Model*>::iterator it_model = models.begin();
	std::vector<Model*>::iterator end_model = models.end();

	for (; it_model != end_model; ++it_model)
	{
		std::list<Cluster*>::iterator it_cluster = (*it_model)->cluster_table_entry.clusters.begin();
		std::list<Cluster*>::iterator end_cluster = (*it_model)->cluster_table_entry.clusters.end();

		calcCloudExtremums((*it_model)->cluster_table_entry.clusters, extremums, (*it_model));
	}
}

#if HAVE_OPENCV_300
#if HAVE_VIZ
void doCloudViz(const cv::String widget_id, int part_of_affine_matrix, std::list<Cluster*>& clusters, const std::vector<Extremum>* extremums, Model * model)
{
	int i_cloud_point = 0;
	int cloud_size = calcCloudSize(clusters, model);
	// ������ ����� � �����. ������� � ����� CV_32FC3
	cv::Mat cloud(cv::Size(1, cloud_size), CV_32FC3);

	// ������� ������ ������ ��� ������ � ��������� ��� ���������� �������
	cv::Mat colors(cloud.size(), CV_8UC3);
	
	std::list<Cluster* >::const_reverse_iterator it_cluster = clusters.rbegin();
	std::list<Cluster* >::const_reverse_iterator end_cluster = clusters.rend();

	for (int i_cluster = clusters.size() - 1, j_cluster = 0; it_cluster != end_cluster; ++it_cluster, --i_cluster)
	{
		bool is_rejected_cluster = !(*it_cluster)->isAsumed(model);
		if (do_not_display_rejected_clusters && is_rejected_cluster)
			continue;

		CvScalar _color = Cluster::getClusterColor(i_cluster);

		std::list<struct ClPoint*>::const_iterator it_point = (*it_cluster)->points.begin();
		std::list<struct ClPoint*>::const_iterator end_point = (*it_cluster)->points.end();

		for (; it_point != end_point; ++it_point)
		{
			cv::Vec3d *ddata = cloud.ptr<cv::Vec3d>(i_cloud_point);
			cv::Vec3f *fdata = cloud.ptr<cv::Vec3f>(i_cloud_point);

			switch (part_of_affine_matrix)
			{
			case 1:
				{
					double affine_matrix_0 = (*it_point)->crd[0];
					double affine_matrix_1 = (*it_point)->crd[1];
					double affine_matrix_2 = (*it_point)->crd[2];

					if (cloud.depth() == CV_32F)
						*fdata = cv::Vec3f(
						extremums ? extremums->operator[](0).normal(affine_matrix_0) : affine_matrix_0
						, extremums ? extremums->operator[](1).normal(affine_matrix_1) : affine_matrix_1
						, extremums ? extremums->operator[](2).normal(affine_matrix_2) : affine_matrix_2);

					if (cloud.depth() == CV_64F)
						*ddata = cv::Vec3d(
						extremums ? extremums->operator[](0).normal(affine_matrix_0) : affine_matrix_0
						, extremums ? extremums->operator[](1).normal(affine_matrix_1) : affine_matrix_1
						, extremums ? extremums->operator[](2).normal(affine_matrix_2) : affine_matrix_2);

				}
				break;
			case 2:
				{
					double affine_matrix_3 = (*it_point)->crd[3];
					double affine_matrix_4 = (*it_point)->crd[4];
					double affine_matrix_5 = (*it_point)->crd[5];

					if (cloud.depth() == CV_32F)
						*fdata = cv::Vec3f(
						extremums ? extremums->operator[](3).normal(affine_matrix_3) : affine_matrix_3
						, extremums ? extremums->operator[](4).normal(affine_matrix_4) : affine_matrix_4
						, extremums ? extremums->operator[](5).normal(affine_matrix_5) : affine_matrix_5);

					if (cloud.depth() == CV_64F)
						*ddata = cv::Vec3d(
						extremums ? extremums->operator[](3).normal(affine_matrix_3) : affine_matrix_3
						, extremums ? extremums->operator[](4).normal(affine_matrix_4) : affine_matrix_4
						, extremums ? extremums->operator[](5).normal(affine_matrix_5) : affine_matrix_5);

				}
				break;
			}

			cv::Scalar_<uchar> _c = _color;
			cv::Vec3b *cdata = colors.ptr<cv::Vec3b>(i_cloud_point);
			*cdata = cv::Vec3b(_c[0], _c[1], _c[2]);

			++i_cloud_point;
		}

	}

	if (cloud_size > 0)
		viz.showWidget(widget_id, cv::viz::WCloud(cloud, colors)
		, cv::Affine3d().translate(cv::Vec3d(part_of_affine_matrix == 1 ? +1.0 : -1.0, 0.0, 0.0))
		);
}


void doCloudViz()
{
	std::vector<Extremum> extremums;
	calcCloudExtremums(extremums);	

	std::vector<Model*>::iterator it_model = models.begin();
	std::vector<Model*>::iterator end_model = models.end();

	for (; it_model != end_model; ++it_model)
	{
		//doCloudViz("M1", 1, (*it).second.clusters, &extremums);
		//doCloudViz("M2", 2, (*it).second.clusters, &extremums);
		doCloudViz("M1", 1, (*it_model)->cluster_table_entry.clusters, NULL, (*it_model));
		doCloudViz("M2", 2, (*it_model)->cluster_table_entry.clusters, NULL, (*it_model));
	}

	viz.spinOnce();
}

#endif
#endif


enum methods
{
	DouglasPeucker, 
#if (_MSC_VER > 1500)
	mapnik_radial_distance,
	mapnik_douglas_peucker,
	mapnik_douglas_peucker_relative_segment_distance,
	mapnik_visvalingam_whyatt,
	mapnik_visvalingam_whyatt_relative_effective_area,
	mapnik_visvalingam_whyatt_vertex_angle,
	mapnik_zhao_saalfeld,
#endif
	my_line_fitting_algorithm_sum_alpha_tolerance,
	my_line_fitting_algorithm_epsilon_sector_tolerance,
	my_line_fitting_algorithm_equial_segment_length,
};


void apply_line_fitting_switch(methods method
	, int i_contour
	, std::vector<std::vector<cv::Point> >& contours
	, int i_scale
	, double m_scale
	, double hypothenuse
	, std::vector<cv::Point2d>& line_fitting
	)
{
	switch (method)
	{
	case DouglasPeucker:
		{
			// approximates contour or a curve using Douglas-Peucker algorithm
			double epsilon = 2;// arclen*0.02;
			approxPolyDP(cv::Mat(contours[i_contour]), line_fitting, epsilon, true);
		}
		break;
#if (_MSC_VER > 1500)
	case mapnik_visvalingam_whyatt_relative_effective_area:
		{
			double tolerance = 0.025;
			mapnik_simplify<int>(contours[i_contour], tolerance
				, "visvalingam-whyatt-relative-effective-area", line_fitting);
		}
		break;
	case mapnik_visvalingam_whyatt_vertex_angle:
		{
			double tolerance = 12 * 0.0004*hypothenuse*hypothenuse;
			mapnik_simplify<int>(contours[i_contour], tolerance
				, "visvalingam-whyatt-vertex-angle", line_fitting);
		}
		break;
	case mapnik_visvalingam_whyatt:
		{
			//double tolerance = 0.0004*hypothenuse*hypothenuse;
			double tolerance = i_scale * m_scale*hypothenuse*hypothenuse;
			mapnik_simplify<int>(contours[i_contour], tolerance
				, "visvalingam-whyatt", line_fitting);
		}
		break;
	case mapnik_douglas_peucker_relative_segment_distance:
		{
			double tolerance = 0.04;
			mapnik_simplify<int>(contours[i_contour], tolerance
				, "douglas-peucker-relative-segment-distance", line_fitting);
		}
		break;
	case mapnik_zhao_saalfeld:
		{
			//https://github.com/mapnik/mapnik/issues/2748
			//Zhao - Saalfeld simplification implementation is suboptimal

			// I've been trying to understand the Zhao-Saalfeld simplification paper and while the description is a bit tricky,
			//I think I figured out the main idea, which is not how the Mapnik attempt is done.

			//In the code(https://github.com/mapnik/mapnik/blob/master/include/mapnik/simplify_converter.hpp#L261), 
			//each vertex check needs to go through the fit_sleeve routine, which itself iterates over a vector of sleeve points, 
			//doing an inside check on a rectangular polygon for each point, and this is not linear time algorithm because sleeve 
			//can contain any number of points. What the theorems and lemmas in the paper actually say is that you can avoid this
			//work completely, keeping an angle sector bound when starting a sleeve and then narrowing it down on each sleeve 
			//addition with a simple constant calculation, bringing the algorithm to linear time complexity. While the constant 
			//may be high due to expensive trigonometric function calls, it's still linear so it should be a huge improvement for 
			//lines with huge amount of points that are simplified a lot.


			//I am using Zhao - Saalfeld in some code I'm writing, and discovered that the algorithm in the paper has a bug, thought 
			//I'd give you a heads up since this is the only other project I've found using it!

			//The bug is this: consider a polyline which runs from a->b and back to a, with all points lying on the line ab.
			//All of these points lie within the ZS sector bound, so it will construct a simplifiying segment(a, a), no matter 
			//how far b is from a. (you get the same bug if you disallow backtracking along the line, you only need to backtrack 
			//along the sector bound making a narrow triangle).It doesn't appear to have been ZS's intention to discard narrow 
			//features like this, they describe the algorithm as removing points that are within some epsilon of the approximating 
			//segment.The bug is in the definition of d(p_1, q, p_2); they mix up line and segment distances in the paragraph 
			//immediately following that.

			//I guess for some uses, this kind of simplification will be ok.What I was looking for was to select parts of a map 
			//covering a route, so discarding points like this didn't work for me. I now use a variant of ZS where I keep track 
			//of the point furthest from p_1 inside the sleeve, and if I split the route, I do it there. This is backtracking, 
			//so ZS is no longer linear time (the worst case is O(n^2) with quite contrived routes), but gives the results I wanted.


			double tolerance = 0.005 * hypothenuse;
			//double tolerance = 1;
			mapnik_simplify<int>(contours[i_contour], tolerance
				, "zhao-saalfeld", line_fitting);
		}
		break;
#endif
	case my_line_fitting_algorithm_sum_alpha_tolerance:
		{
			double max_sum_alpha = 0.4 * pi;
			line_fitting_algorithm_sum_alpha_tolerance(max_sum_alpha, contours[i_contour], line_fitting);
		}
		break;

	case my_line_fitting_algorithm_epsilon_sector_tolerance:
		{
			double tolerance = pi / 36;
			line_fitting_algorithm_epsilon_sector_tolerance(tolerance, contours[i_contour], line_fitting);
		}
		break;

	case my_line_fitting_algorithm_equial_segment_length:
		{
			// ���������� �������� ���������� ������ �������� ����������� ����� ���������
			// �������� ������ ����������� ����������� �� ���
			double dist = i_scale*m_scale;
			// � �������� ��������� line_fitting ���������� ������������ �������
			line_fitting_algorithm_equial_segment_length(contours[i_contour], dist, line_fitting);
		}
		break;
	}


}


cv::RNG rng(12345);
cv::RNG rng2(12345);
void apply_contour_filtering(std::vector<std::vector<cv::Point> >& contours, std::vector<std::vector<cv::Point> >& contours_out);

void ContourRecognitionDirectoryEnumerator::test(const char * fn_in)
{
	cv::Mat input_image;
	cv::namedWindow("input_image", CV_WINDOW_AUTOSIZE);
	cv::waitKey(0);

	// ��������� �����������
	input_image = cv::imread(fn_in);
	// ���������� ��� �������� ���������� ��������� �� ������ ������ �����
	cv::Mat my_adaptive_canny_output_image;
	// ������ ���������
	int aperture_size = 3;
	// ���� ������� ������ ����� ������������ �����
	/*
	cv::MyAdaptiveCanny(input_image, my_adaptive_canny_output_image, aperture_size, false);
	// �����������
	cv::imshow("my_adaptive_canny_output_image", my_adaptive_canny_output_image);
	cv::waitKey(0);
	*/
	

	std::string fn = fn_in;
	std::string _f = fn.substr(fn.find_last_of('/') + 1);
	std::string _n = _f.substr(0, 1);


	cv::Mat src_gray;
	// ���� ����� ������� 3 ��� 4
	if (input_image.channels() == 3 || input_image.channels() == 4)
	{
		// ��������� � �������� ������
		cvtColor(input_image, src_gray, CV_BGR2GRAY);
	}
	else
	{
		// ���������
		src_gray = input_image.clone();
	}
	// ����� ��������
	cv::imshow("src_gray", src_gray);
	//cv::waitKey(0);


	// Find contours
	std::vector<std::vector<cv::Point> > contours;
	std::vector<cv::Vec4i> hierarchy;



	// ?? � ���������� �������� contours ��������� ����� ������ ������ ��������������� ��������
	// ��������� ��������� ����� �������� ��������� � my_adaptive_canny_output_image
	// CV_RETR_TREE � ��������� ��� �������, � ������������ ������ �������� ��������� ��������
	// CV_CHAIN_APPROX_SIMPLE � ������� ��������������, ������������, � ������������ ���� 
	// ?? ��� ������ ��������������, ������������, ������������ ���� 
	//my_findContours(my_adaptive_canny_output_image, contours, hierarchy, CV_RETR_TREE, CV_LINK_RUNS /*CV_CHAIN_APPROX_SIMPLE*/, cv::Point(0, 0));

	my_findContours(src_gray, contours, hierarchy, CV_RETR_TREE, CV_LINK_RUNS, cv::Point(0, 0));

	std::vector<std::vector<cv::Point> > contours_filtered;
	apply_contour_filtering(contours, contours_filtered);

	int n = 30;

	{
		/// Draw contours
		cv::Mat drawing = cv::Mat::zeros(src_gray.size(), CV_8UC3);
		cv::Mat drawing_n = cv::Mat::zeros(cv::Size(n*src_gray.size().width, n*src_gray.size().height), CV_8UC3);
		cv::Mat drawing_filtered = cv::Mat::zeros(src_gray.size(), CV_8UC3);
		cv::Mat drawing_filtered_n = cv::Mat::zeros(cv::Size(n*src_gray.size().width, n*src_gray.size().height), CV_8UC3);
		cv::Mat drawing_filtered_cont = cv::Mat::zeros(src_gray.size(), CV_8UC3);
		for (int i = 0; i < contours.size(); i++)
		{
			unsigned char r = rng.uniform(0, 255);
			unsigned char g = rng.uniform(0, 255);
			unsigned char b = rng.uniform(0, 255);

			// ��������� ������
			int fontFace = 1;
			double fontScale = 0.6;
			int thickness = 1;

			char str[1024];

			for (int j = 0; j < contours[i].size(); ++j)
			{
				cv::Point& pt = contours[i][j];
				if (pt.y >= 0 && pt.y < drawing.rows && pt.x >= 0 && pt.x < drawing.cols)
					drawing.at<cv::Vec3b>(pt.y, pt.x) = cv::Vec3b(r, g, b);


				sprintf_s(str, 1024, "%d\0", j);


				int baseline;
				cv::Size text_size = cv::getTextSize(str, fontFace, fontScale, thickness, &baseline);

				double part = rng2.uniform(-2.0, 2.0);

				cv::Point org = cvPoint(n + pt.x * n - 1, n + pt.y * n - 1 + text_size.height*part);
				// ������� ������
				cv::putText(drawing_n, str, org, fontFace, fontScale, cv::Vec3b(r, g, b), thickness);
			}

			for (int j = 0; j < contours_filtered[i].size(); ++j)
			{
				cv::Point& pt = contours_filtered[i][j];
				if (pt.y >= 0 && pt.y < drawing_filtered.rows && pt.x >= 0 && pt.x < drawing_filtered.cols)
					drawing_filtered.at<cv::Vec3b>(pt.y, pt.x) = cv::Vec3b(r, g, b);

				cv::Point org = cvPoint(n + pt.x * n - 1, n + pt.y * n - 1);
				if (org.y >= 0 && org.y < drawing_filtered_n.rows && org.x >= 0 && org.x < drawing_filtered_n.cols){
					drawing_filtered_n.at<cv::Vec3b>(org.y, org.x) = cv::Vec3b(r, g, b);
				}

				sprintf_s(str, 1024, "%d\0", j);


				int baseline;
				cv::Size text_size = cv::getTextSize(str, fontFace, fontScale, thickness, &baseline);

				org.y += text_size.height;
				// ������� ������
				cv::putText(drawing_filtered_n, str, org, fontFace, fontScale, cv::Vec3b(r, g, b), thickness);
			}

			drawContours(drawing_filtered_cont, contours_filtered, i, cv::Vec3b(r, g, b), 1, 8);

		}

		for (int r = 0; r < src_gray.size().height; ++r){
			for (int c = 0; c < src_gray.size().width; ++c){
				int R = n + r*n - 1;
				int C = n + c*n - 1;
				unsigned char v = src_gray.at<unsigned char>(r, c);
				drawing_n.at<cv::Vec3b>(R, C) = cv::Vec3b(v, v, v);
			}
		}


		/// Show in a window
		cv::namedWindow("Contours", CV_WINDOW_AUTOSIZE);
		cv::imshow("Contours", drawing);
		cv::imshow("Contours_filtered", drawing_filtered);
		cv::imshow("drawing_filtered_cont", drawing_filtered_cont);
		cv::imshow("drawing_filtered_n", drawing_filtered_n);

		cv::imwrite("E:\\Contour.bmp", src_gray);
		cv::imwrite("E:\\Contours.bmp", drawing);
		cv::imwrite("E:\\Contours_filtered.bmp", drawing_filtered);
		cv::imwrite("E:\\Contours_filtered_n.bmp", drawing_filtered_n);
		cv::imwrite("E:\\Contours_n.bmp", drawing_n);
		//cv::waitKey(0);
	}

}


void ContourRecognitionDirectoryEnumerator::work(const char * fn_in)
{
	cv::Mat input_image;
	// ��������� �����������
	input_image = cv::imread(fn_in);
	// ���������� ��� �������� ���������� ��������� �� ������ ������ �����
	cv::Mat my_adaptive_canny_output_image;
	// ������ ���������
	int aperture_size = 3;
	// ���� ������� ������ ����� ������������ �����
	if (!build_models)
	{
		// ����������� �����
		//cv::Canny(input_image, canny_output_i, threshold1, threshold2, aperture_size, false);
		// ������������ �����
		//cv::MyCannyEx(input_image, my_canny_output_image, integral1/*25*/, integral2/*12.5*/, aperture_size, false);
		// ���������� �����
		cv::MyAdaptiveCanny(input_image, my_adaptive_canny_output_image, aperture_size, false);
		// �����������
		cv::imshow("my_adaptive_canny_output_image", my_adaptive_canny_output_image);
	}

	std::string fn = fn_in;
	std::string _f = fn.substr(fn.find_last_of('/') + 1);
	std::string _n = _f.substr(0, 1);

	cv::Mat src_gray;
	// ���� ����� ������� 3 ��� 4
	if (input_image.channels() == 3 || input_image.channels() == 4)
	{
		// ��������� � �������� ������
		cvtColor(input_image, src_gray, CV_BGR2GRAY);
	}
	else
	{
		// ���������
		src_gray = input_image.clone();
	}
	// ����� ��������
	cv::imshow("src_gray", src_gray);
	// ������� ���������� ������� 
	int mlevel = 5; 
	cv::Mat bin;
	// ��� �������������� ������, � ��� ����������� MultiOtsuThresholds
	if (build_models)
	{
		cv::Mat multi_otso_thresholds;

		MultiOtsuThresholds(src_gray, multi_otso_thresholds, mlevel);
		int multi_otso_back_thresh = multi_otso_thresholds.ptr<int>()[mlevel - 1];

		printf("multi_otso_back_thresh = %d\n", multi_otso_back_thresh);

		threshold(src_gray, bin, multi_otso_back_thresh - 1, 255, cv::THRESH_BINARY);
		cv::imshow("bin", bin);
	}
	// Find contours
	std::vector<std::vector<cv::Point> > contours;
	std::vector<cv::Vec4i> hierarchy;

	if (build_models)
	{
		// CV_CHAIN_APPROX_SIMPLE � ������� ��������������, ������������, � ������������ ����
		// ��������� ��������� �������� ��������� � ������ � bin
		// ??? ��� ������ ��������������, ������������, ������������ ���� 
		findContours(bin, contours, hierarchy, CV_RETR_TREE, CV_CHAIN_APPROX_SIMPLE, cv::Point(0, 0));
	}
	else
	{
		// ?? � ���������� �������� contours ��������� ����� ������ ������ ��������������� ��������
		// ��������� ��������� ����� �������� ��������� � my_adaptive_canny_output_image
		// CV_RETR_TREE � ��������� ��� �������, � ������������ ������ �������� ��������� ��������
		// CV_CHAIN_APPROX_SIMPLE � ������� ��������������, ������������, � ������������ ���� 
		// ?? ��� ������ ��������������, ������������, ������������ ���� 
		my_findContours(my_adaptive_canny_output_image, contours, hierarchy, CV_RETR_TREE, /*CV_LINK_RUNS*/ CV_CHAIN_APPROX_SIMPLE, cv::Point(0, 0));
	}

	if (build_models){
		// ������ �������
		models.push_back(new Model());
		models.back()->fn = fn_in;
		models.back()->image = input_image.clone();
	}
	else
	{
		// �������� �� �������
		std::vector<Model*>::iterator it_model = models.begin();
		std::vector<Model*>::iterator end_model = models.end();
		// ������������ �����
		for (; it_model != end_model; ++it_model)
		{
			(*it_model)->scene_drawing = cv::Mat::zeros(input_image.size(), CV_8UC3);
			(*it_model)->scene_image = input_image.clone();
		}

		g_scene_image = input_image.clone();
	}

	cv::RNG rng;
	cv::Mat drawing = cv::Mat::zeros(input_image.size(), CV_8UC3);
	cv::Mat drawing2 = cv::Mat::zeros(input_image.size(), CV_8UC3);

	printf("contours.size() = %d\n", contours.size());

	// ���������� ����������� ����� ����� � �������� ������ / �����
	size_t min_contour_size = build_models ? MIN_CONTOUR_SIZE_MODEL : MIN_CONTOUR_SIZE_SCENE;

	for (size_t i_contour = 0; i_contour < contours.size(); ++i_contour)
	{
		if (contours[i_contour].size() < min_contour_size)
			continue;

		printf("i_contour %d %d\n", i_contour, contours.size());

		//line fitting algorithm to compute the polygonal approximations

		double arclen = cv::arcLength(cv::Mat(contours[i_contour]), true);
		// printf("arclen = %f\n", arclen);
		// ������������� ��� ����� �������
		cv::Rect shapeRect = cv::boundingRect(contours[i_contour]);
		// ����� ��������� �������������
		double hypothenuse = sqrt(shapeRect.width * shapeRect.width + shapeRect.height * shapeRect.height);
		//printf("hypothenuse=%f\n", hypothenuse);

		if (build_models)
		{
			if (0 == i_contour)
				models.back()->shapeRect = shapeRect;
			else
			{
				// ����������� ���������������, ��. ������������ ��������� ��� ��� ���������������
				models.back()->shapeRect |= shapeRect;
			}
		}



		if (true)
		{
			// TODO free
			std::list<SuperSegments*> super_segments;

			int N_scale = 1;
			double m_scale = 1.0;

			methods method;
#if (_MSC_VER > 1500)
			method = mapnik_visvalingam_whyatt;

				// ?? 6 � 10 ��� ����������
			N_scale
				= build_models
				? 6 //N_SCALE_MODEL
				: 10; //N_SCALE_SCENE

			// ?? 0.00005 � 0.000025 ��� ����������
			m_scale
				= build_models
				? 0.00005 //M_SCALE_MODEL
				: 0.000025; //M_SCALE_SCENE
#endif
			//methods method = mapnik_visvalingam_whyatt_vertex_angle;
			//methods method = mapnik_visvalingam_whyatt_relative_effective_area;
			//methods method = mapnik_zhao_saalfeld;
			//methods method = my_line_fitting_algorithm_sum_alpha_tolerance;
			//methods method = my_line_fitting_algorithm_epsilon_sector_tolerance;

			method = my_line_fitting_algorithm_equial_segment_length;

			// ����������� ����� ���������� ������������� ��� ������ � �����
			N_scale
				= build_models
				? N_SCALE_MODEL
				: N_SCALE_SCENE;

			// ����������� ���� ���������� �������������

			m_scale
				= build_models
				? M_SCALE_MODEL
				: M_SCALE_SCENE;

			N_scale = build_models ? N_SCALE_MODEL : N_SCALE_SCENE;
			m_scale = build_models ? M_SCALE_MODEL : M_SCALE_SCENE;

				// ���� ���������� ������� �������� ��������������
				// ������, ��� �������� �����������
				for (int n_points = N_POINTS_MIN; n_points <= N_POINTS_MAX; n_points+=2)
				{
					// ������ �������� ��������������
					super_segments.push_back(new SuperSegments(n_points));

			// � ����� �� ����� ���������� ������������ ���������� ����������� � ������ ��������� ������������� ������� ����������
			for (int i_scale = 1; i_scale <= N_scale; ++i_scale)
			{
				std::vector<cv::Point2d> line_fitting;
				// ����������� �� ������������ ������ �������������� ��������� � ������ ������
				apply_line_fitting_switch(method, i_contour, contours, i_scale, m_scale, hypothenuse, line_fitting);

				printf("line_fitting.size() = %d\n", line_fitting.size());
				// ���� ���������� ������� �������� ��������������
				// ������, ��� �������� �����������
				// � ��� ����������� - ����� ����� �������� �����������
				//for (int n_points = N_POINTS_MIN; n_points <= N_POINTS_MAX; n_points+=2)
				//{
				//	// ������ �������� ��������������
				//	super_segments.push_back(new SuperSegments(n_points));
					

					// ?? �� ������ ���� � ���� ��������, �����?
					// ����������������� - ������ ������
					if (build_models)
					{
						int j0 = 0;
						for (; j0 < n_points - 1; ++j0)
							calc_super_segments(n_points, line_fitting, j0, super_segments.back()->super_segments_list, i_contour, i_scale);
					}
					else
					{
						int j0 = 0;
						for (; j0 < n_points - 1; ++j0)
							calc_super_segments(n_points, line_fitting, j0, super_segments.back()->super_segments_list, i_contour, i_scale);
					}
				}
			}


			// ���������� �������������� � �������� 1
			draw_super_segments(drawing, super_segments, 1);

			if (build_models)
			{
				models.back()->fill(super_segments);
				models.back()->contours.push_back(contours[i_contour]);
			}
			else
			{
				std::list<SuperSegments*>::iterator it = super_segments.begin();
				std::list<SuperSegments*>::iterator end = super_segments.end();

				for (; it != end; ++it)
				{
					(*it)->build_correspondence_table(models, drawing2);
				}
			}
		}

		printf("i_contour %d %d end\n", i_contour, contours.size());
	}

	cv::imshow("Contours", drawing);
	cv::imshow("super_segment_with_corresponding_gray_code", drawing2);

	if (!build_models)
	{
		// ��� �����
#if 0
		build_histogram_of_error_of_overdetermined_get_affine_matrix(correspondence_table);
		build_histogram_of_determinant_R(correspondence_table);
		build_histogram_of_angle_between_model_and_scene_super_segment_directions(correspondence_table);
		build_histograms_of_hypotheses_affine_matrixes(correspondence_table);
#endif
		build_cluster_table(models);

		if (0)
		{
			std::vector<Model*>::iterator it_model = models.begin();
			std::vector<Model*>::iterator end_model = models.end();
			for (; it_model != end_model; ++it_model)
			{
				std::string winname = (*it_model)->get_super_segment_with_corresponding_gray_code_winname();
				cv::namedWindow(winname, CV_WINDOW_AUTOSIZE);
				//cv::createTrackbar("hash_table", winname, &(*it_model).hash_table_index, (*it_model).hash_table.size(), update_hash_table_index, &(*it_model));
				if ((*it_model)->correspondence_table_entry.hypotheses.size())
					cv::createTrackbar("correspondence_table", winname, &(*it_model)->correspondence_table_index, (*it_model)->correspondence_table_entry.hypotheses.size(), update_correspondence_table_index, (*it_model));
				bool do_not_display_rejected_clusters = true;
				// ���������� ���������
				int num_clusters = (*it_model)->cluster_table_entry.calcAsumedClusters(do_not_display_rejected_clusters);
				// ���� ����� ���������, �� ������� �������
				if (num_clusters)
					cv::createTrackbar("cluster_table", winname, &(*it_model)->cluster_table_index, num_clusters, update_cluster_table_index, (*it_model));
				cv::imshow(winname, (*it_model)->scene_drawing);
			}
		}

		cv::namedWindow("scene_image", CV_WINDOW_AUTOSIZE);

		// ���� �������� ������������������ ����� ��������
		for (int epoch = 0; epoch < 10; ++epoch)
		{
			calc_clusters_items();

			if (cluster_items.size())
				cv::createTrackbar("sorted_clusters", "scene_image", &ClusterItem::cluster_items_index, cluster_items.size(), update_cluster_items_index, NULL);


			cv::Mat output_image = input_image.clone();
			draw_clusters(output_image);
			cv::imshow("output_image", output_image);


#if HAVE_OPENCV_300
#if HAVE_VIZ
			doCloudViz();
#endif
#endif

			char ch = cv::waitKey();
			//���� ������������ �������� ������� 't' ���������� ���� ����� �������� ���������
			if ('t' == ch)
			{
				cluster_items.clear();
				do_epoch(models);
			}
			else
				break;
		}
	}
}

extern Extremum supersegment_val_extremum;
#if USE_SEGMENT_RELATIVE_LENGTH
extern Extremum supersegment_part_extremum;
#endif

#if 0
void ContourRecognitionDirectoryEnumerator::called_foo(char * fn_in, LARGE_INTEGER filesize)
{
	work(fn_in);

	printf("supersegment_val_extremum = %f %f\n", supersegment_val_extremum.minimum, supersegment_val_extremum.maximum);
#if USE_SEGMENT_RELATIVE_LENGTH
	printf("supersegment_part_extremum = %f %f\n", supersegment_part_extremum.minimum, supersegment_part_extremum.maximum);
#endif

	if (!build_models)
		cv::waitKey();
	else
		cv::waitKey(100);

	cluster_items.clear();

	std::vector<Model*>::iterator it_model = models.begin();
	std::vector<Model*>::iterator end_model = models.end();

	for (; it_model != end_model; ++it_model)
	{
		(*it_model)->correspondence_table_entry.freeHypotheses();
		(*it_model)->cluster_table_entry.freeClusterisationResults();
	}

}
#else
void ContourRecognitionDirectoryEnumerator::called_foo(char * fn_in, LARGE_INTEGER filesize)
{
	test(fn_in);
	cv::waitKey();
}
#endif

