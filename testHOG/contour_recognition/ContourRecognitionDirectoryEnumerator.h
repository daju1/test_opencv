#pragma once



#include "../DirectoryEnumerator.h"



class ContourRecognitionDirectoryEnumerator
	: public DirectoryEnumerator
{
	std::string output_directory_my_canny;
	std::string output_directory_canny;
public:

	// ������ �������
	int threshold1;
	int threshold2;

	double integral1;
	double integral2;

	// �������� ����� ��� �����
	void setOutputDirectoryMyCanny(std::string dir)
	{
		output_directory_my_canny = dir;
		CreateDirectoryA(dir.c_str(), NULL);
	}

	// �������� ����� ��� ����� ������������
	void setOutputDirectoryCanny(std::string dir)
	{
		output_directory_canny = dir;
		CreateDirectoryA(dir.c_str(), NULL);
	}

	ContourRecognitionDirectoryEnumerator(const char* fn, const char* ext)
		: DirectoryEnumerator(fn, ext)
	{
		// ������ �� ��������� 
		threshold1 = 106;
		threshold2 = 206;
		// ������������ ������ �� ���������
		//integral1 = 25.0;
		//integral2 = 12.5;
	}

	virtual bool IsStop()
	{
		return false;
	}

	void test(const char * fn_in);
	void work(const char * fn_in);
	virtual void called_foo(char * fn_in, LARGE_INTEGER filesize);
};


void initPlots();
