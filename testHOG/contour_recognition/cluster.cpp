#include "cluster.h"
#include "Model.h"
#include "Constants.h"

#include <set>
// ���������� ������ ���������
const int Cluster::clasterColorsNumber = 16;
// ��������� ����� �������� �� ������� ��������
CvScalar Cluster::getClusterColor(int clusterIdx)
{
   CvScalar line_color;
   line_color = CV_RGB(0, 0, 0);

   switch (clusterIdx % clasterColorsNumber)
   {
	   case 0:  line_color = CV_RGB(255,  0,   0); break;
	   case 1:  line_color = CV_RGB(0,  255,   0); break;
	   case 2:  line_color = CV_RGB(255, 255,  0); break;
	   case 3:  line_color = CV_RGB(0,    0, 255); break;
	   case 4:  line_color = CV_RGB(0,  255, 255); break;
	   case 5:  line_color = CV_RGB(255,  0, 255); break;
	   case 6:  line_color = CV_RGB(125,  0, 255); break;
	   case 7:  line_color = CV_RGB(255,  0, 125); break;
	   case 8:  line_color = CV_RGB(125,  0, 125); break;
	   case 9:  line_color = CV_RGB(0,  125,   0); break;
	   case 10: line_color = CV_RGB(125, 255,  0); break;
	   case 11: line_color = CV_RGB(255, 125,  0); break;
	   case 12: line_color = CV_RGB(125, 125,  0); break;
	   case 13: line_color = CV_RGB(0,  125, 255); break;
	   case 14: line_color = CV_RGB(0,  255, 125); break;
	   case 15: line_color = CV_RGB(0,  125, 125); break;
   }
   return line_color;
}
// ���������� ����� � �������
void Cluster::add(struct ClPoint* point, double learning_step)
{
	points.push_back(point);
	// � ����� ������ �� �������
	point->cluster = this;
	int N = points.size(); // ����� �����
	// ���� ��� ��� �� ��������� = 0, �� ��� = 1/N
	if (0 == learning_step) 
		learning_step = 1.0 / N;

	for (size_t i = 0; i < center.size(); ++i)
	{
		// ��������������� ������ ��������
		center[i] = center[i] + learning_step * (point->crd[i] - center[i]);
	}
}
// ������ ���.�������� � ��������� �����
void Cluster::calc_angles_sigma(
	  std::vector<double>& angles // ?? ��� �� ������ ����� ��� ��������
	, double& mean_angle // ���.�������� �����
	, double& sigma_angle // ��� �����
	)
{
	mean_angle = 0.0;
	// �� ���� �����, ���.��������
	for (int i = 0; i < (int) angles.size(); ++i)
	{
		mean_angle += angles[i];
	}
	mean_angle /= angles.size();

	double dispersion_angle = 0.0;
	// �� ���� �����, ���������
	for (int i = 0; i < (int) angles.size(); ++i)
	{
		dispersion_angle += Sqw(angles[i] - mean_angle);
	}
	dispersion_angle /= angles.size();

	sigma_angle = sqrt(dispersion_angle);
}
//  �������� ���������
bool Cluster::test(
	  struct ClPoint* point 
	, double max_error // ������
	, double max_sigma_angle) // ����. ��� �����
{
	double test_m[6];
	
	std::vector<cv::Point2f> src;
	std::vector<cv::Point2f> dst;

	extract_super_segment_points(points, src, dst);
	point->extract_super_segment_points(src, dst);
	// ��������� ������� ��������� ��������������
	double test_error = GetAffineMatrix(test_m, src, dst);
	// ���� ������ ������ ������������ �� �����
	if (test_error > max_error) return false;
	// ������� ������������
	double test_determinant_R = test_m[0] * test_m[4] - test_m[1] * test_m[3];
	// ������ ����� ����� ��������� �������������� ������ � �����
	std::vector<double> angles;
	// ��������� ����� ����� ��������� �������������� ������ � ����� 
	extract_angle_between_model_and_scene_super_segment_directions(angles);
	point->extract_angle_between_model_and_scene_super_segment_directions(angles);

	double test_mean_angle;
	double test_sigma_angle;
	Cluster::calc_angles_sigma(angles, test_mean_angle, test_sigma_angle);
	// ���� ��� ������ ����������� - �����
	if (test_sigma_angle > max_sigma_angle)
		return false;

#if 0
	double test_A, test_B, test_W, test_H;
	Cluster::calc_shear_angles(test_m, test_mean_angle, test_A, test_B, test_W, test_H);

	double test_shear_angle_x = 180 * atan(test_A) / pi;
	double test_shear_angle_y = 180 * atan(test_B) / pi;

	if (fabs(test_shear_angle_x) > max_shear_angle)
		return false;

	if (fabs(test_shear_angle_y) > max_shear_angle)
		return false;

	if (fabs(test_W / test_H) > max_scale_ratio)
		return false;

	if (fabs(test_H / test_W) > max_scale_ratio)
		return false;
#endif
	return true;
}
// ���������� ���������� �� ����� �� ������ ��������
double Clusterisator::calc_dist2(struct ClPoint* point, Cluster* cluster)
{
	double dist2 = 0.0;
	for (int i = 0; i < m_ndims; ++i)
	{
		dist2 += Sqw(point->crd[i] - cluster->center[i]);
	}
	return dist2;
}
// ��������� ���������� ��������
struct Cluster* Clusterisator::get_nearest_cluster(struct ClPoint* point, std::list<Cluster*>& clusters)
{
	// �������� �� ���������
	std::list<Cluster* >::const_iterator it_cluster = clusters.begin();
	std::list<Cluster* >::const_iterator end_cluster = clusters.end();

	struct Cluster* nearest = NULL;
	double min_dist2 = DBL_MAX;

	for (int i_cluster = 0; it_cluster != end_cluster; ++it_cluster, ++i_cluster)
	{
		// ���������� ����������
		double dist_time2 = calc_dist2(point, (*it_cluster));
		// ����� ������������
		if (min_dist2 > dist_time2){
			min_dist2 = dist_time2;
			nearest = (*it_cluster);
		}
	}
	return nearest;
}
// �������� ��������
bool Cluster::isAsumed(Model * model)
{
	// �������� �� ������ �������� - ����� �����
	if (points.size() < MINIMAL_CLUSTER_SIZE)
		return false;

	// ��������� ������������ ������� ��������
	size_t effective_cluster_size = get_effective_cluster_size_merge();

	// ������� �� ����������� ������ ��������
	if (effective_cluster_size < MINIMAL_EFFECTIVE_CLUSTER_SIZE)
		return false;

	// ���� ����� �� ����� �������, �� ���������� ��������������
	calcShapeRect();
	modelSupersegmentsShapeRectRelation = sqrt(double(modelSupersegmentsShapeRect.area()) / model->shapeRect.area());
	// ������ ������� ��������� ��������������
	calcAffineMatrix();

	// ����������� ������
	if (_error > MAX_ERROR)
		return false;

	// ����������� �������
	if (modelSupersegmentsShapeRectRelation < MIN_SHAPE_RECT_RELATION)
		return false;

	// ����������� ������������
	if (determinant_R < MIN_CLUSTER_DETERMINANT_R)
		return false;

	// ����������� ���� ����� �� �
	if (fabs(shear_angle_x) > MAX_SHEAR_ANGLE)
		return false;

	// ����������� ���� ����� �� �
	if (fabs(shear_angle_y) > MAX_SHEAR_ANGLE)
		return false;

	// ����������� ���������� �������������
	if (fabs(W / H) > MAX_SCALE_RATIO)
		return false;

	if (fabs(H / W) > MAX_SCALE_RATIO)
		return false;

	return true;
}

// ??? ������ ������������ ������� �������� ��� ��������������

// �������� ������������� ������ �������� struct super_segment_effective_measure ��� ������� �������� i_scale ���� ������������� ������.

// ������������ ������������ ������ ����� ������ �� ��������� ��������
// ���� ������ ����� ��������� � ������� ��������������. 

// ������� �������������� ����������� � 
// bool operator < (const super_segment_effective_measure& m1, const super_segment_effective_measure& m2)
// � �������� ������� ����� �������, ����� ���������������� ������������� � ���� ��������� ���������� ����
// ������ ��������� � ����� ��� ������� �������� ����������� � ������� ���� ������� ���� ���� ������������ ������
std::map<int, super_segment_effective_measures_list> Cluster::get_effective_cluster_size()
{
	// �������� �� ������ ��������
	std::list<struct ClPoint*>::iterator it_point = points.begin();
	std::list<struct ClPoint*>::iterator end_point = points.end();
	// �����
	std::map<int, super_segment_effective_measure_set > measure_sets;
	// ���� �� ����� ��������
	for (; it_point != end_point; ++it_point)
	{
		// ������ �������� ������������� ������ 
		int i_scale = (*it_point)->hypothes->model_super_segment.i_scale;
		cv::Rect shapeRect = cv::boundingRect((*it_point)->hypothes->model_super_segment.points);

		// ������������ �������������� ������
		measure_sets[i_scale].m_set.insert(super_segment_effective_measure(
			(*it_point)->hypothes->model_super_segment.i_contour
			, (*it_point)->hypothes->model_super_segment.start_line_index
			, (*it_point)->hypothes->model_super_segment.points.size()
			, shapeRect));
	}

	std::map<int, super_segment_effective_measures_list> ans;

	// �������� �� ������������� �����
	std::map<int, super_segment_effective_measure_set >::iterator it_set = measure_sets.begin();
	std::map<int, super_segment_effective_measure_set >::iterator end_set = measure_sets.end();
	// ���� �� ������������� �����
	for (; it_set != end_set; ++it_set)
	{
		ans.insert(std::map<int, super_segment_effective_measures_list>::value_type((*it_set).first, (*it_set).second.get_list()));
	}

	return ans;
}
/*
size_t Cluster::get_effective_cluster_size_max()
{
	size_t ans = 0;

	std::map<int, super_segment_effective_measures_list> cluster_sizes = get_effective_cluster_size();

	std::map<int, super_segment_effective_measures_list>::iterator it = cluster_sizes.begin();
	std::map<int, super_segment_effective_measures_list>::iterator end = cluster_sizes.end();
	for (; it != end; ++it)
	{
		if (ans < (*it).second.m_list.size())
			ans = (*it).second.m_list.size();
	}

	return ans;
}

size_t Cluster::get_effective_cluster_size_sum()
{
	size_t ans = 0;

	std::map<int, super_segment_effective_measures_list> cluster_sizes = get_effective_cluster_size();

	std::map<int, super_segment_effective_measures_list>::iterator it = cluster_sizes.begin();
	std::map<int, super_segment_effective_measures_list>::iterator end = cluster_sizes.end();
	for (; it != end; ++it)
	{
		ans += (*it).second.m_list.size();
	}

	return ans;
}
*/
size_t Cluster::get_effective_cluster_size_merge()
{

	super_segment_effective_measures_set a_super_segment_effective_measures_set;

	std::map<int, super_segment_effective_measures_list> cluster_sizes = get_effective_cluster_size();

	std::map<int, super_segment_effective_measures_list>::iterator it_i_scale = cluster_sizes.begin();
	std::map<int, super_segment_effective_measures_list>::iterator end_i_scale = cluster_sizes.end();
	for (; it_i_scale != end_i_scale; ++it_i_scale)
	{
		std::list<super_segment_effective_measures>::iterator it = (*it_i_scale).second.m_list.begin();
		std::list<super_segment_effective_measures>::iterator end = (*it_i_scale).second.m_list.end();
		for (; it != end; ++it)
		{
			a_super_segment_effective_measures_set.m_set.insert((*it));
		}
	}

	size_t ans = a_super_segment_effective_measures_set.size();
	return ans;
}

std::string Cluster::get_effective_cluster_size_str()
{
	std::stringstream ss;

	std::map<int, super_segment_effective_measures_list> cluster_sizes = get_effective_cluster_size();

	std::map<int, super_segment_effective_measures_list>::iterator it = cluster_sizes.begin();
	std::map<int, super_segment_effective_measures_list>::iterator end = cluster_sizes.end();
	for (; it != end; ++it)
	{
		ss << (*it).first << " " << (*it).second.m_list.size()  << " " ;
	}

	return ss.str();
}


// ���������� ����� ��������������
void ClPoint::extract_super_segment_points(
	  std::vector<cv::Point2f>& src // �������� ������ ����� ������������� ������ �� ��������
	, std::vector<cv::Point2f>& dst // �������� ������ ����� ������������� ����� �� ��������
	)
{
	for (size_t i = 0;
		i < hypothes->scene_super_segment.points.size() &&
		i < hypothes->model_super_segment.points.size(); ++i)
	{
		// � src ������ ����� �� �������� ������������� ������ 
		src.push_back(hypothes->model_super_segment.points[i]);
		// � dst ������ ����� �� �������� ������������� �����
		dst.push_back(hypothes->scene_super_segment.points[i]);
	}
}


// ���������� ����� ����� ������������ ������������� ������ � �����
void ClPoint::extract_angle_between_model_and_scene_super_segment_directions(std::vector<double>& angles)
{
	// � ���� ��������� ���� ����� ������������ ������������� ������ � ����� �������� 
	angles.push_back(hypothes->angle_between_model_and_scene_super_segment_directions);
}
// ���������� ������������� �� �����
void Cluster::extract_super_segment_points(
	  std::list<struct ClPoint*>& points // ����� ��������
	, std::vector<cv::Point2f>& src // ������ �����
	, std::vector<cv::Point2f>& dst // �������� ������ ����� 
	)
{
	// �������� �� ������ Clpoint
	std::list<struct ClPoint*>::iterator it_point = points.begin();
	std::list<struct ClPoint*>::iterator end_point = points.end();
	// ���� �� ������
	for (; it_point != end_point; ++it_point)
	{
		// ���������� �������������
		(*it_point)->extract_super_segment_points(src, dst);
	}
}


// ��������� ����� ����� ��������� �������������� ������ � ����� 
void Cluster::extract_angle_between_model_and_scene_super_segment_directions(std::vector<double>& angles)
{
	// �������� �� ������
	std::list<struct ClPoint*>::iterator it_point = points.begin();
	std::list<struct ClPoint*>::iterator end_point = points.end();
	// ���� �� ������
	for (; it_point != end_point; ++it_point)
	{
		(*it_point)->extract_angle_between_model_and_scene_super_segment_directions(angles);
	}
}


// ��������� ������� ��������� �������������� ������ ����� src � ����� ����� dst
double Cluster::GetAffineMatrix(double m[6], std::vector<cv::Point2f>& src, std::vector<cv::Point2f>& dst)
{
	double _error;
	cv::Mat M0 = getAffineTransformOverdetermined(src.data(), dst.data(), src.size(), _error);

	cv::Mat matM(2, 3, CV_64F, m);
	CV_Assert((M0.type() == CV_32F || M0.type() == CV_64F) && M0.rows == 2 && M0.cols == 3);
	M0.convertTo(matM, matM.type());

	return _error;
}


// ������� �������� �������������� ��� ����� ��������
double Cluster::GetAffineMatrix(
	  double m[6]
	, std::list<struct ClPoint*>& points) // ??
{
	std::vector<cv::Point2f> src;
	std::vector<cv::Point2f> dst;

	extract_super_segment_points(points, src, dst);

	return GetAffineMatrix(m, src, dst);
}


// ��������� ������� ��������� ��������������
double Cluster::GetAffineMatrix(double m[6])
{
	return GetAffineMatrix(m, points);
}

void Cluster::calcShapeRect()
{
	modelSupersegmentsShapeRect = cv::Rect();

	// �������� �� ������
	std::list<struct ClPoint*>::iterator it_point = points.begin();
	std::list<struct ClPoint*>::iterator end_point = points.end();

	for (int i_point = 0; it_point != end_point; ++it_point, ++i_point)
	{
		cv::Rect rect = cv::boundingRect((*it_point)->hypothes->model_super_segment.points);

		if (0 == i_point)
		{
			modelSupersegmentsShapeRect = rect;
		}
		else
		{
			modelSupersegmentsShapeRect |= rect;
		}
	}
}


// ������ ������������� �� �������
void Cluster::calcAffineMatrix()
{
	// ������
	_error = GetAffineMatrix(m);
	// �����������
	determinant_R = m[0] * m[4] - m[1] * m[3];

	std::vector<double> angles;
	// ����
	extract_angle_between_model_and_scene_super_segment_directions(angles);
	Cluster::calc_angles_sigma(angles, mean_angle, sigma_angle);

	Cluster::calc_shear_angles(m, mean_angle, A, B, W, H);
	// ���� � ��������
	shear_angle_x = 180 * atan(A) / pi;
	shear_angle_y = 180 * atan(B) / pi;
}

// ������ ���������� � ����������� �� ���� ��������
void Cluster::calc_shear_angles(double m[6], double mean_angle, double& A, double& B, double& W, double& H)
{
	//> mshearx:=linalg[matrix](3,3,[1,A,0, 0,1,0, 0,0,1]);
	//> msheary: = linalg[matrix](3, 3, [1, 0, 0, B, 1, 0, 0, 0, 1]);
	//> mscale: = linalg[matrix](3, 3, [W, 0, 0, 0, H, 0, 0, 0, 1]);
	//> mrotate: = linalg[matrix](3, 3, [cos(theta), sin(theta), 0, -sin(theta), cos(theta), 0, 0, 0, 1]);
	//> linalg[multiply](mshearx, msheary);
	//> linalg[multiply](mshearx, msheary, mscale);
	//> linalg[multiply](mshearx, msheary, mscale, mrotate);
	//> solve( {
	//         (1 + A*B)*W*cos(theta) - A*H*sin(theta) = m0,
	//         (1 + A*B)*W*sin(theta) + A*H*cos(theta) = m1,
	//         B*W*cos(theta) - H*sin(theta) = m3,
	//         B*W*sin(theta) + H*cos(theta) = m4},
	//       [A, B, H, W]);
	//  [
	//     A = (-cos(theta)*m1 + m0*sin(theta)) / (-cos(theta)*m4 + sin(theta)*m3), 
	//     B = -(m3 ^ 2 * cos(theta)*sin(theta) - m4 ^ 2 * sin(theta)*cos(theta) + 2 * m4*sin(theta) ^ 2 * m3 - m3*m4) / (m0*m4 - m1*m3), 
	//     H = -(-2 * cos(theta)*m4*sin(theta)*m3 + m3 ^ 2 * sin(theta) ^ 2 - m4 ^ 2 * sin(theta) ^ 2 + m4 ^ 2) / (-cos(theta)*m4 + sin(theta)*m3), 
	//     W = -(m0*m4 - m1*m3) / (-cos(theta)*m4 + sin(theta)*m3)
	//  ]

	// ������ ���� ���������� � ����������� �� �������� �������� ���� ��������
	double theta = pi * mean_angle / 180;

	A = (-cos(theta)*m[1] + m[0]*sin(theta)) / (-cos(theta)*m[4] + sin(theta)*m[3]);
	B = -(m[3] * m[3] * cos(theta)*sin(theta) - m[4] * m[4] * sin(theta)*cos(theta) + 2 * m[4] * sin(theta) * sin(theta) * m[3] - m[3] * m[4]) / (m[0] * m[4] - m[1] * m[3]);
	H = -(-2 * cos(theta)*m[4] * sin(theta)*m[3] + m[3] * m[3] * sin(theta) * sin(theta) - m[4] * m[4] * sin(theta) * sin(theta) + m[4] * m[4]) / (-cos(theta)*m[4] + sin(theta)*m[3]);
	W = -(m[0] * m[4] - m[1] * m[3]) / (-cos(theta)*m[4] + sin(theta)*m[3]);
}

