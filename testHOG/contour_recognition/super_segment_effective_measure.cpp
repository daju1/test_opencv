#include "super_segment_effective_measure.h"

bool operator < (const super_segment_effective_measure& m1, const super_segment_effective_measure& m2)
{
	if (m1.i_contour != m2.i_contour)
		return m1.i_contour < m2.i_contour;

	return m1.start_line_index < m2.start_line_index;
}

bool operator < (const super_segment_effective_measures& m1, const super_segment_effective_measures& m2)
{
	if (m1.i_contour != m2.i_contour)
		return m1.i_contour < m2.i_contour;

	if (m1.shapeRect.x != m2.shapeRect.x)
		return m1.shapeRect.x < m2.shapeRect.x;

	if (m1.shapeRect.y != m2.shapeRect.y)
		return m1.shapeRect.y < m2.shapeRect.y;

	if (m1.shapeRect.width != m2.shapeRect.width)
		return m1.shapeRect.width < m2.shapeRect.width;

	return m1.shapeRect.height < m2.shapeRect.height;
}

super_segment_effective_measures_list super_segment_effective_measure_set::get_list()
{
	super_segment_effective_measures_list res;

	std::set<super_segment_effective_measure>::const_iterator it1 = m_set.begin();
	std::set<super_segment_effective_measure>::const_iterator it2 = m_set.begin(); ++it2;
	std::set<super_segment_effective_measure>::const_iterator end = m_set.end();

	if (it1 == end)
		return res;

	res.m_list.push_back(super_segment_effective_measures((*it1).i_contour, (*it1).shapeRect));

	for (; it2 != end; ++it1, ++it2)
	{
		if ((*it1).i_contour != (*it2).i_contour)
		{
			res.m_list.push_back(super_segment_effective_measures((*it2).i_contour, (*it2).shapeRect));
		}
		else if ((*it1).start_line_index + (*it1).point_size < (*it2).start_line_index)
		{
			res.m_list.push_back(super_segment_effective_measures((*it2).i_contour, (*it2).shapeRect));
		}
		else
		{
			//minimum area rectangle containing rect2 and rect3 
			// http://stackoverflow.com/questions/30507141/is-there-any-function-in-opencv-to-find-the-intersection-union-and-complements
			res.m_list.back().shapeRect |= (*it2).shapeRect;
		}
	}

	return res;
}

size_t super_segment_effective_measures_set::size()
{
	size_t res = 0;

	std::set<super_segment_effective_measures>::const_iterator it1 = m_set.begin();
	std::set<super_segment_effective_measures>::const_iterator it2 = m_set.begin(); ++it2;
	std::set<super_segment_effective_measures>::const_iterator end = m_set.end();

	if (it1 == end)
		return res;

	++res;

	for (; it2 != end; ++it1, ++it2)
	{
		if ((*it1).i_contour != (*it2).i_contour)
		{
			++res;
			continue;
		}

		//(rectangle intersection)
		cv::Rect intersect = (*it1).shapeRect & (*it2).shapeRect;
		if (intersect.area() == 0.0)
		{
			++res;
		}
	}

	return res;
}
