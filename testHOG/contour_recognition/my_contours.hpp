
#include "opencv2/core.hpp"
namespace cv
{

/** @brief Finds contours in a binary image.

The function retrieves contours from the binary image using the algorithm @cite Suzuki85 . The contours
are a useful tool for shape analysis and object detection and recognition. See squares.c in the
OpenCV sample directory.

@note Source image is modified by this function. Also, the function does not take into account
1-pixel border of the image (it's filled with 0's and used for neighbor analysis in the algorithm),
therefore the contours touching the image border will be clipped.

@param image Source, an 8-bit single-channel image. Non-zero pixels are treated as 1's. Zero
pixels remain 0's, so the image is treated as binary . You can use compare , inRange , threshold ,
adaptiveThreshold , Canny , and others to create a binary image out of a grayscale or color one.
The function modifies the image while extracting the contours. If mode equals to RETR_CCOMP
or RETR_FLOODFILL, the input can also be a 32-bit integer image of labels (CV_32SC1).
@param contours Detected contours. Each contour is stored as a vector of points.
@param hierarchy Optional output vector, containing information about the image topology. It has
as many elements as the number of contours. For each i-th contour contours[i] , the elements
hierarchy[i][0] , hiearchy[i][1] , hiearchy[i][2] , and hiearchy[i][3] are set to 0-based indices
in contours of the next and previous contours at the same hierarchical level, the first child
contour and the parent contour, respectively. If for the contour i there are no next, previous,
parent, or nested contours, the corresponding elements of hierarchy[i] will be negative.
@param mode Contour retrieval mode, see cv::RetrievalModes
@param method Contour approximation method, see cv::ContourApproximationModes
@param offset Optional offset by which every contour point is shifted. This is useful if the
contours are extracted from the image ROI and then they should be analyzed in the whole image
context.
*/
CV_EXPORTS_W void my_findContours(InputOutputArray image, OutputArrayOfArrays contours,
	OutputArray hierarchy, int mode,
	int method, Point offset = Point());

/** @overload */
CV_EXPORTS void my_findContours(InputOutputArray image, OutputArrayOfArrays contours,
	int mode, int method, Point offset = Point());
}