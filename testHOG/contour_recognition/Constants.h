#if 0
// ���������� �������� �� ����� ����� �� ��������� ����� ������� � ���� �� ������������� ����������
// ����������� �������, ������� ����� ����� �� ����������� MIN_CONTOUR_SIZE_
// ��� ������ � ��� �����
#define MIN_CONTOUR_SIZE_MODEL 50
#define MIN_CONTOUR_SIZE_SCENE 10

// ���� ������������� ����������
// ��� ������������� ���������� ���������� ��������������� ������
// �������� ��������� ����� ���������� �������������  N_SCALE_ �  ����� M_SCALE_
// ��� ������ � ��� �����
#define N_SCALE_MODEL 4
#define N_SCALE_SCENE 4
#define M_SCALE_MODEL 4
#define M_SCALE_SCENE 12

// ����������� � ������������ ����� ����� � ��������������
#define N_POINTS_MIN 7
#define N_POINTS_MAX 11

// ���� ����-����������� �����
// ����� ���������� ��� ��������������� �����
#define N_PREDICATES_ANGLES 9
// ������������ ������� ����� ���������� ��������������, 
// ������������ �� �����, ����� ������� ������������ �����������������
#define GREY_CODE_MAXIMAL_DIFFERENCE_TO_BE_EQUIAL_ANGLES 0

// ���� ����-����������� ���� ��������
// ������������ �� ��� ��������������� � ���������� � ����� 
// ��� � ������������� ����� ��������
// ���� ��, ������ ��������� ����� ������� �������� 
// � ����� ����� ������������� ��� �������� ������� ���������� ��������
// ����� ����������������� �����, ���� ������� ������������� �������� ���������� 
// ���������� �������� ���������� �����
// ���� ������ my_line_fitting_algorithm_equial_segment_length
// �� �������� ���� �������� �� ����� ������
#define USE_SEGMENT_RELATIVE_LENGTH 0 
// ����� ���������� ��� ��������������� ������������� ���� ��������
#define N_PREDICATES_RELATIVE_LENGTH 3
// ������������ ������� ����� ���������� ��������������, 
// ����������� �� ������������� �����, ����� ������� ������������ �����������������
#define GREY_CODE_MAXIMAL_DIFFERENCE_TO_BE_EQUIAL_RELATIVE_LENGTH 0

// ��� ���������� ������ �������
// ����� �������������� ������������� ��������
// �� ��������������� ��������� ������������
#define MAX_HYPOTHES_DETERMINANT_R 10
// ?? ��� ���� �������� ������ � ������������������� ������� (�������������� �� �������, � ������� ���������)
// ����� - ��� ������ �������� http://compgraphics.info/2D/affine_transform.php � ��� ����� ������������ ������� R? ��� �������� �������������� ������� ���� ����� ���������� � |R|. 
// MAX_HYPOTHES_DETERMINANT_R � ����������� ������ ��� ���������� ������ �������� � ����� ��������� �������, ������� ������ �� �������

#define MAX_HYPOTHES_ERROR 10

// ��� ������������� (�������� ������������������ ����� ��������)
// �������� �������� ����� ������ �������� (����� ������ ����� ��������)
// ���� ��� � ���������� � ��� ���������� �������� 
// ������ ������������� ������� �������� ��������������
// �������� MAX_CLUSTERING_ERROR
// ��� ������������������ ���������� ����� ����� ��������������� ������ � �����
// �������� MAX_CLUSTERING_SIGMA_ANGLE
#define MAX_CLUSTERING_ERROR 50
#define MAX_CLUSTERING_SIGMA_ANGLE 4

// ��� ���������� ��������� ���������
// ����� ������������� ������������� �������� 
// �� ��������������� ����������������� ������������

// ����������� ������ �������� (����� ������� � ��������)
#define MINIMAL_CLUSTER_SIZE 2
// ����������� ����������� ������ �������� ����������� � ������ ����, 
// ��� ������������� ������ ������� ����� ����������� ���� ����� �� ����������� ������
// ������� ����� ����������� �� ������������� ���� ����� ��������
// (������ ������� ���������� ������������ ������� �������� ����� ��������)
#define MINIMAL_EFFECTIVE_CLUSTER_SIZE 2
// ��������� ������� �������������� ����������� ��������������� �������� �� ������ � ������� ������
#define MIN_SHAPE_RECT_RELATION 0.6
// ����������� ����������� R 
#define MIN_CLUSTER_DETERMINANT_R 0.001
// ������������ ���� �����
#define MAX_SHEAR_ANGLE 10
// ������������ ��������� ���������� ������������� �� ���� � ������
#define MAX_SCALE_RATIO 2
// ������������ ������ ������������� ������� �������� ��������������
#define MAX_ERROR 0.8

#else
// ��� ��������� ��� �����
// ���������� �������� �� ����� ����� �� ��������� ����� ������� � ���� �� ������������� ����������
#define MIN_CONTOUR_SIZE_MODEL 50
#define MIN_CONTOUR_SIZE_SCENE 10

// ���� ������������� ����������
#define N_SCALE_MODEL 8
#define N_SCALE_SCENE 8
#define M_SCALE_MODEL 10
#define M_SCALE_SCENE 5

// ����� ����� � ��������������
#define N_POINTS_MIN 9
#define N_POINTS_MAX 25

// ���� ����-����������� �����
#define N_PREDICATES_ANGLES 9
#define GREY_CODE_MAXIMAL_DIFFERENCE_TO_BE_EQUIAL_ANGLES 0

// ���� ����-����������� ���� ��������
#define USE_SEGMENT_RELATIVE_LENGTH 0
#define N_PREDICATES_RELATIVE_LENGTH 3
#define GREY_CODE_MAXIMAL_DIFFERENCE_TO_BE_EQUIAL_RELATIVE_LENGTH 0

// ��� ���������� ������ �������
#define MAX_HYPOTHES_DETERMINANT_R 100
#define MAX_HYPOTHES_ERROR 10

// ��� ������������� (�������� ������������������ ����� ��������)
#define MAX_CLUSTERING_ERROR 50
#define MAX_CLUSTERING_SIGMA_ANGLE 4

// ��� ���������� ��������� ���������
#define MINIMAL_CLUSTER_SIZE 3
#define MINIMAL_EFFECTIVE_CLUSTER_SIZE 3
#define MIN_SHAPE_RECT_RELATION 0.6
#define MIN_CLUSTER_DETERMINANT_R 0.001
#define MAX_SHEAR_ANGLE 10
#define MAX_SCALE_RATIO 2
#define MAX_ERROR 0.8

#endif
