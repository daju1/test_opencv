#include "Model.h"
#include "CorrespondenceTableEntry.h"
#include "SuperSegments.h"
#include "HashTable.h"
#include "ClusterItem.h"
#include "Constants.h"


Model::Model()
	: correspondence_table_index(0)
	, cluster_table_index(0)
	, cluster_table_entry(this)
	, learning_step(0.0)
{
	max_error = MAX_CLUSTERING_ERROR;
	max_sigma_angle = MAX_CLUSTERING_SIGMA_ANGLE;
}

Model::~Model()
{
	std::map<int, HashTable*>::iterator it = hash_tables.begin();
	std::map<int, HashTable*>::iterator end = hash_tables.end();

	for (; it != end; ++it)
	{
		if ((*it).second)
			delete (*it).second;
	}

	hash_tables.clear();
}


void Model::fill(std::list<SuperSegments*> & super_segments)
{
	std::list<SuperSegments*>::iterator it = super_segments.begin();
	std::list<SuperSegments*>::iterator end = super_segments.end();

	for (; it != end; ++it)
	{
		int n_points = (*it)->n_points;
		hash_tables.insert(std::map<int, HashTable*>::value_type(n_points, new HashTable((*it)->n_points)));
		hash_tables[n_points]->fill((*it)->super_segments_list);
	}
}

void Model::print_clusters()
{
	std::list<Cluster*>::iterator it_cluster = cluster_table_entry.clusters.begin();
	std::list<Cluster*>::iterator end_cluster = cluster_table_entry.clusters.end();

	printf("points, errors, determinant_R, rel, mean_angle, sigma_angle, shear_angle_x , shear_angle_y \n");

	for (; it_cluster != end_cluster; ++it_cluster)
	{
		if (!(*it_cluster)->isAsumed(this))
			continue;

		printf("%3d, %3.6f, %3.6f, %3.6f, %3.6f, %3.6f, %3.6f, %3.6f\n"
			, (*it_cluster)->points.size()

			, (*it_cluster)->_error
			, (*it_cluster)->determinant_R
			, (*it_cluster)->modelSupersegmentsShapeRectRelation
			, (*it_cluster)->mean_angle
			, (*it_cluster)->sigma_angle
			, (*it_cluster)->shear_angle_x
			, (*it_cluster)->shear_angle_y
			);
	}
	printf("\n");
}

void Model::build_cluster_table_entry()
{
	std::vector<Extremum> extremums;
	correspondence_table_entry.calcExtremumsOfHypothesesAffineMatrixes(extremums);

	printf("ExtremumsOfHypothesesAffineMatrixes\n");
	for (int i = 0; i < extremums.size(); ++i)
	{
		printf("[%f %f] ", extremums[i].minimum, extremums[i].maximum);
	}
	printf("\n");


	std::vector<Hypothes*>::iterator it_hypothes = correspondence_table_entry.hypotheses.begin();
	std::vector<Hypothes*>::iterator end_hypothes = correspondence_table_entry.hypotheses.end();

	for (int i_hypotheses = 0; it_hypothes != end_hypothes; ++it_hypothes, ++i_hypotheses)
	{
		printf("i_hypotheses %d %d\r", i_hypotheses, correspondence_table_entry.hypotheses.size());

		Hypothes& hypothes = *(*it_hypothes);

		int ndims = 6;
		cluster_table_entry.Clusterisation(
			ndims
			, hypothes
			, extremums);
	}
	printf("\n");

	cluster_table_entry.Clusterisation(max_error
		, max_sigma_angle
		, 0.0);

	print_clusters();
	init_learning_step();
}

void Model::init_learning_step()
{
	size_t max_points_size = 0;
	{
		std::list<Cluster*>::iterator it_cluster = cluster_table_entry.clusters.begin();
		std::list<Cluster*>::iterator end_cluster = cluster_table_entry.clusters.end();
		
		for (; it_cluster != end_cluster; ++it_cluster)
		{
			if (!(*it_cluster)->isAsumed(this))
				continue;

			if (max_points_size < (*it_cluster)->points.size())
				max_points_size = (*it_cluster)->points.size();
		}
	}
	printf("max_points_size = %d\n", max_points_size);
	learning_step = 1.0 / max_points_size;
	if (0 == max_points_size)
		learning_step = 0.5;
	printf("learning_step = %f\n", learning_step);
}

void Model::do_epoch()
{
	cluster_table_entry.Clusterisation(max_error
		, max_sigma_angle
		, learning_step);

	print_clusters();

	learning_step *= 0.9;
}


void Model::draw_on_scene_with_translation(double M[6], cv::Mat& drawing, cv::Scalar color)
{
	std::vector<std::vector<cv::Point> >::iterator it_model_contour = contours.begin();
	std::vector<std::vector<cv::Point> >::iterator end_model_contour = contours.end();

	for (; it_model_contour != end_model_contour; ++it_model_contour)
	{
		std::vector<cv::Point>& model_contour = (*it_model_contour);

		std::vector<cv::Point> scene_contour;
		scene_contour.resize(model_contour.size(), cv::Point());

		for (int j = 0; j < scene_contour.size(); ++j)
		{
			scene_contour[j].x = M[0] * model_contour[j].x + M[1] * model_contour[j].y + M[2];
			scene_contour[j].y = M[3] * model_contour[j].x + M[4] * model_contour[j].y + M[5];
		}

		for (int j = 0; j < scene_contour.size() - 1; ++j)
		{
			cv::line(drawing, scene_contour[j], scene_contour[j + 1], color, 2);
		}
	}
}

extern std::vector<Model*> models;
#if 0
void Model::draw_by_hash_table_index()
{
	destroyOtherModelsWindows();

	cv::RNG rng;

	std::string winname = get_super_segment_with_corresponding_gray_code_winname();
	std::map<GrayCode, std::list<SuperSegment> >::const_iterator it = hash_table.begin();
	std::map<GrayCode, std::list<SuperSegment> >::const_iterator end = hash_table.end();

	for (int index = 0; it != end; ++it, ++index)
	{
		if (index != hash_table_index)
			continue;

		cv::Mat input_image = this->image.clone();
		cv::Mat scene_image = this->drawing.clone();

		std::list<SuperSegment>::const_iterator it_super_segment = (*it).second.begin();
		std::list<SuperSegment>::const_iterator end_super_segment = (*it).second.end();

		for (; it_super_segment != end_super_segment; ++it_super_segment)
		{
			draw_super_segment(input_image, (*it_super_segment), CV_RGB(255, 0, 0), 3);
		}

		GrayCode gray_code = (*it).first;

		std::vector<Hypothes*>::const_iterator it_hypotheses = correspondence_table[this].hypotheses.begin();
		std::vector<Hypothes*>::const_iterator end_hypotheses = correspondence_table[this].hypotheses.end();


		for (; it_hypotheses != end_hypotheses; ++it_hypotheses)
		{
			if (gray_code.code != (*it_hypotheses)->model_super_segment.gray_code)
				continue;

			cv::Scalar color = cv::Scalar(rng.uniform(0, 255), rng.uniform(0, 255), rng.uniform(0, 255));
			
			draw_super_segment(scene_image, (*it_hypotheses)->scene_super_segment, color, 3);
			draw_super_segment(input_image, (*it_hypotheses)->model_super_segment, CV_RGB(0, 0, 255), 3);
		}



		cv::imshow(winname, scene_image);
		cv::imshow(fn, input_image);

		break;
	}
}
#endif

void Model::destroyOtherModelsWindows()
{
	std::vector<Model*>::iterator it_model = models.begin();
	std::vector<Model*>::iterator end_model = models.end();
	for (; it_model != end_model; ++it_model)
	{
		Model * model = (*it_model);

		if (this == model)
			continue;

		cv::destroyWindow(model->fn);
	}
}

void Model::draw_by_correspondence_table_index()
{
	destroyOtherModelsWindows();

	cv::Mat input_image = this->image.clone();
	cv::Mat scene_drawing = this->scene_drawing.clone();

	cv::RNG rng;

	std::string winname = get_super_segment_with_corresponding_gray_code_winname();

	char str[1024];
	int fontFace = 1;
	double fontScale = 0.9;
	int thickness = 1;
	int baseline;

	std::vector<Hypothes*>::const_iterator it_hypotheses = correspondence_table_entry.hypotheses.begin();
	std::vector<Hypothes*>::const_iterator end_hypotheses = correspondence_table_entry.hypotheses.end();


	for (int index = 0; it_hypotheses != end_hypotheses; ++it_hypotheses, ++index)
	{
		if (index != correspondence_table_index)
			continue;

		cv::Scalar color = cv::Scalar(rng.uniform(0, 255), rng.uniform(0, 255), rng.uniform(0, 255));

		draw_super_segment(scene_drawing, (*it_hypotheses)->scene_super_segment, color, 3, true);
		draw_super_segment(input_image, (*it_hypotheses)->model_super_segment, CV_RGB(0, 0, 255), 3, true);

		sprintf_s(str, 1024, "i_scale = %d", (*it_hypotheses)->scene_super_segment.i_scale);
		cv::Size text_size = cv::getTextSize(str, fontFace, fontScale, thickness, &baseline);
		cv::Point org = cvPoint(10, 10 + text_size.height);
		cv::putText(scene_drawing, str, org, fontFace, fontScale, color, thickness);

		sprintf_s(str, 1024, "points.size = %d", (*it_hypotheses)->scene_super_segment.points.size());
		text_size = cv::getTextSize(str, fontFace, fontScale, thickness, &baseline);
		org = cvPoint(10, 10 + text_size.height);
		cv::putText(scene_drawing, str, org, fontFace, fontScale, color, thickness);


		sprintf_s(str, 1024, "i_scale = %d", (*it_hypotheses)->model_super_segment.i_scale);
		text_size = cv::getTextSize(str, fontFace, fontScale, thickness, &baseline);
		org = cvPoint(10, 10 + text_size.height);
		cv::putText(input_image, str, org, fontFace, fontScale, color, thickness);

		sprintf_s(str, 1024, "points.size = %d", (*it_hypotheses)->model_super_segment.points.size());
		text_size = cv::getTextSize(str, fontFace, fontScale, thickness, &baseline);
		org = cvPoint(10, 10 + text_size.height);
		cv::putText(input_image, str, org, fontFace, fontScale, color, thickness);


	}


	cv::imshow(winname, scene_drawing);
	cv::imshow(fn, input_image);
}






void Model::draw_by_cluster_table_index()
{
	destroyOtherModelsWindows();

	cv::Mat input_image = this->image.clone();
	cv::Mat scene_drawing = this->scene_drawing.clone();
	cv::Mat scene_image = this->scene_image.clone();

	cv::RNG rng;

	my_cluster_point_data cluster_point_data;
	cluster_point_data.rng = &rng;
	cluster_point_data.input_image = &input_image;
	cluster_point_data.scene_drawing = &scene_drawing;

	my_cluster_data cluster_data;
	cluster_data.model = this;
	cluster_data.scene_image = &scene_image;
	cluster_data.input_image = &input_image;


	std::string winname = get_super_segment_with_corresponding_gray_code_winname();
	bool do_not_display_rejected_clusters = true;
	
	cluster_table_entry.enumAsumedClusters(do_not_display_rejected_clusters, my_cluster_callback_fun, &cluster_data, my_cluster_point_callback_fun, &cluster_point_data, cluster_table_index);
	
	cv::rectangle(input_image, shapeRect, CV_RGB(255, 0, 0));

	cv::imshow("scene_image", scene_image);
	cv::imshow(winname, scene_drawing);
	cv::imshow(fn, input_image);
}

#if 0
void update_hash_table_index(int k, void* p)
{
	((Model*)p)->hash_table_index = k;
	((Model*)p)->draw_by_hash_table_index();
}
#endif

void update_correspondence_table_index(int k, void* p)
{
	((Model*)p)->correspondence_table_index = k;
	((Model*)p)->draw_by_correspondence_table_index();
}

void update_cluster_table_index(int k, void* p)
{
	((Model*)p)->cluster_table_index = k;
	((Model*)p)->draw_by_cluster_table_index();
}