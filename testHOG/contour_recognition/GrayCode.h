#pragma once

#include <string>


struct GrayCode
{
	std::string code; // ��� ���
	size_t i_contour; // ����� �������
	int start_line_index; // ����� ��������� �����
	int n_points; // ����� �����

	// �����������
	GrayCode(const std::string code_, size_t i_contour_, int start_line_index_, int n_points_)
	{
		code = code_;
		i_contour = i_contour_;
		start_line_index = start_line_index_;
		n_points = n_points_;
	}
};

bool operator < (const GrayCode& gc1, const GrayCode& gc2);