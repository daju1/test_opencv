#pragma once
#include "Histogram.h"
#include "Hypothes.h"
#include "Extremum.h"

#include <vector>

struct CorrespondenceTableEntry
{
	std::vector<Hypothes*> hypotheses;

	CorrespondenceTableEntry()
	{

	}

	virtual ~CorrespondenceTableEntry()
	{
		freeHypotheses();
	}

	void freeHypotheses()
	{
		std::vector<Hypothes*>::iterator it = hypotheses.begin();
		std::vector<Hypothes*>::iterator end = hypotheses.end();

		for (; it != end; ++it)
		{
			delete (*it);
		}

		hypotheses.clear();
	}

	void calcExtremumsOfHypothesesAffineMatrixes(std::vector<Extremum>& extremums)
	{
		extremums.resize(6);

		std::vector<Hypothes*>::iterator it_hypothes = hypotheses.begin();
		std::vector<Hypothes*>::iterator end_hypothes = hypotheses.end();

		for (; it_hypothes != end_hypothes; ++it_hypothes)
		{
			Hypothes& hypothes = *(*it_hypothes);

			// ���������� ������ �������
			if (hypothes.isBad())
				return;


			for (int i = 0; i < 6; ++i)
				extremums[i].put(hypothes.affine_matrix.at<double>(i));
		}

	}

	void buildHistogramOfHypothesesAffineMatrixes(std::vector<Histogram*>& histograms, std::vector<Extremum>* extremums)
	{
		std::vector<Hypothes*>::iterator it_hypothes = hypotheses.begin();
		std::vector<Hypothes*>::iterator end_hypothes = hypotheses.end();

		for (; it_hypothes != end_hypothes; ++it_hypothes)
		{
			Hypothes& hypothes = *(*it_hypothes);

			for (int i = 0; i < 6; ++i)
				histograms[i]->add(extremums
				? (*extremums)[i].normal(hypothes.affine_matrix.at<double>(i))
				: hypothes.affine_matrix.at<double>(i));
		}
	}

	void calcExtremumOfDeterminantR(Extremum& extremum)
	{
		std::vector<Hypothes*>::iterator it_hypothes = hypotheses.begin();
		std::vector<Hypothes*>::iterator end_hypothes = hypotheses.end();

		for (; it_hypothes != end_hypothes; ++it_hypothes)
		{
			Hypothes& hypothes = *(*it_hypothes);

			extremum.put(hypothes.determinant_R);
		}

	}

	void buildHistogramOfDeterminantR(Histogram& histogram, Extremum* extremum)
	{
		std::vector<Hypothes*>::iterator it_hypothes = hypotheses.begin();
		std::vector<Hypothes*>::iterator end_hypothes = hypotheses.end();

		for (; it_hypothes != end_hypothes; ++it_hypothes)
		{
			Hypothes& hypothes = *(*it_hypothes);

			histogram.add(extremum
				? extremum->normal(hypothes.determinant_R)
				: hypothes.determinant_R);
		}
	}

	void calcExtremumOfAngleBetweenModelAndSceneSuperSegmentDirections(Extremum& extremum)
	{
		std::vector<Hypothes*>::iterator it_hypothes = hypotheses.begin();
		std::vector<Hypothes*>::iterator end_hypothes = hypotheses.end();

		for (; it_hypothes != end_hypothes; ++it_hypothes)
		{
			Hypothes& hypothes = *(*it_hypothes);

			extremum.put(hypothes.angle_between_model_and_scene_super_segment_directions);
		}

	}

	void buildHistogramOfAngleBetweenModelAndSceneSuperSegmentDirections(Histogram& histogram, Extremum* extremum)
	{
		std::vector<Hypothes*>::iterator it_hypothes = hypotheses.begin();
		std::vector<Hypothes*>::iterator end_hypothes = hypotheses.end();

		for (; it_hypothes != end_hypothes; ++it_hypothes)
		{
			Hypothes& hypothes = *(*it_hypothes);

			histogram.add(extremum
				? extremum->normal(hypothes.angle_between_model_and_scene_super_segment_directions)
				: hypothes.angle_between_model_and_scene_super_segment_directions);
		}
	}

	void calcExtremumOfErrorOfOverdeterminedGetAffineMatrix(Extremum& extremum)
	{
		std::vector<Hypothes*>::iterator it_hypothes = hypotheses.begin();
		std::vector<Hypothes*>::iterator end_hypothes = hypotheses.end();

		for (; it_hypothes != end_hypothes; ++it_hypothes)
		{
			Hypothes& hypothes = *(*it_hypothes);

			extremum.put(hypothes.error_of_overdetermined_get_affine_matrix);
		}

	}

	void buildHistogramOfErrorOfOverdeterminedGetAffineMatrix(Histogram& histogram, Extremum* extremum)
	{
		std::vector<Hypothes*>::iterator it_hypothes = hypotheses.begin();
		std::vector<Hypothes*>::iterator end_hypothes = hypotheses.end();

		for (; it_hypothes != end_hypothes; ++it_hypothes)
		{
			Hypothes& hypothes = *(*it_hypothes);

			histogram.add(extremum
				? extremum->normal(hypothes.error_of_overdetermined_get_affine_matrix)
				: hypothes.error_of_overdetermined_get_affine_matrix);
		}
	}
};
