#pragma once
#include "../cvplot.h"

#include <opencv/cv.h>
#include <opencv/highgui.h>
#include "opencv2/imgproc/imgproc.hpp"

class Histogram
{
	float summ;
	float mean;
	int N;
	float dispersion;
	bool is_hist_centred;


	//////////////////////////////
	float bin_width;
	int hist_len;
	int * hist;

public:

	Histogram(bool centred, int _hist_len = 100, float _bin_width = 0.05f)
	{
		hist_len = _hist_len;
		bin_width = _bin_width;
		hist = new int[hist_len];

		summ = 0.0;
		mean = 0.0;
		N = 0;
		dispersion = 0.0;
		is_hist_centred = centred;

		memset(hist, 0, hist_len*sizeof(int));
	}



	virtual ~Histogram()
	{
		if (hist)
			delete[] hist;
		hist = NULL;
	}

	double get_dispersion() { return dispersion; }

	void add(float val)
	{
		int bin = int(val / bin_width);
		//printf("Histogram add val = %f bin = %d", val, bin);

		if (is_hist_centred)
		{
			bin += hist_len / 2;
			//printf(" %d",  bin);
		}

		//printf("\n");

		if (bin < hist_len && bin >= 0)
			++hist[bin];
		else
		{
			printf("val=%f bin=%d do not correspond with hist_len = %d interval\n", val, bin, hist_len);
			return;
		}

		N += 1;
		summ += val;
		mean = summ / N;

		//printf("N=%d, summ=%f, mean=%f\n", N, summ, mean);

		dispersion = 0.0;
		for (int bin = 0; bin < hist_len; ++bin)
		{
			float bin_center = (bin * bin_width) + 0.5f * bin_width;
			//if (hist[bin])
			///   printf("%d bin_center=%f", hist[bin], bin_center);

			if (is_hist_centred)
			{
				bin_center -= hist_len * bin_width / 2;
				//   if (hist[bin])
				//      printf(" %f", bin_center);
			}

			dispersion += hist[bin] * (bin_center - mean) * (bin_center - mean);

			//if (hist[bin])
			//      printf(" %f %f %f\n", (bin_center - mean), (bin_center - mean) * (bin_center - mean), dispersion);

		}
		dispersion /= N;
		//printf("dispersion %f\n", dispersion);
	}

	void plot(const char* name)
	{
		CvPlot::clear(name);
		CvPlot::plot(name, hist, hist_len, 1);
	}

	void plotting(IplImage* plot, CvSize& plot_size, int plot_marg, double fontScale, const char* name)
	{
		CvScalar _color = CV_RGB(0, 255, 0);

		int max_hist = 0;
		for (int i = 0; i < hist_len; ++i)
		{
			if (max_hist < hist[i])
				max_hist = hist[i];
		}

		if (max_hist > 0)
		{
			for (int i = 0; i < hist_len - 1; ++i)
			{
				int x_h1 = (int)((plot_size.width - 2 * plot_marg) * i / hist_len);
				int y_h1 = (int)((plot_size.height - 2 * plot_marg) * hist[i] / max_hist);
				int x_h2 = (int)((plot_size.width - 2 * plot_marg) * (i + 1) / hist_len);
				int y_h2 = (int)((plot_size.height - 2 * plot_marg) * hist[i + 1] / max_hist);
				cvLine(plot, cvPoint(x_h1 + plot_marg, plot_size.height - y_h1 - plot_marg), cvPoint(x_h2 + plot_marg, plot_size.height - y_h2 - plot_marg), _color, 1);
			}
		}

		int fontFace = CV_FONT_HERSHEY_COMPLEX_SMALL;

		int thickness = 1;
		CvFont font;
		cvInitFont(&font, CV_FONT_HERSHEY_COMPLEX_SMALL, fontScale, fontScale);

		int h = 4;
		int x0 = plot_size.width / 6;

		int baseline = 0;

		const int nBufferSize = 1024;
		char text[nBufferSize];
		sprintf_s(text, nBufferSize, name);
		cv::Size textSize = cv::getTextSize(text, fontFace, fontScale, thickness, &baseline);
		cvPutText(plot, text, cv::Point(x0, 2 * (textSize.height + h)), &font, CV_RGB(255, 255, 255));

		sprintf_s(text, nBufferSize, "hist mu=%0.1f di=%0.3f si=%0.1f\0", mean, dispersion, sqrt(dispersion));
		textSize = cv::getTextSize(text, fontFace, fontScale, thickness, &baseline);
		cvPutText(plot, text, cv::Point(x0, 3 * (textSize.height + h)), &font, CV_RGB(255, 255, 255));
	}


};
