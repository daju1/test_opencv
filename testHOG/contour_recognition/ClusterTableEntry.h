#pragma once 
#pragma warning (disable:4550)
#include "cluster.h"
#include "Extremum.h"


typedef void (cluster_point_callback_fun)(ClPoint * point, void * p, int i);
typedef void (cluster_callback_fun)(Cluster * cluster, void * p, int i);

// ������� ������� ���������
struct ClusterTableEntry
{
	// ������ � ������� ��������� ��������� � ������ ���������� ������
	// ��� ������� �� ����� �������� � ���������

	// ������ �����
	std::list<ClPoint*> cluster_points;
	// ������ ���������
	std::list<Cluster*> clusters;
	// ��������� �� ������
	Model * model;
	// �����������
	ClusterTableEntry(Model * model_)
		: model(model_)
	{
	}
	// ����������
	virtual ~ClusterTableEntry()
	{
		freeClusterisationResults();
	}	
	// ??
	static void enumClusterPoints(
		  Cluster* cluster
		, cluster_point_callback_fun cluster_point_fun, void * p)
	{
		// �������� �� ������
		std::list<struct ClPoint*>::const_iterator it_point = cluster->points.begin();
		std::list<struct ClPoint*>::const_iterator end_point = cluster->points.end();

		for (int i_point = 0; it_point != end_point; ++it_point, ++i_point)
		{
			if (cluster_point_fun)
				cluster_point_fun(*it_point, p, i_point);
		}
	}
	// ??
	void enumAsumedClusters(bool do_not_display_rejected_clusters, cluster_callback_fun cluster_fun, void * cluster_data, cluster_point_callback_fun cluster_point_fun, void * cluster_point_data, int cluster_table_index)
	{
		printf("clusters_size = %d\n", clusters.size());

		std::list<Cluster* >::const_reverse_iterator it_cluster = clusters.rbegin();
		std::list<Cluster* >::const_reverse_iterator end_cluster = clusters.rend();

		for (int n_asumed_cluster = -1; it_cluster != end_cluster; ++it_cluster)
		{
			bool is_rejected_cluster = !(*it_cluster)->isAsumed(model);
			if (do_not_display_rejected_clusters && is_rejected_cluster)
				continue;

			++n_asumed_cluster;

			if (cluster_table_index != n_asumed_cluster)
				continue;

			if (cluster_fun)
				cluster_fun((*it_cluster), cluster_data, n_asumed_cluster);

			enumClusterPoints((*it_cluster), cluster_point_fun, cluster_point_data);

			break;
		}
	}
	// ����������� ���������� ���������
	int calcAsumedClusters(bool do_not_display_rejected_clusters)
	{
		int num_clusters = 0;
		// �������� �� ���������
		std::list<Cluster* >::const_reverse_iterator it_cluster = clusters.rbegin();
		std::list<Cluster* >::const_reverse_iterator end_cluster = clusters.rend();

		for (; it_cluster != end_cluster; ++it_cluster)
		{
			bool is_rejected_cluster = !(*it_cluster)->isAsumed(model);
			if (do_not_display_rejected_clusters && is_rejected_cluster)
				continue;

			++num_clusters;
		}
		return num_clusters;
	}


	void Clusterisation(int ndims
		, const Hypothes& hypothes                 // ��������
		, const std::vector<Extremum>& extremums); // ������ �����������

	void Clusterisation(double max_error
		, double max_sigma_angle
		, double learning_step);

	void erase_cluster_point_from_cluster(ClPoint* pt);

	// ������� ����������� �������������
	void freeClusterisationResults()
	{
		std::list<ClPoint*>::iterator it_point = cluster_points.begin();
		std::list<ClPoint*>::iterator end_point = cluster_points.end();

		// �������� ����� ��������
		for (; it_point != end_point; ++it_point)
		{
			delete (*it_point);
		}

		cluster_points.clear();

		std::list<Cluster*>::iterator it_cluster = clusters.begin();
		std::list<Cluster*>::iterator end_cluster = clusters.end();

		// �������� ������ ���������
		for (; it_cluster != end_cluster; ++it_cluster)
		{
			delete (*it_cluster);
		}

		clusters.clear();
	}
};

void initClusterisator();
