#pragma once

#include <opencv/cv.h>
#include <opencv/highgui.h>
#include "opencv2/imgproc/imgproc.hpp"

// ����������� �������, ������ � �������
#include <set>
#include <map>
#include <list>


// ������ � �������������
struct super_segment_effective_measure
{
	size_t i_contour;     // ����� ������� �� ������ �������� ����������� ������������
	size_t start_line_index; // ����� ��������� ����� �������������
	size_t point_size;    // ����� �����
	cv::Rect shapeRect;

	// �����������
	super_segment_effective_measure(
		size_t i_contour_,
		size_t start_line_index_,
		size_t point_size_,
		cv::Rect shapeRect_)
		: i_contour(i_contour_)
		, start_line_index(start_line_index_)
		, point_size(point_size_)
		, shapeRect(shapeRect_)
	{
	}
};
// ���������
bool operator < (const super_segment_effective_measure& m1, const super_segment_effective_measure& m2);

struct super_segment_effective_measures
{
	size_t i_contour;     // ����� ������� �� ������ �������� ����������� ������������
	cv::Rect shapeRect;
	super_segment_effective_measures(
		size_t i_contour_
		, cv::Rect shapeRect_)
		: i_contour(i_contour_)
		, shapeRect(shapeRect_)
	{
	}
};

bool operator < (const super_segment_effective_measures& m1, const super_segment_effective_measures& m2);

struct super_segment_effective_measures_list
{
	std::list<super_segment_effective_measures> m_list;
};

struct super_segment_effective_measure_set
{
	std::set<super_segment_effective_measure> m_set;
	super_segment_effective_measures_list get_list();
};

struct super_segment_effective_measures_set
{
	std::set<super_segment_effective_measures> m_set;
	size_t size();
};
