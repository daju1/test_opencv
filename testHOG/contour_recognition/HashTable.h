#pragma once

#include "GrayCode.h"
#include "SuperSegment.h"

#include <vector>
#include <string>
#include <map>
#include <list>

class Model;

struct HashTable
{
	int n_points; // ����� ���������
	// ����������� ���-�������
	HashTable(int n_points_)
	{
		n_points = n_points_;
	}
	// ����� - ���� (�������), �������� (������ �������������� � ���� ���������)
	std::map<GrayCode, std::list<SuperSegment> > hash_table;
	// ���������� ������
	void fill(std::list<SuperSegment>& super_segments);


	// ���������� ������� ������������ ������ � ������������� �����
	bool build_correspondence_table(Model * model
		, const SuperSegment& scene_super_segment
		, std::string super_segment_gray_code
		);

	// ���������� ������������� ��� �������� ������� ������������
	void draw_super_segment_with_corresponding_gray_code(Model * model // ������
		, const SuperSegment& scene_super_segment                      // ������������ �����
		, std::string super_segment_gray_code                          // �������
		, cv::Mat& input_image                                         // �����������
		, cv::Scalar color);                                           // ����

};
