#include "SuperSegment.h"
#include "Model.h"
#include "Hypothes.h"
#include "Extremum.h"
#include "Constants.h"

// ���������� ���� ������� ������, �������� ����� �������
double getAngle(cv::Point2d const& v0, cv::Point2d const& v1)
{
	double a = std::atan2((v1.y - v0.y), (v1.x - v0.x));
	return a;
}
// ������������� �� �������
// ���������� ����, ������� ����� �����, �������� ����� ������ ����� ������������� ������������� 
void line_fitting_algorithm_epsilon_sector_tolerance(double tolerance, std::vector<cv::Point>& contour, std::vector<cv::Point2d>& simplified)
{
	// ���� �� ������� ������� ����� ������ 3, �� ������ �� ������
	if (contour.size() < 3)
		return;
	// ��������� ������ ����� � �������� �����
	simplified.push_back(contour[0]);
	Extremum extremum;
	// ���� �� ������ ����� �� ���������
	for (size_t j = 1; j < contour.size(); ++j)
	{
		// ���� ������, ������������ ��� ����������� ����� � ������� � �������� �������
		double angle = getAngle(simplified.back(), contour[j]);
		// ���������� �����������
		extremum.put(angle);
		// ���� �������� �������� ���������� ������ �������
		if (fabs(extremum.delta()) >= tolerance)
		{
			// ��������� ���������� �����
			simplified.push_back(contour[j-1]);
			// �������� ����������
			extremum.reset();
		}
	}
}
// ���������� ���� ����� ����� �������, ������������� �� ������ 3 �����
double getAngleABC(cv::Point2d a, cv::Point2d b, cv::Point2d c)
{
	cv::Point2d ab = cv::Point2d( b.x - a.x, b.y - a.y );
	cv::Point2d cb = cv::Point2d( b.x - c.x, b.y - c.y );

	double dot = (ab.x * cb.x + ab.y * cb.y); // dot product
	double cross = (ab.x * cb.y - ab.y * cb.x); // cross product

	double alpha = atan2(cross, dot);

	return alpha;
}
// ���������� ����� 2 �������, ��� ���������� �����
double distance2(cv::Point2d pt0, cv::Point2d pt1)
{
	return
		(pt0.x - pt1.x) * (pt0.x - pt1.x)
		+ (pt0.y - pt1.y) * (pt0.y - pt1.y);
}


/*
	������ ������� ���������
	solve(
	{(x-pt_pre_x) / (pt_x-pt_pre_x) = (y-pt_pre_y)/(pt_y-pt_pre_y), // ��������� ������ ���������� ����� ����� (pt_pre_x, pt_pre_y) � (pt_x, pt_y) - ������� ������� ���������� ���������
	(x-current_x)^2 + (y-current_y)^2 = dist^2} // ��������� ���������� �������� dist � � ������� � ����� (current_x, y-current_y)
	, [x, y]);
*/
bool calc_fitting(double& d_pre       // ���������� d �� ���������� ����
	, double dist                     // ����� ��������� �� ������� ����������� ������
	, double d                        // ���������� ����� ������� ������ ����������� � �������� ����� ��������� ������������� ������������� � ������� ������ ���������� ��������� 
	, cv::Point2d& current            // ������� ����� ��� ����������� � �������� ����� ��������� ������������� �������������
	, const cv::Point2d& pt_pre       // ���������� ����� ��������� ��������� \    ������� �������
	, const cv::Point2d& pt           // ������� ����� ���������� ���������   /    ���������� ���������
	, std::vector<cv::Point2d>& out)  // �������� ����� ��������� ������������� �������������
{
	// �������� ������� ���������
	//solve( {(x-pt_pre_x) / (pt_x-pt_pre_x) = (y-pt_pre_y)/(pt_y-pt_pre_y), (x-current_x)^2 + (y-current_y)^2 = dist^2}, [x, y]);
	// ������� ���������� � ������� ����������� �������� Maple 10
	//x = (-pt_pre_x*pt_y - RootOf((-2 * pt_y*pt_pre_y + pt_x ^ 2 + pt_pre_x ^ 2 + pt_y ^ 2 + pt_pre_y ^ 2 - 2 * pt_x*pt_pre_x)*_Z ^ 2 + (4 * current_y*pt_y*pt_pre_y - 2 * pt_pre_x*pt_pre_y*current_x - 2 * pt_pre_y ^ 2 * current_y - 2 * current_x*pt_y*pt_x - 2 * pt_y ^ 2 * current_y + 2 * pt_x*pt_pre_y*pt_pre_x - 2 * pt_y*pt_pre_x ^ 2 - 2 * pt_pre_y*pt_x ^ 2 + 2 * pt_pre_x*pt_y*pt_x + 2 * current_x*pt_pre_x*pt_y + 2 * pt_x*pt_pre_y*current_x)*_Z + current_y ^ 2 * pt_y ^ 2 + current_y ^ 2 * pt_pre_y ^ 2 - dist ^ 2 * pt_y ^ 2 - dist ^ 2 * pt_pre_y ^ 2 - 2 * pt_pre_x*pt_y*pt_pre_y*pt_x + 2 * current_x*pt_y*pt_pre_y*pt_x + 2 * current_x*pt_pre_y*pt_pre_x*pt_y + current_x ^ 2 * pt_y ^ 2 + current_x ^ 2 * pt_pre_y ^ 2 - 2 * current_x*pt_y ^ 2 * pt_pre_x - 2 * current_x*pt_pre_y ^ 2 * pt_x - 2 * current_x ^ 2 * pt_y*pt_pre_y + 2 * dist ^ 2 * pt_y*pt_pre_y + pt_pre_x ^ 2 * pt_y ^ 2 + pt_pre_y ^ 2 * pt_x ^ 2 - 2 * current_y ^ 2 * pt_y*pt_pre_y)*pt_x 
	//                    + RootOf((-2 * pt_y*pt_pre_y + pt_x ^ 2 + pt_pre_x ^ 2 + pt_y ^ 2 + pt_pre_y ^ 2 - 2 * pt_x*pt_pre_x)*_Z ^ 2 + (4 * current_y*pt_y*pt_pre_y - 2 * pt_pre_x*pt_pre_y*current_x - 2 * pt_pre_y ^ 2 * current_y - 2 * current_x*pt_y*pt_x - 2 * pt_y ^ 2 * current_y + 2 * pt_x*pt_pre_y*pt_pre_x - 2 * pt_y*pt_pre_x ^ 2 - 2 * pt_pre_y*pt_x ^ 2 + 2 * pt_pre_x*pt_y*pt_x + 2 * current_x*pt_pre_x*pt_y + 2 * pt_x*pt_pre_y*current_x)*_Z + current_y ^ 2 * pt_y ^ 2 + current_y ^ 2 * pt_pre_y ^ 2 - dist ^ 2 * pt_y ^ 2 - dist ^ 2 * pt_pre_y ^ 2 - 2 * pt_pre_x*pt_y*pt_pre_y*pt_x + 2 * current_x*pt_y*pt_pre_y*pt_x + 2 * current_x*pt_pre_y*pt_pre_x*pt_y + current_x ^ 2 * pt_y ^ 2 + current_x ^ 2 * pt_pre_y ^ 2 - 2 * current_x*pt_y ^ 2 * pt_pre_x - 2 * current_x*pt_pre_y ^ 2 * pt_x - 2 * current_x ^ 2 * pt_y*pt_pre_y + 2 * dist ^ 2 * pt_y*pt_pre_y + pt_pre_x ^ 2 * pt_y ^ 2 + pt_pre_y ^ 2 * pt_x ^ 2 - 2 * current_y ^ 2 * pt_y*pt_pre_y)*pt_pre_x + pt_pre_y*pt_x) / (-pt_y + pt_pre_y), 
	//y =                   RootOf((-2 * pt_y*pt_pre_y + pt_x ^ 2 + pt_pre_x ^ 2 + pt_y ^ 2 + pt_pre_y ^ 2 - 2 * pt_x*pt_pre_x)*_Z ^ 2 + (4 * current_y*pt_y*pt_pre_y - 2 * pt_pre_x*pt_pre_y*current_x - 2 * pt_pre_y ^ 2 * current_y - 2 * current_x*pt_y*pt_x - 2 * pt_y ^ 2 * current_y + 2 * pt_x*pt_pre_y*pt_pre_x - 2 * pt_y*pt_pre_x ^ 2 - 2 * pt_pre_y*pt_x ^ 2 + 2 * pt_pre_x*pt_y*pt_x + 2 * current_x*pt_pre_x*pt_y + 2 * pt_x*pt_pre_y*current_x)*_Z + current_y ^ 2 * pt_y ^ 2 + current_y ^ 2 * pt_pre_y ^ 2 - dist ^ 2 * pt_y ^ 2 - dist ^ 2 * pt_pre_y ^ 2 - 2 * pt_pre_x*pt_y*pt_pre_y*pt_x + 2 * current_x*pt_y*pt_pre_y*pt_x + 2 * current_x*pt_pre_y*pt_pre_x*pt_y + current_x ^ 2 * pt_y ^ 2 + current_x ^ 2 * pt_pre_y ^ 2 - 2 * current_x*pt_y ^ 2 * pt_pre_x - 2 * current_x*pt_pre_y ^ 2 * pt_x - 2 * current_x ^ 2 * pt_y*pt_pre_y + 2 * dist ^ 2 * pt_y*pt_pre_y + pt_pre_x ^ 2 * pt_y ^ 2 + pt_pre_y ^ 2 * pt_x ^ 2 - 2 * current_y ^ 2 * pt_y*pt_pre_y)

	//y = RootOf(
	//           (-2 * pt_y*pt_pre_y + pt_x ^ 2 + pt_pre_x ^ 2 + pt_y ^ 2 + pt_pre_y ^ 2 - 2 * pt_x*pt_pre_x)*_Z ^ 2 
	//         + (4 * current_y*pt_y*pt_pre_y - 2 * pt_pre_x*pt_pre_y*current_x - 2 * pt_pre_y ^ 2 * current_y - 2 * current_x*pt_y*pt_x - 2 * pt_y ^ 2 * current_y + 2 * pt_x*pt_pre_y*pt_pre_x - 2 * pt_y*pt_pre_x ^ 2 - 2 * pt_pre_y*pt_x ^ 2 + 2 * pt_pre_x*pt_y*pt_x + 2 * current_x*pt_pre_x*pt_y + 2 * pt_x*pt_pre_y*current_x)*_Z 
	//         + current_y ^ 2 * pt_y ^ 2 + current_y ^ 2 * pt_pre_y ^ 2 - dist ^ 2 * pt_y ^ 2 - dist ^ 2 * pt_pre_y ^ 2 - 2 * pt_pre_x*pt_y*pt_pre_y*pt_x + 2 * current_x*pt_y*pt_pre_y*pt_x + 2 * current_x*pt_pre_y*pt_pre_x*pt_y + current_x ^ 2 * pt_y ^ 2 + current_x ^ 2 * pt_pre_y ^ 2 - 2 * current_x*pt_y ^ 2 * pt_pre_x - 2 * current_x*pt_pre_y ^ 2 * pt_x - 2 * current_x ^ 2 * pt_y*pt_pre_y + 2 * dist ^ 2 * pt_y*pt_pre_y + pt_pre_x ^ 2 * pt_y ^ 2 + pt_pre_y ^ 2 * pt_x ^ 2 - 2 * current_y ^ 2 * pt_y*pt_pre_y)

	// x = (-pt_pre_x*pt_y - y*pt_x + y*pt_pre_x + pt_pre_y*pt_x) / (-pt_y + pt_pre_y); 

	double epsilon = 1e-4;
	// ���� �������� [d_pre, d] �������� � ���� ����� ��������� �� ������� ����������� ������
	// �� ������ ��������� ����� ��������� ������ ����� ������ �� ������� ������� ���������� ���������
	// ���� ������ ����� ����������� �������� ������� ���������� ���������
	// � ����������� ������� dist ���������� �� ����� current
	if (d_pre <= dist && dist <= d)
	{
		cv::Point2d new_current;

		// analitical
		// a *y ^ 2 + b * y + c = 0;
		double a, b, c, sqrt_D, x, y;
		// ���� ������� ������� ���������� ��������� � ��������� �� epsilon �� �������� ��������������
		if (fabs(pt.y - pt_pre.y) > epsilon)
		{
			// ��������� ������������ ����������� ���������
			a = (-2.0 * pt.y*pt_pre.y + pt.x * pt.x + pt_pre.x * pt_pre.x + pt.y * pt.y + pt_pre.y * pt_pre.y - 2.0 * pt.x * pt_pre.x);
			b = (4.0 * current.y*pt.y*pt_pre.y - 2.0 * pt_pre.x*pt_pre.y*current.x - 2.0 * pt_pre.y * pt_pre.y * current.y - 2 * current.x*pt.y*pt.x - 2.0 * pt.y * pt.y * current.y + 2.0 * pt.x*pt_pre.y*pt_pre.x - 2.0 * pt.y * pt_pre.x * pt_pre.x - 2.0 * pt_pre.y*pt.x * pt.x + 2.0 * pt_pre.x*pt.y*pt.x + 2.0 * current.x*pt_pre.x*pt.y + 2.0 * pt.x*pt_pre.y*current.x);
			c = (current.y * current.y * pt.y * pt.y + current.y * current.y * pt_pre.y * pt_pre.y - dist * dist * pt.y * pt.y - dist * dist * pt_pre.y * pt_pre.y - 2.0 * pt_pre.x*pt.y*pt_pre.y*pt.x + 2.0 * current.x*pt.y*pt_pre.y*pt.x + 2.0 * current.x*pt_pre.y*pt_pre.x*pt.y + current.x * current.x * pt.y * pt.y + current.x * current.x * pt_pre.y * pt_pre.y - 2.0 * current.x*pt.y * pt.y * pt_pre.x - 2.0 * current.x*pt_pre.y * pt_pre.y * pt.x - 2 * current.x * current.x * pt.y*pt_pre.y + 2 * dist * dist * pt.y*pt_pre.y + pt_pre.x * pt_pre.x * pt.y * pt.y + pt_pre.y * pt_pre.y * pt.x * pt.x - 2 * current.y * current.y * pt.y*pt_pre.y);
			// ������������
			double D = b*b - 4.0 * a * c;

			sqrt_D = sqrt(D);
			// ���� ���� �� ������� ����������� ���������
			y = (-b + sqrt_D) / (2.0 * a);
			// ����������� � ��������� ������ ���������� ����� ����� (pt_pre.x, pt_pre.y) � (pt.x, pt.y) �������� ������� ���������� ���������
			x = (-pt_pre.x*pt.y - y*pt.x + y*pt_pre.x + pt_pre.y*pt.x) / (-pt.y + pt_pre.y);
		}
		// ���� ������� ������� ���������� ��������� � ��������� �� epsilon �� �������� ������������
		else if (fabs(pt.x - pt_pre.x) > epsilon)
		{
			// ��������� ������������ ����������� ���������
			a = (-2.0 * pt.x*pt_pre.x + pt.y * pt.y + pt_pre.y * pt_pre.y + pt.x * pt.x + pt_pre.x * pt_pre.x - 2.0 * pt.y * pt_pre.y);
			b = (4.0 * current.x*pt.x*pt_pre.x - 2.0 * pt_pre.y*pt_pre.x*current.y - 2.0 * pt_pre.x * pt_pre.x * current.x - 2 * current.y*pt.x*pt.y - 2.0 * pt.x * pt.x * current.x + 2.0 * pt.y*pt_pre.x*pt_pre.y - 2.0 * pt.x * pt_pre.y * pt_pre.y - 2.0 * pt_pre.x*pt.y * pt.y + 2.0 * pt_pre.y*pt.x*pt.y + 2.0 * current.y*pt_pre.y*pt.x + 2.0 * pt.y*pt_pre.x*current.y);
			c = (current.x * current.x * pt.x * pt.x + current.x * current.x * pt_pre.x * pt_pre.x - dist * dist * pt.x * pt.x - dist * dist * pt_pre.x * pt_pre.x - 2.0 * pt_pre.y*pt.x*pt_pre.x*pt.y + 2.0 * current.y*pt.x*pt_pre.x*pt.y + 2.0 * current.y*pt_pre.x*pt_pre.y*pt.x + current.y * current.y * pt.x * pt.x + current.y * current.y * pt_pre.x * pt_pre.x - 2.0 * current.y*pt.x * pt.x * pt_pre.y - 2.0 * current.y*pt_pre.x * pt_pre.x * pt.y - 2 * current.y * current.y * pt.x*pt_pre.x + 2 * dist * dist * pt.x*pt_pre.x + pt_pre.y * pt_pre.y * pt.x * pt.x + pt_pre.x * pt_pre.x * pt.y * pt.y - 2 * current.x * current.x * pt.x*pt_pre.x);

			// ������������
			double D = b*b - 4.0 * a * c;

			sqrt_D = sqrt(D);
			// ���� ���� �� ������� ����������� ���������
			x = (-b + sqrt_D) / (2.0 * a);
			// ����������� � ��������� ������ ���������� ����� ����� (pt_pre.x, pt_pre.y) � (pt.x, pt.y) �������� ������� ���������� ���������
			y = (-pt_pre.y*pt.x - x*pt.y + x*pt_pre.y + pt_pre.x*pt.y) / (-pt.x + pt_pre.x);
		}
		else
		{
			// ���� �� ������ �������� ���������, ���� �������, ���-�� ������, �������� ������� �������
			assert(false); 
		}

		// ��������� �������� �� (x,y) �� ������� ������� ���������� ���������
		if (
			((pt_pre.x - epsilon <= x && x <= pt.x + epsilon) || (pt_pre.x + epsilon >= x && x >= pt.x - epsilon))
			&&
			((pt_pre.y - epsilon <= y && y <= pt.y + epsilon) || (pt_pre.y + epsilon >= y && y >= pt.y - epsilon))
			)
		{
			// ���� ��, �� ����� �������
			new_current.x = x;
			new_current.y = y;
		}

		else
		{
			// ���� ���
			// ���� ������ ������� ����������� ���������
			// ����������� � ��������� ������ ���������� ����� ����� �������� ������� ���������� ���������
			if (fabs(pt.y - pt_pre.y) > epsilon)
			{
				y = (-b - sqrt_D) / (2.0 * a);
				x = (-pt_pre.x*pt.y - y*pt.x + y*pt_pre.x + pt_pre.y*pt.x) / (-pt.y + pt_pre.y);
			}
			else if (fabs(pt.x - pt_pre.x) > epsilon)
			{
				x = (-b - sqrt_D) / (2.0 * a);
				y = (-pt_pre.y*pt.x - x*pt.y + x*pt_pre.y + pt_pre.x*pt.y) / (-pt.x + pt_pre.x);
			}
			else
				assert(false);

			// � ����� ��������� �������� �� (x,y) �� ������� ������� ���������� ���������
			if (
				((pt_pre.x - epsilon <= x && x <= pt.x + epsilon) || (pt_pre.x + epsilon >= x && x >= pt.x - epsilon))
				&&
				((pt_pre.y - epsilon <= y && y <= pt.y + epsilon) || (pt_pre.y + epsilon >= y && y >= pt.y - epsilon))
				)
			{
				// ���� ��, �� ����� �������
				new_current.x = x;
				new_current.y = y;
			}

			else
				// ���� ���, ������ ���-�� �� ���, � ��������� ���-�� ������
				assert(false);

		}
		// ��������� ��������� �������
		out.push_back(new_current);
		// ��������� ��������������� ����������
		current = new_current;
		d_pre = 0.0;

		return true;
	}

	return false;
}

// ���������� ������� �� �������� ������ �����
void line_fitting_algorithm_equial_segment_length(std::vector<cv::Point>& contour, double dist, std::vector<cv::Point2d>& out)
{
	// ���� ����� � ������� ������ 2, �� ������ �� ������
	if (contour.size() < 2)
		return;
	// ��������� ���������� �����
	double dist2 = dist; 
	// � �������� ����� ��������� ������ ����� ���������� ���������
	cv::Point2d current = contour[0];
	out.push_back(current);

	double d_pre = 0.0;
	// ��������� �� ���� ������
	for (size_t i = 1; i < contour.size(); ++i)
	{
		// ���������� ����� ������� ������ ����������� � �������� ����� ��������� ������������� ������������� � ������� ������ ���������� ��������� 
		double d = sqrt(distance2(current, contour[i]));
		// ���������� ����� ��� ��������� ������ �����
		if (calc_fitting(d_pre, dist2, d, current, contour[i - 1], contour[i], out))
		{
			do
			{
				d = sqrt(distance2(current, contour[i]));
			} while (calc_fitting(d_pre, dist2, d, current, current, contour[i], out));
		}
		d_pre = d;
	}
}
// ���������� ����� �����: ����, ����, ��������� ����
void update_summ_alpha(const double angle, double& alpha, double& sum_alpha)
{
	alpha = pi - angle;
	if (alpha > pi)
		alpha -= 2.0 * pi;
	if (alpha < -pi)
		alpha += 2.0 * pi;

	sum_alpha += alpha;
}
// ������������� ������������� �� ������ ������������ ������� �� ����
void line_fitting_algorithm_sum_alpha_tolerance(double max_sum_alpha, std::vector<cv::Point>& contour, std::vector<cv::Point2d>& simplified)
{
	// ���� ����� � ������� ������ 3, �� ������ �� ������ 
	if (contour.size() < 3)
		return;
	// ��������� � ������ ������ �����
	simplified.push_back(contour[0]);
	// ����� ����� 0
	double sum_alpha = 0.0;
	// ���� �� ���� ������
	for (size_t j = 1; j < contour.size() - 1; ++j){
		// ���� ����� 2 �������, ������������� ����� �������
		double angle = getAngleABC(contour[j - 1], contour[j], contour[j + 1]);

		double alpha;
		// ����������� ����
		update_summ_alpha(angle, alpha, sum_alpha);
		// ���� ����������� ���� ��������� ����������, ���������� �����
		if (fabs(sum_alpha) >= max_sum_alpha && j < contour.size() - 2)
		{
			simplified.push_back(contour[j]);
			sum_alpha = 0.0;
		}
	}
}

#define calc_super_segments_func calc_super_segments_forward
#define calc_super_segments_iterator const_iterator
#define calc_super_segments_iterator_begin begin
#define calc_super_segments_iterator_end end
#define i_sign +1
// ��� ���� ��������� ������ ��� ����������� ������ ��������, ������ ��� �������� ��������
// � ����� ���������, ��� �������� ��� ��������
#include "calc_super_segments_cpp.h"

#undef calc_super_segments_iterator
#undef calc_super_segments_func
#undef calc_super_segments_iterator_begin
#undef calc_super_segments_iterator_end
#undef i_sign

#define calc_super_segments_func calc_super_segments_reverse
#define calc_super_segments_iterator const_reverse_iterator
#define calc_super_segments_iterator_begin rbegin
#define calc_super_segments_iterator_end rend
#define i_sign -1
#include "calc_super_segments_cpp.h"

#undef calc_super_segments_iterator
#undef calc_super_segments_func
#undef calc_super_segments_iterator_begin
#undef calc_super_segments_iterator_end
#undef i_sign

void calc_super_segments(int n_points, std::vector<cv::Point2d>& line_fitting_out, int j0, std::list<SuperSegment>& super_segments, size_t i_contour, int i_scale)
{
	// ������ ������
	calc_super_segments_forward(n_points, line_fitting_out, j0, super_segments, i_contour, i_scale);
}
// ����� ������������ ������ ���������� �������
// ��� ����������� ������� ������� ��� ������ � ������������ ������
void calc_super_segments_2(int n_points, std::vector<cv::Point>& line_fitting_out, int j0, std::vector<SuperSegment>& super_segments, size_t i_contour, int i_scale)
{
	if ( (int) line_fitting_out.size() < n_points + j0)
		return;

	std::vector<cv::Point>::const_iterator it0 = line_fitting_out.begin() + j0;
	std::vector<cv::Point>::const_iterator end = line_fitting_out.end();

	bool common_edge = false;

	int start_line_index = 0;
	super_segments.push_back(SuperSegment(i_contour, start_line_index, i_scale));
	super_segments.back().points.push_back(*it0);

	if (common_edge){
		super_segments.back().points.push_back(*(it0 + 1));
	}

	int sum_points = super_segments.back().points.size();

	int j_end
		= common_edge
		? line_fitting_out.size() - 1
		: line_fitting_out.size();

	std::vector<cv::Point>::const_iterator it = it0 + 1;
	for (int j = j0 + 1; j < j_end; ++j, ++it){

		if (!common_edge){
			super_segments.back().points.push_back(*it);
		}
		if (common_edge){
			super_segments.back().points.push_back(*(it + 1));
		}

		++sum_points;

		if (sum_points >= n_points)
		{
			start_line_index = j;
			super_segments.push_back(SuperSegment(i_contour, start_line_index, i_scale));
			if (common_edge){
				super_segments.back().points.push_back(*it);
				super_segments.back().points.push_back(*(it + 1));
			}
			else{
				super_segments.back().points.push_back(*it);
			}

			sum_points = super_segments.back().points.size();
		}
	}
}
// ������ ���������� ��� ����������
void calc_super_segments_old(int n_points, std::vector<cv::Point>& line_fitting_out, int j0, std::vector<SuperSegment>& super_segments, size_t i_contour, int i_scale)
{
	if ( (int) line_fitting_out.size() < n_points + j0)
		return;

	bool common_edge = false;

	int start_line_index = 0;
	super_segments.push_back(SuperSegment(i_contour, start_line_index, i_scale));
	super_segments.back().points.push_back(line_fitting_out[j0]);

	if (common_edge)
		super_segments.back().points.push_back(line_fitting_out[j0 + 1]);

	int sum_points = super_segments.back().points.size();

	int j_end 
		= common_edge 
		? line_fitting_out.size() - 1
		: line_fitting_out.size();

	for (int j = j0 + 1; j < j_end; ++j){

		if (!common_edge)
			super_segments.back().points.push_back(line_fitting_out[j]);
		if (common_edge)
			super_segments.back().points.push_back(line_fitting_out[j + 1]);

		++sum_points;

		if (sum_points >= n_points)
		{
			start_line_index = j;
			super_segments.push_back(SuperSegment(i_contour, start_line_index, i_scale));
			if (common_edge){
				super_segments.back().points.push_back(line_fitting_out[j]);
				super_segments.back().points.push_back(line_fitting_out[j + 1]);
			}
			else{
				super_segments.back().points.push_back(line_fitting_out[j]);
			}


			sum_points = super_segments.back().points.size();
		}
	}
}

// ���������� ���� ���� �����
// ������� ����� ���������� �� 0 �� 1
// ������ �������� ����� ����������
// ������� ���������� ����������� ��������� 9 ����������
// ���� ����� ������, ������������� ������ letter += 3;
// ���������� ������ ��������
std::string calc_gray_code(double val, int n_predicates)
{
	std::string code;
	if (val < 0.5)
		code += 'A';
	else
		code += 'B';

	double step = 0.5 / n_predicates;
	char letter = 'C';
	for (int n = 1; n < n_predicates; ++n)
	{
		if (val < 0.5 - n*step)
			code += letter;
		else if (val < 1.0 - n*step)
			code += letter + 1;
		else
			code += letter + 2;

		letter += 3;
	}

	return code;
}


// ���������� �������������: �����������, ������������, ����, �������, ���� ���������� �����
void draw_super_segment(cv::Mat& drawing, const SuperSegment& super_segment, cv::Scalar color, int thickness, bool draw_points)
{
	if (thickness <= 0)
		thickness = 1;

	if (thickness > 255)
		thickness = 1;


	// ��������� �� ���� ������ �������������
	for (size_t j = 0; j < super_segment.points.size() - 1; ++j)
	{
	    // ���������� ����� ����� ����� ������� � �������� ������ � ��������
		cv::line(drawing, super_segment.points[j], super_segment.points[j + 1], color, thickness);
	}

	if (!draw_points)
		return;

	// ���� ����� ���������� �����
	// �� ����������� �� ������ �������������
	for (size_t j = 0; j < super_segment.points.size(); ++j)
	{
		// ������������ ����� �������� ������ � ��������
		cv::circle(drawing, super_segment.points[j], 2 * thickness, color);
		//printf("super_segment.points[%d] %f %f\n", j, super_segment.points[j].x, super_segment.points[j].y);
	}
}

// ������ ����� �������������
void SuperSegment::print() const
{
	printf("super_segment{%d} ", points.size());
	// ��������� �� ������ �������������
	for (size_t j = 0; j < points.size(); ++j)
	{
		printf("[%f %f] ", points[j].x, points[j].y);
	}
	printf("\n");
}
// ���������� ������������������ ��������������: �����������, ������������������ ��������������, �������
void draw_super_segments(cv::Mat& drawing, std::list<SuperSegment>& super_segments, int thickness)
{
	cv::RNG rng;
	// �������� �� ��������������
	std::list<SuperSegment>::const_iterator it_ss = super_segments.begin();
	std::list<SuperSegment>::const_iterator end_ss = super_segments.end();
	for (; it_ss != end_ss; ++it_ss)
	{
		// ���������� ����
		cv::Scalar color = cv::Scalar(rng.uniform(0, 255), rng.uniform(0, 255), rng.uniform(0, 255));
		// ���������� �������������
		draw_super_segment(drawing, (*it_ss), color, thickness);
	}
}

Extremum supersegment_val_extremum;
#if USE_SEGMENT_RELATIVE_LENGTH
Extremum supersegment_part_extremum;
#endif
// ���������� ���� ���� ������������
// ������ �������� - �������� ��������, ����� ����������������� ��������

std::string calc_super_segment_gray_code(const SuperSegment& super_segment, double& sum_alpha)
{
	std::string super_segment_gray_code;
#if USE_SEGMENT_RELATIVE_LENGTH
	std::vector<double> super_segment_lengthes(super_segment.points.size() - 1);

	double common_super_segment_length = 0.0;
	// ��������� �� ������ �������������
	for (int j = 0; j < super_segment.points.size() - 1; ++j)
	{
		super_segment_lengthes[j] = sqrt(distance2(super_segment.points[j], super_segment.points[j + 1]));
		common_super_segment_length += super_segment_lengthes[j];
	}
	// ��������� �� ������ �������������
	for (int j = 0; j < super_segment.points.size() - 1; ++j)
	{
		// ������ �����
		double part = super_segment_lengthes[j] / common_super_segment_length;

		supersegment_part_extremum.put(part);
		// ?? ��� ��� ����� ���� ������� ��������� �������� ������
		std::string code = calc_gray_code(part, N_PREDICATES_RELATIVE_LENGTH);
		super_segment_gray_code += code;
	}
#endif
	// ����� �� ������ �� ������, �� �������������
	for (size_t j = 1; j < super_segment.points.size() - 1; ++j)
	{
		// ���� ����� 2 �������, ����������� �� 3 ������
		double angle = getAngleABC(super_segment.points[j - 1], super_segment.points[j], super_segment.points[j + 1]);

		double alpha;
		// ��������� ����
		update_summ_alpha(angle, alpha, sum_alpha);
		// ��� �� ���� val?
		double val = (alpha + pi) / (2 * pi);
		supersegment_val_extremum.put(val);
		// ������������� ���� � ���� ���
		std::string code = calc_gray_code(val, N_PREDICATES_ANGLES);
		// ��������� � ����-���� �������������
		super_segment_gray_code += code;
	}

	return super_segment_gray_code;
}
// ���������� ���� �� ������� ������
double getAngleABCD(cv::Point2d a, cv::Point2d b, cv::Point2d c, cv::Point2d d)
{
	cv::Point2d ab = cv::Point2d( b.x - a.x, b.y - a.y );
	cv::Point2d cd = cv::Point2d( d.x - c.x, d.y - c.y );

	double dot = (ab.x * cd.x + ab.y * cd.y); // dot product
	double cross = (ab.x * cd.y - ab.y * cd.x); // cross product

	double alpha = atan2(cross, dot);

	return alpha;
}