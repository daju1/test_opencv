#include "HashTable.h"
#include "Model.h"
// ���������� ���-�������
void HashTable::fill(std::list<SuperSegment>& super_segments)
{
	// �������� �� ������ ��������������
	std::list<SuperSegment>::iterator it_ss = super_segments.begin();
	std::list<SuperSegment>::iterator end_ss = super_segments.end();
	// ���� �� ������
	for (; it_ss != end_ss; ++it_ss)
	{
		// ���� ���������� ��������� ������ �������������, �� ������ �� ������
		if ((*it_ss).points.size() < n_points)
			continue;
		// ��������� ����
		double sum_alpha = 0.0;
		// ������ ��������
		std::string super_segment_gray_code = calc_super_segment_gray_code((*it_ss), sum_alpha);
		// ��������� �������� �������������
		(*it_ss).gray_code = super_segment_gray_code;
		// ����� ��������� �� �������
		std::map<GrayCode, std::list<SuperSegment> >::iterator found = hash_table.find(GrayCode(super_segment_gray_code, -1, -1, n_points));
		if (found != hash_table.end())
		{
			// ���� ���� ��������� ������� �� ����� ��������
			(*found).second.push_back((*it_ss));
		}
		else
		{
			// ���� ���, �� � ���-������� ��������� ����� ������ (�������, ������ ��������������)
			hash_table.insert(std::map<GrayCode, std::list<SuperSegment> >::value_type((*it_ss).getGrayCode(), std::list<SuperSegment>()));
			hash_table[(*it_ss).getGrayCode()].push_back((*it_ss));
		}
	}
}
// ���������� ������� ������������
bool HashTable::build_correspondence_table(Model * model // ������
	, const SuperSegment& scene_super_segment            // ������������
	, std::string super_segment_gray_code                // �������
	)
{
	// �������� �� ������������� � ������������ ���������
	std::map<GrayCode, std::list<SuperSegment> >::iterator found_model_super_segment = hash_table.find(GrayCode(super_segment_gray_code, -1, -1, n_points));
	
	if (found_model_super_segment == hash_table.end())
		return false;

	const std::list<SuperSegment>& model_super_segments = (*found_model_super_segment).second;
	// �������� �� ����� ��������������
	std::list<SuperSegment>::const_iterator it_super_segment = model_super_segments.begin();
	std::list<SuperSegment>::const_iterator end_super_segment = model_super_segments.end();
	// ���� �� ��������������
	for (; it_super_segment != end_super_segment; ++it_super_segment)
	{
		// ������������
		const SuperSegment& model_super_segment = (*it_super_segment);
		// � ������ ������� ������ � �������� ������������ � ������������ � ������������� ����� 
		model->correspondence_table_entry.hypotheses.push_back(
			new Hypothes(
			//  scene
			scene_super_segment, //  ������������ �����

			// model
			model_super_segment, // ������������ ������
			model                // ��������� �� ������
			)
			);

		Hypothes& hypothes = *model->correspondence_table_entry.hypotheses.back();
		// ������ ���� ����� ��������� �������������� �����������
		hypothes.calc_angle_between_model_and_scene_super_segment_directions();
		// ���������� ��������� �������������� ��������
		hypothes.calc_affine_translation();
	}
	return true;
}


void HashTable::draw_super_segment_with_corresponding_gray_code(Model * model // ������
	, const SuperSegment& scene_super_segment                                 // ������������ �����
	, std::string super_segment_gray_code                                     // �������
	, cv::Mat& input_image                                                    // �����������
	, cv::Scalar color)                                                       // ����
{
	// ����� �������������� �� ��������
	std::map<GrayCode, std::list<SuperSegment> >::iterator found = hash_table.find(GrayCode(super_segment_gray_code, -1, -1, n_points));
	if (found == hash_table.end())
		return;

	const std::list<SuperSegment>& model_super_segments = (*found).second;
	// �������� ��������� ��������������
	std::list<SuperSegment>::const_iterator it_super_segment = model_super_segments.begin();
	std::list<SuperSegment>::const_iterator end_super_segment = model_super_segments.end();
	// ���������� ������������ ������� 
	draw_super_segment(input_image, scene_super_segment, color, 1);
	// ���������� ������������� �� ������
	draw_super_segment(model->scene_drawing, scene_super_segment, CV_RGB(255, 255, 255), 5);
}