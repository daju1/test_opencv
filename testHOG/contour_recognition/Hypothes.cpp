#include "Hypothes.h"
#include <iostream>
#include "Constants.h"


// extension for n points;
//http://stackoverflow.com/questions/11237948/findhomography-getperspectivetransform-getaffinetransform
// ������ ������� ��������� �������������� � ������ 
cv::Mat getAffineTransformOverdetermined(const cv::Point2f src[], const cv::Point2f dst[], int n, double& _error)
{
	//printf("count dots=%d\n",n);
	cv::Mat M(2, 3, CV_64F), X(6, 1, CV_64F, M.data); // �������� ����������: � - ������� �������� ��������������
	double* a = (double*)malloc(12 * n*sizeof(double));
	double* b = (double*)malloc(2 * n*sizeof(double));
	cv::Mat A(2 * n, 6, CV_64F, a), B(2 * n, 1, CV_64F, b); // �������� ���������� ��� �������

	for (int i = 0; i < n; i++)
	{
		int j = i * 12;   // 2 equations (in x, y) with 6 members: skip 12 elements
		int k = i * 12 + 6; // second equation: skip extra 6 elements
		a[j] = a[k + 3] = src[i].x;
		a[j + 1] = a[k + 4] = src[i].y;
		a[j + 2] = a[k + 5] = 1;
		a[j + 3] = a[j + 4] = a[j + 5] = 0;
		a[k] = a[k + 1] = a[k + 2] = 0;

		b[i * 2] = dst[i].x;
		b[i * 2 + 1] = dst[i].y;
	}

	solve(A, B, X, cv::DECOMP_SVD);
	//solve(A, B, X, cv::DECOMP_NORMAL);

	// http://compgraphics.info/2D/affine_transform.php
	// ��� �������� �������������� ������� ���� ����� ���������� � |R|. 
	// double detR = X.at<double>(0) * X.at<double>(4) - X.at<double>(1) * X.at<double>(3);
	cv::Mat rB = A * X;
	cv::Mat err = rB - B;

	double _er = 0.0;

	double * perr = err.ptr<double>();
	for (int i = 0; i < err.rows; ++i)
	{
		_er += (perr[i] * perr[i]);
	}
	_error = sqrt(_er) / n;

	delete a;
	delete b;
	return M; // ���������� ������� ��������� ��������������
}
// �� ����������
/*
void test_getAffineTransformOverdetermined()
{
	// call original transform
	std::vector<cv::Point2f> src(3);
	std::vector<cv::Point2f> dst(3);
	src[0] = cv::Point2f(0.0, 0.0); src[1] = cv::Point2f(1.0, 0.0); src[2] = cv::Point2f(0.0, 1.0);
	dst[0] = cv::Point2f(0.0, 0.0); dst[1] = cv::Point2f(1.0, 0.0); dst[2] = cv::Point2f(0.0, 1.0);
	cv::Mat M = cv::getAffineTransform(cv::Mat(src), cv::Mat(dst));
	std::cout << M << std::endl;
	// call new transform
	src.resize(4); src[3] = cv::Point2f(22, 2);
	dst.resize(4); dst[3] = cv::Point2f(22, 2);
    //	cv::Mat M2 = getAffineTransformOverdetermined(src.data(), dst.data(), src.size());
    //	std::cout << M2 << std::endl;
}
*/
// ���� ����� ��������������� ��������� �������������� ����� � ������
void Hypothes::calc_angle_between_model_and_scene_super_segment_directions()
{
	// ��������������� ������ ����� � ������������� � �������� ������ � �����
	angle_between_model_and_scene_super_segment_directions
		= 180 * getAngleABCD(
		scene_super_segment.points[1], scene_super_segment.points[scene_super_segment.points.size() - 2],
		model_super_segment.points[1], model_super_segment.points[model_super_segment.points.size() - 2]) / pi;
}
// ����������� �������� ��������������
// ������� �������� �������������� ����� ������������� ����� � ������ 
void Hypothes::calc_affine_translation()
{
	affine_matrix = getAffineTransformOverdetermined(
		&model_super_segment.points[0]
		, &scene_super_segment.points[0]
		, model_super_segment.points.size()
		, error_of_overdetermined_get_affine_matrix
	);
	// ����������� ������ ��������������
	determinant_R = affine_matrix.at<double>(0) * affine_matrix.at<double>(4) - affine_matrix.at<double>(1) * affine_matrix.at<double>(3);
}

// ���������� ������ �������
bool Hypothes::isBad() const
{
	// ���������� ������ �������
	// ���� �������� ����������� ��������� �����
	// �.�. �������������� ������� ���������� ���������� ��������
	// �� ����� �������� ���������� 
	if (fabs(determinant_R) > MAX_HYPOTHES_DETERMINANT_R)
		return true;
	// ���� ������ ��������� �������������� ���� ������ - ���� �����
	if (error_of_overdetermined_get_affine_matrix > MAX_HYPOTHES_ERROR)
		return true;

	return false;
}
