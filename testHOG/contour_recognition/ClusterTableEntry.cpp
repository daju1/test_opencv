#include "ClusterTableEntry.h"

Clusterisator clusterisator;



void ClusterTableEntry::Clusterisation(int ndims
	, const Hypothes& hypothes
	, const std::vector<Extremum>& extremums)
{
	// ���������� ������ �������
	if (hypothes.isBad())
		return;

	// ������� ��������� �������������� ��������
	const cv::Mat& affine_matrix = hypothes.affine_matrix;

	ClPoint* cluster_point = new ClPoint(ndims, &hypothes);

	//cluster_point->crd[0] = angle_between_model_and_scene_super_segment_directions;
	bool to_print = false;
	if (fabs(affine_matrix.at<double>(0)) > 10)
		to_print = true;

	if (to_print)
		printf("crd ");


	for (int i = 0; i < 6; ++i)
	{
		// ���������� �����������
		cluster_point->crd[i] = extremums[i].normal(affine_matrix.at<double>(i));
		//cluster_point->crd[i] = affine_matrix.at<double>(i) / abs(hypothes.determinant_R);

		if (to_print)
			printf("%f ", cluster_point->crd[i]);
	}

	if (to_print)
	{
		printf(" affine ");
		for (int i = 0; i < 6; ++i)
		{
			printf("%f ", affine_matrix.at<double>(i));
		}

		// ������ ������������������
		printf(" error %f ",
			hypothes.error_of_overdetermined_get_affine_matrix);
		// �����������
		printf(" det_R %f ",
			hypothes.determinant_R);
		// ?? ��� ���������� ������������� ������������� � ������������� ����������� ������ { } ��� ������, ������� � �.�.
		// � ������ ������ ������ ��������� ���������
		// �� ������ ��� ������ ���������� ������, ����� ������ ������ ������ ������� ����������� ���������� � �������� � �� ������� ����������� ������ ����������
		// ��� ���� ����������� ������ ������ ���������� �� ������ �� ������ �����������
		{
			printf("scene ");
			hypothes.scene_super_segment.print();
			printf("model ");
			hypothes.model_super_segment.print();
		}
		printf("\n");
	}

	cluster_points.push_back(cluster_point);
}

void ClusterTableEntry::erase_cluster_point_from_cluster(ClPoint* pt)
{
	std::list<Cluster*>::iterator it_cluster = this->clusters.begin();
	std::list<Cluster*>::iterator end_cluster = this->clusters.end();

	for (; it_cluster != end_cluster; ++it_cluster)
	{
		std::list<struct ClPoint*>::iterator it = (*it_cluster)->points.begin();
		std::list<struct ClPoint*>::iterator end = (*it_cluster)->points.end();
		for (; it != end; ++it)
		{
			if ((*it) == pt)
			{
				(*it_cluster)->points.erase(it);
				return;
			}
		}

	}
}

void ClusterTableEntry::Clusterisation(double max_error
	, double max_sigma_angle
	, double learning_step)
{
	clusterisator.init_hists();
	// �������� �� ������ ��������
	std::list<ClPoint*>::iterator it_cluster_point = cluster_points.begin();
	std::list<ClPoint*>::iterator end_cluster_point = cluster_points.end();
	// ���� �� ������
	// TODO  �� ����� ���� �������� ����� �������� ��������� ������ ������� ����� ��������,
	// http://neuronus.com/nn/38-theory/961-nejronnye-seti-kokhonena.html
	for (; it_cluster_point != end_cluster_point; ++it_cluster_point)
	{
		ClPoint* pt = (*it_cluster_point);
		erase_cluster_point_from_cluster(pt);

		bool calc_all_clasters_dispersion;
		clusterisator.process(
			(*it_cluster_point),
			cluster_points,
			clusters,
			max_error, max_sigma_angle,
			learning_step,
			calc_all_clasters_dispersion);
#if 0
		if (calc_all_clasters_dispersion)
		{
			clusterisator.zero_all_clusters_dispersion();
			clusterisator.post_process(clusters);
			clusterisator.norm_all_clusters_dispersion();
		}
#endif
	}
}
// ������������� ��������������
void initClusterisator()
{
	bool use_predefined_clustering_dispersion = true;
	clusterisator.init(6);
}
