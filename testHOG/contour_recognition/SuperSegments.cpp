#include "SuperSegments.h"
#include "Model.h"
#include "HashTable.h"

void draw_super_segments(cv::Mat& drawing, std::list<SuperSegments*>& super_segments, int thickness)
{
	std::list<SuperSegments*>::iterator it = super_segments.begin();
	std::list<SuperSegments*>::iterator end = super_segments.end();

	for (; it != end; ++it)
	{
		draw_super_segments(drawing, (*it)->super_segments_list, thickness);
	}
}

void build_correspondence_table(std::vector<Model*>& models
	, const SuperSegment& scene_super_segment
	, std::string super_segment_gray_code
	, int n_points
	)
{
	std::vector<Model*>::iterator it_model = models.begin();
	std::vector<Model*>::iterator end_model = models.end();
	for (; it_model != end_model; ++it_model)
	{
		Model * model = (*it_model);
		model->hash_tables[n_points]->build_correspondence_table(model
			, scene_super_segment
			, super_segment_gray_code);
	}
}



void draw_super_segment_with_corresponding_gray_code(std::vector<Model*>& models
	, const SuperSegment& scene_super_segment
	, std::string super_segment_gray_code
	, cv::Mat& input_image
	, int n_points
	)
{
	cv::RNG rng;

	std::vector<Model*>::iterator it_model = models.begin();
	std::vector<Model*>::iterator end_model = models.end();
	for (; it_model != end_model; ++it_model)
	{
		cv::Scalar color = cv::Scalar(rng.uniform(0, 255), rng.uniform(0, 255), rng.uniform(0, 255));

		(*it_model)->hash_tables[n_points]->draw_super_segment_with_corresponding_gray_code((*it_model)
			, scene_super_segment
			, super_segment_gray_code
			, input_image
			, color);
	}
}


void SuperSegments::build_correspondence_table(std::vector<Model*>& models
	, cv::Mat& drawing2)
{
	std::list<SuperSegment>::iterator it_ss = super_segments_list.begin();
	std::list<SuperSegment>::iterator end_ss = super_segments_list.end();
	for (; it_ss != end_ss; ++it_ss)
	{
		if ((*it_ss).points.size() < n_points)
			continue;

		double sum_alpha = 0.0;
		std::string super_segment_gray_code = calc_super_segment_gray_code((*it_ss), sum_alpha);
		(*it_ss).gray_code = super_segment_gray_code;

		draw_super_segment_with_corresponding_gray_code(models
			, (*it_ss)
			, super_segment_gray_code
			, drawing2
			, n_points);

		::build_correspondence_table(models
			, (*it_ss)
			, super_segment_gray_code
			, n_points);
	}
}
