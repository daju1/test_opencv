#include "ClusterItem.h"
#include "Model.h"
#include <set>
// ������ ����������� �������� ��� �����������, ���������� ���������
int ClusterItem::cluster_items_index = 0;
// ��������� ��������������� ���������� ���������
extern std::set<ClusterItem> cluster_items;
extern cv::Mat g_scene_image;


ClusterItem::ClusterItem(Model* model_
	, Cluster * cluster_)
		: model(model_)
		, cluster(cluster_)
{
	value = cluster->_error / (cluster->modelSupersegmentsShapeRectRelation);
}
// �������� ��������� ��������� ��������
// ��������������� ������� ����������
bool operator< (const ClusterItem& item1, const ClusterItem& item2)
{
	// ����������� ������ ��������
	size_t sz1 = item1.cluster->get_effective_cluster_size_merge();
	size_t sz2 = item2.cluster->get_effective_cluster_size_merge();

	if (sz1 != sz2)
		return sz1 > sz2;

	return item1.value < item2.value;
}

void draw_by_cluster_items_index()
{
	cv::Mat scene_image = g_scene_image.clone();
	// �������� �� ��������� ���������
	std::set<ClusterItem>::iterator it = cluster_items.begin();
	std::set<ClusterItem>::iterator end = cluster_items.end();

	for (int i = 0; it != end; ++it, ++i)
	{
		if (i != ClusterItem::cluster_items_index)
			continue;

		// (*it).model->destroyOtherModelsWindows();
		// ��������� ����������� ������
		cv::Mat input_image = (*it).model->image.clone();
		// ��������� ����������� �����
		cv::Mat scene_drawing = (*it).model->scene_drawing.clone();
		// ��������� ��������� �����
		cv::RNG rng;

		my_cluster_point_data cluster_point_data;
		cluster_point_data.rng = &rng;
		// ������� ����������� ��� ���������
		cluster_point_data.input_image = &input_image;
		cluster_point_data.scene_drawing = &scene_drawing;


		my_cluster_data cluster_data;
		cluster_data.model = (*it).model;
		cluster_data.scene_image = &scene_image;
		cluster_data.input_image = &input_image;

		my_cluster_callback_fun((*it).cluster, &cluster_data, i);

		ClusterTableEntry::enumClusterPoints((*it).cluster, my_cluster_point_callback_fun, &cluster_point_data);
		// ���������� ��������
		std::string winname = (*it).model->get_super_segment_with_corresponding_gray_code_winname();

		cv::imshow("scene_image", scene_image);
		cv::imshow("scene_drawing", scene_drawing);
		cv::imshow("model_image", input_image);

		break;
	}
}
// ���������� ������� ���������
void update_cluster_items_index(int k, void* p)
{
	ClusterItem::cluster_items_index = k;
	draw_by_cluster_items_index();
}

void my_cluster_callback_fun(Cluster * cluster, void * p, int i)
{
	my_cluster_data * data = (my_cluster_data *)p;

	CvScalar color = CV_RGB(0, 255, 0);
	// ���������� ������ �� ����� � ������������ � �������������� cluster->m ������ color
	data->model->draw_on_scene_with_translation(cluster->m, *data->scene_image, color);
	// ����
	color = CV_RGB(255, 0, 0);

	char str[1024];
	sprintf_s(str, 1024, "error = %f", cluster->_error);

	// ��������� ������
	int fontFace = 1;
	double fontScale = 0.9;
	int thickness = 1;

	int baseline;
	cv::Size text_size = cv::getTextSize(str, fontFace, fontScale, thickness, &baseline);

	cv::Point org = cvPoint(10, 10 + text_size.height);
	// ������� ������
	cv::putText(*data->scene_image, str, org, fontFace, fontScale, color, thickness);

	sprintf_s(str, 1024, "determinant_R = %f", cluster->determinant_R);
	org.y += 10 + text_size.height;
	cv::putText(*data->scene_image, str, org, fontFace, fontScale, color, thickness);

#if CALC_CLUSTER_DISPERSION
	{
		std::stringstream stream;
		stream << "disp ";
		for (int i = 0; i < cluster->dispersions.size(); ++i)
			stream << cluster->dispersions[i] << " ";

		org.y += 10 + text_size.height;
		cv::putText(*data->scene_image, stream.str(), org, fontFace, fontScale, color, thickness);
	}
#endif

	{
		std::stringstream stream;
		for (size_t i = 0; i < cluster->center.size() / 2; ++i)
			stream << cluster->center[i] << " ";

		org.y += 10 + text_size.height;
		cv::putText(*data->scene_image, stream.str(), org, fontFace, fontScale, color, thickness);
	}

	{
		std::stringstream stream;
		for (size_t i = cluster->center.size() / 2; i < cluster->center.size(); ++i)
			stream << cluster->center[i] << " ";

		org.y += 10 + text_size.height;
		cv::putText(*data->scene_image, stream.str(), org, fontFace, fontScale, color, thickness);
	}

	sprintf_s(str, 1024, "mean_angle = %f sigma_angle = %f", cluster->mean_angle, cluster->sigma_angle);
	org.y += 10 + text_size.height;
	cv::putText(*data->scene_image, str, org, fontFace, fontScale, color, thickness);

	sprintf_s(str, 1024, "modelSupersegmentsShapeRectRelation = %f", cluster->modelSupersegmentsShapeRectRelation);
	org.y += 10 + text_size.height;
	cv::putText(*data->scene_image, str, org, fontFace, fontScale, color, thickness);

	sprintf_s(str, 1024, "shear_angles = %f %f", cluster->shear_angle_x, cluster->shear_angle_y);
	org.y += 10 + text_size.height;
	cv::putText(*data->scene_image, str, org, fontFace, fontScale, color, thickness);

	sprintf_s(str, 1024, "A B = %f %f", cluster->A, cluster->B);
	org.y += 10 + text_size.height;
	cv::putText(*data->scene_image, str, org, fontFace, fontScale, color, thickness);

	sprintf_s(str, 1024, "W H = %f %f", cluster->W, cluster->H);
	org.y += 10 + text_size.height;
	cv::putText(*data->scene_image, str, org, fontFace, fontScale, color, thickness);

	sprintf_s(str, 1024, "cluster_points_size = %d", cluster->points.size());
	org.y += 10 + text_size.height;
	cv::putText(*data->scene_image, str, org, fontFace, fontScale, color, thickness);

	sprintf_s(str, 1024, "effective_cluster_size = %s", cluster->get_effective_cluster_size_str().c_str());
	org.y += 10 + text_size.height;
	cv::putText(*data->scene_image, str, org, fontFace, fontScale, color, thickness);

	sprintf_s(str, 1024, "effective_cluster_size_merge = %d", cluster->get_effective_cluster_size_merge());
	org.y += 10 + text_size.height;
	cv::putText(*data->scene_image, str, org, fontFace, fontScale, color, thickness);


	std::list<struct ClPoint*>::iterator it_point = cluster->points.begin();
	std::list<struct ClPoint*>::iterator end_point = cluster->points.end();

	for (; it_point != end_point; ++it_point)
	{
		// ��������� ���������
		sprintf_s(str, 1024, 
			"c %2d st %2d sz %2d sc %d %d"
			"er %3.6f det %0.6f angl %3.6f "
			, (*it_point)->hypothes->model_super_segment.i_contour
			, (*it_point)->hypothes->model_super_segment.start_line_index
			, (*it_point)->hypothes->model_super_segment.points.size()
			, (*it_point)->hypothes->model_super_segment.i_scale
			, (*it_point)->hypothes->scene_super_segment.i_scale
			, (*it_point)->hypothes->error_of_overdetermined_get_affine_matrix
			, (*it_point)->hypothes->determinant_R
			, (*it_point)->hypothes->angle_between_model_and_scene_super_segment_directions
			);

		org.y += 10 + text_size.height;
		cv::putText(*data->scene_image, str, org, fontFace, fontScale, color, thickness);
	}
	// ������������� �� ������, � ������� ������������� ����������
	cv::rectangle(*data->input_image, cluster->modelSupersegmentsShapeRect, CV_RGB(255, 0, 0));
}

void my_cluster_point_callback_fun(ClPoint * point, void * p, int i_point)
{
	my_cluster_point_data * data = (my_cluster_point_data *)p;
	cv::Scalar color = cv::Scalar(data->rng->uniform(0, 255), data->rng->uniform(0, 255), data->rng->uniform(0, 255));

	//int thickness = std::min<int>(10, (point->cluster->points.size() - i_point));
	int thickness = 3;
	draw_super_segment(*data->scene_drawing, point->hypothes->scene_super_segment, color, thickness, true);
	draw_super_segment(*data->input_image, point->hypothes->model_super_segment, color, thickness, true);
}
