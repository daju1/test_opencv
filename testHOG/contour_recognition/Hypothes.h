#pragma once

#include "SuperSegment.h"


class Model;
struct Hypothes
{
	//  scene
	SuperSegment scene_super_segment;//  ������������ �����
	

	// model
	SuperSegment model_super_segment;// ������������ ������
	Model * model;                   // ������
	// �����������
	Hypothes(
		//  scene
		const SuperSegment& _scene_super_segment, // ������������ �����

		// model
		const SuperSegment& _model_super_segment, // ������������ ������
		Model * _model
		)
		: scene_super_segment(_scene_super_segment)
		, model_super_segment(_model_super_segment)

	{
		this->model = _model;
	}

	// �����������
	Hypothes(const Hypothes& h)
		: scene_super_segment(h.scene_super_segment) // ������������ �����
		, model_super_segment(h.model_super_segment) // ������������ ������
		, model(h.model)                             // ������

		, affine_matrix(h.affine_matrix)             // ������� ��������� ��������������
		// ���� ����� ��������������� ���������
		, angle_between_model_and_scene_super_segment_directions(h.angle_between_model_and_scene_super_segment_directions)
		, determinant_R(h.determinant_R)             // ����������� ������� ��������� ��������������
		// ������ ������������������ �������
		, error_of_overdetermined_get_affine_matrix(h.error_of_overdetermined_get_affine_matrix)
	{
	}


	// ���� ����� ������������ ������������� ����� � ������
	double angle_between_model_and_scene_super_segment_directions;
	// ������� ��������� ��������������
	cv::Mat affine_matrix;
	// ����� ������������ ������� R: ��� �������� �������������� ������� ���� ����� ���������� � |R|.
	double determinant_R;
	// ������ ������������������ ������� ��������� ��������������
	double error_of_overdetermined_get_affine_matrix;
	// ���������� ���� ����� ������������� ������������� � ������
	void calc_angle_between_model_and_scene_super_segment_directions();
	// ���������� ������� ��������� ��������������
	void calc_affine_translation();
	// ���������� ������ �������
	bool isBad() const;

};

cv::Mat getAffineTransformOverdetermined(const cv::Point2f src[], const cv::Point2f dst[], int n, double& _error);

