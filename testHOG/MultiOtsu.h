#include <opencv/cv.h>
#include <opencv/highgui.h>

namespace cv
{

	CV_EXPORTS_W void MultiOtsu(InputArray _src, OutputArray _dst, int mlevel);

	CV_EXPORTS_W void MultiOtsuEdges(InputArray _src, OutputArray _dst, int mlevel);

	CV_EXPORTS_W void MultiOtsu2(InputArray _src, OutputArray _dst, OutputArray _thresholds, int mlevel);

	CV_EXPORTS_W void MultiOtsuThresholds(InputArray _src, OutputArray _thresholds, int mlevel);
}
