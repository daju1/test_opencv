#include <iostream>
#include <vector>
#include <string>
#include <algorithm>

#include <mapnik/simplify.hpp>
#include <mapnik/simplify_converter.hpp>
#include <mapnik/vertex_adapters.hpp>


// stl
#include <stdexcept>

// Convenience method for test cases
void simplify(std::string const& wkt_in, double tolerance, std::string const& method, std::string const& expected)
{
    mapnik::geometry::line_string<double> line;
    line.push_back(mapnik::geometry::point<double>(0, 0));
    line.push_back(mapnik::geometry::point<double>(2, 2));
    line.push_back(mapnik::geometry::point<double>(3, 5));
    line.push_back(mapnik::geometry::point<double>(4, 1));
    line.push_back(mapnik::geometry::point<double>(5, 0));
    line.push_back(mapnik::geometry::point<double>(6, 7));
    line.push_back(mapnik::geometry::point<double>(7, 0));

    mapnik::geometry::line_string_vertex_adapter<double> va(line);
    mapnik::simplify_converter<mapnik::geometry::line_string_vertex_adapter<double> > generalizer(va);
    generalizer.set_simplify_algorithm(mapnik::simplify_algorithm_from_string(method).get());
    generalizer.set_simplify_tolerance(tolerance);

    //suck the vertices back out of it
    mapnik::CommandType cmd;
    double x, y;
    while ((cmd = (mapnik::CommandType)generalizer.vertex(&x, &y)) != mapnik::SEG_END)
    {
        printf("x=%f, y=%f, cmd=%u\n", x, y, cmd);
    }
}

void simplify_converter_test()
{
    simplify(   std::string("LineString(0 0,2 2,3 5,4 1,5 0,6 7,7 0)"),
                4, "douglas-peucker",
                std::string("LineString(0 0,6 7,7 0)"));

    simplify(   std::string("LineString(0 0,2 2,3 5,4 1,5 0,6 7,7 0)"),
                4, "douglas-peucker-relative-segment-distance",
                std::string("LineString(0 0,6 7,7 0)"));

    simplify(   std::string("LineString(0 0,2 2,3 5,4 1,5 0,6 7,7 0)"),
                4, "visvalingam-whyatt",
                std::string("LineString(0 0,6 7,7 0)"));

    simplify(   std::string("LineString(0 0,2 2,3 5,4 1,5 0,6 7,7 0)"),
                4, "visvalingam-whyatt-relative-effective-area",
                std::string("LineString(0 0,6 7,7 0)"));

    simplify(   std::string("LineString(0 0,2 2,3 5,4 1,5 0,6 7,7 0)"),
                2, "douglas-peucker",
                std::string("LineString(0 0,3 5,5 0,6 7,7 0)"));

    simplify(   std::string("LineString(10 0,9 -4,7 -7,4 -9,0 -10,-4 -9,-7 -7,-9 -4,-10 0,-9 4,-7 7,-4 9,0 10,4 9,7 7,9 4)"),
                4, "douglas-peucker",
                std::string("LineString(10 0,0 -10,-10 0,0 10,9 4)"));

    simplify(   std::string("LineString(0 0,1 1,2 2,0 10,0 0)"),
                10, "douglas-peucker",
                std::string("LineString(0 0,0 0)"));

    simplify(   std::string("LineString(0 0,1 1,2 2,0 10,0 0)"),
                8, "douglas-peucker",
                std::string("LineString(0 0,0 10,0 0)"));

    simplify(   std::string("LineString(0 0,1 1,2 2,0 10,0 0)"),
                1, "douglas-peucker",
                std::string("LineString(0 0,2 2,0 10,0 0)"));

    simplify(   std::string("LineString(0 0, 1 -1, 2 2, 0 -10, 0 0, -5 7, 4 6)"),
                3, "douglas-peucker",
                std::string("LineString(0 0,0 -10,-5 7,4 6)"));
}
