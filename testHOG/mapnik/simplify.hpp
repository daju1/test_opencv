#ifndef MAPNIK_SIMPLIFY_HPP
#define MAPNIK_SIMPLIFY_HPP

// mapnik
#include <mapnik/config.hpp>

// stl
#include <string>

// boost
#include <boost/optional.hpp>

namespace mapnik
{

enum simplify_algorithm_e
{
    radial_distance = 0,
    douglas_peucker,
    douglas_peucker_relative_segment_distance,
    visvalingam_whyatt,
    visvalingam_whyatt_relative_effective_area,
    visvalingam_whyatt_vertex_angle,
    zhao_saalfeld
};

MAPNIK_DECL boost::optional<simplify_algorithm_e> simplify_algorithm_from_string(std::string const& name);
MAPNIK_DECL boost::optional<std::string> simplify_algorithm_to_string(simplify_algorithm_e algorithm);

}

#endif // MAPNIK_SIMPLIFY_HPP
