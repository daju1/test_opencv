#include <mapnik/simplify_converter.hpp>

namespace mapnik
{
	double getAngleABC(vertex2d a, vertex2d b, vertex2d c)
	{
		vertex2d ab; ab.x = b.x - a.x; ab.y = b.y - a.y;
		vertex2d cb; cb.x = b.x - c.x; cb.y = b.y - c.y;

		double dot = (ab.x * cb.x + ab.y * cb.y); // dot product
		double cross = (ab.x * cb.y - ab.y * cb.x); // cross product

		double alpha = atan2(cross, dot);

		return alpha;
	}
}