#include "TrainModelInfo.h"

class TrainModelLoader
{
public:
	TrainModelLoader(int mk = -1);
	virtual ~TrainModelLoader();

	void load(const std::string& markupFile, const std::string& imageFile);
	void clear();
	int getModelsCount();
	std::vector<TrainModelInfo> getModels();
	void useGrayImages(bool f);
	bool isGrayImages();

private:
	Vertices2DList loadMarkup(const std::string& markupFile);

private:
	std::vector<TrainModelInfo> models;
	int marksCount;
	bool grayFlag;
};

void initAamToolboxPath();

bool loadModelInfo(const std::string& fileName,
	std::vector<ModelPathType>& modelPaths);