#include "TrainModelLoader.h"

#include <opencv/cv.h>
#include <opencv/highgui.h>
#include "opencv2/imgproc/imgproc.hpp"
#if HAVE_OPENCV_300
#include "opencv2/imgcodecs/imgcodecs.hpp"
#endif
#include <fstream>
#include <iostream>
#include <stdio.h>



std::string AamToolboxPath;
void initAamToolboxPath()
{
	const char * aam_toolbox_path = getenv("AAM_TOOLBOX_PATH");
	if (!aam_toolbox_path)
	{
		printf("Error: you must set enveroment variable AAM_TOOLBOX_PATH");
		return;
	}

	AamToolboxPath = aam_toolbox_path;
	if (!(AamToolboxPath.back() == '/' || AamToolboxPath.back() == '\\'))
	{
		AamToolboxPath += '/';
	}
}


void de_quote(std::string& part)
{
	int i1_quote = part.find_first_of('\"');
	if (i1_quote > -1)
		part = part.substr(i1_quote + 1);

	int i2_quote = part.find_last_of('\"');
	if (i2_quote > -1)
		part = part.substr(0, i2_quote);
}

bool loadModelInfo(const std::string& fileName,
	std::vector<ModelPathType>& modelPaths)
{
	std::ifstream stream(fileName.c_str());

	if (!stream)
	{
		return false;
	}

	while (!stream.eof())
	{
		std::string line;
		std::getline(stream, line);

		if (!line.empty())
		{
			bool result = false;
			int icomma = line.find_first_of(',');
			if (icomma < 0)
				continue;

			std::string part1 = line.substr(0, icomma);
			de_quote(part1);

			std::string part2 = line.substr(icomma + 1);
			de_quote(part2);

			{
				ModelPathType modelPath(part1, part2);
				modelPaths.push_back(modelPath);
			}

		}
	}

	stream.close();

	return true;
}


TrainModelLoader::TrainModelLoader(int mk)
: marksCount(mk)
, grayFlag(true)
{
}

TrainModelLoader::~TrainModelLoader()
{
}

void TrainModelLoader::load(const std::string& markupFile,
	const std::string& imageFile)
{
	TrainModelInfo model;

	cv::Mat img = cv::imread(AamToolboxPath + imageFile);

	if (img.empty())
	{
		std::cout << "Not found image: " << imageFile << std::endl;
		return;
	}

	model.setImage(img, this->grayFlag);

	Vertices2DList markup = loadMarkup(AamToolboxPath + markupFile);
	model.setVertices(markup);

	this->models.push_back(model);
}

Vertices2DList TrainModelLoader::loadMarkup(
	const std::string& markupFile)
{
	Vertices2DList result;
	std::ifstream stream(markupFile.c_str());

	if (!stream)
	{
		throw /*FileNotFoundError()*/;
	}

	while (!stream.eof())
	{
		std::string line;
		std::getline(stream, line);

		if (!line.empty())
		{
			std::istringstream stringStream(line);
			PointInfo info;

			stringStream >> info.pointNumber >> info.point.x >>
				info.point.y;
			result.push_back(info.point);
		}
	}

	if (result.size() != this->marksCount && this->marksCount != -1)
	{
		throw /*InvalidMarkupFile()*/;
	}

	return result;
}

int TrainModelLoader::getModelsCount()
{
	return this->models.size();
}

std::vector<TrainModelInfo> TrainModelLoader::getModels()
{
	return this->models;
}

void TrainModelLoader::useGrayImages(bool f)
{
	this->grayFlag = f;
}

bool TrainModelLoader::isGrayImages()
{
	return this->grayFlag;
}
