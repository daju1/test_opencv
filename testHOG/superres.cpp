
#include <vector>
#include <list>
#include <limits>
#if !HAVE_OPENCV_300

#include "opencv2/opencv_modules.hpp"
#include "opencv2/opencv_modules.hpp"
#include "opencv2/core/core.hpp"
#include "opencv2/imgproc/imgproc.hpp"

#include "opencv2/ts/ts.hpp"
#include "opencv2/superres/superres.hpp"
#include "opencv2/ts/gpu_perf.hpp"

#else

#include "opencv2/core.hpp"
#include "opencv2/core/cuda.hpp"
//#include "opencv2/ts.hpp"
//#include "opencv2/ts/cuda_perf.hpp"
#include "opencv2/superres.hpp"

#endif

#include "opencv2/superres/optical_flow.hpp"
#include "opencv2/highgui/highgui.hpp"
#include "opencv2/objdetect/objdetect.hpp"
#include "opencv2/imgproc/imgproc.hpp"

using namespace cv;
using namespace cv::superres;

#if !HAVE_OPENCV_300
void test_superres()
{
	//createSuperResolution_BTVL1_GPU();
	Ptr<SuperResolution> srobj = createSuperResolution_BTVL1();
	Ptr<DenseOpticalFlowExt> opflow = createOptFlow_Farneback();

	srobj->set("scale", 6);
	srobj->set("opticalFlow", opflow);

	//Ptr<FrameSource> frameSource = createFrameSource_Video("Z:/VID_20150511_180029.3gp");
	//Ptr<FrameSource> frameSource = createFrameSource_Video("Z:/VID_20150511_180655.3gp");
	//Ptr<FrameSource> frameSource = createFrameSource_Video("Z:/VID_20150511_182042.3gp");
	//Ptr<FrameSource> frameSource = createFrameSource_Video("Z:/Camera/VID_20150522_121331.3gp");
	//Ptr<FrameSource> frameSource = createFrameSource_Video("Z:/Camera/VID_20150522_131323.3gp");
	Ptr<FrameSource> frameSource = createFrameSource_Camera(0);


	
	srobj->setInput(frameSource);

	Mat frame_super;

	int mode = 0;

	int iframe = 1;

	for (;;)
	{
		if (1 == mode)
			srobj->nextFrame(frame_super);
		else
			frameSource->nextFrame(frame_super);
		imshow("output", frame_super);

		int ch = cv::waitKey(0);
		//printf("%d", ch);

		if ('s' == ch)
		{
			char str[1024];
			//sprintf(str, "Z:/VID_20150511_180029_%03d.jpg", iframe);
			//sprintf(str, "Z:/VID_20150511_180655_%03d.jpg", iframe);
			//sprintf(str, "Z:/VID_20150511_182042_%03d.jpg", iframe);
			//sprintf(str, "Z:/VID_20150522_121331_%03d.jpg", iframe);
			sprintf(str, "Z:/VID_20150522_131323_%03d.jpg", iframe);
			cv::imwrite(str, frame_super);

			printf("Frame %s saved\n", str);
		}

		if ('r' == ch)
		{
			mode = 1;
		}		
		
		if ('e' == ch)
		{
			mode = 0;
		}

		//waitKey(1);

		++iframe;
	}
}

#else

namespace
{
    class OneFrameSource_CPU : public FrameSource
    {
    public:
        explicit OneFrameSource_CPU(const Mat& frame) : frame_(frame) {}

        void nextFrame(OutputArray frame)
        {
            frame.getMatRef() = frame_;
        }

        void reset()
        {
        }

    private:
        Mat frame_;
    };

#if 0
    class OneFrameSource_CUDA : public FrameSource
    {
    public:
        explicit OneFrameSource_CUDA(const GpuMat& frame) : frame_(frame) {}

        void nextFrame(OutputArray frame)
        {
            frame.getGpuMatRef() = frame_;
        }

        void reset()
        {
        }

    private:
        GpuMat frame_;
    };
#endif

    class ZeroOpticalFlow : public DenseOpticalFlowExt
    {
    public:
        virtual void calc(InputArray frame0, InputArray, OutputArray flow1, OutputArray flow2)
        {
            cv::Size size = frame0.size();

            if (!flow2.needed())
            {
                flow1.create(size, CV_32FC2);
                flow1.setTo(cv::Scalar::all(0));
            }
            else
            {
                flow1.create(size, CV_32FC1);
                flow2.create(size, CV_32FC1);

                flow1.setTo(cv::Scalar::all(0));
                flow2.setTo(cv::Scalar::all(0));
            }
        }

        virtual void collectGarbage()
        {
        }
    };
}

static const std::string NAME = "../camera.avi";
static const std::string NAME2 = "../camera2.avi";

void write_video()
{
	VideoCapture inputVideo(0);              // Open input
	if (!inputVideo.isOpened())
	{
		printf("Could not open the input video:\n");;
		return;
	}

	{
		int ex = static_cast<int>(inputVideo.get(CV_CAP_PROP_FOURCC));     // Get Codec Type- Int form
		char EXT[] = { (char)(ex & 0XFF), (char)((ex & 0XFF00) >> 8), (char)((ex & 0XFF0000) >> 16), (char)((ex & 0XFF000000) >> 24), 0 };
		printf("EXT = %s ex=%d\n", EXT, ex);
	}

	Size sz = Size((int)inputVideo.get(CV_CAP_PROP_FRAME_WIDTH),    // Acquire input size
		(int)inputVideo.get(CV_CAP_PROP_FRAME_HEIGHT));

	VideoWriter outputVideo;                                        // Open the output
	//outputVideo.open(NAME, ex = -1, inputVideo.get(CV_CAP_PROP_FPS), sz, true);
	outputVideo.open(NAME, 0, inputVideo.get(CV_CAP_PROP_FPS), sz, true);

	if (!outputVideo.isOpened())
	{
		printf("Could not open the output video for write:\n");
		return;
	}

	Mat src;

	bool to_write = false;

	for (;;) //Show the image captured in the window and repeat
	{
		inputVideo >> src;              // read
		if (src.empty())
			break;

		cv::imshow("src", src);
		int ch = cv::waitKey(1);

		if (27 == ch)
			break;

		if ('w' == ch)
			to_write = true;

		if (to_write)
			outputVideo << src;
	}

	outputVideo.release();

	printf("Finished writing\n");
}


void write_video2()
{
	VideoCapture inputVideo(NAME);              // Open input
	if (!inputVideo.isOpened())
	{
		printf("Could not open the input video:\n");;
		return;
	}

	{
		int ex = static_cast<int>(inputVideo.get(CV_CAP_PROP_FOURCC));     // Get Codec Type- Int form
		char EXT[] = { (char)(ex & 0XFF), (char)((ex & 0XFF00) >> 8), (char)((ex & 0XFF0000) >> 16), (char)((ex & 0XFF000000) >> 24), 0 };
		printf("EXT = %s ex=%d\n", EXT, ex);
	}

	Size sz = Size((int)inputVideo.get(CV_CAP_PROP_FRAME_WIDTH),    // Acquire input size
		(int)inputVideo.get(CV_CAP_PROP_FRAME_HEIGHT));

	VideoWriter outputVideo;                                        // Open the output
	outputVideo.open(NAME2, 0, inputVideo.get(CV_CAP_PROP_FPS), sz, true);

	if (!outputVideo.isOpened())
	{
		printf("Could not open the output video for write:\n");
		return;
	}

	Mat src;


	for (;;) //Show the image captured in the window and repeat
	{
		inputVideo >> src;              // read
		if (src.empty())
			break;

		cv::imshow("src", src);
		int ch = cv::waitKey(0);

		if (27 == ch)
			break;

		outputVideo << src;
	}

	outputVideo.release();

	printf("Finished writing\n");
}

static String lefteye_cascade_name = "haarcascade_mcs_lefteye.xml";
static String righteye_cascade_name = "haarcascade_mcs_righteye.xml";

static String eyes_cascade_name = "haarcascade_eye_tree_eyeglasses.xml";

static CascadeClassifier lefteye_cascade;
static CascadeClassifier righteye_cascade;
static CascadeClassifier eyes_cascade;
static std::string window_name = "Capture - Face detection";

void load_cascadeclassifier()
{
	const char * opencv_sources = getenv("OPENCV_SOURCES");
	if (!opencv_sources)
	{
		printf("Set eviroment variable OPENCV_SOURCES\n");
		return;
	}
	String directory = String(opencv_sources) + "/data/haarcascades/";

	//-- 1. Load the cascades
	if (!lefteye_cascade.load(directory + lefteye_cascade_name))
	{
		printf("--(!)Error loading\n");
		return;
	}

	if (!righteye_cascade.load(directory + righteye_cascade_name))
	{
		printf("--(!)Error loading\n");
		return;
	}

	if (!eyes_cascade.load(directory + eyes_cascade_name))
	{
		printf("--(!)Error loading\n");
		return;
	}
}


bool detect_eyes(Mat frame, std::list<std::vector<Rect> >& eyes_list)
{
	std::vector<Rect> faces;
	Mat frame_gray;

	cv::cvtColor(frame, frame_gray, CV_BGR2GRAY);
	equalizeHist(frame_gray, frame_gray);

	std::vector<Rect> eyes;
	eyes_cascade.detectMultiScale(frame_gray, eyes, 1.1, 2, 0 | CV_HAAR_SCALE_IMAGE, Size(30, 30));

	if (eyes.size() != 2)
	{
		eyes_list.push_back(std::vector<Rect>());
		return false;
	}

	std::vector<Rect> eyes2(2);

	if (eyes[0].x < eyes[1].x)
	{
		eyes2[0] = eyes[0];
		eyes2[1] = eyes[1];
	}
	else
	{
		eyes2[0] = eyes[1];
		eyes2[1] = eyes[0];
	}

	eyes_list.push_back(eyes2);

	for (size_t j = 0; j < eyes.size(); j++)
	{
		cv::rectangle(frame, eyes[j], Scalar(255, 0, 0));
	}

	imshow(window_name, frame);

	return true;
}

void read_video_using_cascadeclassifier(std::list<std::vector<Rect> >& eyes_list)
{
	load_cascadeclassifier();

	cv::VideoCapture capture;
	capture.open(NAME2);
	if (!capture.isOpened())
	{
		printf("Could not open the input video:\n");;
		return;
	}

	Mat src;
	for (;;) //Show the image captured in the window and repeat
	{
		capture >> src;              // read
		if (src.empty())
			break;

		cv::imshow("wrote", src);
		detect_eyes(src, eyes_list);

		int ch = cv::waitKey(1);
		if (27 == ch)
			break;
	}
}

void read_video_using_eyes_list(std::list<std::vector<Rect> >& eyes_list)
{
	std::vector<cv::Size> sz(2);

	{
		for (int i = 0; i < 2; ++i)
			sz[i] = cv::Size(INT_MAX, INT_MAX);

		std::list<std::vector<Rect> >::const_iterator it = eyes_list.begin();
		std::list<std::vector<Rect> >::const_iterator end = eyes_list.end();
		for (; it != end; ++it)
		{
			if ((*it).size() < 2)
				continue;


			for (int i = 0; i < 2; ++i){
				if (0 == (*it)[i].width)
					break;;
				if (0 == (*it)[i].height)
					break;

				sz[i] = cv::Size(
					std::min<int>((*it)[i].width, sz[i].width),
					std::min<int>((*it)[i].height, sz[i].height));
			}
		}
	}


	cv::VideoCapture capture;
	capture.open(NAME2);
	if (!capture.isOpened())
	{
		printf("Could not open the input video:\n");;
		return;
	}

	std::vector<std::string> names(2);
	names[0] = "../eye1.avi";
	names[1] = "../eye2.avi";

	std::vector<VideoWriter> outputVideos(2);
	for (int i = 0; i < 2; ++i)
		outputVideos[i].open(names[i], 0, capture.get(CV_CAP_PROP_FPS), sz[i], true);

	int j = 0;

	Mat src;
	std::list<std::vector<Rect> >::const_iterator it = eyes_list.begin();
	std::list<std::vector<Rect> >::const_iterator end = eyes_list.end();
	for (; it != end; ++it)
	{
		capture >> src;              // read

		if (src.empty())
			break;

		if ((*it).size() < 2)
			continue;

		for (int i = 0; i < 2; ++i)
		{
			Rect roi = Rect(
				(*it)[i].x + ((*it)[i].width - sz[i].width) / 2,
				(*it)[i].y + ((*it)[i].height - sz[i].height) / 2,
				sz[i].width,
				sz[i].height
				);

			Mat eye = src(roi);

			if (eye.empty())
				break;
			
			cv::imshow(names[i], eye);
			outputVideos[i] << eye;

			char str[1024];
			sprintf(str, "../eye%d/img_%04d.jpg", i + 1, j);

			cv::imwrite(str, eye);
		}

		++j;


		int ch = cv::waitKey(1);
		if (27 == ch)
			break;
	}

	for (int i = 0; i < 2; ++i)
		outputVideos[i].release();

}



int contrast_brightness(Mat image, Mat& new_image,
	double alpha, /**< Simple contrast control */
	int beta  /**< Simple brightness control */
	)
{
	/// Read image given by user
	new_image = Mat::zeros(image.size(), image.type());

	/// Initialize values
	//std::cout << " Basic Linear Transforms " << std::endl;
	//std::cout << "-------------------------" << std::endl;
	//std::cout << "* Enter the alpha value [1.0-3.0]: "; std::cin >> alpha;
	//std::cout << "* Enter the beta value [0-100]: "; std::cin >> beta;

	/// Do the operation new_image(i,j) = alpha*image(i,j) + beta
	for (int y = 0; y < image.rows; y++)
	{
		for (int x = 0; x < image.cols; x++)
		{
			for (int c = 0; c < 3; c++)
			{
				new_image.at<Vec3b>(y, x)[c] =
					saturate_cast<uchar>(alpha*(image.at<Vec3b>(y, x)[c]) + beta);
			}
		}
	}

	/// Create Windows
	namedWindow("Original Image", 1);
	namedWindow("New Image", 1);

	/// Show stuff
	imshow("Original Image", image);
	imshow("New Image", new_image);

	/// Wait until user press some key
	return 0;
}


void write_frame(Mat& dst, const char * folder, int j)
{
	Mat new_image;
	contrast_brightness(dst, new_image,
		1.0, /**< Simple contrast control */
		50  /**< Simple brightness control */
		);

	char str[1024];
	sprintf(str, "%s/brightn_%04d.bmp", folder, j);
	cv::imwrite(str, new_image);

	sprintf(str, "%s/img_%04d.bmp", folder, j);
	cv::imwrite(str, dst);

	Mat frame_gray;
	cv::cvtColor(new_image, frame_gray, CV_BGR2GRAY);
	sprintf(str, "%s/gray_%04d.bmp", folder, j);
	cv::imwrite(str, frame_gray);

}


void read_video(std::string fn)
{
	cv::VideoCapture capture;
	capture.open(fn);
	if (!capture.isOpened())
	{
		printf("Could not open the input video:\n");;
		return;
	}

	{
		int ex = static_cast<int>(capture.get(CV_CAP_PROP_FOURCC));     // Get Codec Type- Int form
		char EXT[] = { (char)(ex & 0XFF), (char)((ex & 0XFF00) >> 8), (char)((ex & 0XFF0000) >> 16), (char)((ex & 0XFF000000) >> 24), 0 };
		printf("EXT = %s ex=%d\n", EXT, ex);
	}

	int j = 1;

	Mat src;
	for (;;) //Show the image captured in the window and repeat
	{
		capture >> src;              // read
		if (src.empty())
			break;

		write_frame(src, "../eye1_superres", j);

		++j;


		cv::imshow("wrote", src);
		int ch = cv::waitKey(1);
		if (27 == ch)
			break;
	}
}




void test_superres()
{
	//write_video();
	//write_video2();
	//read_video();

#if 0
	std::list<std::vector<Rect> > eyes_list;
	read_video_using_cascadeclassifier(eyes_list);
	read_video_using_eyes_list(eyes_list);
#endif

	//read_video("../eye1_superres.avi");



	const int scale = 3;
	const int iterations = 200;
	const int temporalAreaRadius = 5;
	//Ptr<DenseOpticalFlowExt> opticalFlow(new ZeroOpticalFlow);
	Ptr<DenseOpticalFlowExt> opticalFlow = createOptFlow_Farneback();
	Ptr<FrameSource> frameSource = createFrameSource_Video("../eye1.avi");

#if 0
    if (PERF_RUN_CUDA())
    {
        Ptr<SuperResolution> superRes = createSuperResolution_BTVL1_CUDA();

        superRes->setScale(scale);
        superRes->setIterations(iterations);
        superRes->setTemporalAreaRadius(temporalAreaRadius);
        superRes->setOpticalFlow(opticalFlow);

        superRes->setInput(makePtr<OneFrameSource_CUDA>(GpuMat(frame)));

        GpuMat dst;
        superRes->nextFrame(dst);

        TEST_CYCLE_N(10) superRes->nextFrame(dst);

        CUDA_SANITY_CHECK(dst, 2);
    }
    else
#endif
    {
        Ptr<SuperResolution> superRes = createSuperResolution_BTVL1();

        superRes->setScale(scale);
        superRes->setIterations(iterations);
        superRes->setTemporalAreaRadius(temporalAreaRadius);
        superRes->setOpticalFlow(opticalFlow);

		superRes->setInput(frameSource);

		Mat src;
		frameSource->nextFrame(src);
		cv::imshow("src", src);

		//superRes->setInput(makePtr<OneFrameSource_CPU>(src));
 
        Mat dst;
        superRes->nextFrame(dst);
		if (dst.empty())
			return;


		cv::imshow("output", dst);

		VideoWriter outputVideo;                                        // Open the output
		outputVideo.open("../eye1_superres.avi", 0, 25, cv::Size(dst.cols, dst.rows), true);

		if (!outputVideo.isOpened())
		{
			printf("Could not open the output video for write:\n");
			return;
		}

		int j = 0;
 
		for (;;)
		{
			outputVideo << dst;

			write_frame(dst, "../eye1_superres", j);

			++j;

			frameSource->nextFrame(src);
			if (src.empty())
				break;

			cv::imshow("src", src);

			superRes->nextFrame(dst);
			if (dst.empty())
				break;

			cv::imshow("output", dst);
			int ch = cv::waitKey(1);
		}

		outputVideo.release();

        ///CPU_SANITY_CHECK(dst);
    }
}

#endif