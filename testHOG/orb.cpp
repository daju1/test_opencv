#include "opencv2/imgproc/imgproc.hpp"
#if !HAVE_OPENCV_300
#include "opencv2/ts/ts.hpp"
#endif
#include "opencv2/highgui/highgui.hpp"
#include "opencv2/features2d/features2d.hpp"

#if !HAVE_OPENCV_300
using namespace std;
using namespace cv;
using namespace perf;
using std::tr1::make_tuple;
using std::tr1::get;



void use_orb(Mat& frame)
{
	Mat gray_frame;// = imread(filename, IMREAD_GRAYSCALE);
	cvtColor(frame, gray_frame, CV_BGR2GRAY);


	Mat mask;
	ORB detector(1500, 1.3f, 1);
	vector<KeyPoint> points;

	Mat descriptors;

	//detector(gray_frame, mask, points, descriptors, true);
	detector(gray_frame, mask, points, descriptors, false);
	//detector(gray_frame, mask, points);

	sort(points.begin(), points.end(), comparators::KeypointGreater());

	for (size_t i = 0; i < points.size(); ++i)
	{
		const KeyPoint& kp = points[i];
		cv::circle(frame, kp.pt, 2 /*cvRound(kp.size / 2)*/, CV_RGB(255, 0, 0));
	}

	//imshow("orb descriptors", descriptors);

}

void use_orb(const char * filename)
{
	Mat frame = imread(filename);

	if (frame.empty())
	{
		FAIL() << "Unable to load source image " << filename;
		return;
	}

	use_orb(frame);

	imshow("frame orb", frame);
	cv::waitKey(0);
}

int orb_video()
{
	VideoCapture cap(CV_CAP_ANY);
	cap.set(CV_CAP_PROP_FRAME_WIDTH, 320);
	cap.set(CV_CAP_PROP_FRAME_HEIGHT, 240);
	if (!cap.isOpened())
		return -1;

	Mat img;

	namedWindow("frame orb", CV_WINDOW_AUTOSIZE);
	while (true)
	{
		cap >> img;
		if (!img.data)
			continue;

		use_orb(img);

		cv::imshow("frame orb", img);


		if (waitKey(20) >= 0)
			break;
	}
	return 0;
}

#endif