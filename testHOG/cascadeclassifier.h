
enum ProcessType
{
	PT_CPU,
#ifdef HAVE_CUDA
	PT_GPU,
#endif
	PT_OCL
};

int use_cascadeclassifier(const char* cascadeName, const char * inputName, bool isInputImage, bool isInputVideo, bool isInputCamera, bool toWriteImageBack = false, ProcessType proc_type = ProcessType::PT_CPU);
