#pragma once 

#include <windows.h>
#include <tchar.h> 
#include <string> 
#include <stdio.h>
#pragma comment(lib, "User32.lib")



class IDirectoryEnumerator
{
public:
   virtual void called_foo(char * fn_in_dump, LARGE_INTEGER filesize) = 0;
};



class DirectoryEnumerator 
   : public IDirectoryEnumerator

{
   std::string m_fn;
   std::string m_ext;

   int enum_dir(const char * dir);

public:
   int enum_files(const char * dir);

   DirectoryEnumerator(const char* fn, const char* ext);

   virtual bool IsStop() = 0;

   void enumerate(const char * dir)
   {
	   enum_dir(dir);
	   enum_files(dir);
   }

   static void DisplayErrorBox(LPTSTR lpszFunction, BOOL bShowMsgBox);
};



