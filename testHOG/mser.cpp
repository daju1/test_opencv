#if !HAVE_OPENCV_300
#include "opencv2/ts/ts.hpp"
#endif
#include "opencv2/imgproc/imgproc.hpp"
#include "opencv2/imgproc/imgproc_c.h"
#include "opencv2/features2d/features2d.hpp"
#include "opencv2/highgui/highgui.hpp"
#include <iostream>


#include <vector>
#include <string>
using namespace std;
using namespace cv;



int CV_MserTest_LoadBoxes(const char* path, vector<CvBox2D>& boxes)
{
	boxes.clear();
	FILE* f = fopen(path, "r");

	if (f == NULL)
	{
		return 0;
	}

	while (!feof(f))
	{
		CvBox2D box;
		int values_read = fscanf(f, "%f,%f,%f,%f,%f\n", &box.angle, &box.center.x, &box.center.y, &box.size.width, &box.size.height);
		CV_Assert(values_read == 5);
		boxes.push_back(box);
	}
	fclose(f);
	return 1;
}

int CV_MserTest_SaveBoxes(const char* path, const vector<CvBox2D>& boxes)
{
	FILE* f = fopen(path, "w");
	if (f == NULL)
	{
		return 0;
	}
	for (int i = 0; i<(int)boxes.size(); i++)
	{
		fprintf(f, "%f,%f,%f,%f,%f\n", boxes[i].angle, boxes[i].center.x, boxes[i].center.y, boxes[i].size.width, boxes[i].size.height);
	}
	fclose(f);
	return 1;
}

int CV_MserTest_CompareBoxes(const vector<CvBox2D>& boxes1, const vector<CvBox2D>& boxes2, float max_rel_diff)
{
	if (boxes1.size() != boxes2.size())
		return 0;

	for (int i = 0; i<(int)boxes1.size(); i++)
	{
		float rel_diff;
		if (!((boxes1[i].angle == 0.0f) && (abs(boxes2[i].angle) < max_rel_diff)))
		{
			float angle_diff = (float)fmod(boxes1[i].angle - boxes2[i].angle, 180);
			// for angular correctness, it makes no sense to use a "relative" error.
			// a 1-degree error around 5 degrees is equally bas as around 250 degrees.
			// in correct cases, angle_diff can now be a bit above 0 or a bit below 180
			if (angle_diff > 90.0f)
			{
				angle_diff -= 180.0f;
			}
			rel_diff = (float)fabs(angle_diff);
			if (rel_diff > max_rel_diff)
				return i;
		}

		if (!((boxes1[i].center.x == 0.0f) && (abs(boxes2[i].center.x) < max_rel_diff)))
		{
			rel_diff = abs(boxes1[i].center.x - boxes2[i].center.x) / abs(boxes1[i].center.x);
			if (rel_diff > max_rel_diff)
				return i;
		}

		if (!((boxes1[i].center.y == 0.0f) && (abs(boxes2[i].center.y) < max_rel_diff)))
		{
			rel_diff = abs(boxes1[i].center.y - boxes2[i].center.y) / abs(boxes1[i].center.y);
			if (rel_diff > max_rel_diff)
				return i;
		}
		if (!((boxes1[i].size.width == 0.0f) && (abs(boxes2[i].size.width) < max_rel_diff)))
		{
			rel_diff = abs(boxes1[i].size.width - boxes2[i].size.width) / abs(boxes1[i].size.width);
			if (rel_diff > max_rel_diff)
				return i;
		}

		if (!((boxes1[i].size.height == 0.0f) && (abs(boxes2[i].size.height) < max_rel_diff)))
		{
			rel_diff = abs(boxes1[i].size.height - boxes2[i].size.height) / abs(boxes1[i].size.height);
			if (rel_diff > max_rel_diff)
				return i;
		}
	}

	return -1;
}

const char* get_data_path(){
	return "D:/cpplibs/opencv/opencv_extra/opencv_extra/testdata/cv/";
}

#if !HAVE_OPENCV_300
void use_mser(Mat& img)
{
	Mat yuv;
	cvtColor(img, yuv, COLOR_BGR2YCrCb);
	vector<vector<Point> > msers;
	MSER()(yuv, msers);

	vector<CvBox2D> boxes;
	vector<CvBox2D> boxes_orig;
	for (size_t i = 0; i < msers.size(); i++)
	{
		RotatedRect box = fitEllipse(msers[i]);
		box.angle = (float)CV_PI / 2 - box.angle;
		boxes.push_back(box);

		{
			vector < Point > hull;
			convexHull(Mat(msers[i]), hull);
			//double area = contourArea(Mat(msers[i]));
			//double hullArea = contourArea(Mat(hull));
			//double ratio = area / hullArea;

			//printf("area=%f hullArea=%f ratio=%f\n", area, hullArea, ratio);

			for (size_t j = 0; j < hull.size(); j++)
			{
				size_t j_1 = j == 0 ? hull.size() - 1 : j - 1;
				Point pt1 = hull[j_1];
				Point pt2 = hull[j];
				cv::line(img, pt1, pt2, CV_RGB(0, 0, 255));
			}
		}
	}

#if 0
	string boxes_path = string(get_data_path()) + "mser/boxes.txt";
	string calc_boxes_path = string(get_data_path()) + "mser/boxes.calc.txt";

	if (!CV_MserTest_LoadBoxes(boxes_path.c_str(), boxes_orig))
	{
		CV_MserTest_SaveBoxes(boxes_path.c_str(), boxes);
		printf("Unable to open data file mser/boxes.txt\n");
		//set_failed_test_info(cvtest::TS::FAIL_MISSING_TEST_DATA);
		return;
	}

	const float dissimularity = 0.01f;
	int n_box = CV_MserTest_CompareBoxes(boxes_orig, boxes, dissimularity);
	if (n_box < 0)
	{
		//ts->set_failed_test_info(cvtest::TS::OK);
	}
	else
	{
		CV_MserTest_SaveBoxes(calc_boxes_path.c_str(), boxes);
		//ts->set_failed_test_info(cvtest::TS::FAIL_BAD_ACCURACY);
		printf("Incorrect correspondence in box %d\n", n_box);
	}


	for (size_t i = 0; i < boxes.size(); ++i)
	{
		const CvBox2D& box = boxes[i];
		CvPoint2D32f pt[4];
		cvBoxPoints(box, pt);

		for (size_t j = 0; j < 4; j++)
		{
			size_t j_1 = j == 0 ? 3 : j - 1;
			Point pt1 = pt[j_1];
			Point pt2 = pt[j];
			cv::line(img, pt1, pt2, CV_RGB(255,0, 0 ));
		}
	}
#endif
}

void use_mser(const char* image_path)
{
	Mat img = imread(image_path);
	if (img.empty())
	{
		printf("Unable to open image mser/puzzle.png\n");
		//ts->set_failed_test_info(cvtest::TS::FAIL_MISSING_TEST_DATA);
		return;
	}

	use_mser(img);

	//cv::namedWindow("result"); 
	cv::imshow("result", img);
	cv::waitKey(0);
}




void CV_MserTest_run()
{
	string image_path = string(get_data_path()) + "mser/puzzle.png";

	use_mser(image_path.c_str());
}

int mser_video()
{
	VideoCapture cap(CV_CAP_ANY);
	cap.set(CV_CAP_PROP_FRAME_WIDTH, 320);
	cap.set(CV_CAP_PROP_FRAME_HEIGHT, 240);
	if (!cap.isOpened())
		return -1;

	Mat img;

	namedWindow("video capture", CV_WINDOW_AUTOSIZE);
	while (true)
	{
		cap >> img;
		if (!img.data)
			continue;

		use_mser(img);

		cv::imshow("video capture", img);


		if (waitKey(20) >= 0)
			break;
	}
	return 0;
}
#endif
