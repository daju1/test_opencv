// WARNING: this sample is under construction! Use it on your own risk.
#if defined _MSC_VER && _MSC_VER >= 1400
#pragma warning(disable : 4100)
#endif

#include "windows.h"

#include <iostream>
#include <iomanip>

#include "opencv2/objdetect/objdetect.hpp"
#include "opencv2/highgui/highgui.hpp"
#include "opencv2/imgproc/imgproc.hpp"

#if !HAVE_OPENCV_300
#include "opencv2/contrib/contrib.hpp"
#include "opencv2/gpu/gpu.hpp"
#endif

using namespace std;
using namespace cv;
#if !HAVE_OPENCV_300
using namespace cv::gpu;
#endif

int use_cascadeclassifier_cpu(string cascadeName, string inputName, bool isInputImage, bool isInputVideo, bool isInputCamera, bool toWriteImageBack, double detectScaleFactor, Size minSize, Size maxSize);

static void help()
{
    cout << "Usage: ./cascadeclassifier_gpu \n\t--cascade <cascade_file>\n\t(<image>|--video <video>|--camera <camera_id>)\n"
            "Using OpenCV version " << CV_VERSION << endl << endl;
}


template<class T>
void convertAndResize(const T& src, T& gray, T& resized, double scale)
{
    if (src.channels() == 3)
    {
        cvtColor( src, gray, CV_BGR2GRAY );
    }
    else
    {
        gray = src;
    }

    Size sz(cvRound(gray.cols * scale), cvRound(gray.rows * scale));

    if (scale != 1)
    {
        resize(gray, resized, sz);
    }
    else
    {
        resized = gray;
    }
}


static void matPrint(Mat &img, int lineOffsY, Scalar fontColor, const string &ss)
{
    int fontFace = FONT_HERSHEY_DUPLEX;
    double fontScale = 0.8;
    int fontThickness = 2;
    Size fontSize = cv::getTextSize("T[]", fontFace, fontScale, fontThickness, 0);

    Point org;
    org.x = 1;
    org.y = 3 * fontSize.height * (lineOffsY + 1) / 2;
    putText(img, ss, org, fontFace, fontScale, CV_RGB(0,0,0), 5*fontThickness/2, 16);
    putText(img, ss, org, fontFace, fontScale, fontColor, fontThickness, 16);
}


static void displayState(Mat &canvas, bool bHelp, bool bGpu, bool bLargestFace, bool bFilter, double fps)
{
    Scalar fontColorRed = CV_RGB(255,0,0);
    Scalar fontColorNV  = CV_RGB(118,185,0);

    ostringstream ss;
    ss << "FPS = " << setprecision(1) << fixed << fps;
    matPrint(canvas, 0, fontColorRed, ss.str());
    ss.str("");
    ss << "[" << canvas.cols << "x" << canvas.rows << "], " <<
        (bGpu ? "GPU, " : "CPU, ") <<
        (bLargestFace ? "OneFace, " : "MultiFace, ") <<
        (bFilter ? "Filter:ON" : "Filter:OFF");
    matPrint(canvas, 1, fontColorRed, ss.str());

    // by Anatoly. MacOS fix. ostringstream(const string&) is a private
    // matPrint(canvas, 2, fontColorNV, ostringstream("Space - switch GPU / CPU"));
    if (bHelp)
    {
        matPrint(canvas, 2, fontColorNV, "Space - switch GPU / CPU");
        matPrint(canvas, 3, fontColorNV, "M - switch OneFace / MultiFace");
        matPrint(canvas, 4, fontColorNV, "F - toggle rectangles Filter");
        matPrint(canvas, 5, fontColorNV, "H - toggle hotkeys help");
        matPrint(canvas, 6, fontColorNV, "1/Q - increase/decrease scale");
    }
    else
    {
        matPrint(canvas, 2, fontColorNV, "H - toggle hotkeys help");
    }
}

#if !HAVE_OPENCV_300
int main_cascadeclassifier_cpu(int argc, const char *argv[])
{
	if (argc == 1)
	{
		help();
		return -1;
	}

	if (getCudaEnabledDeviceCount() == 0)
	{
		return cerr << "No GPU found or the library is compiled without GPU support" << endl, -1;
	}

	cv::gpu::printShortCudaDeviceInfo(cv::gpu::getDevice());

	string cascadeName;
	string inputName;
	bool isInputImage = false;
	bool isInputVideo = false;
	bool isInputCamera = false;

	for (int i = 1; i < argc; ++i)
	{
		if (string(argv[i]) == "--cascade")
			cascadeName = argv[++i];
		else if (string(argv[i]) == "--video")
		{
			inputName = argv[++i];
			isInputVideo = true;
		}
		else if (string(argv[i]) == "--camera")
		{
			inputName = argv[++i];
			isInputCamera = true;
		}
		else if (string(argv[i]) == "--help")
		{
			help();
			return -1;
		}
		else if (!isInputImage)
		{
			inputName = argv[i];
			isInputImage = true;
		}
		else
		{
			cout << "Unknown key: " << argv[i] << endl;
			return -1;
		}
	}


	bool toWriteImageBack = false;
	double detectScaleFactor = 1.05;
	Size minSize = Size(10, 10);
	Size maxSize = Size();

	return use_cascadeclassifier_cpu(cascadeName, inputName, isInputImage, isInputVideo, isInputCamera, toWriteImageBack, detectScaleFactor, minSize, maxSize);
}
#endif

int use_cascadeclassifier_cpu(string cascadeName, string inputName, bool isInputImage, bool isInputVideo, bool isInputCamera, bool toWriteImageBack, double detectScaleFactor, Size minSize, Size maxSize)
{
    CascadeClassifier cascade_cpu;
    if (!cascade_cpu.load(cascadeName))
    {
        return cerr << "ERROR: Could not load cascade classifier \"" << cascadeName << "\"" << endl, help(), -1;
    }

    VideoCapture capture;
    Mat image;

    if (isInputImage)
    {
		image = imread(inputName);
		if (image.empty())
			return -1;
        CV_Assert(!image.empty());
    }
    else if (isInputVideo)
    {
        capture.open(inputName);
        CV_Assert(capture.isOpened());
    }
    else
    {
		//capture = cvCaptureFromCAM(-1);
		capture.open(CV_CAP_ANY);
		capture.set(CV_CAP_PROP_FRAME_WIDTH, 320);
		capture.set(CV_CAP_PROP_FRAME_HEIGHT, 240);

        CV_Assert(capture.isOpened());
    }

    //namedWindow("result", 1);

    Mat frame, frame_cpu, gray_cpu, resized_cpu, faces_downloaded, frameDisp;
    vector<Rect> facesBuf_cpu;

	CvVideoWriter* writer = NULL;

	if (isInputVideo && toWriteImageBack)
	{
		size_t islash = inputName.find_last_of("/\\");
		string dir = inputName.substr(0, islash);
		string fn = inputName.substr(islash + 1);
		string outDir = dir + "_out/";
		CreateDirectory(outDir.c_str(), NULL);
		string outName = outDir + fn;

		int w = capture.get(CV_CAP_PROP_FRAME_WIDTH);
		int h = capture.get(CV_CAP_PROP_FRAME_HEIGHT);
		writer = cvCreateVideoWriter(outName.c_str(), 
			/*CV_FOURCC('D', 'I', 'V', 'X')*/
			CV_FOURCC('X', 'V', 'I', 'D')
			, 
			25, cvSize(w, h), 1);
	}



    /* parameters */

    double resizeScaleFactor = 1.0;
    bool findLargestObject = false;
    bool filterRects = true;
    bool helpScreen = false;

    int detections_num;
    for (;;)
    {
        if (isInputCamera || isInputVideo)
        {
            capture >> frame;
            if (frame.empty())
            {
                continue;
            }
        }

        (image.empty() ? frame : image).copyTo(frame_cpu);
        convertAndResize(frame_cpu, gray_cpu, resized_cpu, resizeScaleFactor);

#if !HAVE_OPENCV_300
        TickMeter tm;
        tm.start();
#endif

        {
			//Size minSize = Size(10, 10);
			//int minNeighbors = (filterRects || findLargestObject) ? 4 : 0;
			int minNeighbors = 2;
			cascade_cpu.detectMultiScale(resized_cpu, facesBuf_cpu, detectScaleFactor,
				minNeighbors,
				(findLargestObject ? CV_HAAR_FIND_BIGGEST_OBJECT : 0) | CV_HAAR_SCALE_IMAGE,
				minSize, maxSize);

            detections_num = (int)facesBuf_cpu.size();
        }

        if (detections_num)
        {
            for (int i = 0; i < detections_num; ++i)
            {
				rectangle(frame_cpu, facesBuf_cpu[i], Scalar(255));
            }
        }

#if !HAVE_OPENCV_300
        tm.stop();
        double detectionTime = tm.getTimeMilli();
        double fps = 1000 / detectionTime;

        //print detections to console
        cout << setfill(' ') << setprecision(2);
        cout << setw(6) << fixed << fps << " FPS, " << detections_num << " det";
#endif 
        if ((filterRects || findLargestObject) && detections_num > 0)
        {
            Rect *faceRects = /*useGPU ? faces_downloaded.ptr<Rect>() :*/ &facesBuf_cpu[0];
            for (int i = 0; i < min(detections_num, 2); ++i)
            {
                cout << ", [" << setw(4) << faceRects[i].x
                     << ", " << setw(4) << faceRects[i].y
                     << ", " << setw(4) << faceRects[i].width
                     << ", " << setw(4) << faceRects[i].height << "]";
            }
        }
        cout << endl;

//        cvtColor(resized_cpu, frameDisp, CV_GRAY2BGR);
        //displayState(frameDisp, helpScreen, useGPU, findLargestObject, filterRects, fps);
		imshow("result", frame_cpu);

		if (isInputImage && toWriteImageBack)
		{
			size_t islash = inputName.find_last_of("/\\");
			string dir = inputName.substr(0, islash);
			string fn = inputName.substr(islash+1);
			string outDir = dir + "_out/";
			CreateDirectory(outDir.c_str(), NULL);
			string outName = outDir + fn;
			imwrite(outName, frame_cpu);
		}

		if (isInputVideo && toWriteImageBack)
			cvWriteFrame(writer, &(IplImage)frame_cpu);

        char key = (char)waitKey(5);
        if (key == 27)
        {
            break;
        }

        switch (key)
        {
        case ' ':
        //    useGPU = !useGPU;
            break;
        case 'm':
        case 'M':
            findLargestObject = !findLargestObject;
            break;
        case 'f':
        case 'F':
            filterRects = !filterRects;
            break;
        /*case '1':
            scaleFactor *= 1.05;
            break;
        case 'q':
        case 'Q':
            scaleFactor /= 1.05;
            break;*/
        case 'h':
        case 'H':
            helpScreen = !helpScreen;
            break;
        }

		if (isInputImage)
		{
			if (detections_num)
				return detections_num;
			return 0;
		}
    }

	if (isInputVideo && toWriteImageBack)
	{
		cvReleaseVideoWriter(&writer);
	}

    return 0;
}
