#include <Windows.h>
//#include "opencv2/ts/ts.hpp"
#include "opencv2/imgproc/imgproc.hpp"
#include "opencv2/highgui/highgui.hpp"
#include <iostream>


using namespace std;
using namespace cv;
//using namespace perf;

//CV_ENUM(BorderType, BORDER_REPLICATE, BORDER_CONSTANT, BORDER_REFLECT, BORDER_REFLECT_101)

//PERF_TEST_P(Img_BlockSize_ApertureSize_BorderType, cornerEigenValsAndVecs,
//	testing::Combine(
//	testing::Values("stitching/a1.png", "cv/shared/pic5.png"),
//	testing::Values(3, 5),
//	testing::Values(3, 5),
//	BorderType::all()
//	)
//	)
//

Mat hwnd2mat(HWND hwnd);

void test_cornerEigenValsAndVecs()
{

#if 1
	String filename = "D:/cpplibs/opencv/opencv_extra/opencv_extra/testdata/stitching/a1.png";
	int blockSize = 3;
	int apertureSize = 3;
#else
	String filename = "D:/cpplibs/opencv/opencv_extra/opencv_extra/testdata/cv/shared/pic5.png";
	int blockSize = 5;
	int apertureSize = 5;
#endif
	int borderType = BORDER_REPLICATE;

	int n;
	std::cin >> n;

	HWND hDesktopWnd = ::GetDesktopWindow();
	Mat src = hwnd2mat(hDesktopWnd);

	cv::Mat src_gray;
	cvtColor(src, src_gray, cv::COLOR_BGR2GRAY);

	//Mat src = imread(filename, IMREAD_GRAYSCALE);

	Mat dst;
	cornerEigenValsAndVecs(src_gray, dst, blockSize, apertureSize, borderType);

	Mat l1;
	extractChannel(dst, l1, 0);

	cv::imshow("src", src);
	cv::imshow("cornerEigenValsAndVecs", l1);
	cv::waitKey(0);
}
/*
{
	cvtColor(src, src_gray, CV_BGR2GRAY);

	int blockSize = 3;
	int apertureSize = 3;

	myHarris_dst = Mat::zeros(src_gray.size(), CV_32FC(6));
	cornerEigenValsAndVecs(src_gray, myHarris_dst, blockSize, apertureSize, BORDER_DEFAULT);

	Mat l1 = Mat::zeros(myHarris_dst.size(), CV_32FC1),
		l2 = Mat::zeros(myHarris_dst.size(), CV_32FC1),
		x1 = Mat::zeros(myHarris_dst.size(), CV_32FC1),
		x2 = Mat::zeros(myHarris_dst.size(), CV_32FC1),
		y1 = Mat::zeros(myHarris_dst.size(), CV_32FC1),
		y2 = Mat::zeros(myHarris_dst.size(), CV_32FC1);
	int ch[] =
	{ 0, 0 };
	mixChannels(&myHarris_dst, 1, &l1, 1, ch, 1);
	ch[0] = 1;
	mixChannels(&myHarris_dst, 1, &l2, 1, ch, 1);
	ch[0] = 2;
	mixChannels(&myHarris_dst, 1, &x1, 1, ch, 1);
	ch[0] = 3;
	mixChannels(&myHarris_dst, 1, &x2, 1, ch, 1);
	ch[0] = 4;
	mixChannels(&myHarris_dst, 1, &y1, 1, ch, 1);
	ch[0] = 5;
	mixChannels(&myHarris_dst, 1, &y2, 1, ch, 1);

	Point minLoc;
	Point maxLoc;
	double minValL1;
	double maxValL1;

	vector< Point2f > corners;
	Mat harrisCorners = Mat::zeros(src.size(), CV_8UC1);
	goodFeaturesToTrack(src_gray, corners, 14, 0.001, 5, noArray(), 3, true, 0.04);

	for (uint i = 0; i < corners.size(); i++)
	{
		circle(harrisCorners, corners[i], 0, Scalar(255, 255, 255), 1);
	}

	Mat eigenDirection = Mat::zeros(src.size(), src.type());

	for (int i = 0; i < src.rows; i++)
	{
		for (int j = 0; j < src.cols; j++)
		{
			if (harrisCorners.at< uchar >(i, j) != 0)
			{
				//DIRECTION LOWEST CHANGE
				line(src, Point(j, i), Point(j - 30 * y1.at< float >(i, j), i - 30 * x1.at< float >(i, j)), Scalar(20, 20, 255), 1, 1);
				line(src, Point(j, i), Point(j - 30 * y2.at< float >(i, j), i - 30 * x2.at< float >(i, j)), Scalar(255, 20, 20), 1, 1);
				circle(src, Point(j, i), 0, Scalar(20, 255, 20), 1);
			}
		}
	}
}
*/