/**
   Examples of fitting a line segment to a set of (2D or 3D)
   points. This source code extends the one in [Bradski and Kaehler,
   2008].

   @author Young-Woo Seo (youngwoo@cmu.edu)

   [Bradski and Kaehler, 2008] Gary Bradski and Adrian Kaehler,
   Learning OpenCV: Computer Vision with the OpenCV Library,
   pp. 456-457, 2008.

   compile this

   g++ `pkg-config --cflags --libs opencv` lineFittingTest.cpp -o lineFittingTest
 */

#include <opencv/cv.h>
#include <opencv/highgui.h>
#include <math.h>
#include <iostream>

using namespace std;

void fitLineToPoints( CvPoint* points, int num_points, CvPoint & p1, CvPoint & p2 );

int main_lineFittingTest()
{
  IplImage* img = cvCreateImage( cvSize(500, 500), 8, 3 );
  CvRNG rng = cvRNG(-1);

  cvNamedWindow( "fitline", 1 );

  for( ;; )
  {
    char key;
    int i;
    int count = cvRandInt( &rng ) % 100 + 1;
    int outliers = count/5;
    float a = cvRandReal( &rng ) * 200;
    float b = cvRandReal( &rng ) * 40;
    float angle = cvRandReal( &rng ) * CV_PI;
    float cos_a = cos( angle );
    float sin_a = sin( angle );

    CvPoint pt1, pt2;
    CvPoint* points = new CvPoint[count];
    CvMat pointMat = cvMat( 1, count, CV_32SC2, points );
    float line[4];
    float d, t;

    b = MIN(a*0.3, b);

    // points close to the line
    for( i = 0; i < count - outliers; i++ )
    {
      float x = (cvRandReal(&rng)*2-1)*a;
      float y = (cvRandReal(&rng)*2-1)*b;
      points[i].x = cvRound(x*cos_a - y*sin_a + img->width/2);
      points[i].y = cvRound(x*sin_a + y*cos_a + img->height/2);
    }

    // points randomly generated
    for(; i < count; i++ )
    {
      points[i].x = cvRandInt( &rng ) % img->width;
      points[i].y = cvRandInt( &rng ) % img->height;
    }

    // use OpenCV function to fit a line to points.  Because the
    // performance of this line-fitting depends on the parameters
    // values of cvFitLine() function, it is important to *tune* the
    // parameters, to make it work better.
    cvFitLine( &pointMat, CV_DIST_L2, 1, 0.01, 0.01, line );
    cvZero( img );

    CvPoint p1,p2;

    // this function fit, using the Eigen analysis of the scatter
    // matrix of points' coordinates, a line segment to a given point
    // set. It doesn't require much of parameter tunning.
    fitLineToPoints( points, count, p1, p2 );

    for( i = 0; i < count; i++ )
      cvCircle( img,
		points[i],
		2, 
		(i < count - outliers) ? CV_RGB(255, 0, 0) : CV_RGB(255,255,0), CV_FILLED, CV_AA, 0
		);

    d = sqrt((double)line[0]*line[0] + (double)line[1]*line[1]);
    line[0] /= d;
    line[1] /= d;

    t = (float)(img->width + img->height);
    pt1.x = cvRound(line[2] - line[0]*t);
    pt1.y = cvRound(line[3] - line[1]*t);
    pt2.x = cvRound(line[2] - line[0]*t);
    pt2.x = cvRound(line[3] - line[1]*t);
    
    // A green line segment by cvFitLine()
    cvLine( img, pt1, pt2, CV_RGB(0,255,0), 3, CV_AA, 0 );

    // A blue line segment by fitLineToPoints()
    cvLine( img, p1,  p2,  CV_RGB(0,0,255), 3, CV_AA, 0 );

    cvShowImage( "fitline", img );
    
    key = (char) cvWaitKey( 0 );

    if( key == 27 || key == 'q' )
      break;

    delete [] points;
  }

  cvDestroyWindow("fitline");

  return 0;
}

/**
 * Compute the Eigen values and vectors of the scatter matrix of
 * points' coordinates to fit a line segment to the input points.
 *
 * @param points a pointer-to-CvPoint
 * @param num_points number of input points
 * @param pt1 a reference-to-CvPoint
 * @param pt2 a reference-to-CvPoint
 *
 * Refer the following paper for more details:
 *
 * [Kahn et al., 1990] P. Kahn, L. Kitchen, and E. Riseman. A fast
 * line finder for vision-guided robot navigation.  IEEE Transactions
 * on Pattern Analysis and Machine Intelligence, 12(11):1098–1102,
 * 1990.
 */
void fitLineToPoints( CvPoint* points, 
		      int num_points, 
		      CvPoint & pt1, 
		      CvPoint & pt2 )
{
  float mean_x = 0, mean_y = 0;
  int ii;
  
  CvMat* xx = cvCreateMat( num_points, 1, CV_32FC1 );
  CvMat* yy = cvCreateMat( num_points, 1, CV_32FC1 );  
  
  for ( ii = 0; ii < num_points; ii++ )
  {
    cvmSet( xx, ii, 0, points[ii].x );
    cvmSet( yy, ii, 0, points[ii].y );
    //cout << ii << ", x=" << points[ii].x << ", y=" << points[ii].y << endl;
  }

  CvScalar mean;
  mean = cvAvg( xx, 0 );
  mean_x = mean.val[0];
  mean = cvAvg( yy, 0 );
  mean_y = mean.val[0];

  float vals[4] = { 0.0, 0.0, 0.0, 0.0 };  

  for ( ii = 0; ii < num_points; ii++ )
  {
    vals[0] += pow( cvmGet( xx, ii, 0 ) - mean_x, 2 );
    vals[1] += ( cvmGet( xx, ii, 0 )-mean_x )*( cvmGet( yy, ii, 0)-mean_y );
    vals[3] += pow( cvmGet( yy, ii, 0 ) - mean_y, 2 );
  }

  vals[2] = vals[1];    

  CvMat D = cvMat( 2, 2, CV_32FC1, vals );
  CvMat *eigenVec = cvCreateMat( 2, 2, CV_32FC1 );
  CvMat *eigenVal = cvCreateMat( 2, 1, CV_32FC1 );
  cvZero( eigenVec );
  cvZero( eigenVal );

  cvEigenVV( &D, eigenVec, eigenVal, DBL_EPSILON, -1, -1 );  

  //cout << "Eigen values: " << endl;
  //cout << "cvmGet( eigenVal, 0, 0 )=" << cvmGet( eigenVal, 0, 0 ) << ", cvmGet( eigenval, 1, 0 )=" << cvmGet( eigenVal, 1, 0 ) << endl;
  //cout << "Eigen vectors: " << endl;
  //cout << "cvmGet( eigenVec, 0, 0 )=" << cvmGet( eigenVec, 0, 0 ) << ", cvmGet( eigenVec, 0, 1 )=" << cvmGet( eigenVec, 0, 1 ) << endl;
  //cout << "cvmGet( eigenVec, 1, 0 )=" << cvmGet( eigenVec, 1, 0 ) << ", cvmGet( eigenVec, 1, 1 )=" << cvmGet( eigenVec, 1, 1 ) << endl;

  float theta = atan2( cvmGet( eigenVec, 1, 0 ), cvmGet( eigenVec, 0, 0 ) );

  CvPoint maxloc = { 0, 0 };
  CvPoint minloc = { 0, 0 };
  double minval = 0, maxval = 0;
  cvMinMaxLoc( xx, &minval, &maxval, &minloc, &maxloc, 0 );
  float max_x = maxval;
  float min_x = minval;
  
  cvMinMaxLoc( yy, &minval, &maxval, &minloc, &maxloc, 0 );
  float max_y = maxval;
  float min_y = minval;

  float len = sqrt( pow(max_x - min_x, 2) + pow(max_y - min_y, 2) );
  float x1 = mean_x - cos(theta) * (len/2);
  float x2 = mean_x + cos(theta) * (len/2);
  float y1 = mean_y - sin(theta) * (len/2);
  float y2 = mean_y + sin(theta) * (len/2);

  pt1.x = x1;
  pt1.y = y1;
  
  pt2.x = x2;
  pt2.y = y2;

  /*
  float conf = 1;
  if ( cvmGet( eigenVal, 1, 0 ) > 0 )
  {
    conf = cvmGet( eigenVal, 0, 0 ) / cvmGet( eigenVal, 1, 0 );
  }

  cout << "theta: atan2( evec[1], evec[0] )=" << theta << ", confidence=" << conf << endl;
  */

  cvReleaseMat( &xx );
  cvReleaseMat( &yy );
  cvReleaseMat( &eigenVec );
  cvReleaseMat( &eigenVal );
}

