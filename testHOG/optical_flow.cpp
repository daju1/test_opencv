#include "opencv2/imgproc/imgproc.hpp"
#include "opencv2/highgui/highgui.hpp"
//#include "opencv2/objdetect/objdetect.hpp"
//#include "opencv2/contrib/contrib.hpp"


#include <opencv2/video/video.hpp>

#if !HAVE_OPENCV_300
#include "opencv2/ts/ts.hpp"
#endif

#include <stdio.h>
#include <string.h>
#include <ctype.h>

#include <iostream>
#include <iomanip>

using namespace cv;
using namespace std; 


void drawOptFlowMap(const Mat& flow, Mat& cflowmap, int step,
	double scale, const Scalar& color)

{
	double xx = 0, yy = 0;
	int x, y;
	for (y = 0; y < cflowmap.rows; y += step)
	for (x = 0; x < cflowmap.cols; x += step)
	{

		const Point2f& fxy = flow.at<Point2f>(y, x);
		//���� ����� ����� ��������� ��� �������
		xx += fxy.x;
		yy += fxy.y;
		line(cflowmap, Point(x, y), Point(cvRound(x + fxy.x), cvRound(y + fxy.y)),
			color);

		circle(cflowmap, Point(x, y), 2, color, -1);
	}

	xx /= x*y;
	yy /= x*y;
}



int dense_optical_flow_video()
{
	VideoCapture cap(CV_CAP_ANY);
	cap.set(CV_CAP_PROP_FRAME_WIDTH, 320);
	cap.set(CV_CAP_PROP_FRAME_HEIGHT, 240);
	if (!cap.isOpened())
		return -1;

	Ptr<DenseOpticalFlow> tvl1 = createOptFlow_DualTVL1();

	if (!tvl1)
		return -1;


	Mat frame1;
	Mat frame2;
	Mat flow;
	Mat cflow;

	int c = 0;
	while (!frame1.data && ++c < 10)
		cap >> frame1;

	if (!frame1.data)
		return -1;

	int frame_type = frame1.type();
		
	cvtColor(frame1, frame1, CV_BGR2GRAY);
	vector<Mat> mv;


	namedWindow("video capture", CV_WINDOW_AUTOSIZE);
	namedWindow("flow1", CV_WINDOW_AUTOSIZE);
	namedWindow("flow2", CV_WINDOW_AUTOSIZE);


	while (true)
	{
		cap >> frame2;
		if (!frame2.data)
			continue;

		cvtColor(frame2, frame2, CV_BGR2GRAY);

		tvl1->calc(frame1, frame2, flow);

		cvtColor(frame1, cflow, CV_GRAY2BGR);

		drawOptFlowMap(flow, cflow, 16, 1.5, CV_RGB(0, 255, 0));

		imshow("flow", cflow);

		cv::split(flow, mv);
		imshow("video capture", frame2);
		imshow("flow1", mv[0]);
		imshow("flow2", mv[1]);

		frame1 = frame2;
		if (waitKey(20) >= 0)
			break;
	}
	return 0;
}


int farneback_optical_flow_video()
{
	VideoCapture cap(CV_CAP_ANY);
	//cap.set(CV_CAP_PROP_FRAME_WIDTH, 320);
	//cap.set(CV_CAP_PROP_FRAME_HEIGHT, 240);
	if (!cap.isOpened())
		return -1;

	Mat frame;
	Mat prevgray, gray, flow, cflow;

	int c = 0;
	while (!frame.data && ++c < 10)
		cap >> frame;

	if (!frame.data)
		return -1;

	cvtColor(frame, prevgray, CV_BGR2GRAY);
	vector<Mat> mv;


	namedWindow("video capture", CV_WINDOW_AUTOSIZE);
	namedWindow("flow1", CV_WINDOW_AUTOSIZE);
	namedWindow("flow2", CV_WINDOW_AUTOSIZE);


	while (true)
	{
		cap >> frame;
		if (!frame.data)
			continue;

		cvtColor(frame, gray, CV_BGR2GRAY);

		calcOpticalFlowFarneback(prevgray, gray, flow, 0.5, 3, 15, 3, 5, 1.2, 0);

		cvtColor(prevgray, cflow, CV_GRAY2BGR);

		drawOptFlowMap(flow, cflow, 16, 1.5, CV_RGB(0, 255, 0));

		imshow("flow", cflow);


		cv::split(flow, mv);
		imshow("video capture", frame);
		imshow("flow1", mv[0]);
		imshow("flow2", mv[1]);

		prevgray = gray;
		if (waitKey(20) >= 0)
			break;
	}
	return 0;
}

